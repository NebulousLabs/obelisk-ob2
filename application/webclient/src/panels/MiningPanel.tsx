// -------------------------------------------------------------------------------------------------
// Copyright 2018 Obelisk Inc.
// -------------------------------------------------------------------------------------------------

import * as _ from 'lodash'
import Content from 'components/Content';
import { Formik, FormikProps } from 'formik';
import { fetchMiningConfig, resetTuningParameters, setMiningConfig } from 'modules/Main/actions';
import { getLastError, getMiningConfig } from 'modules/Main/selectors';
import { MiningConfig } from 'modules/Main/types';
import {
  OptimizationModeHashrate,
  OptimizationModeBalanced,
  OptimizationModeEfficiency,
} from 'utils/optmizationMode'
import * as React from 'react'
import { connect, DispatchProp } from 'react-redux'
import withStyles, { InjectedProps, InputSheet } from 'react-typestyle'
import { Button, Dimmer, Form, Header, Loader, Message, Popup } from 'semantic-ui-react'

import { getHistory } from 'utils';

const history = getHistory();

interface ConnectProps {
  miningConfig: MiningConfig;
  lastError?: string;
  miningForm: string;
}

type CombinedProps = ConnectProps & InjectedProps & DispatchProp<any>;

const fanModeOptions = [
  { text: 'Automatic', value: 0, key: 0 },
  { text: 'Constant Speed', value: 1, key: 1 },
]

const fanSpeedOptions = (() => {
  const result = [];
  for (let i = 40; i <= 100; i++) {
    result.push({ text: `${i}%`, value: i, key: i });
  }
  return result;
})();

const rebootIntervalOptions = (() => {
  // First three entries are special
  const result = [
    { text: 'NEVER', value: 0, key: 0 },
    { text: '0.5 hours', value: 1800, key: 1800 },
    { text: '1 hour', value: 3600, key: 3600 },
  ];

  // Add the rest
  for (let i = 2; i <= 24; i++) {
    const seconds = i * 3600;
    result.push({ text: `${i} hours`, value: seconds, key: seconds });
  }
  return result;
})();

const exhaustSetpointOptions = (() => {
  const result = [];
  for (let i = 50; i <= 75; i++) {
    result.push({ text: `${i}C`, value: i, key: i });
  }
  return result;
})();

const performanceNumberOptions = (() => {
  const result = [];
  for (let i = 1950; i <= 2600; i += 10) {
    result.push({ text: `${i}`, value: i, key: i });
  }
  return result;
})();

const clockDividerOptions = [
  { text: '1', value: 1, key: 1 },
  { text: '2', value: 2, key: 2 },
  { text: '4', value: 4, key: 4 },
  { text: '8', value: 8, key: 8 },
]

const clockBiasOptions = (() => {
  const result = []
  for (let i = -5; i <= 5; i++) {
    result.push({ text: `${i > 0 ? '+' : ''}${i}`, value: i, key: i })
  }
  return result
})()

const biasGradientOptions = [
  { text: 'None', value: 'none', key: 0 },
  { text: 'Low', value: 'low', key: 1 },
  { text: 'Medium', value: 'medium', key: 2 },
  { text: 'High', value: 'high', key: 3 },
]

/*
Misc.
- controlmgr should ease changes to clock/bias of more than one step, one step per second
- 
*/

class MiningPanel extends React.PureComponent<CombinedProps> {
  public static styles: InputSheet<{}> = {
    buttonGroup: {
      width: '100%',
      marginBottom: '20px !important',
    },
    indent: {
      marginLeft: 20,
      marginBottom: 20,
    },
    chipStatsContainer: {
      display: 'flex',
    },
    chipStatsEntry: {
      textAlign: 'center',
      flex: 1,
    },
    hashboardEntryEven: {
      background: '#202020',
      padding: 20,
      marginBottom: 20,
    },
    hashboardEntryOdd: {
      padding: 20,
      marginBottom: 20,
    },
    hashboardHeader: {
       marginTop: '12px !important',
       marginBottom: '2px !important',
    }
  }

  componentWillMount() {
    if (this.props.dispatch) {
      this.props.dispatch(fetchMiningConfig.started({}));
    }
  }

  render() {
    const { classNames, miningForm } = this.props;

    const renderSave = (dirty: any) => {
      console.log('miningForm = ' + miningForm)
      switch (miningForm) {
        case 'started':
          return (
            <Dimmer active>
              <Loader />
            </Dimmer>
          );
        case 'failed':
          return <span>Failed</span>;
        case 'done':
          if (dirty) {
            return <Button type="submit">SAVE</Button>;
          }
          return <span>Done</span>;
      }
      if (dirty) {
        return <Button type="submit">SAVE</Button>;
      }
      return undefined;
    };

    return (
      <Content>
        <Header as="h1">Mining Config</Header>
        <Formik
          initialValues={this.props.miningConfig}
          enableReinitialize={true}
          onSubmit={(values: MiningConfig, formikBag: FormikProps<MiningConfig>) => {
            if (this.props.dispatch) {
              const dispatch = this.props.dispatch

              // TEMPHACK: Change the biasOffset to see if it gets reported
	      // values.hbConfig[0].chipConfig[0].biasOffset = 1
	      // values.hbConfig[0].chipConfig[31].biasOffset = 1


              dispatch(setMiningConfig.started(values));
              setTimeout(() => dispatch(fetchMiningConfig.started({})), 500);
            }
          }}
          validateOnChange={true}
          validateOnBlur={true}
          validate={(values: MiningConfig): any => {
            const errors: any = {};
            return errors;
          }}
          render={(formikBag: FormikProps<MiningConfig>) => {
            const formikProps = {
              ...formikBag,
              handleBlur: (e: any, data: any) => {
                if (data && data.name) {
                  formikProps.setFieldValue(data.name, data.value);
                  formikProps.setFieldTouched(data.name);
                }
              },
              handleChange: (e: any, data: any) => {
                if (data && data.name) {
                  formikProps.setFieldValue(data.name, data.value);
                }
              },
            };

            return (
              <div>
                <Form onSubmit={formikProps.handleSubmit}>
                  <Header as="h2">Performance</Header>
                  <Message
                   icon="info"
                   header="Performance Number"
                   content={
                     'The Performance Number specifies the relative performance level of the miner. Generally higher is better, ' +
                     'but it will reach a point where the miner cannot be pushed any further without errors.'
                   }
                  />
                  <Message
                   icon="info"
                   header="Clock Divider &amp; Bias"
                   content={
                     'The clockspeed of Obelisk miners is not set directly as it is in some other miners. ' +
                     'Instead, you have control over a clock divider and a clock bias.  The divider takes the ' +
                     'base clock frequency and divides it by the selected value (i.e., 1, 2, 4 or 8).  Then the clock bias ' +
                     'is applied, which moves the clock frequency closer to the previous or next divider level depending ' +
                     'on whether the bias is negative or positive.  The values for the bias range from -5 to +5.'
                   }
                  />
                  <div className={classNames.indent}>
                  {_.map(formikProps.values.hbConfig, (hbConfig: any, i: number) => (
                    <div>
                      <Header as="h3" className={classNames.hashboardHeader}>HASHBOARD {hbConfig.id}</Header>
                      <Form.Group widths='equal'>
                        <Form.Dropdown
                          options={performanceNumberOptions}
                          selection={true}
                          label={`PERFORMANCE NUMBER`}
                          name={`hbConfig[${i}].performanceNumber`}
                          onChange={formikProps.handleChange}
                          onBlur={formikProps.handleBlur}
                          value={formikProps.values.hbConfig[i].performanceNumber}
                          error={_.get(formikProps.errors, ['hbConfig', i, 'performanceNumber'])}
                        />
                        <Form.Dropdown
                          options={clockDividerOptions}
                          selection={true}
                          label={`DIVIDER`}
                          name={`hbConfig[${i}].clockDivider`}
                          onChange={formikProps.handleChange}
                          onBlur={formikProps.handleBlur}
                          value={formikProps.values.hbConfig[i].clockDivider}
                          error={_.get(formikProps.errors, ['hbConfig', i, 'clockDivider'])}
                        />
                        <Form.Dropdown
                          options={clockBiasOptions}
                          selection={true}
                          label={`BIAS`}
                          name={`hbConfig[${i}].clockBias`}
                          onChange={formikProps.handleChange}
                          onBlur={formikProps.handleBlur}
                          value={formikProps.values.hbConfig[i].clockBias}
                          error={_.get(formikProps.errors, ['hbConfig', i, 'clockBias'])}
                        />
                      </Form.Group>
                    </div>
                  ))}
                  </div>
                  <Header as="h2">Fan Control</Header>
                  <Message
                    icon="info"
                    header="Fan Control Modes"
                    content={
                      ' Constant Speed mode will run the fan at the specified speed.' +
                      'Automatic mode will adjust the fan speed every 10 seconds to try to bring the ' +
                      'reat heatsink temperature up or down to the specified REAR HEATSINK TEMP. SETPOINT.'
                    }
                  />
                  <Form.Dropdown
                    options={fanModeOptions}
                    selection={true}
                    label="FAN CONTROL MODE"
                    name="fanMode"
                    onChange={formikProps.handleChange}
                    onBlur={formikProps.handleBlur}
                    value={formikProps.values.fanMode}
                    error={formikProps.errors.fanMode}
                  />
                  <div className={classNames.indent}>
                    {formikProps.values.fanMode === 0 ? (
                      [
                        <Form.Dropdown
                          options={exhaustSetpointOptions}
                          selection={true}
                          label="REAR HEATSINK TEMP. SETPOINT"
                          name="exhaustSetpoint"
                          onChange={formikProps.handleChange}
                          onBlur={formikProps.handleBlur}
                          value={formikProps.values.exhaustSetpoint}
                          error={formikProps.errors.exhaustSetpoint}
                          key={formikProps.values.fanMode}
                        />,
                      ]
                    ) : (
                      <Form.Dropdown
                        options={fanSpeedOptions}
                        selection={true}
                        label="FAN SPEED"
                        name="fanSpeedPercent"
                        onChange={formikProps.handleChange}
                        onBlur={formikProps.handleBlur}
                        value={formikProps.values.fanSpeedPercent}
                        error={formikProps.errors.fanSpeedPercent}
                        key={formikProps.values.fanMode}
                      />
                    )}
                  </div>
                  <Header as="h2">Misc. Options</Header>
                  <Form.Dropdown
                    options={rebootIntervalOptions}
                    selection={true}
                    label="REBOOT EVERY"
                    name="rebootIntervalSecs"
                    onChange={formikProps.handleChange}
                    onBlur={formikProps.handleBlur}
                    value={formikProps.values.rebootIntervalSecs}
                    error={formikProps.errors.rebootIntervalSecs}
                  />
                 {renderSave(formikProps.dirty)}
                </Form>
              </div>
            );
          }}
        />
      </Content>
    );
  }
}

const mapStateToProps = (state: any, props: any): ConnectProps => ({
  miningConfig: getMiningConfig(state.Main),
  lastError: getLastError(state.Main),
  miningForm: state.Main.forms.miningForm,
});

const miningPanel = withStyles()<any>(MiningPanel);

export default connect<ConnectProps, any, any>(mapStateToProps)(miningPanel);
