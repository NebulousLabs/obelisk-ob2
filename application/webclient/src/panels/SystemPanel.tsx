// -------------------------------------------------------------------------------------------------
// Copyright 2018 Obelisk Inc.
// -------------------------------------------------------------------------------------------------

import Content from 'components/Content';
import { Formik, FormikProps } from 'formik';
import _ = require('lodash');
import {
  changePassword,
  fetchSystemConfig,
  fetchVersions,
  rebootMiner,
  setSystemConfig,
  clearFormStatus,
  factoryReset,
  runSoftwareRollback,
  runSoftwareUpdate,
  checkForSoftwareUpdate,
  identifyUnit,
} from 'modules/Main/actions';
import {
  getLatestFirmwareVersion,
  getActiveFirmwareVersion,
  getRollbackFirmwareVersion,
  getLastError,
  getSystemConfig,
} from 'modules/Main/selectors';
import { SystemConfig } from 'modules/Main/types';
import * as React from 'react';
import { connect, DispatchProp } from 'react-redux';
import withStyles, { InjectedProps, InputSheet } from 'react-typestyle';
import { Button, Dimmer, Form, Header, Loader, Message, Progress } from 'semantic-ui-react';
import { getHistory } from 'utils';

const history = getHistory();

const timezones = require('utils/timezones.json');
const Dropzone = require('react-dropzone').default;

interface ConnectProps {
  systemConfig: SystemConfig;
  lastError?: string;
  systemForm: string;
  passwordForm: string;
  activeFirmwareVersion: string;
  latestFirmwareVersion: string;
  rollbackFirmwareVersion: string;
}

interface State {
  isCheckingForUpdates: boolean;
}

type CombinedProps = ConnectProps & InjectedProps & DispatchProp<any>;

const timezoneOptions = _.map(timezones, (tz: any, index: number) => {
  const displayName = _.replace(tz.name, '_', ' ');
  return {
    text: `${displayName} (${tz.offset})`,
    value: tz.name,
    key: index,
  };
});

class SystemPanel extends React.PureComponent<CombinedProps, State> {
  public static styles: InputSheet<{}> = {
    buttonGroup: {
      width: '100%',
    },
    firmwareLabel: {
      fontSize: 17,
      fontFamily: 'Karbon Regular',
    },
    firmwareMessage: {
      fontSize: 17,
      fontFamily: 'Karbon Regular',
      color: '#e4001c',
      marginBottom: 16,
    },
    updateLoader: {
      display: 'inline-block',
      marginLeft: 8,
    },
    updateLabel: {
      marginLeft: 8,
    },
  };

  constructor(props: CombinedProps) {
    super(props);

    this.state = { isCheckingForUpdates: false };
  }

  componentWillMount() {
    if (this.props.dispatch) {
      this.props.dispatch(fetchSystemConfig.started({}));
      this.props.dispatch(clearFormStatus());
    }
  }

  render() {
    const {
      classNames,
      lastError,
      systemForm,
      passwordForm,
      latestFirmwareVersion,
      activeFirmwareVersion,
      rollbackFirmwareVersion,
    } = this.props;
    const { isCheckingForUpdates } = this.state;

    const renderSave = (dirty: any) => {
      switch (systemForm) {
        case 'started':
          return (
            <Dimmer active>
              <Loader />
            </Dimmer>
          );
        case 'failed':
          return <span>Failed</span>;
        case 'done':
          if (dirty) {
            return <Button type="submit">SAVE</Button>;
          }
          return <span>Done</span>;
      }
      if (dirty) {
        return <Button type="submit">SAVE</Button>;
      }
      return undefined;
    };

    const renderChangePassword = (formikProps: any) => {
      switch (passwordForm) {
        case 'started':
          return (
            <Dimmer active>
              <Loader />
            </Dimmer>
          );
        case 'failed':
          return <span>Failed</span>;
        case 'done':
          if (formikProps.dirty) {
            return <Button type="submit">SAVE</Button>;
          }
          return <span>Done</span>;
      }
      return (
        <Button type="button" onClick={formikProps.handleChangePassword}>
          CHANGE PASSWORD
        </Button>
      );
    };

    return (
      <Content>
        <Header as="h1">System Config</Header>
        <Formik
          initialValues={{
            ...(this.props.systemConfig || {}),
            oldPassword: '',
            newPassword: '',
          }}
          enableReinitialize={true}
          onSubmit={(values: SystemConfig, formikBag: FormikProps<SystemConfig>) => {
            if (this.props.dispatch) {
              const valuesToSend = {
                timezone: values.timezone,
              };
              const dispatch = this.props.dispatch;
              dispatch(setSystemConfig.started(valuesToSend));
              setTimeout(() => dispatch(fetchSystemConfig.started({})), 500);
            }
          }}
          validateOnChange={true}
          validateOnBlur={true}
          validate={(values: SystemConfig): any => {
            const errors: any = {};
            return errors;
          }}
          render={(formikBag: FormikProps<SystemConfig>) => {
            const formikProps = {
              ...formikBag,
              handleBlur: (e: any, data: any) => {
                if (data && data.name) {
                  formikProps.setFieldValue(data.name, data.value);
                  formikProps.setFieldTouched(data.name);
                }
              },
              handleChange: (e: any, data: any) => {
                if (data && data.name) {
                  formikProps.setFieldValue(data.name, data.value);
                }
              },
              handleOptimizationModeChange: (mode: any) => {
                formikProps.setFieldValue('optimizationMode', mode);
              },

              handleReboot: () => {
                const { dispatch } = this.props;

                if (dispatch) {
                  if (
                    confirm(
                      'This will immediately reboot the device and log you out of this app.' +
                        'You will be logged out of this app.\n\nDo you want to continue?',
                    )
                  ) {
                    dispatch(rebootMiner.started({}));
                    history.push('/login');
                  }
                }
              },

              handleConfigReset: () => {
                const { dispatch } = this.props;
                if (dispatch) {
                  if (
                    confirm(
                      'This will replace your current configuration settings for Pools, Mining, System and ' +
                        'Network with the factory defaults.  ' +
                        'You will be logged out of this app.\n\nDo you want to continue?',
                    )
                  ) {
                    dispatch(factoryReset.started({}));
                    history.push('/login');
                  }
                }
              },

              handleChangePassword: () => {
                const { dispatch } = this.props;
                const { oldPassword, newPassword } = formikProps.values;

                if (dispatch) {
                  dispatch(
                    changePassword.started({
                      username: 'admin',
                      oldPassword: oldPassword || '',
                      newPassword: newPassword || '',
                    }),
                  );
                }
              },

              handleSoftwareUpdate: () => {
                const { dispatch } = this.props;

                if (
                  confirm(
                    `This will update the miner software to ${latestFirmwareVersion} and then reboot it.  ` +
                      `You will be logged out of this app.`,
                  )
                ) {
                  if (dispatch) {
                    dispatch(runSoftwareUpdate.started({}));
                    history.push('/login');
                  }
                }
              },

              handleSoftwareRollback: () => {
                const { dispatch } = this.props;

                if (
                  confirm(
                    'This will rollback the miner software to the previous version and then reboot it.  ' +
                      'You will be logged out of this app.\n\nNOTE: You should only perform a rollback if you are ' +
                      'experiencing problems with the latest version.',
                  )
                ) {
                  if (dispatch) {
                    dispatch(runSoftwareRollback.started({}));
                    history.push('/login');
                  }
                }
              },

              handleCheckForSoftwareUpdate: () => {
                const { dispatch } = this.props;
                // Wait 10 seconds and then refresh
                if (dispatch) {
                  this.setState({ isCheckingForUpdates: true });
                  dispatch(checkForSoftwareUpdate.started({ autoupdate: false }));
                  setTimeout(() => {
                    dispatch(fetchVersions.started({}));
                  }, 10000);
                  setTimeout(() => {
                    this.setState({ isCheckingForUpdates: false });
                  }, 11000);
                }
              },

              handleStartFlashing: () => {
                const { dispatch } = this.props;

                if (dispatch) {
                  dispatch(
                    identifyUnit.started({
                      enabled: true,
                    }),
                  );
                }
              },

              handleStopFlashing: () => {
                const { dispatch } = this.props;

                if (dispatch) {
                  dispatch(
                    identifyUnit.started({
                      enabled: false,
                    }),
                  );
                }
              },
            };

            const updateAvailable =
              latestFirmwareVersion &&
              latestFirmwareVersion.length + 0 &&
              activeFirmwareVersion !== latestFirmwareVersion;

            const rollbackAvailable = rollbackFirmwareVersion !== '';

            const firmwareMessage = updateAvailable
              ? 'A new software update is available to install!'
              : undefined;

            return (
              <div>
                <Form onSubmit={formikProps.handleSubmit}>
                  <Header as="h2">Time Config</Header>
                  <Form.Dropdown
                    label="TIME ZONE"
                    name="timezone"
                    placeholder="Select your time zone"
                    onChange={formikProps.handleChange}
                    onBlur={formikProps.handleBlur}
                    selection={true}
                    search={true}
                    options={timezoneOptions}
                    value={formikProps.values.timezone}
                  />
                  {renderSave(formikProps.dirty)}
                </Form>

                <Form onSubmit={formikProps.handleSubmit}>
                  <Header as="h2">Change Password</Header>
                  <Form.Input label="USERNAME" name="username" value="admin" />
                  <Form.Input
                    type="password"
                    label="OLD PASSWORD"
                    name="oldPassword"
                    onChange={formikProps.handleChange}
                    onBlur={formikProps.handleBlur}
                    value={formikProps.values.oldPassword}
                    error={!!_.get(formikProps.errors, ['oldPassword'], '')}
                  />
                  <Form.Input
                    type="password"
                    label="NEW PASSWORD"
                    name="newPassword"
                    onChange={formikProps.handleChange}
                    onBlur={formikProps.handleBlur}
                    value={formikProps.values.newPassword}
                    error={!!_.get(formikProps.errors, ['newPassword'], '')}
                  />
                  {renderChangePassword(formikProps)}
                </Form>

                <Form>
                  <Header as="h2">Firmware Update</Header>
                  <div className={classNames.firmwareLabel}>
                    The current firmware version is {activeFirmwareVersion}.
                  </div>
                  <div className={classNames.firmwareMessage}>{firmwareMessage}</div>
                  {updateAvailable ? (
                    <Button
                      icon="play circle outline"
                      onClick={formikProps.handleSoftwareUpdate}
                      content={'UPDATE TO ' + latestFirmwareVersion}
                    />
                  ) : (
                    undefined
                  )}
                  {rollbackAvailable ? (
                    <Button
                      icon="undo"
                      onClick={formikProps.handleSoftwareRollback}
                      content={'ROLLBACK TO ' + rollbackFirmwareVersion}
                    />
                  ) : (
                    undefined
                  )}
                  {isCheckingForUpdates ? (
                    <div className={classNames.updateLoader}>
                      <Loader active={true} inline={true} />
                      <span className={classNames.updateLabel}>CHECKING FOR UPDATE...</span>
                    </div>
                  ) : (
                    <Button
                      icon="refresh"
                      onClick={formikProps.handleCheckForSoftwareUpdate}
                      content={'CHECK FOR UPDATE'}
                    />
                  )}
                </Form>
                <Form>
                  <Header as="h2">System Actions</Header>
                  <Message
                    icon="warning sign"
                    header="Reboot the Miner"
                    content={
                      'This will immediately reboot the miner.  You will need to reload this ' +
                      'page and login again.'
                    }
                  />
                  <Button onClick={formikProps.handleReboot}>REBOOT</Button>
                  <Message
                    icon="warning sign"
                    header="Factory Reset"
                    content={
                      'This will reset all configuration on the miner to factory defaults, and reboot it.  ' +
                      'You will need to reload this page and login again.'
                    }
                  />
                  <Button onClick={formikProps.handleConfigReset}>FACTORY RESET</Button>
                  <Message
                    icon="warning sign"
                    header="Identify Device"
                    content={
                      'These actions will start/stop alternately flashing the front green and red LEDs on the device.'
                    }
                  />
                  <Button onClick={formikProps.handleStartFlashing}>START FLASHING</Button>
                  <Button onClick={formikProps.handleStopFlashing}>STOP FLASHING</Button>
                </Form>
              </div>
            );
          }}
        />
      </Content>
    );
  }
}

const mapStateToProps = (state: any, props: any): ConnectProps => ({
  systemConfig: getSystemConfig(state.Main),
  lastError: getLastError(state.Main),
  systemForm: state.Main.forms.systemForm,
  passwordForm: state.Main.forms.passwordForm,
  activeFirmwareVersion: getActiveFirmwareVersion(state.Main),
  latestFirmwareVersion: getLatestFirmwareVersion(state.Main),
  rollbackFirmwareVersion: getRollbackFirmwareVersion(state.Main),
});

const systemPanel = withStyles()<any>(SystemPanel);

export default connect<ConnectProps, any, any>(mapStateToProps)(systemPanel);
