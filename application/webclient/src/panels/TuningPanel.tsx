// -------------------------------------------------------------------------------------------------
// Copyright 2018 Obelisk Inc.
// -------------------------------------------------------------------------------------------------

import _ = require('lodash')
import * as React from 'react'
import { connect, DispatchProp } from 'react-redux'
import withStyles, { InjectedProps, InputSheet } from 'react-typestyle'
import { Header, Label, Table, TextArea } from 'semantic-ui-react'

import Content from 'components/Content'
import Stat from 'components/Stat'
import { fetchDashboardStatus, fetchMiningConfig } from 'modules/Main/actions'
import { getDashboardStatus, getMiningConfig } from 'modules/Main/selectors'
import {
  DashboardStatus,
  HashboardStatus,
  MiningConfig,
  NUM_CHIPS_PER_HASHBOARD,
} from 'modules/Main/types'

interface ConnectProps {
  miningConfig?: MiningConfig
  dashboardStatus?: DashboardStatus
}

type CombinedProps = ConnectProps & InjectedProps & DispatchProp<any>

class TuningPanel extends React.PureComponent<CombinedProps> {
  public static styles: InputSheet<{}> = {
    table: {
      $debugName: 'table',
      fontFamily: 'Karbon Regular',
      tableLayout: 'fixed',
    },
  }

  timer: any

  refreshData = () => {
    const { dispatch } = this.props
    if (dispatch) {
      dispatch(fetchDashboardStatus.started({}))
      dispatch(fetchMiningConfig.started({}))
    }
  }

  componentWillMount() {
    this.timer = setInterval(this.refreshData, 5000)
    this.refreshData()
  }

  componentWillUnmount() {
    if (this.timer) {
      clearInterval(this.timer)
    }
  }

  render() {
    const { classNames, dashboardStatus } = this.props

    let devStatus
    let boardCols
    let boardHeaders
    let chipRows: any = []

    if (dashboardStatus) {
      const { hashboardStatus } = dashboardStatus
      devStatus = [
        // <Stat label="Pool Diff." value={dashboardStatus.poolDifficulty} key="poolDifficulty" />,
      ]

      const hashboardTableMap = {
        'GOOD NONCES': (s: HashboardStatus[]) =>
          _.map(s, (h: HashboardStatus, i) => (
            <Table.Cell key={i} textAlign="center">
              {h.goodNonces}
            </Table.Cell>
          )),
        'BAD NONCES': (s: HashboardStatus[]) =>
          _.map(s, (h: HashboardStatus, i) => (
            <Table.Cell key={i} textAlign="center">
              {h.totalNonces - h.goodNonces}
            </Table.Cell>
          )),
        'TOTAL NONCES': (s: HashboardStatus[]) =>
          _.map(s, (h: HashboardStatus, i) => (
            <Table.Cell key={i} textAlign="center">
              {h.totalNonces}
            </Table.Cell>
          )),
        //        'POOL NONCES': (s: HashboardStatus[]) =>
        //          _.map(s, (h: HashboardStatus, i) => (
        //            <Table.Cell key={i} textAlign="center">
        //              {h.poolNonces}
        //            </Table.Cell>
        //          )),
        'PERFORMANCE #': (s: HashboardStatus[]) =>
          _.map(s, (h: HashboardStatus, i) => (
            <Table.Cell key={i} textAlign="center">
              {h.performanceNumber}
            </Table.Cell>
          )),
        CLOCK: (s: HashboardStatus[]) =>
          _.map(s, (h: HashboardStatus, i) => (
            <Table.Cell key={i} textAlign="center">
              /{h.clockDivider}
              {h.clockBias < 0 ? h.clockBias : '+' + h.clockBias}
            </Table.Cell>
          )),
      }

      /*
  <Header as="h4">CHIP HASHRATES (GH/S)</Header>
  <div className={classNames.indent}>
    <div className={classNames.chipStatsContainer}>
      {_.map(
        formikProps.values.perfConfig[boardIndex].chipPerfConfig,
        (chipPerfInfo: ChipPerfInfo, chipIndex: number) =>
          <Popup content={`Chip # ${chipIndex+1}`}
           key={chipIndex}
           on='hover'
           trigger={<div className={classNames.chipStatsEntry}>{chipPerfInfo.hashrateGHs}</div>}
           position='bottom center'/>
        )}
    </div>
  </div>
  */
      boardHeaders = hashboardStatus.map((hashboardStatus, i) => {
        return (
          <Table.HeaderCell key={hashboardStatus.boardNum} textAlign="center">
            BOARD {hashboardStatus.boardNum}
          </Table.HeaderCell>
        )
      })

      // TODO: This needs to map over each hashboard!!!!
      boardCols = _.map(Object.keys(hashboardTableMap), (name: string, index: number) => {
        return (
          <Table.Row key={index}>
            <Table.Cell>{name}</Table.Cell>
            {hashboardTableMap[name](hashboardStatus)}
          </Table.Row>
        )
      })

      // Create the chip rows
      for (let c = 0; c < NUM_CHIPS_PER_HASHBOARD; c++) {
        const chipCols = [<Table.Cell>CHIP {c + 1}</Table.Cell>]

        for (let h = 0; h < hashboardStatus.length; h++) {
          const chipStats = hashboardStatus[h].chipStatus[c]
          chipCols.push(
            <Table.Cell textAlign="center">
              <div>GOOD NONCES: {chipStats.goodNonces}</div>
              <div>BAD NONCES:  {chipStats.totalNonces - chipStats.goodNonces}</div>
              <div>TOTAL NONCES: {chipStats.totalNonces}</div>
              <div>BIAS OFFSET: {chipStats.biasOffset < 0 ? chipStats.biasOffset : '+' + chipStats.biasOffset}</div>
            </Table.Cell>
          )
        }

        chipRows.push(
          <Table.Row key={'chip' + (c + 1)}>
            {chipCols}
          </Table.Row>
        )
      }
    }

    return (
      <Content>
        <Header as="h1">Tuning/Stats</Header>

        <Header as="h2">Device</Header>
        <Table definition={true} striped={true} unstackable={true} className={classNames.table}>
          <Table.Body>{devStatus}</Table.Body>
        </Table>

        <Header as="h2">Hashboards</Header>
        <Table definition={true} striped={true} unstackable={true} className={classNames.table}>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell width={4} />
              {boardHeaders}
            </Table.Row>
          </Table.Header>
          <Table.Body>
            {boardCols}
            {chipRows}
          </Table.Body>
        </Table>

      </Content >
    )
  }
}

const mapStateToProps = (state: any, props: any): ConnectProps => ({
  miningConfig: getMiningConfig(state.Main),
  dashboardStatus: getDashboardStatus(state.Main),
})

const tuningPanel = withStyles()<any>(TuningPanel)
export default connect<ConnectProps, any, any>(mapStateToProps)(tuningPanel)
