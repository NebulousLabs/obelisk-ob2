// -------------------------------------------------------------------------------------------------
// Copyright 2018 Obelisk Inc.
// -------------------------------------------------------------------------------------------------

import * as React from 'react'
import { Icon, Menu } from 'semantic-ui-react'
import withStyles, { InjectedProps, InputSheet } from 'react-typestyle'
import * as Colors from 'utils/colors'

interface Props {
  active: boolean
  name: string
  label: string
  iconName: string
  onClick: () => void
  badge?: string
}

type CombinedProps = Props & InjectedProps

class SidebarMenuItem extends React.Component<CombinedProps> {
  public static styles: InputSheet<{}> = {
    badge: {
      background: Colors.brightRed,
      borderRadius: 20,
      height: 22,
      width: 22,
      position: 'absolute',
      right: 22,
      top: 28,
      paddingTop: 3,
      fontSize: 18,
      fontFamily: 'Karbon-Regular'
    },
  }

  render() {
    const { active, badge, classNames, name, iconName, label, onClick } = this.props

    let badgeElem = undefined
    if (badge) {
      badgeElem = <div className={classNames.badge}>{badge}</div>
    }

    const iconAny: any = iconName // Work around stupid type definitions for Icon
    return (
      <Menu.Item name={name} active={active} onClick={onClick}>
        <Icon name={iconAny} />
        {label}
        {badgeElem}
      </Menu.Item>
    )
  }
}

export default withStyles()<any>(SidebarMenuItem)
