// -------------------------------------------------------------------------------------------------
// Copyright 2018 Obelisk Inc.
// -------------------------------------------------------------------------------------------------

export const NUM_FANS = 4
export const MAX_HASHBOARDS = 4
export const NUM_CHIPS_PER_HASHBOARD = 32

// -------------------------------------------------------------------------------------------------
// User interfaces
// -------------------------------------------------------------------------------------------------
export interface UserInfo {
  username: string;
}

export interface LoginRequest {
  username: string;
  password: string;
}

export interface LoginResponse {
  username: string;
  deviceModel: string;
}

// -------------------------------------------------------------------------------------------------
// Inventory interfaces
// -------------------------------------------------------------------------------------------------
export interface VersionInfo {
  activeFirmwareVersion: string;
  latestFirmwareVersion: string;
  rollbackFirmwareVersion: string;
  cgminerVersion: any;
}

// -------------------------------------------------------------------------------------------------
// Config interfaces
// -------------------------------------------------------------------------------------------------
export interface NetworkConfig {
  macAddress?: string
  ipAddress?: string
  subnetMask?: string
  dhcpEnabled?: string
  gateway?: string
  dnsServer?: string
  hostname?: string
}

export interface PoolConfig {
  url: string
  worker: string
  password: string
}

export interface ChipMiningConfig {
  biasOffset: number
}

export interface BoardMiningConfig {
  id: number
  performanceNumber: number
  clockDivider: number
  clockBias: number
  chipConfig: ChipMiningConfig[]
}

export interface MiningConfig {
  rebootIntervalSecs: number
  fanMode: number
  fanSpeedPercent: number
  exhaustSetpoint: number
  hbConfig: BoardMiningConfig[]
}

export const MAX_POOLS = 3;

export interface UserInfo {
  userInfo: UserInfo;
}

export interface SystemConfig {
  timezone?: string;

  // These are here just to satisfy Formik type handling - never send them with SystemConfig
  oldPassword?: string;
  newPassword?: string;
}

export interface HashingConfig {
  clockrate: number;
}

// -------------------------------------------------------------------------------------------------
// Status interfaces
// -------------------------------------------------------------------------------------------------
export interface HashrateEntry {
  time: number;
  'Board 1'?: number;
  'Board 2'?: number;
  'Board 3'?: number;
  Total?: number;
}

export interface StatsEntry {
  name: string;
  value: string;
}

export interface PoolStatus {
  url: string;
  worker: string;
  status: string;
  accepted: number;
  rejected: number;
  difficulty: number;
}

export interface ChipStatus {
  goodNonces: number
  totalNonces: number

  // Current know settings
  biasOffset: number
}

export interface HashboardStatus {
  boardNum: number
  status: string
  firmwareVersion: string
  intakeTemp: number
  exhaustTemp: number
  exhaustTempSetpoint: number

  // TODO: These are strings due to a bug in wvalue in Crow.  It doesn't seem to like
  //       adding doubles or float to an object.
  hashrate1min: string
  hashrate5min: string
  hashrate15min: string

  goodNonces: number
  totalNonces: number

  // These represent the current values of the corresponding config settings
  // It's possible they may differ if values are eased to over time for safety.
  performanceNumber: number
  clockDivider: number
  clockBias: number
  current: number
  voltage: number

  isTuning: boolean
  noiseCounter: number
  watchdogCounter: number

  chipStatus: ChipStatus[]
}

export interface DashboardStatus {
  hashrateData: HashrateEntry[];
  poolStatus: PoolStatus[];
  hashboardStatus: HashboardStatus[];
  systemInfo: StatsEntry[];
}

export interface ChangePasswordParams {
  username: string;
  oldPassword: string;
  newPassword: string;
}

export interface CheckForSoftwareUpdateParams {
  autoupdate: boolean;
}

// Identify the unit visually by flashing (or not flashing) the front LEDs alternately
export interface IdentifyUnitParams {
  enabled: boolean;
}

export interface FileFragment {
  data: string;
  offset: number;
}

export interface FormState {
  poolForm: string;
  systemForm: string;
  passwordForm: string;
  networkForm: string;
  miningForm: string;
}

export interface State {
  showSidebar: boolean;
  deviceModel: string;
  username: string;

  // Inventory
  versions: VersionInfo;

  // Configs
  networkConfig: NetworkConfig;
  poolsConfig: PoolConfig[];
  systemConfig: SystemConfig;
  miningConfig: MiningConfig;

  // Statuses
  dashboardStatus: DashboardStatus

  // Requests
  activeRequestType?: string;
  lastRequestType?: string;
  lastRequestParams?: any;
  lastError?: string;

  // Change routes when a request succeeds/fails
  nextRouteOnSuccess?: string;
  nextRouteOnFail?: string;

  // UI forms
  forms: FormState;

  diagnostics?: string
}

export interface RunUpdateResponse {
  message: string;
}
