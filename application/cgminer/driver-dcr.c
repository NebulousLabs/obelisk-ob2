#include <string.h>
#include <stdio.h>
#include <unistd.h>
#define __USE_GNU
#include <pthread.h>

#include "common.h"
#include "miner.h"
#include "logger.h"
#include "list.h"
#include "ipc.h"
#include "driver-dcr.h"

static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

static struct list nonce_list;

static bool clean_active = false;

static uint32_t pool_nonces = 0;
static uint32_t good_nonces = 0;
static uint32_t bad_nonces = 0;

static struct cgpu_info *cgpu;

static pool_info_t pool_stats[MAX_POOLS];

static uint8_t chip_target[MIDSTATE_SIZE] =
{
    0x00, 0x00, 0x00, 0x00, 0xff, 0xff, 0xff, 0xff,
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff
};

static uint8_t pool_target[MAX_POOLS][MIDSTATE_SIZE];

/*
 * Blake-256 constants, taken verbatim from the final
 * spec, "SHA-3 proposal BLAKE" version 1.3, published
 * on December 16, 2010 - section 2.1.1.
 */
static const uint32_t BLAKE256_CONSTS[16] =
{
    0x243F6A88U, 0x85A308D3U, 0x13198A2EU, 0x03707344U,
    0xA4093822U, 0x299F31D0U, 0x082EFA98U, 0xEC4E6C89U,
    0x452821E6U, 0x38D01377U, 0xBE5466CFU, 0x34E90C6CU,
    0xC0AC29B7U, 0xC97C50DDU, 0x3F84D5B5U, 0xB5470917U
};

static const uint32_t v12 = 0xA4093D82U;
static const uint32_t v13 = 0x299F3470U;

/*
 * Taken from the same document as the above constants,
 * these are the initial values for Blake-256, which
 * are shared by SHA-256 - found in section 2.1.1.
 */
static const uint32_t BLAKE256_IV[8] =
{
    0x6A09E667U, 0xBB67AE85U, 0x3C6EF372U, 0xA54FF53AU,
    0x510E527FU, 0x9B05688CU, 0x1F83D9ABU, 0x5BE0CD19U
};

/*
 * Sigma values - permutations of 0 -15, used in Blake-256.
 * Found in the aforementioned spec in section 2.1.2.
 */
static const uint8_t BLAKE256_SIGMA[10][16] =
{
    {  0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15 },
    { 14, 10,  4,  8,  9, 15, 13,  6,  1, 12,  0,  2, 11,  7,  5,  3 },
    { 11,  8, 12,  0,  5,  2, 15, 13, 10, 14,  3,  6,  7,  1,  9,  4 },
    {  7,  9,  3,  1, 13, 12, 11, 14,  2,  6,  5, 10,  4,  0, 15,  8 },
    {  9,  0,  5,  7,  2,  4, 10, 15, 14,  1, 11, 12,  6,  8,  3, 13 },
    {  2, 12,  6, 10,  0, 11,  8,  3,  4, 13,  7,  5, 15, 14,  1,  9 },
    { 12,  5,  1, 15, 14, 13,  4, 10,  0,  7,  6,  3,  9,  2,  8, 11 },
    { 13, 11,  7, 14, 12,  1,  3,  9,  5,  0, 15,  4,  8,  6,  2, 10 },
    {  6, 15, 14,  9, 11,  3,  0,  8, 12,  2, 13,  7,  1,  4, 10,  5 },
    { 10,  2,  8,  4,  7,  6,  1,  5, 15, 11,  9, 14,  3, 12, 13 , 0 }
};

static uint8_t dcr_boards;
static struct list job_list;
static uint8_t current_job_id = 1;

static job_t *find_job_by_dcr_id(
    uint8_t job_id
)
{
    struct list_entry *entry;

    TAILQ_FOREACH(entry, &job_list.head, next_entry)
    {
        job_t *job = entry->data;

        if (job && (job->job_id == job_id))
            return job;
    }
    return NULL;
}

static job_t *find_job_by_pool_id(
    char *job_id
)
{
    struct list_entry *entry;

    TAILQ_FOREACH(entry, &job_list.head, next_entry)
    {
        job_t *job = entry->data;

        if (job && (strcmp(job->work->job_id, job_id) == 0))
            return job;
    }
    return NULL;
}

static void dcrBlake256CompressBlock(
    uint32_t *state,
    const uint32_t *m,
    const uint32_t t
)
{
    uint32_t v[16];

    memcpy(v, state, MIDSTATE_SIZE);
    memcpy(v + 8, BLAKE256_CONSTS, MIDSTATE_CONSTANT_SIZE);

    v[12] ^= t;
    v[13] ^= t;

    for(int i = 0; i < 14; ++i)
    {
        uint32_t rnd = (i > 9) ? i - 10 : i;
        
        G(v[0], v[4], v[8], v[12], m[BLAKE256_SIGMA[rnd][0]], m[BLAKE256_SIGMA[rnd][1]], BLAKE256_CONSTS[BLAKE256_SIGMA[rnd][1]], BLAKE256_CONSTS[BLAKE256_SIGMA[rnd][0]]);
        G(v[1], v[5], v[9], v[13], m[BLAKE256_SIGMA[rnd][2]], m[BLAKE256_SIGMA[rnd][3]], BLAKE256_CONSTS[BLAKE256_SIGMA[rnd][3]], BLAKE256_CONSTS[BLAKE256_SIGMA[rnd][2]]);
        G(v[2], v[6], v[10], v[14], m[BLAKE256_SIGMA[rnd][4]], m[BLAKE256_SIGMA[rnd][5]], BLAKE256_CONSTS[BLAKE256_SIGMA[rnd][5]], BLAKE256_CONSTS[BLAKE256_SIGMA[rnd][4]]);
        G(v[3], v[7], v[11], v[15], m[BLAKE256_SIGMA[rnd][6]], m[BLAKE256_SIGMA[rnd][7]], BLAKE256_CONSTS[BLAKE256_SIGMA[rnd][7]], BLAKE256_CONSTS[BLAKE256_SIGMA[rnd][6]]);
        
        G(v[0], v[5], v[10], v[15], m[BLAKE256_SIGMA[rnd][8]], m[BLAKE256_SIGMA[rnd][9]], BLAKE256_CONSTS[BLAKE256_SIGMA[rnd][9]], BLAKE256_CONSTS[BLAKE256_SIGMA[rnd][8]]);
        G(v[1], v[6], v[11], v[12], m[BLAKE256_SIGMA[rnd][10]], m[BLAKE256_SIGMA[rnd][11]], BLAKE256_CONSTS[BLAKE256_SIGMA[rnd][11]], BLAKE256_CONSTS[BLAKE256_SIGMA[rnd][10]]);
        G(v[2], v[7], v[8], v[13], m[BLAKE256_SIGMA[rnd][12]], m[BLAKE256_SIGMA[rnd][13]], BLAKE256_CONSTS[BLAKE256_SIGMA[rnd][13]], BLAKE256_CONSTS[BLAKE256_SIGMA[rnd][12]]);
        G(v[3], v[4], v[9], v[14], m[BLAKE256_SIGMA[rnd][14]], m[BLAKE256_SIGMA[rnd][15]], BLAKE256_CONSTS[BLAKE256_SIGMA[rnd][15]], BLAKE256_CONSTS[BLAKE256_SIGMA[rnd][14]]);
    }

    for(int i = 0; i < 8; ++i)
        state[i] ^= v[i] ^ v[i + 8];
}

/* Build a Decred block header that is ready for hashing. */
static void dcrBuildBlockHeader(
    DecredBlockHeader* header,
    struct work* work
)
{
    struct pool* pool = work->pool;

    memset(header, 0, sizeof(DecredBlockHeader));
    hex2bin((uint8_t*)header->version, (const char*)&pool->bbversion, strlen(pool->bbversion));
    hex2bin(header->prevHash, work->prev_hash, HASH_SIZE * 2);

    // Swap the prev_hash byte order
    uint32_t *swap = (uint32_t*)header->prevHash;
    int i = 0;
    for(i = 0; i < HASH_SIZE; i++)
        swap[i] = __bswap_32(swap[i]);

    memcpy(header->merkleRoot, pool->coinbase1, pool->coinbase1_len - 4);  // We trim off the last 4 bytes as they are unused
    memcpy(header->extraNonce1, pool->nonce1bin, sizeof(header->extraNonce1));
    memcpy(header->extraNonce2, &work->nonce2, sizeof(header->extraNonce2));

    // Stake version comes from coinbase2
    memcpy(header->stakeVersion, pool->coinbase2, sizeof(header->stakeVersion));
}

/*
 * dcrCompressToMidstate will take the header and do the compressions
 * required to produce the midstate.
 */
static void dcrCompressToMidstate(
    uint8_t *midstate,
    uint8_t *header
)
{
    /*
     * Have to typecast the midstate to a uint32_t because the whole
     * blake256 library operates off of arrays of 32 bit integers.
     */
    uint32_t* castMidstate = (uint32_t *)midstate;
    uint32_t* castHeader = (uint32_t *)header;

    memcpy(castMidstate, BLAKE256_IV, MIDSTATE_SIZE);
    dcrBlake256CompressBlock(castMidstate, castHeader, 0x200);
    dcrBlake256CompressBlock(castMidstate, castHeader + 16, 0x400);
}

/*
 * dcrPrepareMidstate will take a 180 byte block header and process it into a
 * midstate that can be passed to the ASIC. The ASIC will not be able to
 * process a full header.
 *
 * The input to the ASIC will be the midstate plus the final 52 bytes of the
 * header. The nonce appears in bytes 12-16 of the headerTail (which itself is
 * the final 52 bytes of the header).
 */
static void dcrPrepareMidstate(
    uint8_t *midstate,
    uint8_t *header
)
{
    /* Start by swapping the endian-ness. */
    uint32_t *swap = (uint32_t *)header;
    int i = 0;

    for(i = 0; i < 45; i++)
        swap[i] = __bswap_32(swap[i]);
    dcrCompressToMidstate(midstate, header);
}

/* computeTarget coverts a difficulty into a target. */
static void computeTarget(uint8_t *target, double difficulty)
{
    uint64_t baseDifficulty = 0xffffffffffffffff;  /* Differs between SIA and DCR */
    uint64_t adjustedDifficulty = baseDifficulty/(uint64_t)difficulty;
    int i;

    memcpy(target, chip_target, MIDSTATE_SIZE);
    for(i = 0; i < 8; i++)
    {
        target[11-i] = adjustedDifficulty % 256;
        adjustedDifficulty /= 256;
    }
}

/*
 * checksumMeetsTarget determines whether the checksum meets the
 * provided target.
 */
static bool checksumMeetsTarget(uint8_t *checksum, uint8_t *target)
{
    for(int i = 0; i < MIDSTATE_SIZE; i++)
    {
        if(checksum[31-i] > target[i])
            return false;
        if(checksum[31-i] < target[i])
            return true;
    }
    return true;
}

/*
 * dcrMidstateMeetsProvidedTarget checks whether the provided midstate and
 * header tail hash to the provided target.
 */
static bool dcrMidstateMeetsProvidedTarget(
    uint8_t *midstate,
    uint8_t *headerTail
)
{
    uint32_t checksum[8];
    uint32_t m[16];
    int i;

    memcpy(checksum, midstate, MIDSTATE_SIZE);
    memset(m, 0x00, sizeof(m));
    memcpy(m, headerTail, DECRED_HEADER_TAIL_SIZE);
    m[13] = 0x80000001U;
    m[15] = 0x000005A0U;
    dcrBlake256CompressBlock(checksum, m, 0x5A0);
    
    for(i = 0; i < 8; i++)
        checksum[i] = __bswap_32(checksum[i]);
    
    return checksumMeetsTarget((uint8_t *)checksum, chip_target);
}

static int dcrHeaderMeetsChipTargetAndPoolDifficulty(
    uint8_t *midstate,
    uint8_t *headerTail,
    uint8_t *target
)
{
	uint32_t checksum[8];
	uint32_t m[16];
	int i;

	memcpy(checksum, midstate, MIDSTATE_SIZE);
	memset(m, 0x00, sizeof(m));
	memcpy(m, headerTail, DECRED_HEADER_TAIL_SIZE);
	m[13] = 0x80000001U;
	m[15] = 0x000005A0U;
	dcrBlake256CompressBlock(checksum, m, 0x5A0);

	for(i = 0; i < 8; i++)
		checksum[i] = __bswap_32(checksum[i]);

	if (checksumMeetsTarget((uint8_t *)checksum, target))
		return 2;

	if (checksumMeetsTarget((uint8_t *)checksum, chip_target))
		return 1;

	return 0;
}

static int dcrValidNonce(
    struct work *work,
    nonce_t nonce,
    uint32_t extra_nonce2
)
{
	uint8_t midstate[MIDSTATE_SIZE];
	uint8_t header_tail[DECRED_HEADER_TAIL_SIZE];

	memcpy(midstate, work->midstate, MIDSTATE_SIZE);
	memcpy(header_tail, work->header_tail, DECRED_HEADER_TAIL_SIZE);
	memcpy(header_tail + DECRED_HEADER_TAIL_NONCE_OFFSET, &nonce, sizeof(nonce_t));
	memcpy(header_tail + DECRED_HEADER_TAIL_EXTRANONCE2_OFFSET, &extra_nonce2, sizeof(uint32_t));

	/* Check if it meets the pool's stratum difficulty. */
	if (!work->pool) {
		return dcrMidstateMeetsProvidedTarget(midstate, header_tail);
	}
	return dcrHeaderMeetsChipTargetAndPoolDifficulty(midstate, header_tail, pool_target[work->pool->pool_no]);
}

static void update_pool_stats(void)
{
    int i;

	for (i = 0; i < total_pools; i++)
    {
        struct pool *pool;
        pool_info_t *lstats = NULL;
        bool send_update;
        pool_status_t status = PS_DISABLED;
        int j;

        send_update = false;
		pool = pools[i];

        /* First find the corresponding pool entry */
        for (j = 0; j < MAX_POOLS; ++j)
        {
            if (pool_stats[j].pool_index == pool->pool_no)
            {
                lstats = &pool_stats[j];
                break;
            }
        }
    
        if (lstats == NULL)
        {
            /* Find the next available slot to use to store stats */
            for (j = 0; j < MAX_POOLS; ++j)
            {
                if (pool_stats[j].pool_index == DEFAULT_POOL_INDEX)
                {
                    lstats = &pool_stats[j];
                    send_update = true;
                    break;
                }
            }
        }
    
        if (lstats)
        {
            lstats->pool_index = pool->pool_no;
            if (lstats->accepted != pool->accepted)
            {
                lstats->accepted = pool->accepted;
                send_update = true;
            }
            if (lstats->rejected != pool->rejected)
            {
                lstats->rejected = pool->rejected;
                send_update = true;
            }
            switch (pool->enabled)
            {
                case POOL_DISABLED: status = PS_DISABLED; break;
                case POOL_ENABLED: status = PS_ENABLED; break;
                case POOL_REJECTING: status = PS_REJECTING; break;
            }
            if (lstats->status != status)
            {
                lstats->status = status;
                send_update = true;
            }
            if (lstats->idle != pool->idle)
            {
                lstats->idle = pool->idle;
                send_update = true;
            }
            if (send_update)
            {
                log_entry(LEVEL_DEBUG, "[%s] updating pool %d stats", __func__, pool->pool_no);
                log_entry(LEVEL_DEBUG, "[%s] \taccepted: %lld", __func__, pool->accepted);
                log_entry(LEVEL_DEBUG, "[%s] \trejected: %lld", __func__, pool->rejected);
                switch (pool->enabled)
                {
                    case POOL_ENABLED:
                        if (pool->idle)
                        {
                            log_entry(LEVEL_DEBUG, "[%s] \tstatus: DEAD", __func__);
                        } else
                        {
                            log_entry(LEVEL_DEBUG, "[%s] \tstatus: ALIVE", __func__);
                        }
                    break;
                    case POOL_DISABLED: log_entry(LEVEL_DEBUG, "[%s] \tstatus: DISABLED", __func__); break;
                    case POOL_REJECTING: log_entry(LEVEL_DEBUG, "[%s] \tstatus: REJECTING", __func__); break;
                }
                ipc_send_stats(&ob2_config, lstats);
            }
        }
	}
}

void dcr_job_complete(
    uint8_t job_id
)
{
    struct list_entry *entry;

    log_entry(LEVEL_DEBUG, "[%s] received DCR job complete", __func__);

    pthread_mutex_lock(&mutex);
    TAILQ_FOREACH(entry, &job_list.head, next_entry)
    {
        job_t *job = entry->data;

        if (job && (job->job_id == job_id))
        {
            del_entry(&job_list, entry);
            break;
        }
    }
    pthread_mutex_unlock(&mutex);
}

void dcr_job_fail(
    uint8_t job_id
)
{
    struct list_entry *entry;

    log_entry(LEVEL_DEBUG, "[%s] received DCR job fail", __func__);

    pthread_mutex_lock(&mutex);
    TAILQ_FOREACH(entry, &job_list.head, next_entry)
    {
        job_t *job = entry->data;

        if (job && (job->job_id == job_id))
        {
            int rc = ipc_send_dcr_work(&ob2_config, job);
            if (rc == 0)
                job->state = POOL_JOB_ACTIVE;
            break;
        }
    }
    pthread_mutex_unlock(&mutex);
}

void dcr_queue_nonce(
    tlv_ipc_cgminer_dcr_nonce_t *info
)
{
    job_t *job = find_job_by_dcr_id(info->job_id);

    if (job)
    {
        pthread_mutex_lock(&mutex);
#if 0
        log_entry(LEVEL_DEBUG, "[%s] received DCR job nonce", __func__);
        log_entry(LEVEL_DEBUG, "[%s] \tjob ID: %d", __func__, info->job_id);
        log_entry(LEVEL_DEBUG, "[%s] \t nonce: %08x", __func__, info->nonce);
        log_entry(LEVEL_DEBUG, "[%s] \t    m5: %08x", __func__, info->m5);
#endif
        /* Only queue nonces with a matching job ID */
        if (job->state == POOL_JOB_ACTIVE)
        {
            if (!add_entry(&nonce_list, info, sizeof(tlv_ipc_cgminer_dcr_nonce_t)))
                log_entry(LEVEL_ERROR, "[%s] add_entry failed", __func__);
        }
        pthread_mutex_unlock(&mutex);
    }
}

void dcr_gen_stratum_work(
    struct pool *pool,
    struct work *work
)
{
    DecredBlockHeader header;
    uint8_t *pHeader = (uint8_t*)&header;

    cg_memcpy(work->prev_hash, pool->prev_hash, HASH_SIZE * 2);

    dcrBuildBlockHeader(&header, work);
    dcrPrepareMidstate(work->midstate, pHeader);
    memcpy(work->header_tail, &pHeader[DECRED_HEADER_TAIL_OFFSET_IN_BLOCK], DECRED_HEADER_TAIL_SIZE);
}

void dcr_set_clean(void)
{
    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    clean_active = true;
}

void dcr_set_difficulty(
    int pool_id,
    double difficulty
)
{
    log_entry(LEVEL_DEBUG, "[%s] called", __func__);
    if (pool_id < MAX_POOLS)
    {
        computeTarget(pool_target[pool_id], difficulty);
        ipc_send_difficulty(&ob2_config, difficulty);
    }
}

void dcr_process_nonces(void)
{
    if (nonce_list.size > 0)
    {
        struct list_entry *entry;
        uint32_t submitted = 0;
        struct cgpu_info* cgpu = devices[0];

        pthread_mutex_lock(&mutex);
        TAILQ_FOREACH(entry, &nonce_list.head, next_entry)
        {
            tlv_ipc_cgminer_dcr_nonce_t *info = entry->data;

            if (info)
            {
                job_t *job = find_job_by_dcr_id(info->job_id);

                if (job)
                {
                    int rc = dcrValidNonce(job->work, info->nonce, info->m5);
                    if (rc == 0)
                        ++bad_nonces;
                    else if (rc == 1)
                        ++good_nonces;
                    else if (rc == 2)
                    {
                        ++pool_nonces;
                        log_entry(LEVEL_DEBUG, "[%s] submitting nonce: %s, %08x, %s, %08x",
                                  __func__,
                                  job->work->job_id,
                                  info->m5,
                                  job->work->ntime,
                                  htonl(info->nonce));
                        submit_nonce(cgpu->thr[0], job->work, info->nonce, htonl(info->m5));
                        ++submitted;
                        log_entry(LEVEL_DEBUG, "[%s] pool nonces: %d, good nonces: %d, bad nonces: %d", __func__, pool_nonces, good_nonces, bad_nonces);
                        log_entry(LEVEL_DEBUG, "[%s] accepted: %d, rejected: %d", __func__, cgpu->accepted, cgpu->rejected);
                    }
                }
            }
            del_entry(&nonce_list, entry);
        }
        pthread_mutex_unlock(&mutex);
    }

    update_pool_stats();
}

/* CGMiner driver API */
static void dcr_detect(
    bool hotplug
)
{
    int i;

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    /* Wait to make sure that we have our info on the HB config */
    while (1)
    {
        if (ob2_config.initialized)
            break;
        sleep(1);
    }

    for (i = 0; i < MAX_POOLS; ++i)
    {
        memset(&pool_stats[i], 0, sizeof(pool_stats_t));
        pool_stats[i].pool_index = DEFAULT_POOL_INDEX;
    }

    /*
     * Check to see if there are any DCR hashing boards in the system.
     * If not we'll exit...otherwise we'll setup so that cgminer can
     * continue init.
     */
    if (ob2_config.hbcount == 0)
    {
        log_entry(LEVEL_ERROR, "[%s] no hashing boards found...exiting", __func__);
        cgminer_shutdown();
    }

    for (i = 0; i < ob2_config.hbcount; ++i)
    {
        if (ob2_config.hbinfo[i].type == HB_DCR)
        {
            log_entry(LEVEL_INFO, "[%s] DCR board %d found", __func__, dcr_boards);
            ++dcr_boards;
        }
    }

    if (dcr_boards == 0)
    {
        log_entry(LEVEL_ERROR, "[%s] no DCR hashing boards found...exiting", __func__);
        cgminer_shutdown();
    }

    cgpu = cgcalloc(sizeof(*cgpu), 1);
    cgpu->drv = &dcr_drv;
    cgpu->name = "Obelisk.Chain";
    cgpu->threads = 1;
    add_cgpu(cgpu);
    cgpu->device_id = 0;

    init_list(&nonce_list);
    init_list(&job_list);
}

static struct api_data *dcr_api_stats(
    struct cgpu_info* cgpu
)
{
    log_entry(LEVEL_DEBUG, "[%s] called", __func__);
    return NULL;
}

static void dcr_identify(
    __maybe_unused struct cgpu_info* cgpu
)
{
    log_entry(LEVEL_DEBUG, "[%s] called", __func__);
}

static bool dcr_thread_prepare(
    struct thr_info* thr
)
{
    log_entry(LEVEL_DEBUG, "[%s] called", __func__);
    return true;
}

/* dcr_scanwork is called repeatedly by cgminer to perform scanning work. */
static int64_t dcr_scanwork(
    __maybe_unused struct thr_info* thr
)
{
    bool work_pending = false;
    job_t *current_job = NULL;
    struct work *work;

    /* Get work from the queue until we find non-stale work */
    while (true)
    {
        work = get_work(thr, thr->id);
        if (work)
        {
            /* Discard stale work */
            if (work->id < work->pool->stale_share_id)
            {
                log_entry(LEVEL_INFO, "[%s] discarding stale work: %s", __func__, work->job_id);
                free_work(work);
                continue;
            }
            break;
        }
    }

    if (work)
    {
        struct list_entry *entry;
        job_t *check_job = find_job_by_pool_id(work->job_id);

        if (check_job == NULL)
        {
            job_t job;

            memset(&job, 0, sizeof(job_t));
            job.work = work;
            memcpy(job.v, job.work->midstate, MIDSTATE_SIZE);
            memcpy(job.v + 8, BLAKE256_CONSTS, MIDSTATE_CONSTANT_SIZE);
            memcpy(job.m, job.work->header_tail, DECRED_HEADER_TAIL_SIZE);
            job.v[12] = v12;
            job.v[13] = v13;
            job.job_id = current_job_id;
            ++current_job_id;
            if (current_job_id == 0)
                current_job_id = 1;
            job.state = POOL_JOB_PENDING;
            entry = add_entry(&job_list, &job, sizeof(job_t));
            if (entry == NULL)
                log_entry(LEVEL_ERROR, "[%s] add_entry failed", __func__);
            else
            {
                current_job = entry->data;
                work_pending = true;
                log_entry(LEVEL_DEBUG, "[%s] added job %d:", __func__, job.job_id);
            }
            if (job_list.size > MAX_JOBS)
            {
                entry = TAILQ_FIRST(&job_list.head);
                if (entry)
                {
                    job_t *oldest = entry->data;
                    log_entry(LEVEL_DEBUG, "[%s] removing oldest job: %d:", __func__, oldest->job_id);
                    free_work(oldest->work);
                    del_entry(&job_list, entry);
                }
            }
            log_entry(LEVEL_DEBUG, "[%s] job list size: %d:", __func__, job_list.size);
        }
        else
        {
            log_entry(LEVEL_DEBUG, "[%s] job already running for %s - discarding work", __func__, work->job_id);
            free_work(work);
        }
    }

    pthread_mutex_lock(&mutex);
    if (work_pending && current_job)
    {
        int rc;

        {
            int i;
            log_entry(LEVEL_DEBUG, "[%s] id: %d", __func__, current_job->job_id);
            for (i = 0; i < DCR_NUM_VREGS; ++i)
                log_entry(LEVEL_DEBUG, "[%s] V%02d: %08x", __func__, i, current_job->v[i]);
            for (i = 0; i < DCR_NUM_MREGS; ++i)
                log_entry(LEVEL_DEBUG, "[%s] M%02d: %08x", __func__, i, current_job->m[i]);
        }
        log_entry(LEVEL_DEBUG, "[%s] sending job id: %s, ntime: %s", __func__, current_job->work->job_id, current_job->work->ntime);
        /* Submit job for processing */
        rc = ipc_send_dcr_work(&ob2_config, current_job);
        if (rc == 0)
            current_job->state = POOL_JOB_ACTIVE;
    }
    pthread_mutex_unlock(&mutex);

    return 0;
}

static bool dcr_queue_full(
    struct cgpu_info* cgpu
)
{
    //log_entry(LEVEL_DEBUG, "[%s] called", __func__);
    return true;
}

static void dcr_flush_work(
    struct cgpu_info* cgpu
)
{
    log_entry(LEVEL_DEBUG, "[%s] called", __func__);
}

static void dcr_shutdown(
    struct thr_info* thr
)
{
    log_entry(LEVEL_DEBUG, "[%s] called", __func__);
}

struct device_drv dcr_drv = {
    .drv_id = DRIVER_dcr,
    .dname = "Obelisk OB2 DCR",
    .name = "Obelisk OB2",
    .drv_detect = dcr_detect,
    .get_api_stats = dcr_api_stats,
    .identify_device = dcr_identify,
    .thread_prepare = dcr_thread_prepare,
    .hash_work = hash_queued_work,
    .scanwork = dcr_scanwork,
    .queue_full = dcr_queue_full,
    .flush_work = dcr_flush_work,
    .thread_shutdown = dcr_shutdown
};
