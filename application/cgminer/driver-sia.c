#include <string.h>
#include <stdio.h>
#include <unistd.h>
#define __USE_GNU
#include <pthread.h>

#include "common.h"
#include "miner.h"
#include "logger.h"
#include "list.h"
#include "driver-sia.h"
#include "ipc.h"

#define SIA_STEP_VAL (0x00000000000003F1)

static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

static struct list nonce_list;

static bool clean_active = false;

static uint32_t step_nonces = 0;
static uint32_t pool_nonces = 0;
static uint32_t good_nonces = 0;
static uint32_t bad_nonces = 0;

static struct cgpu_info *cgpu;

static pool_info_t pool_stats[MAX_POOLS];

static uint8_t chip_target[MIDSTATE_SIZE] =
{
    0x00, 0x00, 0x00, 0x00, 0xff, 0xff, 0xff, 0xff, /* FIXME - 5 leading 0's, not 4 */
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff
};

static uint8_t base_pool_target[MIDSTATE_SIZE] =
{
    0x00, 0x00, 0x00, 0x00, 0xff, 0xff, 0xff, 0xff,
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff
};

static uint8_t pool_target[MAX_POOLS][MIDSTATE_SIZE];

static const uint64_t blake2b_IV[8] =
{
  0x6a09e667f3bcc908ULL, 0xbb67ae8584caa73bULL,
  0x3c6ef372fe94f82bULL, 0xa54ff53a5f1d36f1ULL,
  0x510e527fade682d1ULL, 0x9b05688c2b3e6c1fULL,
  0x1f83d9abfb41bd6bULL, 0x5be0cd19137e2179ULL
};

static const uint8_t blake2b_sigma[12][16] =
{
  {  0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15 } ,
  { 14, 10,  4,  8,  9, 15, 13,  6,  1, 12,  0,  2, 11,  7,  5,  3 } ,
  { 11,  8, 12,  0,  5,  2, 15, 13, 10, 14,  3,  6,  7,  1,  9,  4 } ,
  {  7,  9,  3,  1, 13, 12, 11, 14,  2,  6,  5, 10,  4,  0, 15,  8 } ,
  {  9,  0,  5,  7,  2,  4, 10, 15, 14,  1, 11, 12,  6,  8,  3, 13 } ,
  {  2, 12,  6, 10,  0, 11,  8,  3,  4, 13,  7,  5, 15, 14,  1,  9 } ,
  { 12,  5,  1, 15, 14, 13,  4, 10,  0,  7,  6,  3,  9,  2,  8, 11 } ,
  { 13, 11,  7, 14, 12,  1,  3,  9,  5,  0, 15,  4,  8,  6,  2, 10 } ,
  {  6, 15, 14,  9, 11,  3,  0,  8, 12,  2, 13,  7,  1,  4, 10,  5 } ,
  { 10,  2,  8,  4,  7,  6,  1,  5, 15, 11,  9, 14,  3, 12, 13 , 0 } ,
  {  0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15 } ,
  { 14, 10,  4,  8,  9, 15, 13,  6,  1, 12,  0,  2, 11,  7,  5,  3 }
};

static uint8_t sia_boards;
static struct list job_list;
static uint8_t current_job_id = 1;

static job_t *find_job_by_id(
    uint8_t job_id
)
{
    struct list_entry *entry;

    TAILQ_FOREACH(entry, &job_list.head, next_entry)
    {
        job_t *job = entry->data;

        if (job && (job->job_id == job_id))
            return job;
    }
    return NULL;
}

static job_t *find_job_by_pool_id(
    char *job_id
)
{
    struct list_entry *entry;

    TAILQ_FOREACH(entry, &job_list.head, next_entry)
    {
        job_t *job = entry->data;

        if (job && (strcmp(job->work->job_id, job_id) == 0))
            return job;
    }
    return NULL;
}

/* computeTarget coverts a difficulty into a target. */
static void computeTarget(uint8_t *target, double difficulty)
{
    uint64_t baseDifficulty = 0xffff000000000000; /* Differs between SIA and DCR */
    uint64_t adjustedDifficulty = baseDifficulty/(uint64_t)difficulty;
    int i;

    memcpy(target, base_pool_target, MIDSTATE_SIZE);
    for(i = 0; i < 8; i++)
    {
        target[11-i] = adjustedDifficulty % 256;
        adjustedDifficulty /= 256;
    }
}

/*
 * checksumMeetsTarget determines whether the checksum meets the
 * provided target.
 */
static bool checksumMeetsTarget(uint8_t *checksum, uint8_t *target)
{
    for(int i = 0; i < MIDSTATE_SIZE; i++)
    {
        if(checksum[i] > target[i])
            return false;
        if(checksum[i] < target[i])
            return true;
    }
    return true;
}

static void blake2b_set_lastnode(
    blake2b_state *S
)
{
    S->f[1] = (uint64_t)-1;
}

/* Some helper functions, not necessarily useful */
static int blake2b_is_lastblock(
    const blake2b_state *S
)
{
    return S->f[0] != 0;
}

static void blake2b_set_lastblock(
    blake2b_state *S
)
{
    if (S->last_node)
        blake2b_set_lastnode(S);

    S->f[0] = (uint64_t)-1;
}

static void blake2b_increment_counter(
    blake2b_state *S,
    const uint64_t inc
)
{
    S->t[0] += inc;
    S->t[1] += (S->t[0] < inc);
}

static void blake2b_init0(
    blake2b_state *S
)
{
    size_t i;

    memset(S, 0, sizeof(blake2b_state));
    for (i = 0; i < 8; ++i)
        S->h[i] = blake2b_IV[i];
}

/* init xors IV with input parameter block */
static int blake2b_init_param(
    blake2b_state *S,
    const blake2b_param *P
)
{
    const uint8_t *p = (const uint8_t *)P;
    size_t i;

    blake2b_init0(S);

    /* IV XOR ParamBlock */
    for (i = 0; i < 8; ++i)
        S->h[i] ^= load64(p + sizeof(S->h[i]) * i);

    S->outlen = P->digest_length;
    return 0;
}

static int blake2b_init(
    blake2b_state *S,
    size_t outlen
)
{
    blake2b_param P[1];

    if ((outlen == 0) || (outlen > BLAKE2B_OUTBYTES))
        return -1;

    P->digest_length = (uint8_t)outlen;
    P->key_length    = 0;
    P->fanout        = 1;
    P->depth         = 1;
    memset(&P->leaf_length, 0, sizeof(P->leaf_length));
    memset(&P->node_offset, 0, sizeof(P->node_offset));
    memset(&P->xof_length,  0, sizeof(P->xof_length));
    P->node_depth    = 0;
    P->inner_length  = 0;
    memset(P->reserved, 0, sizeof(P->reserved));
    memset(P->salt,     0, sizeof(P->salt));
    memset(P->personal, 0, sizeof(P->personal));

    return blake2b_init_param(S, P);
}

static void blake2b_compress(
    blake2b_state *S,
    const uint8_t *block
)
{
    uint64_t m[16];
    uint64_t v[16];
    size_t i;

    for (i = 0; i < 16; ++i)
        m[i] = load64(block + i * sizeof(m[i]));

    for (i = 0; i < 8; ++i)
        v[i] = S->h[i];

    v[ 8] = blake2b_IV[0];
    v[ 9] = blake2b_IV[1];
    v[10] = blake2b_IV[2];
    v[11] = blake2b_IV[3];
    v[12] = blake2b_IV[4] ^ S->t[0];
    v[13] = blake2b_IV[5] ^ S->t[1];
    v[14] = blake2b_IV[6] ^ S->f[0];
    v[15] = blake2b_IV[7] ^ S->f[1];

    ROUND(0);
    ROUND(1);
    ROUND(2);
    ROUND(3);
    ROUND(4);
    ROUND(5);
    ROUND(6);
    ROUND(7);
    ROUND(8);
    ROUND(9);
    ROUND(10);
    ROUND(11);

    for (i = 0; i < 8; ++i)
        S->h[i] = S->h[i] ^ v[i] ^ v[i + 8];
}

static int blake2b_update(
    blake2b_state *S,
    const void *pin,
    size_t inlen
)
{
    const unsigned char *in = (const unsigned char *)pin;

    if (inlen > 0)
    {
        size_t left = S->buflen;
        size_t fill = BLAKE2B_BLOCKBYTES - left;
        if (inlen > fill)
        {
            S->buflen = 0;
            memcpy(S->buf + left, in, fill);
            blake2b_increment_counter(S, BLAKE2B_BLOCKBYTES);
            blake2b_compress(S, S->buf);
            in += fill; inlen -= fill;
            while(inlen > BLAKE2B_BLOCKBYTES)
            {
                blake2b_increment_counter(S, BLAKE2B_BLOCKBYTES);
                blake2b_compress(S, in);
                in += BLAKE2B_BLOCKBYTES;
                inlen -= BLAKE2B_BLOCKBYTES;
            }
        }
        memcpy(S->buf + S->buflen, in, inlen);
        S->buflen += inlen;
    }
    return 0;
}

static int blake2b_init_key(
    blake2b_state *S,
    size_t outlen,
    const void *key,
    size_t keylen
)
{
    blake2b_param P[1];
    uint8_t block[BLAKE2B_BLOCKBYTES];

    if ((outlen == 0 ) ||
        (outlen > BLAKE2B_OUTBYTES) ||
        (key == NULL) ||
        (keylen == 0) ||
        (keylen > BLAKE2B_KEYBYTES))
        return -1;

    P->digest_length = (uint8_t)outlen;
    P->key_length    = (uint8_t)keylen;
    P->fanout        = 1;
    P->depth         = 1;
    memset(&P->leaf_length, 0, sizeof(P->leaf_length));
    memset(&P->node_offset, 0, sizeof(P->node_offset));
    memset(&P->xof_length,  0, sizeof(P->xof_length));
    P->node_depth    = 0;
    P->inner_length  = 0;
    memset(P->reserved, 0, sizeof(P->reserved));
    memset(P->salt,     0, sizeof(P->salt));
    memset(P->personal, 0, sizeof(P->personal));

    if (blake2b_init_param(S, P) < 0)
        return -1;
  
    memset(block, 0, BLAKE2B_BLOCKBYTES);
    memcpy(block, key, keylen);
    blake2b_update(S, block, BLAKE2B_BLOCKBYTES);
    secure_zero_memory(block, BLAKE2B_BLOCKBYTES); /* Burn the key from stack */

    return 0;
}

static int blake2b_final(
    blake2b_state *S,
    void *out,
    size_t outlen
)
{
    uint8_t buffer[BLAKE2B_OUTBYTES] = {0};
    size_t i;

    if ((out == NULL) || (outlen < S->outlen))
        return -1;

    if (blake2b_is_lastblock(S))
        return -1;

    blake2b_increment_counter(S, S->buflen);
    blake2b_set_lastblock(S);
    memset(S->buf + S->buflen, 0, BLAKE2B_BLOCKBYTES - S->buflen); /* Padding */
    blake2b_compress(S, S->buf);

    for (i = 0; i < 8; ++i)
        store64(buffer + sizeof(S->h[i]) * i, S->h[i]);

    memcpy(out, buffer, S->outlen);
    secure_zero_memory(buffer, sizeof(buffer));
    return 0;
}

static int blake2b(
    void *out,
    size_t outlen,
    const void *in,
    size_t inlen,
    const void *key,
    size_t keylen
)
{
    blake2b_state S[1];

    if (((in == NULL) && (inlen > 0)) ||
        (out == NULL) ||
        ((key == NULL) && (keylen > 0)) ||
        (keylen > BLAKE2B_KEYBYTES) ||
        (outlen == 0) ||
        (outlen > BLAKE2B_OUTBYTES))
        return -1;

    if (keylen > 0)
    {
        if (blake2b_init_key(S, outlen, key, keylen) < 0)
            return -1;
    }
    else
    {
        if (blake2b_init(S, outlen) < 0)
            return -1;
    }

    blake2b_update(S, (const uint8_t *)in, inlen);
    blake2b_final(S, out, outlen);
    return 0;
}

static void siaCalculateStratumHeader(
    uint8_t* header,
    SiaStratumInput input
)
{
	uint8_t arbTx[256];
	uint16_t offset = 0;
	uint8_t checksum[32];
	int i = 0;

	arbTx[0] = 0;
	offset++;

	memcpy(arbTx + offset, input.Coinbase1, input.Coinbase1Size);
	offset += input.Coinbase1Size;

	memcpy(arbTx + offset, input.ExtraNonce1, input.ExtraNonce1Size);
	offset += input.ExtraNonce1Size;

	memcpy(arbTx + offset, input.ExtraNonce2, input.ExtraNonce2Size);
	offset += input.ExtraNonce2Size;

	memcpy(arbTx + offset, input.Coinbase2, input.Coinbase2Size);
	offset += input.Coinbase2Size;

	/* Get the base merkle root by hashing the arbTx. */
	blake2b(checksum, 32, arbTx, offset, NULL, 0);

	/* Use the arbtx and markleBranch to build the root. */
	for (i = 0; i < input.MerkleBranchesLen; i++)
    {
		uint8_t merkleGroup[65];

		/* Build the 65 byte merkle group - [1, merkleBranches[i], checksum] */
		merkleGroup[0] = 1;
		memcpy(merkleGroup+1, input.MerkleBranches[i], 32);
		memcpy(merkleGroup+33, checksum, 32);
		blake2b(checksum, 32, merkleGroup, 65, NULL, 0);
	}

	/* Create the final header. */
	memcpy(header, input.PrevHash, 32);
	memcpy(header+32, input.Nonce, 8);
	memcpy(header+40, input.Ntime, 8);
	memcpy(header+48, checksum, 32);
}

/*
 * siaHeaderMeetsProvidedTarget will check that the hash of the header
 * meets the provided target, generally this will be the target set by
 * the pool.
 */
bool siaHeaderMeetsProvidedTarget(
    uint8_t *header
)
{
	uint8_t checksum[32];

	blake2b(checksum, 32, header, 80, NULL, 0);
	return checksumMeetsTarget((uint8_t *)checksum, chip_target);
}
#if 0
static void dump(
    void *data,
    int len
)
{
    int i, j;
    int current = 0;
    uint8_t *buf = (uint8_t *)data;
    char log_info[256];

    memset(log_info, 0, sizeof(log_info));
    for (i = 0; current < len; i += 16)
    {
        char value[5];
        sprintf(log_info, "%04x: ", i);
        for (j = 0; (j < 16) && (current < len); ++j)
        {
            sprintf(value, "%02x ", buf[current++]);
            strcat(log_info, value);
        }
        log_entry(LEVEL_INFO, "[%s] %s", __func__, log_info);
        memset(log_info, 0, sizeof(log_info));
    }
}

static void count_leading_zeros(
    uint8_t *checksum
)
{
    int i;
    static uint8_t min_leading_zeros = 99;
    uint8_t current = 0;

    for (i = 31; i > 26; --i)
    {
        if (checksum[i] == 0)
            current += 8
        else
        {
            int j;
            uint8_t tmp = checksum[i];
            for (j = 0x80; j > 0; j >>= 1)
            {
                if (tmp & j)
                    break;
                else
                    ++current;
            }
        }
    }
    if (current < min_leading_zeros)
    {
        min_leading_zeros = current;
        log_entry(LEVEL_INFO, "[%s] current min leading zeros: %d", __func__, min_leading_zeros);
    }
}
#endif
/*
 * siaHeaderMeetsChipTargetAndPoolDifficulty returns '0' if the provided
 * header meets neither the chip difficulty nor the pool difficulty, '1'
 * if the provided header meets the chip difficulty but not the pool
 * difficulty, and '2' if the provided header meets the pool difficulty.
 */
static int siaHeaderMeetsChipTargetAndPoolDifficulty(
    uint8_t *header,
    uint8_t *target
)
{
	uint8_t checksum[32];

	blake2b(checksum, 32, header, 80, NULL, 0);

	if (checksumMeetsTarget((uint8_t *)checksum, target))
		return 2;

	if (checksumMeetsTarget((uint8_t *)checksum, chip_target))
		return 1;

	return 0;
}

static int siaValidNonce(
    struct work *work,
    nonce_t nonce
)
{
	uint8_t header[SIA_HEADER_SIZE];

#if 0
	/* Nonces should be divisible by the step size */
	if (nonce % SC1_STEP_VAL != 0)
		return 0;
#endif
	memcpy(header, work->midstate, SIA_HEADER_SIZE);
	memcpy(header + SIA_NONCE_OFFSET, &nonce, sizeof(nonce_t));

	/* Check if it meets the pool's stratum difficulty. */
	if (!work->pool) {
		return siaHeaderMeetsProvidedTarget(header);
	}
	return siaHeaderMeetsChipTargetAndPoolDifficulty(header, pool_target[work->pool->pool_no]);
}

static void update_pool_stats(void)
{
    int i;

	for (i = 0; i < total_pools; i++)
    {
        struct pool *pool;
        pool_info_t *lstats = NULL;
        bool send_update;
        pool_status_t status = PS_DISABLED;
        int j;

        send_update = false;
		pool = pools[i];

        /* First find the corresponding pool entry */
        for (j = 0; j < MAX_POOLS; ++j)
        {
            if (pool_stats[j].pool_index == pool->pool_no)
            {
                lstats = &pool_stats[j];
                break;
            }
        }
    
        if (lstats == NULL)
        {
            /* Find the next available slot to use to store stats */
            for (j = 0; j < MAX_POOLS; ++j)
            {
                if (pool_stats[j].pool_index == DEFAULT_POOL_INDEX)
                {
                    lstats = &pool_stats[j];
                    send_update = true;
                    break;
                }
            }
        }
    
        if (lstats)
        {
            lstats->pool_index = pool->pool_no;
            if (lstats->accepted != pool->accepted)
            {
                lstats->accepted = pool->accepted;
                send_update = true;
            }
            if (lstats->rejected != pool->rejected)
            {
                lstats->rejected = pool->rejected;
                send_update = true;
            }
            switch (pool->enabled)
            {
                case POOL_DISABLED: status = PS_DISABLED; break;
                case POOL_ENABLED: status = PS_ENABLED; break;
                case POOL_REJECTING: status = PS_REJECTING; break;
            }
            if (lstats->status != status)
            {
                lstats->status = status;
                send_update = true;
            }
            if (lstats->idle != pool->idle)
            {
                lstats->idle = pool->idle;
                send_update = true;
            }
            if (send_update)
            {
                log_entry(LEVEL_DEBUG, "[%s] updating pool %d stats", __func__, pool->pool_no);
                log_entry(LEVEL_DEBUG, "[%s] \taccepted: %lld", __func__, pool->accepted);
                log_entry(LEVEL_DEBUG, "[%s] \trejected: %lld", __func__, pool->rejected);
                switch (pool->enabled)
                {
                    case POOL_ENABLED:
                        if (pool->idle)
                        {
                            log_entry(LEVEL_DEBUG, "[%s] \tstatus: DEAD", __func__);
                        } else
                        {
                            log_entry(LEVEL_DEBUG, "[%s] \tstatus: ALIVE", __func__);
                        }
                    break;
                    case POOL_DISABLED: log_entry(LEVEL_DEBUG, "[%s] \tstatus: DISABLED", __func__); break;
                    case POOL_REJECTING: log_entry(LEVEL_DEBUG, "[%s] \tstatus: REJECTING", __func__); break;
                }
                ipc_send_stats(&ob2_config, lstats);
            }
        }
	}
}

void sia_job_complete(
    uint8_t job_id
)
{
    struct list_entry *entry;

    log_entry(LEVEL_DEBUG, "[%s] received SIA job complete", __func__);

    pthread_mutex_lock(&mutex);
    TAILQ_FOREACH(entry, &job_list.head, next_entry)
    {
        job_t *job = entry->data;

        if (job && (job->job_id == job_id))
        {
            del_entry(&job_list, entry);
            break;
        }
    }
    pthread_mutex_unlock(&mutex);
}

void sia_job_fail(
    uint8_t job_id
)
{
    struct list_entry *entry;
    static uint8_t last_id = 0;
    static uint8_t retries = 0;

    log_entry(LEVEL_DEBUG, "[%s] received SIA job fail", __func__);

    pthread_mutex_lock(&mutex);
    TAILQ_FOREACH(entry, &job_list.head, next_entry)
    {
        job_t *job = entry->data;

        if (job && (job->job_id == job_id))
        {
            if (last_id == job->job_id)
                ++retries;
            else
            {
                last_id = job->job_id;
                retries = 1;
            }
            if (retries < 3)
                ipc_send_sia_work(&ob2_config, job);
            break;
        }
    }
    pthread_mutex_unlock(&mutex);
}

void sia_queue_nonce(
    tlv_ipc_cgminer_sia_nonce_t *info
)
{
    job_t *job = find_job_by_id(info->job_id);

    if (job)
    {
        pthread_mutex_lock(&mutex);
#if 0
        log_entry(LEVEL_DEBUG, "[%s] received SIA job nonce", __func__);
        log_entry(LEVEL_DEBUG, "[%s] \tjob ID: %d", __func__, info->job_id);
        log_entry(LEVEL_DEBUG, "[%s] \t nonce: %016llx", __func__, info->nonce);
#endif
        /* Only queue nonces with a matching job ID */
        if (job->state == POOL_JOB_ACTIVE)
        {
            if (!add_entry(&nonce_list, info, sizeof(tlv_ipc_cgminer_sia_nonce_t)))
                log_entry(LEVEL_ERROR, "[%s] add_entry failed", __func__);
        }
        pthread_mutex_unlock(&mutex);
    }
}

void sia_gen_stratum_work(
    struct pool *pool,
    struct work *work
)
{
    SiaStratumInput input;
    int i;

    memset(&input, 0, sizeof(SiaStratumInput));
    hex2bin(input.PrevHash, pool->prev_hash, HASH_SIZE * 2);
    hex2bin(input.Ntime, work->ntime, 16);
    
    input.MerkleBranchesLen = pool->merkles;
    for (i = 0; i < input.MerkleBranchesLen; i++)
        memcpy(input.MerkleBranches[i], pool->swork.merkle_bin[i], HASH_SIZE);

    input.Coinbase1Size = pool->coinbase1_len;
    memcpy(input.Coinbase1, pool->coinbase1, input.Coinbase1Size);
    input.Coinbase2Size = pool->coinbase2_len;
    memcpy(input.Coinbase2, pool->coinbase2, input.Coinbase2Size);

    input.ExtraNonce1Size = 4;
    memcpy(input.ExtraNonce1, pool->nonce1bin,  input.ExtraNonce1Size);
    input.ExtraNonce2Size = 4;
    memcpy(input.ExtraNonce2, &(work->nonce2), input.ExtraNonce2Size);

    siaCalculateStratumHeader(work->midstate, input);
}

void sia_set_clean(void)
{
    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    clean_active = true;
}

void sia_set_difficulty(
    int pool_id,
    double difficulty
)
{
    log_entry(LEVEL_DEBUG, "[%s] called", __func__);
    if (pool_id < MAX_POOLS)
    {
        computeTarget(pool_target[pool_id], difficulty);
        ipc_send_difficulty(&ob2_config, difficulty);
    }
}

void sia_process_nonces(void)
{
    if (nonce_list.size > 0)
    {
        struct list_entry *entry;
        uint32_t submitted = 0;
        struct cgpu_info* cgpu = devices[0];

        pthread_mutex_lock(&mutex);
        TAILQ_FOREACH(entry, &nonce_list.head, next_entry)
        {
            tlv_ipc_cgminer_sia_nonce_t *info = entry->data;

            if (info)
            {
                job_t *job = find_job_by_id(info->job_id);

                if (job)
                {
                    int rc = siaValidNonce(job->work, info->nonce);
                    if (rc == 0)
                        ++bad_nonces;
                    else if (rc == 1)
                        ++good_nonces;
                    else if (rc == 2)
                    {
                        ++pool_nonces;

                        if (info->nonce % SIA_STEP_VAL == 0)
                        {
                            ++step_nonces;

                            log_entry(LEVEL_DEBUG, "[%s] submitting nonce: %s, %s, %llx",
                                      __func__,
                                      job->work->job_id,
                                      job->work->ntime,
                                      info->nonce);
                            submit_nonce(cgpu->thr[0], job->work, info->nonce, job->work->nonce2);
                            ++submitted;
                            log_entry(LEVEL_DEBUG, "[%s] step_nonces: %d, pool nonces: %d, good nonces: %d, bad nonces: %d", __func__, step_nonces, pool_nonces, good_nonces, bad_nonces);
                            log_entry(LEVEL_DEBUG, "[%s] accepted: %d, rejected: %d", __func__, cgpu->accepted, cgpu->rejected);
                        }
                    }
                }
            }
            del_entry(&nonce_list, entry);
        }
        pthread_mutex_unlock(&mutex);
    }

    update_pool_stats();
}

/* CGMiner driver API */
static void sia_detect(
    bool hotplug
)
{
    int i;

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    /* Wait to make sure that we have our info on the HB config */
    while (1)
    {
        if (ob2_config.initialized)
            break;
        sleep(1);
    }

    for (i = 0; i < MAX_POOLS; ++i)
    {
        memset(&pool_stats[i], 0, sizeof(pool_stats_t));
        pool_stats[i].pool_index = DEFAULT_POOL_INDEX;
    }

    /*
     * Check to see if there are any DCR hashing boards in the system.
     * If not we'll exit...otherwise we'll setup so that cgminer can
     * continue init.
     */
    if (ob2_config.hbcount == 0)
    {
        log_entry(LEVEL_ERROR, "[%s] no hashing boards found...exiting", __func__);
        cgminer_shutdown();
    }

    for (i = 0; i < ob2_config.hbcount; ++i)
    {
        if (ob2_config.hbinfo[i].type == HB_SIA)
        {
            log_entry(LEVEL_INFO, "[%s] SIA board %d found", __func__, sia_boards);
            ++sia_boards;
        }
    }

    if (sia_boards == 0)
    {
        log_entry(LEVEL_ERROR, "[%s] no SIA hashing boards found...exiting", __func__);
        cgminer_shutdown();
    }

    cgpu = cgcalloc(sizeof(*cgpu), 1);
    cgpu->drv = &dcr_drv;
    cgpu->name = "Obelisk.Chain";
    cgpu->threads = 1;
    add_cgpu(cgpu);
    cgpu->device_id = 0;

    init_list(&nonce_list);
    init_list(&job_list);
}

static struct api_data *sia_api_stats(
    struct cgpu_info* cgpu
)
{
    log_entry(LEVEL_DEBUG, "[%s] called", __func__);
    return NULL;
}

static void sia_identify(
    __maybe_unused struct cgpu_info* cgpu
)
{
    log_entry(LEVEL_DEBUG, "[%s] called", __func__);
}

static bool sia_thread_prepare(
    struct thr_info* thr
)
{
    log_entry(LEVEL_DEBUG, "[%s] called", __func__);
    return true;
}

static int64_t sia_scanwork(
    __maybe_unused struct thr_info* thr
)
{
    bool work_pending = false;
    job_t *current_job = NULL;
    struct work *work;

    /* Get work from the queue until we find non-stale work */
    while (true)
    {
        work = get_work(thr, thr->id);
        if (work)
        {
            /* Discard stale work */
            if (work->id < work->pool->stale_share_id)
            {
                log_entry(LEVEL_INFO, "[%s] discarding stale work: %s", __func__, work->job_id);
                free_work(work);
                continue;
            }
            break;
        }
    }

    if (work)
    {
        struct list_entry *entry;
        job_t *check_job = find_job_by_pool_id(work->job_id);

        if (check_job == NULL)
        {
            job_t job;

            memset(&job, 0, sizeof(job_t));
            job.work = work;
            memcpy(job.m, job.work->midstate, SIA_HEADER_SIZE);
            job.job_id = current_job_id;
            ++current_job_id;
            if (current_job_id == 0)
                current_job_id = 1;
            job.state = POOL_JOB_PENDING;
            entry = add_entry(&job_list, &job, sizeof(job_t));
            if (entry == NULL)
                log_entry(LEVEL_ERROR, "[%s] add_entry failed", __func__);
            else
            {
                current_job = entry->data;
                work_pending = true;
                log_entry(LEVEL_DEBUG, "[%s] added job %d:", __func__, job.job_id);
            }
            if (job_list.size > MAX_JOBS)
            {
                entry = TAILQ_FIRST(&job_list.head);
                if (entry)
                {
                    job_t *oldest = entry->data;
                    log_entry(LEVEL_DEBUG, "[%s] removing oldest job: %d:", __func__, oldest->job_id);
                    free_work(oldest->work);
                    del_entry(&job_list, entry);
                }
            }
            log_entry(LEVEL_DEBUG, "[%s] job list size: %d:", __func__, job_list.size);
        }
        else
        {
            log_entry(LEVEL_DEBUG, "[%s] job already running for %s - discarding work", __func__, work->job_id);
            free_work(work);
        }
    }

    pthread_mutex_lock(&mutex);
    if (work_pending && current_job)
    {
        int rc;

        {
            int i;
            log_entry(LEVEL_DEBUG, "[%s] id: %d", __func__, current_job->job_id);
            for (i = 0; i < SIA_NUM_MREGS; ++i)
                log_entry(LEVEL_DEBUG, "[%s] M%02d: %016llx", __func__, i, current_job->m[i]);
        }
        log_entry(LEVEL_DEBUG, "[%s] sending job id: %s, ntime: %s", __func__, current_job->work->job_id, current_job->work->ntime);
        /* Submit job for processing */
        rc = ipc_send_sia_work(&ob2_config, current_job);
        if (rc == 0)
            current_job->state = POOL_JOB_ACTIVE;
    }
    pthread_mutex_unlock(&mutex);

    return 0;
}

static bool sia_queue_full(
    struct cgpu_info* cgpu
)
{
    //log_entry(LEVEL_DEBUG, "[%s] called", __func__);
    return true;
}

static void sia_flush_work(
    struct cgpu_info* cgpu
)
{
    log_entry(LEVEL_DEBUG, "[%s] called", __func__);
}

static void sia_shutdown(
    struct thr_info* thr
)
{
    log_entry(LEVEL_DEBUG, "[%s] called", __func__);
}

struct device_drv dcr_drv = {
    .drv_id = DRIVER_sia,
    .dname = "Obelisk OB2 DCR",
    .name = "Obelisk OB2",
    .drv_detect = sia_detect,
    .get_api_stats = sia_api_stats,
    .identify_device = sia_identify,
    .thread_prepare = sia_thread_prepare,
    .hash_work = hash_queued_work,
    .scanwork = sia_scanwork,
    .queue_full = sia_queue_full,
    .flush_work = sia_flush_work,
    .thread_shutdown = sia_shutdown
};
