/*
 * Copyright (C) 2014-2018 MapleLeaf Software, Inc
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * ipc.c
 *
 * Dan Jozwiak: djozwiak@mlsw.biz
 * Date Created: 2014-05-20
 *
 * Description: this file contains the code to process connections and
 * messages for IPC communications. 
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#define __USE_GNU
#include <pthread.h>

#include "common.h"
#include "poolminer.h"
#include "ipc_messages.h"
#include "ipc.h"
#include "logger.h"
#include "ports.h"
#include "miner.h"
#include "ob2.h"
#ifdef USE_DCR
#include "driver-dcr.h"
#elif USE_SIA
#include "driver-sia.h"
#endif

#define THREAD_NAME "cgminer-ipc"

#define STATS_UPDATE_PERIOD 30 /* seconds */

static pthread_t ipc_thread_id;

static msg_ipc_tlv_t *build_identity(void)
{
    void *msg = NULL;
    size_t msg_len = sizeof(msg_ipc_tlv_t) + sizeof(tlv_ipc_identity_t);

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    msg = calloc(1, msg_len);
    if (msg)
    {
        msg_ipc_tlv_t *tlv = msg;
        tlv_ipc_identity_t *identity = (tlv_ipc_identity_t *)tlv->value;

        tlv->type = ATTR_TYPE_IPC_IDENTITY;
        tlv->len = sizeof(tlv_ipc_identity_t);
#ifdef USE_DCR
        identity->id = IPC_CGMINER_DCR;
#elif  USE_SIA
        identity->id = IPC_CGMINER_SIA;
#endif
    }

    return msg;
}

static msg_ipc_tlv_t *build_hbinfo_request(void)
{
    void *msg = NULL;
    size_t msg_len = sizeof(msg_ipc_tlv_t);

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    msg = calloc(1, msg_len);
    if (msg)
    {
        msg_ipc_tlv_t *tlv = msg;

        tlv->type = ATTR_TYPE_IPC_HB_INFO;
    }

    return msg;
}

static msg_ipc_tlv_t *build_difficulty(
    double difficulty
)
{
    void *msg = NULL;
    size_t msg_len = sizeof(msg_ipc_tlv_t) + sizeof(tlv_ipc_cgminer_difficulty_t);

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    msg = calloc(1, msg_len);
    if (msg)
    {
        msg_ipc_tlv_t *tlv = msg;
        tlv_ipc_cgminer_difficulty_t *req = (tlv_ipc_cgminer_difficulty_t *)tlv->value;

#ifdef USE_DCR
        tlv->type = ATTR_TYPE_IPC_DCR_POOL_DIFFICULTY;
#elif  USE_SIA
        tlv->type = ATTR_TYPE_IPC_SIA_POOL_DIFFICULTY;
#endif
        tlv->len = sizeof(tlv_ipc_cgminer_difficulty_t);

        req->difficulty = difficulty;
    }
    return msg;
}

static msg_ipc_tlv_t *build_stats(
    pool_info_t *info
)
{
    void *msg = NULL;
    size_t msg_len = sizeof(msg_ipc_tlv_t) + sizeof(tlv_ipc_cgminer_stats_t);

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    msg = calloc(1, msg_len);
    if (msg)
    {
        msg_ipc_tlv_t *tlv = msg;
        tlv_ipc_cgminer_stats_t *req = (tlv_ipc_cgminer_stats_t *)tlv->value;

        tlv->type = ATTR_TYPE_IPC_POOL_STATS;
        tlv->len = sizeof(tlv_ipc_cgminer_stats_t);

        req->pool_index = info->pool_index;
        req->accepted = info->accepted;
        req->rejected = info->rejected;
        req->status = info->status;
        req->idle = info->idle;
    }
    return msg;
}
#ifdef USE_DCR
static msg_ipc_tlv_t *build_dcr_work(
    job_t *job
)
{
    void *msg = NULL;
    size_t msg_len = sizeof(msg_ipc_tlv_t) + sizeof(tlv_ipc_cgminer_dcr_work_req_t);

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    /* Only build this job if the pool pointer is valid */
    if (job->work->pool)
    {
        msg = calloc(1, msg_len);
        if (msg)
        {
            msg_ipc_tlv_t *tlv = msg;
            tlv_ipc_cgminer_dcr_work_req_t *req = (tlv_ipc_cgminer_dcr_work_req_t *)tlv->value;
    
            tlv->type = ATTR_TYPE_IPC_DCR_WORK_REQ;
            tlv->len = sizeof(tlv_ipc_cgminer_dcr_work_req_t);
    
            req->job_id = job->job_id;
            req->clean = job->work->pool->swork.clean;
            req->difficulty = job->work->pool->sdiff;
            memcpy(req->vreg, job->v, sizeof(uint32_t) * DCR_NUM_VREGS);
            memcpy(req->mreg, job->m, sizeof(uint32_t) * DCR_NUM_MREGS);
        }
    }
    return msg;
}
#elif  USE_SIA
static msg_ipc_tlv_t *build_sia_work(
    job_t *job
)
{
    void *msg = NULL;
    size_t msg_len = sizeof(msg_ipc_tlv_t) + sizeof(tlv_ipc_cgminer_sia_work_req_t);

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    /* Only build this job if the pool pointer is valid */
    if (job->work->pool)
    {
        msg = calloc(1, msg_len);
        if (msg)
        {
            msg_ipc_tlv_t *tlv = msg;
            tlv_ipc_cgminer_sia_work_req_t *req = (tlv_ipc_cgminer_sia_work_req_t *)tlv->value;
    
            tlv->type = ATTR_TYPE_IPC_SIA_WORK_REQ;
            tlv->len = sizeof(tlv_ipc_cgminer_sia_work_req_t);
    
            req->job_id = job->job_id;
            req->clean = job->work->pool->swork.clean;
            req->difficulty = job->work->pool->sdiff;
            memcpy(req->mreg, job->m, sizeof(uint64_t) * SIA_NUM_MREGS);
        }
    }
    return msg;
}
#endif /* USE_DCR/USE_SIA */
static int process_ipc_tlv(
    ob2_config_t *config,
    msg_ipc_tlv_t *tlv
)
{
    msg_ipc_tlv_t *msg = NULL;

    switch (tlv->type)
    {
        case ATTR_TYPE_IPC_HB_INFO:
        {
            tlv_ipc_hb_info_t *hbdata = (tlv_ipc_hb_info_t *)tlv->value;
            int i;

            for (i = 0; i < hbdata->count; ++i)
            {
                memcpy(&config->hbinfo[i], &hbdata->hbinfo[i], sizeof(tlv_hb_info_t));
                log_entry(LEVEL_DEBUG, "[%s] received HB info: id: %d, type: %s, version: %s",
                          __func__,
                          config->hbinfo[i].id,
                          hbtype_to_str(config->hbinfo[i].type),
                          config->hbinfo[i].version);
            }
            config->hbcount = hbdata->count;
            config->initialized = true;
        }
        break;

        case ATTR_TYPE_IPC_WORK_FAIL:
        {
            tlv_ipc_cgminer_job_t *info = (tlv_ipc_cgminer_job_t *)tlv->value;
#ifdef USE_DCR
            dcr_job_fail(info->job_id);
#elif  USE_SIA
            sia_job_fail(info->job_id);
#endif /* USE_DCR/USE_SIA */
        }
        break;

        case ATTR_TYPE_IPC_JOB_COMPLETE:
        {
            tlv_ipc_cgminer_job_t *info = (tlv_ipc_cgminer_job_t *)tlv->value;
#ifdef USE_DCR
            dcr_job_complete(info->job_id);
#elif  USE_SIA
            sia_job_complete(info->job_id);
#endif /* USE_DCR/USE_SIA */
        }
        break;

#ifdef USE_DCR
        case ATTR_TYPE_IPC_DCR_NONCE:
        {
            tlv_ipc_cgminer_dcr_nonce_t *nonce = (tlv_ipc_cgminer_dcr_nonce_t *)tlv->value;
            dcr_queue_nonce(nonce);
        }
        break;
#elif  USE_SIA
        case ATTR_TYPE_IPC_SIA_NONCE:
        {
            tlv_ipc_cgminer_sia_nonce_t *nonce = (tlv_ipc_cgminer_sia_nonce_t *)tlv->value;
            sia_queue_nonce(nonce);
        }
        break;
#endif /* USE_DCR/USE_SIA */

        case ATTR_TYPE_IPC_RESTART_CGM:
            clean_and_exit();
        break;

        default:
            log_entry(LEVEL_ERROR, "[%s] received unknown TLV type from server:%d", __func__, tlv->type);
            return -1;
        break;
    }

    if (msg)
    {
        if (config->ipc_fd > 0)
        {
            int bytes = send(config->ipc_fd, msg, sizeof(msg_ipc_tlv_t) + msg->len, MSG_NOSIGNAL);
            if (bytes < 0)
                log_entry(LEVEL_ERROR, "[%s] send failed: %s", __func__, strerror(errno));
            else if (bytes == 0)
                log_entry(LEVEL_ERROR, "[%s] sent 0 bytes: %s", __func__, strerror(errno));
        }
        else
            log_entry(LEVEL_WARNING, "[%s] not connected, message will not be sent", __func__);
        free(msg);
    }

    return sizeof(msg_ipc_tlv_t) + tlv->len;
}

/***********************************************************
 * NAME: process_ipc_message
 * DESCRIPTION: process a message from IPC server
 *
 * IN:  server file descriptor
 * OUT: none
 ***********************************************************/
int process_ipc_message(
    ob2_config_t *config
)
{
    int bytes_received = 0;
    char buf[MAX_RECV_LEN];
   
    memset(buf, 0, MAX_RECV_LEN);
    bytes_received = recv(config->ipc_fd, buf, MAX_RECV_LEN, 0);
    switch (bytes_received)
    {
        case -1:
        {
            log_entry(LEVEL_ERROR, "[%s] recv error: %s", __func__, strerror(errno));
            return -1;
        }
        break;
        
        case 0:
        {
            log_entry(LEVEL_WARNING, "[%s] far end closed connection", __func__);
            return 0;
        }
        break;
        
        default:
        {
            uint8_t *tmp_tlv;

            /* One or more messages received...process */
            msg_ipc_tlv_t *tlv = (msg_ipc_tlv_t *)buf;
            tmp_tlv = (uint8_t *)tlv;
            while (bytes_received > 0)
            {
                process_ipc_tlv(config, tlv);
                bytes_received -= (sizeof(msg_ipc_tlv_t) + tlv->len);
                if (bytes_received > 0)
                {
                    tmp_tlv += (sizeof(msg_ipc_tlv_t) + tlv->len); /* move to the next message */
                    tlv = (msg_ipc_tlv_t *)tmp_tlv;
                }
            }
        }
    } /* End switch (bytes_received) */
    return 1;
}

/***********************************************************
 * NAME: ipc_server_connect
 * DESCRIPTION: connect to the server (station manager)
 *
 * IN:  none
 * OUT: server file descriptor or -1 on error
 ***********************************************************/
static void ipc_server_connect(
    ob2_config_t *config
)
{
    int fd;
    int opt = 1;
    struct sockaddr_in server_socket;
    msg_ipc_tlv_t *msg;

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    /* Get a tcp file descriptor */
    fd = socket(PF_INET, SOCK_STREAM, 0);
    if (fd < 0)
    {
        log_entry(LEVEL_ERROR, "[%s] failed to create a socket", __func__);
        return;
    }

    /* set this so that we can rebind to it after a crash or a manual kill */
    if (setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt)) < 0)
    {
        log_entry(LEVEL_ERROR, "[%s] failed to set socket option: %s", __func__, strerror(errno));
        goto err;
    }

    /* Setup the tcp port information */
    memset(&server_socket, 0, sizeof(server_socket));
    server_socket.sin_family = AF_INET;
    server_socket.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
    server_socket.sin_port = htons(CONTROL_MGR_PORT);

    if (connect(fd, (struct sockaddr *)&server_socket, sizeof(server_socket)) < 0)
    {
        log_entry(LEVEL_ERROR, "[%s] failed to connect: %s", __func__, strerror(errno));
        goto err;
    }

    log_entry(LEVEL_INFO, "[%s] ipc server connection opened", __func__);

    /* Send identity */
    msg = build_identity();
    if (msg)
    {
        int bytes = send(fd, msg, sizeof(msg_ipc_tlv_t) + msg->len, MSG_NOSIGNAL);
        if (bytes < 0)
            log_entry(LEVEL_ERROR, "[%s] send failed: %s", __func__, strerror(errno));
        else if (bytes == 0)
            log_entry(LEVEL_ERROR, "[%s] sent 0 bytes: %s", __func__, strerror(errno));
        free(msg);
    }

    /* Request our HB configuration */
    msg = build_hbinfo_request();
    if (msg)
    {
        int bytes = send(fd, msg, sizeof(msg_ipc_tlv_t) + msg->len, MSG_NOSIGNAL);
        if (bytes < 0)
            log_entry(LEVEL_ERROR, "[%s] send failed: %s", __func__, strerror(errno));
        else if (bytes == 0)
            log_entry(LEVEL_ERROR, "[%s] sent 0 bytes: %s", __func__, strerror(errno));
        free(msg);
    }

    config->ipc_fd = fd;
    log_entry(LEVEL_DEBUG, "[%s] completed", __func__);
    return;
err:
    close(fd);
    config->ipc_fd = -1;
}

/***********************************************************
 * NAME: ipc_server_disconnect
 * DESCRIPTION: disconnect from the server (control manager)
 *
 * IN:  none
 * OUT: server file descriptor or -1 on error
 ***********************************************************/
static void ipc_server_disconnect(
    ob2_config_t *config
)
{
    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    if (config->ipc_fd > 0)
    {
        close(config->ipc_fd);
        config->ipc_fd = -1;
        log_entry(LEVEL_INFO, "[%s] ipc server connection closed", __func__);
    }
}

/***********************************************************
 * NAME: process_ipc_signal
 * DESCRIPTION: 
 *
 * IN:  generated signal number
 * OUT: none
 ***********************************************************/
static void process_ipc_signal(int sig)
{
    switch(sig)
    {
        case SIGTERM:
            log_entry(LEVEL_INFO, "[%s] %s thread exiting...", __func__, THREAD_NAME);
        break;
    }
}

static void *ipc_thread(void *data)
{
    sigset_t mask;
    sigset_t orig_mask;
    struct sigaction act;
    ob2_config_t *config = (ob2_config_t *)data;

    log_entry(LEVEL_INFO, "[%s] IPC thread started", __func__);

    pthread_setname_np(pthread_self(), THREAD_NAME);

    memset (&act, 0, sizeof(act));
    act.sa_handler = process_ipc_signal;
    /* This thread should shut down on SIGTERM. */
    if (sigaction(SIGTERM, &act, NULL))
        log_entry(LEVEL_ERROR, "[%s] sigaction failed: %s", __func__, strerror(errno));

    sigemptyset(&mask);
    sigaddset(&mask, SIGTERM);

    if (sigprocmask(SIG_BLOCK, &mask, &orig_mask) < 0)
        log_entry(LEVEL_ERROR, "[%s] sigprocmask failed: %s", __func__, strerror(errno));

    /* Setup our server connection */
    ipc_server_connect(config);

    while (1)
    {
        fd_set socket_set;
        int status;
        int track_fd;
        struct timeval timeout;

        track_fd = 0;
        FD_ZERO(&socket_set);
        memset(&timeout, 0, sizeof(timeout));

        if (config->ipc_fd > 0)
        {
            FD_SET(config->ipc_fd, &socket_set);
            if (config->ipc_fd > track_fd)
                track_fd = config->ipc_fd;
        }
        else
        {
            timeout.tv_sec = 30;
            timeout.tv_usec = 0;
        }

        /* wait for activity on the file descriptors */
        if (timeout.tv_sec == 0)
            status = select(track_fd + 1, &socket_set, (fd_set*)0, (fd_set*)0, NULL);
        else
            status = select(track_fd + 1, &socket_set, (fd_set*)0, (fd_set*)0, &timeout);
       
        if (status < 0) /* select() error */
        {
            log_entry(LEVEL_DEBUG, "[%s] select error: %s", __func__, strerror(errno));
            continue;
        }
        else if (status == 0) /* select() timeout */
        {
            /*
             * If we're not connected to the station manager then
             * attempt to reconnect.
             */
            if (config->ipc_fd < 0)
                ipc_server_connect(config);
            continue;
        }

        if (FD_ISSET(config->ipc_fd, &socket_set))
        {
            int status = process_ipc_message(config);
            if (status <= 0)
                ipc_server_disconnect(config);
        }
    }

    return NULL; 
} 

static int send_ipc_msg(
    ob2_config_t *config,
    msg_ipc_tlv_t *msg
)
{
    if (config && (config->ipc_fd > 0))
    {
        if (msg)
        {
            int bytes = send(config->ipc_fd, msg, sizeof(msg_ipc_tlv_t) + msg->len, MSG_NOSIGNAL);
            if (bytes < 0)
                log_entry(LEVEL_ERROR, "[%s] send failed: %s", __func__, strerror(errno));
            else if (bytes == 0)
                log_entry(LEVEL_ERROR, "[%s] sent 0 bytes: %s", __func__, strerror(errno));
            free(msg);
            return 0;
        }
        else
            log_entry(LEVEL_ERROR, "[%s] msg is NULL", __func__);
    }
    else
        log_entry(LEVEL_ERROR, "[%s] config is NULL or invalid ipc_fd", __func__);
    return -1;
}
#ifdef USE_DCR
int ipc_send_dcr_work(
    ob2_config_t *config,
    job_t *job
)
{
    msg_ipc_tlv_t *msg;

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);
    msg = build_dcr_work(job);
    return send_ipc_msg(config, msg);
}
#elif  USE_SIA
int ipc_send_sia_work(
    ob2_config_t *config,
    job_t *job
)
{
    msg_ipc_tlv_t *msg;

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);
    msg = build_sia_work(job);
    return send_ipc_msg(config, msg);
}
#endif /* USE_DCR/USE_SIA */

int ipc_send_difficulty(
    ob2_config_t *config,
    double difficulty
)
{
    msg_ipc_tlv_t *msg;

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);
    msg = build_difficulty(difficulty);
    return send_ipc_msg(config, msg);
}

void ipc_send_stats(
    ob2_config_t *config,
    pool_info_t *info
)
{
    msg_ipc_tlv_t *msg;
#ifdef FILTER
    static time_t last_update = 0;
    time_t current = time(NULL);
#endif

#ifdef FILTER
    if (((current - last_update) > STATS_UPDATE_PERIOD) || (last_update == 0))
    {
        log_entry(LEVEL_DEBUG, "[%s] called", __func__);
        msg = build_stats(info);
        send_ipc_msg(config, msg);
        last_update = current;
    }
#else
    msg = build_stats(info);
    send_ipc_msg(config, msg);
#endif
}

void ipc_init(
    ob2_config_t *config
)
{
    int rc;

    rc = pthread_create(&ipc_thread_id, NULL, ipc_thread, config);
    if (rc < 0)
        log_entry(LEVEL_ERROR, "[%s] failed to create thread: %s", __func__, strerror(errno));
}

void ipc_shutdown(
    ob2_config_t *config
)
{
    pthread_kill(ipc_thread_id, SIGTERM);
    ipc_server_disconnect(config);
}
