#ifndef __LOGGING_H__
#define __LOGGING_H__

#include "config.h"
#include <stdbool.h>
#include <stdarg.h>

#ifdef HAVE_SYSLOG_H
#include <syslog.h>
#else
enum {
	LOG_ERR,
	LOG_WARNING,
	LOG_NOTICE,
	LOG_INFO,
	LOG_DEBUG,
};
#endif

/* debug flags */
extern bool opt_debug;
extern bool opt_log_output;
extern bool opt_realquiet;
extern bool want_per_device_stats;

/* global log_level, messages with lower or equal prio are logged */
extern int opt_log_level;

#define IN_FMT_FFL " in %s %s():%d"

#ifdef USE_OB2

/* Remap logging to use OB2 common logger */
#include "logger.h"

#define LOG_ERR			LEVEL_ERROR
#define LOG_WARNING		LEVEL_WARNING
#define LOG_NOTICE		LEVEL_CRITICAL
#define LOG_INFO		LEVEL_INFO
#define LOG_DEBUG		LEVEL_DEBUG

#define LOGBUFSIZ 256

#define applog(prio, fmt, ...)          log_entry(prio, fmt, ##__VA_ARGS__)
#define _applog(prio, fmt, value)       log_entry(prio, fmt)
#define simplelog(prio, fmt, ...)       log_entry(prio, fmt, ##__VA_ARGS__)
#define applogsiz(prio, _SIZ, fmt, ...) log_entry(prio, fmt, ##__VA_ARGS__)
#define forcelog(prio, fmt, ...)        log_entry(prio, fmt, ##__VA_ARGS__)
#define quit(status, fmt, ...)          log_entry(LOG_INFO, fmt, ##__VA_ARGS__)
#define early_quit(status, fmt, ...)    log_entry(LOG_ERR, fmt, ##__VA_ARGS__)
#define quithere(status, fmt, ...)      log_entry(LOG_ERR, fmt, ##__VA_ARGS__)
#define quitfrom(status, _file, _func, _line, fmt, ...)                        \
    log_entry(LOG_ERR, fmt, ##__VA_ARGS__)

#define wlog(fmt, ...)          log_entry(LEVEL_INFO, fmt, ##__VA_ARGS__)
#define wlogprint(fmt, ...)     log_entry(LEVEL_INFO, fmt, ##__VA_ARGS__)

#else

#define LOGBUFSIZ 256

extern void _applog(int prio, const char *str, bool force);
extern void _simplelog(int prio, const char *str, bool force);

#define applog(prio, fmt, ...) do { \
	if (opt_debug || prio != LOG_DEBUG) { \
		if (use_syslog || opt_log_output || prio <= opt_log_level) { \
			char tmp42[LOGBUFSIZ]; \
			snprintf(tmp42, sizeof(tmp42), fmt, ##__VA_ARGS__); \
			_applog(prio, tmp42, false); \
		} \
	} \
} while (0)

#define simplelog(prio, fmt, ...) do { \
	if (opt_debug || prio != LOG_DEBUG) { \
		if (use_syslog || opt_log_output || prio <= opt_log_level) { \
			char tmp42[LOGBUFSIZ]; \
			snprintf(tmp42, sizeof(tmp42), fmt, ##__VA_ARGS__); \
			_simplelog(prio, tmp42, false); \
		} \
	} \
} while (0)

#define applogsiz(prio, _SIZ, fmt, ...) do { \
	if (opt_debug || prio != LOG_DEBUG) { \
		if (use_syslog || opt_log_output || prio <= opt_log_level) { \
			char tmp42[_SIZ]; \
			snprintf(tmp42, sizeof(tmp42), fmt, ##__VA_ARGS__); \
			_applog(prio, tmp42, false); \
		} \
	} \
} while (0)

#define forcelog(prio, fmt, ...) do { \
	if (opt_debug || prio != LOG_DEBUG) { \
		if (use_syslog || opt_log_output || prio <= opt_log_level) { \
			char tmp42[LOGBUFSIZ]; \
			snprintf(tmp42, sizeof(tmp42), fmt, ##__VA_ARGS__); \
			_applog(prio, tmp42, true); \
		} \
	} \
} while (0)

#define quit(status, fmt, ...) do { \
	if (fmt) { \
		char tmp42[LOGBUFSIZ]; \
		snprintf(tmp42, sizeof(tmp42), fmt, ##__VA_ARGS__); \
		_applog(LOG_ERR, tmp42, true); \
	} \
	_quit(status); \
} while (0)

#define early_quit(status, fmt, ...) do { \
	if (fmt) { \
		char tmp42[LOGBUFSIZ]; \
		snprintf(tmp42, sizeof(tmp42), fmt, ##__VA_ARGS__); \
		_applog(LOG_ERR, tmp42, true); \
	} \
	__quit(status, false); \
} while (0)

#define quithere(status, fmt, ...) do { \
	if (fmt) { \
		char tmp42[LOGBUFSIZ]; \
		snprintf(tmp42, sizeof(tmp42), fmt IN_FMT_FFL, \
				##__VA_ARGS__, __FILE__, __func__, __LINE__); \
		_applog(LOG_ERR, tmp42, true); \
	} \
	_quit(status); \
} while (0)

#define quitfrom(status, _file, _func, _line, fmt, ...) do { \
	if (fmt) { \
		char tmp42[LOGBUFSIZ]; \
		snprintf(tmp42, sizeof(tmp42), fmt IN_FMT_FFL, \
				##__VA_ARGS__, _file, _func, _line); \
		_applog(LOG_ERR, tmp42, true); \
	} \
	_quit(status); \
} while (0)

#ifdef HAVE_CURSES

#define wlog(fmt, ...) do { \
	char tmp42[LOGBUFSIZ]; \
	snprintf(tmp42, sizeof(tmp42), fmt, ##__VA_ARGS__); \
	_wlog(tmp42); \
} while (0)

#define wlogprint(fmt, ...) do { \
	char tmp42[LOGBUFSIZ]; \
	snprintf(tmp42, sizeof(tmp42), fmt, ##__VA_ARGS__); \
	_wlogprint(tmp42); \
} while (0)

#endif
#endif /* USE_OB2 */

#endif /* __LOGGING_H__ */
