#ifndef _DCR_H_
#define _DCR_H_

#include <stdint.h>

#include "common.h"
#include "poolminer.h"
#include "ipc_messages.h"
#include "ob2.h"

#define EXTRANONCE_SIZE                           4
#define NONCE_SIZE                                4
#define HASH_SIZE                                32
#define ARB_TX_SIZE                             137
#define MAX_COINBASE_SIZE                       256
#define MIDSTATE_SIZE                            32
#define MIDSTATE_CONSTANT_SIZE                   32
#define DECRED_HEADER_SIZE                      180
#define DECRED_HEADER_TAIL_OFFSET_IN_BLOCK      128
#define DECRED_HEADER_TAIL_NONCE_OFFSET          12
#define DECRED_HEADER_TAIL_EXTRANONCE2_OFFSET    20
#define DECRED_HEADER_TAIL_SIZE                  52
#define NTIME_STR_SIZE                            8
#define NTIME_SIZE                                8
#define NUM_VREGS                                16
#define NUM_MREGS                                13
#define MAX_WORK_QUEUE_SIZE                       1
#define MAX_JOBS                                  6

struct work;
struct pool;

typedef uint32_t nonce_t;

/*
 * DecredStratumInput defines a list of inputs from a stratum server
 * hat can be turned into a Decred header.
 */
typedef struct
{
    uint8_t version[4];
    uint8_t prevHash[32];
    uint8_t merkleRoot[32];
    uint8_t stakeRoot[32];
    uint8_t voteBits[2];
    uint8_t finalState[6];
    uint8_t voters[2];
    uint8_t freshStake[1];
    uint8_t revocations[1];
    uint8_t poolSize[4];
    uint8_t bits[4];
    uint8_t sBits[8];
    uint8_t height[4];
    uint8_t size[4];
    uint8_t timestamp[4];
    uint8_t nonce[4];

    // The next 3 fields make of 32 bytes of "extra data"
    uint8_t extraNonce1[4];
    uint8_t extraNonce2[4];
    uint8_t extraDataPadding[24]; // TODO: VERIFY

    uint8_t stakeVersion[4];
} DecredBlockHeader;

/* Common C implementation of 32-bit bitwise right rotation */
#define ROTR32(x, y)	(((x) >> (y)) | ((x) << (32 - (y))))

/*
 * Implementation of Blake-256's round function G - iterations of which
 * make up the main compression function. Found in section 2.1.2 of the
 * specification.
 */
#define G(a, b, c, d, msg0, msg1, const1, const0)   do { \
		a = a + b + (msg0 ^ const1); \
		d = ROTR32(d ^ a, 16); \
		c = c + d; \
		b = ROTR32(b ^ c, 12); \
		a = a + b + (msg1 ^ const0); \
		d = ROTR32(d ^ a, 8); \
		c = c + d; \
		b = ROTR32(b ^ c, 7); \
	} while (0)

extern ob2_config_t ob2_config;

extern void dcr_gen_stratum_work(struct pool* pool, struct work* work);
extern void dcr_queue_nonce(tlv_ipc_cgminer_dcr_nonce_t *info);
extern void dcr_job_complete(uint8_t job_id);
extern void dcr_job_fail(uint8_t job_id);
extern void dcr_set_difficulty(int pool_id, double difficulty);
extern void dcr_set_clean(void);
extern void dcr_process_nonces(void);

#endif /* _DCR_H_ */
