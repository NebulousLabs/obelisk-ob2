#ifndef _sia_H_
#define _sia_H_

#include <stdint.h>

#include "common.h"
#include "poolminer.h"
#include "ipc_messages.h"
#include "ob2.h"

#define EXTRANONCE_SIZE                           4
#define NONCE_SIZE                                8
#define HASH_SIZE                                32
#define ARB_TX_SIZE                             145
#define MAX_COINBASE_SIZE                       256
#define MIDSTATE_SIZE                            32
#define MIDSTATE_CONSTANT_SIZE                   32
#define SIA_HEADER_SIZE                          80
#define SIA_NONCE_OFFSET                         32
#define NTIME_STR_SIZE                           16
#define NTIME_SIZE                                8
#define NUM_VREGS                                16
#define NUM_MREGS                                13
#define MAX_WORK_QUEUE_SIZE                       1
#define MAX_JOBS                                  6

struct work;
struct pool;

typedef uint64_t nonce_t;

#define BLAKE2_PACKED(x) x __attribute__((packed))

enum blake2s_constant
{
    BLAKE2S_BLOCKBYTES = 64,
    BLAKE2S_OUTBYTES   = 32,
    BLAKE2S_KEYBYTES   = 32,
    BLAKE2S_SALTBYTES  = 8,
    BLAKE2S_PERSONALBYTES = 8
};

enum blake2b_constant
{
    BLAKE2B_BLOCKBYTES = 128,
    BLAKE2B_OUTBYTES   = 64,
    BLAKE2B_KEYBYTES   = 64,
    BLAKE2B_SALTBYTES  = 16,
    BLAKE2B_PERSONALBYTES = 16
};

typedef struct
{
    uint32_t h[8];
    uint32_t t[2];
    uint32_t f[2];
    uint8_t  buf[BLAKE2S_BLOCKBYTES];
    size_t   buflen;
    size_t   outlen;
    uint8_t  last_node;
} blake2s_state;

typedef struct
{
    uint64_t h[8];
    uint64_t t[2];
    uint64_t f[2];
    uint8_t  buf[BLAKE2B_BLOCKBYTES];
    size_t   buflen;
    size_t   outlen;
    uint8_t  last_node;
} blake2b_state;

typedef struct
{
    blake2s_state S[8][1];
    blake2s_state R[1];
    uint8_t       buf[8 * BLAKE2S_BLOCKBYTES];
    size_t        buflen;
    size_t        outlen;
} blake2sp_state;

typedef struct blake2bp_state__
{
    blake2b_state S[4][1];
    blake2b_state R[1];
    uint8_t       buf[4 * BLAKE2B_BLOCKBYTES];
    size_t        buflen;
    size_t        outlen;
} blake2bp_state;


typedef struct __attribute__((packed))
{
    uint8_t  digest_length; /* 1 */
    uint8_t  key_length;    /* 2 */
    uint8_t  fanout;        /* 3 */
    uint8_t  depth;         /* 4 */
    uint32_t leaf_length;   /* 8 */
    uint32_t node_offset;  /* 12 */
    uint16_t xof_length;    /* 14 */
    uint8_t  node_depth;    /* 15 */
    uint8_t  inner_length;  /* 16 */
    /* uint8_t  reserved[0]; */
    uint8_t  salt[BLAKE2S_SALTBYTES]; /* 24 */
    uint8_t  personal[BLAKE2S_PERSONALBYTES];  /* 32 */
} blake2s_param;

typedef struct __attribute__((packed))
{
    uint8_t  digest_length; /* 1 */
    uint8_t  key_length;    /* 2 */
    uint8_t  fanout;        /* 3 */
    uint8_t  depth;         /* 4 */
    uint32_t leaf_length;   /* 8 */
    uint32_t node_offset;   /* 12 */
    uint32_t xof_length;    /* 16 */
    uint8_t  node_depth;    /* 17 */
    uint8_t  inner_length;  /* 18 */
    uint8_t  reserved[14];  /* 32 */
    uint8_t  salt[BLAKE2B_SALTBYTES]; /* 48 */
    uint8_t  personal[BLAKE2B_PERSONALBYTES];  /* 64 */
} blake2b_param;

typedef struct
{
    blake2s_state S[1];
    blake2s_param P[1];
} blake2xs_state;

typedef struct
{
    blake2b_state S[1];
    blake2b_param P[1];
} blake2xb_state;

/* Padded structs result in a compile-time error */
enum
{
    BLAKE2_DUMMY_1 = 1/(sizeof(blake2s_param) == BLAKE2S_OUTBYTES),
    BLAKE2_DUMMY_2 = 1/(sizeof(blake2b_param) == BLAKE2B_OUTBYTES)
};

typedef struct 
{
	/* Block header information. */
	uint8_t PrevHash[32];
	uint8_t Nonce[8];
	uint8_t Ntime[8];

	/* Merkle branches inputs. */
	uint16_t MerkleBranchesLen;
	uint8_t** MerkleBranches[20][32];

	/* Coinbase inputs. */
	uint16_t Coinbase1Size;
	uint16_t Coinbase2Size;
	uint8_t Coinbase1[256];
	uint8_t Coinbase2[256];

	/* Extra nonce inputs. */
	uint16_t ExtraNonce1Size;
	uint16_t ExtraNonce2Size;
	uint8_t ExtraNonce1[8];
	uint8_t ExtraNonce2[8];
} SiaStratumInput;

#define G(r,i,a,b,c,d)                      \
  do {                                      \
    a = a + b + m[blake2b_sigma[r][2*i+0]]; \
    d = rotr64(d ^ a, 32);                  \
    c = c + d;                              \
    b = rotr64(b ^ c, 24);                  \
    a = a + b + m[blake2b_sigma[r][2*i+1]]; \
    d = rotr64(d ^ a, 16);                  \
    c = c + d;                              \
    b = rotr64(b ^ c, 63);                  \
  } while(0)

#define ROUND(r)                    \
  do {                              \
    G(r,0,v[ 0],v[ 4],v[ 8],v[12]); \
    G(r,1,v[ 1],v[ 5],v[ 9],v[13]); \
    G(r,2,v[ 2],v[ 6],v[10],v[14]); \
    G(r,3,v[ 3],v[ 7],v[11],v[15]); \
    G(r,4,v[ 0],v[ 5],v[10],v[15]); \
    G(r,5,v[ 1],v[ 6],v[11],v[12]); \
    G(r,6,v[ 2],v[ 7],v[ 8],v[13]); \
    G(r,7,v[ 3],v[ 4],v[ 9],v[14]); \
  } while(0)

#define NATIVE_LITTLE_ENDIAN

static inline uint64_t load64(
    const void *src
)
{
#if defined(NATIVE_LITTLE_ENDIAN)
    uint64_t w;
    memcpy(&w, src, sizeof w);
    return w;
#else
    const uint8_t *p = ( const uint8_t * )src;
    return ((uint64_t)(p[0] ) <<  0)|
           ((uint64_t)(p[1] ) <<  8)|
           ((uint64_t)(p[2] ) << 16)|
           ((uint64_t)(p[3] ) << 24)|
           ((uint64_t)(p[4] ) << 32)|
           ((uint64_t)(p[5] ) << 40)|
           ((uint64_t)(p[6] ) << 48)|
           ((uint64_t)(p[7] ) << 56);
#endif
}

static inline void store64(
    void *dst,
    uint64_t w
)
{
#if defined(NATIVE_LITTLE_ENDIAN)
    memcpy(dst, &w, sizeof w);
#else
    uint8_t *p = (uint8_t *)dst;
    p[0] = (uint8_t)(w >>  0);
    p[1] = (uint8_t)(w >>  8);
    p[2] = (uint8_t)(w >> 16);
    p[3] = (uint8_t)(w >> 24);
    p[4] = (uint8_t)(w >> 32);
    p[5] = (uint8_t)(w >> 40);
    p[6] = (uint8_t)(w >> 48);
    p[7] = (uint8_t)(w >> 56);
#endif
}

static inline uint32_t rotr32(
    const uint32_t w,
    const unsigned c
)
{
    return (w >> c) | (w << (32 - c));
}

static inline uint64_t rotr64(
    const uint64_t w,
    const unsigned c
)
{
    return (w >> c) | (w << (64 - c));
}

/* prevents compiler optimizing out memset() */
static inline void secure_zero_memory(
    void *v,
    size_t n
)
{
    static void *(*const volatile memset_v)(void *, int, size_t) = &memset;
    memset_v(v, 0, n);
}

extern ob2_config_t ob2_config;

extern void sia_gen_stratum_work(struct pool* pool, struct work* work);
extern void sia_queue_nonce(tlv_ipc_cgminer_sia_nonce_t *info);
extern void sia_job_complete(uint8_t job_id);
extern void sia_job_fail(uint8_t job_id);
extern void sia_set_difficulty(int pool_id, double difficulty);
extern void sia_set_clean(void);
extern void sia_process_nonces(void);

#endif /* _sia_H_ */
