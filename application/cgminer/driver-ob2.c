/*
 * Copyright (C) 2012-2018 MapleLeaf Software, Inc
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * driver-ob2.c
 *
 * Nathan Jozwiak: njozwiak@mlsw.biz
 * Date Created: 2018-10-23
 *
 * Description: 
 */

/*
cgminer command lines for various pools:

./cgminer --url us-east.luxor.tech:3333 --user 79cc3a0572619864e6a45b0aa8c2294db3d27151ff88ed7e066e33ee545841c13d4797297324.ob2 --pass x --api-listen --api-allow W:127.0.0.1 --syslog

./cgminer --url us-east.siamining.com:3333 --user 79cc3a0572619864e6a45b0aa8c2294db3d27151ff88ed7e066e33ee545841c13d4797297324.ob2 --pass x --api-listen --api-allow W:127.0.0.1 --syslog

./cgminer --url us-east.toastpool.com:3333 --user 79cc3a0572619864e6a45b0aa8c2294db3d27151ff88ed7e066e33ee545841c13d4797297324.ob2 --pass x --api-listen --api-allow W:127.0.0.1 --syslog

./cgminer --url sc.f2pool.com:7778 --user 79cc3a0572619864e6a45b0aa8c2294db3d27151ff88ed7e066e33ee545841c13d4797297324.ob2 --pass x --api-listen --api-allow W:127.0.0.1 --syslog

Add more logging:

SC1:
./cgminer --url us-west.luxor.tech:3333 --user 79cc3a0572619864e6a45b0aa8c2294db3d27151ff88ed7e066e33ee545841c13d4797297324.ob2 --pass x --api-listen --api-allow W:127.0.0.1 --log 4 --protocol-dump --syslog

DCR1:
./cgminer --url us-west.luxor.tech:4445 --user DsViAH5o1dUUe1SjjxpNK9m7g8KrqWeAYW5.ob2 --pass x --api-listen --api-allow W:127.0.0.1 --log 4 --protocol-dump --syslog
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "miner.h"
#include "driver-ob2.h"
#include "compat.h"
#include "config.h"
#include "klist.h"
#include "sha2.h"

#include "logger.h"

static void ob2_detect(bool hotplug)
{
    log_entry(LEVEL_INFO, "[%s] called", __func__);
}

static struct api_data *ob2_api_stats(struct cgpu_info* cgpu)
{
    log_entry(LEVEL_INFO, "[%s] called", __func__);
    return NULL;
}

static void ob2_identify(__maybe_unused struct cgpu_info* cgpu)
{
    log_entry(LEVEL_INFO, "[%s] called", __func__);
}

static bool ob2_thread_prepare(struct thr_info* thr)
{
    log_entry(LEVEL_INFO, "[%s] called", __func__);
    return true;
}

// ob2_scanwork is called repeatedly by cgminer to perform scanning work.
static int64_t ob2_scanwork(__maybe_unused struct thr_info* thr)
{
    log_entry(LEVEL_INFO, "[%s] called", __func__);
    return 0;
}

static bool ob2_queue_full(struct cgpu_info* cgpu)
{
    log_entry(LEVEL_INFO, "[%s] called", __func__);
    return true;
}

static void ob2_flush_work(struct cgpu_info* cgpu)
{
    log_entry(LEVEL_INFO, "[%s] called", __func__);
}

static void ob2_shutdown(struct thr_info* thr)
{
    log_entry(LEVEL_INFO, "[%s] called", __func__);
}


struct device_drv ob2_drv = {
    .drv_id = DRIVER_ob2,
    .dname = "Obelisk OB2 SC1/DCR1",
    .name = "Obelisk OB2",
    .drv_detect = ob2_detect,
    .get_api_stats = ob2_api_stats,
    // TODO: Determine if this needs to be implemented. It's commented out in OB1
    //.get_statline_before = ob2_get_statline_before,
    .identify_device = ob2_identify,
    .thread_prepare = ob2_thread_prepare,
    .hash_work = hash_queued_work,
    .scanwork = ob2_scanwork,
    .queue_full = ob2_queue_full,
    .flush_work = ob2_flush_work,
    .thread_shutdown = ob2_shutdown
};
