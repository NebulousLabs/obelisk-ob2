/*
 * Copyright (C) 2018 MapleLeaf Software, Inc
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * ob2.h
 *
 * Nathan Jozwiak: njozwiak@mlsw.biz
 * Date Created: 2018-10-23
 *
 * Description: 
 */

#ifndef __OB2_H__
#define __OB2_H__

#include <unistd.h>
#include <sys/wait.h>

#include "common.h"
#include "logger.h"
#include "hb.h"
#include "ipc_messages.h"

#ifdef  __cplusplus
extern "C" {
#endif

struct work;

typedef struct
{
    int ipc_fd;
    bool initialized;
    uint8_t hbcount;
    tlv_hb_info_t hbinfo[MAX_HASHING_BOARDS];
} ob2_config_t;

typedef enum
{
    POOL_JOB_INACTIVE,
    POOL_JOB_PENDING,
    POOL_JOB_ACTIVE,
    POOL_JOB_COMPLETE,

    POOL_JOB_END
} pool_job_state_t;

/* Matches enum pool_enable in miner.h */
typedef enum
{
    POOL_INFO_DISABLED,
    POOL_INFO_ENABLED,
    POOL_INFO_REJECTING,
} pool_enable_t;

typedef struct
{
    int pool_index;
    int64_t accepted;
    int64_t rejected;
    pool_status_t status;
    bool idle;
} pool_info_t;

#ifdef USE_DCR
typedef struct
{
    uint8_t job_id;
    uint32_t m[DCR_NUM_MREGS];
    uint32_t v[DCR_NUM_VREGS];
    pool_job_state_t state;
    struct work *work;
} job_t;
#elif USE_SIA
typedef struct
{
    uint8_t job_id;
    uint64_t m[SIA_NUM_MREGS];
    pool_job_state_t state;
    struct work *work;
} job_t;
#endif

static inline void cgminer_shutdown(void)
{
    char cmd[512];
    char *argv[4];
#ifdef USE_DCR
    char *miner_type = {"dcr"};
#elif  USE_SIA
    char *miner_type = {"sia"};
#endif
    pid_t pid;


    sprintf(cmd, "/usr/local/ob2/bin/cgminer-shutdown %s", miner_type);
    log_entry(LEVEL_DEBUG, "[%s] cmd: %s", __func__, cmd);
    argv[0] = "/bin/bash";
    argv[1] = cmd;
    argv[2] = NULL;

    pid = fork();
    switch (pid)
    {
        case -1:
            log_entry(LEVEL_ERROR, "[%s] fork of %s failed", __func__, argv[0]);
        break;

        case 0:
            execvp(argv[0], argv);
        break;

        default:
            waitpid(pid, NULL, 0);
            while (1) sleep(60);
        break;
    }
}

#ifdef  __cplusplus
}
#endif

#endif /* __OB2_H__ */
