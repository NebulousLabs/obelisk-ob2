/*
 * Copyright (C) 2018-2019 MapleLeaf Software, Inc
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * hb.c
 *
 * Dan Jozwiak: djozwiak@mlsw.biz
 * Date Created: 2018-10-17
 *
 * Description: 
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <termios.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "common.h"
#include "logger.h"
#include "list.h"
#include "hb.h"

static hb_node_t hb_nodes[MAX_HASHING_BOARDS];
static uint8_t hbcount;
static struct list hb_list;
static char console[11];

static void msg_dump(
    void *data,
    int len
)
{
    int i, j;
    int current = 0;
    uint8_t *buf = (uint8_t *)data;
    char log_info[256];

    memset(log_info, 0, sizeof(log_info));
    for (i = 0; current < len; i += 16)
    {
        char value[5];
        sprintf(log_info, "%04x: ", i);
        for (j = 0; (j < 16) && (current < len); ++j)
        {
            sprintf(value, "%02x ", buf[current++]);
            strcat(log_info, value);
        }
        log_entry(LEVEL_DEBUG, "[%s] %s", __func__, log_info);
        memset(log_info, 0, sizeof(log_info));
    }
}

#if 0
static hb_node_t *find_hb(
    int fd
)
{
    struct list_entry *entry;

    TAILQ_FOREACH(entry, &hb_list.head, next_entry)
    {
        hb_node_t *hb = entry->data;

        if (hb && (hb->fd == fd))
        {
            return hb;
            break;
        }
    }
    return NULL;
}
#endif

/******************************************************************
 * Function: get_active_console
 * Description: Identify the device node being used as the Linux
 * console port
 * 
 * In:  none
 * 
 * Out: pointer to device node string or NULL on error
 * 
 ******************************************************************/
static char *get_active_console(void)
{
    FILE *active;
    char value[6];
    int rc;

    active = fopen(ACTIVE_CONSOLE, "r");
    if (active == NULL)
    {
        log_entry(LEVEL_ERROR, "[%s] unable to determine active console", __func__);
        return NULL;
    }
    rc = fscanf(active, "%s", value);
    if (rc == 0)
    {
        log_entry(LEVEL_WARNING, "[%s] unable to read active console: %s", __func__, strerror(errno));
        fclose(active);
        return NULL;
    }
    fclose(active);
    sprintf(console, "/dev/%s", value);
    return console;
}

/******************************************************************
 * Function: hb_generate_checksum
 * Description:  
 * 
 * IN:  
 * 
 * Out: returns the calculated checksum
 * 
 ******************************************************************/
uint8_t hb_generate_checksum(
    void *data
)
{
    uint8_t i;
    uint8_t csum = 0;
    uint8_t *tmp = data;
    hb_tlv_t *tlv = (hb_tlv_t *)data;
    uint8_t len = sizeof(hb_tlv_t) + tlv->len;

    for (i = 0; i < len; ++i)
        csum += tmp[i];
    return csum; 
}

/******************************************************************
 * Function: recv_sync_response
 * Description: Receives a buffer to receive serial data into. 
 * 
 * IN:  fd - file descriptor to open serial port
 *      msg_bfr - pointer to msg_t type return message buffer
 * 
 * Out: Returns 0 on success, and -1 on failure, or 1 on timeout
 * 
 ******************************************************************/
static int recv_sync_response(
    hb_node_t *node,
    void *msg,
    int msg_len
)
{
    fd_set read_fds;
    int status;
    int track_fds;
    uint8_t bytes_read = 0;
    uint8_t total_bytes = 0;
    uint8_t bytes_remaining = sizeof(hb_tlv_t) + msg_len + CSUM_SIZE;
    uint8_t msg_csum;
    uint8_t csum_check = 0;
    uint8_t buf[MAX_HB_MSG_LEN];
    uint8_t *pos = buf;
    uint8_t count = 0;

    if (msg && (msg_len > 0))
        memset(msg, 0, msg_len);
    memset(buf, 0, MAX_HB_MSG_LEN);

    while (1)
    {
        struct timeval timeout;

        FD_ZERO(&read_fds);
        track_fds = 0;

        if (node->fd > 0)
        {
            FD_SET(node->fd, &read_fds);
            if (node->fd > track_fds)
                track_fds = node->fd;
        }

        timeout.tv_sec = 0;
        timeout.tv_usec = 50000;

        status = select(track_fds + 1, &read_fds, NULL, NULL, &timeout);
        if (status == -1)
        {
            log_entry(LEVEL_ERROR, "[%s-%d] select error: %s", __func__, __LINE__, strerror(errno));
            if (errno == EINTR)
                continue;
            return -1;
        }
        else if (status == 0)
        {
            /*
             * A timeout occured which means we either got no response or we
             * didn't get a full message. Either is a failure.
             */
            log_entry(LEVEL_WARNING, "[%s-%d] timeout occurred", __func__, __LINE__);
            return -1;
        }
        else if (FD_ISSET(node->fd, &read_fds))
        {
            bytes_read = read(node->fd, pos, bytes_remaining);
            if (bytes_read < 0)
            {
                log_entry(LEVEL_ERROR, "[%s-%d] read failed: %s",
                          __func__, __LINE__, strerror(errno));
                return -1;
            }
            else if (bytes_read == 0)
            {
                log_entry(LEVEL_ERROR, "[%s-%d] EOF on read", __func__, __LINE__);
                return -1;
            }
            ++count; /* Keep track of how many reads it takes for a message */
            bytes_remaining -= bytes_read;
            pos += bytes_read;
            total_bytes += bytes_read;
            if (bytes_remaining)
                /*
                 * If we haven't received a full message yet then go back and
                 * wait for more...
                 */
                continue;
            break;
        }
    } /* End while (1) */

    msg_dump(buf, total_bytes);

    /* We have a full message so let's validate the checksum. */
    msg_csum = buf[total_bytes - 1];

    /*
     * We have a full message...all we have left to do is to run a CRC check on
     * the message. If it doesn't match... we have a CRC error meaning some of
     * the data in our message is corrupted. No retries for this case.
     */
    csum_check = hb_generate_checksum(buf);
    if (csum_check != msg_csum)
    {
        log_entry(LEVEL_ERROR, "[%s-%d] CSUM check failed: calculated 0x%x, received 0x%x",
                  __func__, __LINE__, csum_check, msg_csum);
        return 2;
    }

    /* CSUM check passed... data is good */
    log_entry(LEVEL_DEBUG, "[%s]  message successfully received: %d bytes in %d reads", __func__, total_bytes, count);

    /* Check for a Nak */
    if (buf[0] == MSG_TYPE_HB_NAK)
    {
        log_entry(LEVEL_ERROR, "[%s] nak received", __func__);
        return 1;
    }

    if (msg && (msg_len > 0))
    {
        hb_tlv_t *tlv = (hb_tlv_t *)buf;
        memcpy(msg, tlv->value, tlv->len);
    }
    return 0;
}

/******************************************************************
 * Function: set_blocking
 * Description: Sets the blocking mode for a given FD
 * 
 * IN:  fd - file descriptor to configure
 *      should_block - 1 for true, 0 for false
 * 
 * Out: None
 * 
 ******************************************************************/
static void set_blocking(
    int fd,
    int should_block
)
{
    struct termios tty;
    memset (&tty, 0, sizeof tty);
    if (tcgetattr (fd, &tty) != 0)
    {
        log_entry(LEVEL_ERROR, "[%s] tggetattr failed: %s", __func__, strerror(errno));
        return;
    }

    tty.c_cc[VMIN]  = should_block ? 1 : 0;
    tty.c_cc[VTIME] = 5; /* 0.5 seconds read timeout */

    if (tcsetattr (fd, TCSANOW, &tty) != 0)
        log_entry(LEVEL_ERROR, "[%s] tcsetattr failed: %s", __func__, strerror(errno));
}

/******************************************************************
 * Function: set_interface_attribs
 * Description: Sets the interface attributes for a given FD
 * 
 * IN:  fd - file descriptor to configure
 *      speed - speed setting
 *      parity - parity setting
 * 
 * Out: Returns 0 on success, or -1 on failure
 * 
 ******************************************************************/
static int set_interface_attribs(
    int fd,
    int speed,
    int parity
)
{
    struct termios tty;
    memset(&tty, 0, sizeof tty);
    if (tcgetattr(fd, &tty) != 0)
    {
        log_entry(LEVEL_ERROR, "[%s] tcgetattr failed: %s", __func__, strerror(errno));
        return -1;
    }

    cfsetospeed(&tty, speed);
    cfsetispeed(&tty, speed);

    tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8;     /* 8-bit chars */
    tty.c_iflag &= ~IGNBRK;         /* disable break processing */
    tty.c_iflag &= ~ICRNL;          /* disable CR to LF conversion */
    tty.c_lflag = 0;                /* no signaling chars, no echo, */
                                    /* no canonical processing */
    tty.c_oflag = 0;                /* no remapping, no delays */
    tty.c_cc[VMIN]  = 0;            /* read doesn't block */
    tty.c_cc[VTIME] = 5;            /* 0.5 seconds read timeout */

    tty.c_iflag &= ~(IXON | IXOFF | IXANY); /* shut off xon/xoff ctrl */

    tty.c_cflag |= (CLOCAL | CREAD);/* ignore modem controls, */
                                    /* enable reading */
    tty.c_cflag &= ~(PARENB | PARODD); /* shut off parity */
    tty.c_cflag |= parity;
    tty.c_cflag &= ~CSTOPB;
    tty.c_cflag &= ~CRTSCTS;

    if (tcsetattr(fd, TCSANOW, &tty) != 0)
    {
        log_entry(LEVEL_ERROR, "[%s] tcsetattr failed: %s", __func__, strerror(errno));
        return -1;
    }
    if (tcsetattr(fd, TCSAFLUSH, &tty) != 0)
    {
        log_entry(LEVEL_ERROR, "[%s] tcsetattr failed: %s", __func__, strerror(errno));
        return -1;
    }
    return 0;
}

/******************************************************************
 * Function: hb_open_serial
 * Description: Opens the serial port for the specified interface
 * 
 * IN:  device node
 * 
 * Out: file descriptor or -1 on failure
 * 
 ******************************************************************/
int hb_open_serial(
    char *interface
)
{
    int fd = open(interface, O_RDWR | O_NOCTTY | O_SYNC);
    if (fd < 0)
    {
        log_entry(LEVEL_ERROR, "[%s] open failed for %s: %s", __func__, interface, strerror(errno));
        return -1;
    }

    /* set speed to 115,200 bps, 8n1 (no parity) */
    set_interface_attribs(fd, B115200, 0);

    /* set the fd to non blocking */
    set_blocking(fd, 0); 

    return fd;
}

/******************************************************************
 * Function: hb_close_serial
 * Description: Closes the serial port for the specified node
 * 
 * IN:  HB node
 * 
 * Out: none
 * 
 ******************************************************************/
void hb_close_serial(
    hb_node_t *hb
)
{
    if (hb)
    {
        close(hb->fd);
        hb->fd = 0;
    }
}

/******************************************************************
 * Function: discover_hashing_boards
 * Description: this function probes all possible serial ports for
 * hashing boards. If one is found it's type and version are
 * queried and the node is added to the list.
 * 
 * IN:  HB node list
 * 
 * Out: List entries added as nodes are found
 * 
 ******************************************************************/
static void discover_hashing_boards(
    struct list *list
)
{
    int i;
    char *console_device;

    memset(hb_nodes, 0, sizeof(hb_nodes));

    /* Update the Linux console port in case console is active */
    console_device = get_active_console();

    for (i = 0; i < MAX_HASHING_BOARDS; ++i)
    {
        char device[MAX_HB_DEV_NODE_LEN];
        hb_version_t rsp;
        int rc;

        /* Now let's see if we can detect a hashing at the other end */
        sprintf(device, "%s%d", HB_DEV_NODE_BASE, i);

        /* Avoid probing the device node if the Linux console owns it */
        if (console_device && strcmp(console_device, device) == 0)
            continue;

        log_entry(LEVEL_DEBUG, "[%s] probing %s", __func__, device);

        /* Open the serial port... */
        hb_nodes[i].fd = hb_open_serial(device);
        if (hb_nodes[i].fd < 0)
        {
            log_entry(LEVEL_ERROR, "[%s-%d] failed to open %s: %s",
                      __func__, __LINE__, hb_nodes[i].devnode, strerror(errno));
            hb_nodes[i].fd = 0;
            continue;
        }

        /* Ping the HB... */
        rc = hb_request(MSG_TYPE_HB_NOP, &hb_nodes[i]);
        if (rc < 0)
        {
            hb_nodes[i].type = HB_UNKNOWN;
            close(hb_nodes[i].fd);
            hb_nodes[i].fd = 0;
            continue;
        }

        /* Found an HB...save the info */
        strcpy(hb_nodes[i].devnode, device);
        hb_nodes[i].id = i;
        ++hbcount;
        log_entry(LEVEL_DEBUG, "[%s] found HB on %s...querying...", __func__, hb_nodes[i].devnode);

        /* Get the HB type and version... */
        rc = hb_send_sync_message(MSG_TYPE_HB_VERSION, &hb_nodes[i], NULL, 0, &rsp, sizeof(rsp));
        if (rc < 0)
        {
            log_entry(LEVEL_ERROR, "[%s-%d] processing message for %s",
                      __func__, __LINE__, hb_nodes[i].devnode);
            hb_nodes[i].type = HB_UNKNOWN;
            close(hb_nodes[i].fd);
            hb_nodes[i].fd = 0;
            continue;
        }

        switch (rsp.type)
        {
            case HB_DCR_TYPE: hb_nodes[i].type = HB_DCR; break;
            case HB_SIA_TYPE: hb_nodes[i].type = HB_SIA; break;
            default: hb_nodes[i].type = HB_SIA; break;
        }
        sprintf(hb_nodes[i].version, "%c.%c%c", rsp.version[0], rsp.version[1], rsp.version[2]);
        log_entry(LEVEL_INFO, "[%s] found HB type %s on %s, version %s",
                  __func__, hbtype_to_str(hb_nodes[i].type), hb_nodes[i].devnode,
                  hb_nodes[i].version);

        /* Add the HB to the list */
        if (!add_entry(list, &hb_nodes[i], sizeof(hb_node_t)))
            log_entry(LEVEL_ERROR, "[%s] add_entry for %s failed", __func__, hb_nodes[i].devnode);
    }
    
    return;
}

/******************************************************************
 * Function: hb_send_sync_message
 * Description: sends a message to to the hashing board and waits
 * for a response. 
 * 
 * IN:  type - message type
 *      hb   - pointer to hashing board node data
 *      rsp  - response buffer pointer (may be NULL)
 *      rsp_len - size of response buffer pointer (may be 0)
 * 
 * Out: Returns 0 on success, and -1 on failure or timeout
 * 
 ******************************************************************/
int hb_send_sync_message(
    uint8_t type,
    hb_node_t *hb,
    void *req,
    int req_len,
    void *rsp,
    int rsp_len
)
{
    if (hb)
    {
        int rc;
        int bytes;
        int retries = 0;
        uint8_t csum;
        uint8_t buf[MAX_HB_MSG_LEN];
        uint8_t msg_len = sizeof(hb_tlv_t) + req_len + CSUM_SIZE;
        hb_tlv_t msg;
    
        memset(&msg, 0, sizeof(msg));
        memset(buf, 0, MAX_HB_MSG_LEN);
        msg.type = type;
        if (req_len > 0)
        {
            uint8_t *pos = buf;
            msg.len = req_len;
            memcpy(pos, &msg, sizeof(msg));
            pos += sizeof(msg);
            memcpy(pos, req, req_len);
            csum = hb_generate_checksum(buf);
        }
        else
        {
            csum = hb_generate_checksum(&msg);
            memcpy (buf, &msg, sizeof(msg));
        }
        buf[msg_len - 1] = csum;

retry:
        bytes = write(hb->fd, buf, msg_len);
        if (bytes < 0)
            log_entry(LEVEL_ERROR, "[%s] send failed: %s", __func__, strerror(errno));
        else if (bytes == 0)
            log_entry(LEVEL_ERROR, "[%s] sent 0 bytes: %s", __func__, strerror(errno));

        rc = recv_sync_response(hb, rsp, rsp_len);
        if (rc != 0)
        {
            if (retries > 3)
                return -1;
            log_entry(LEVEL_WARNING, "[%s] retry in progress...", __func__);
            ++retries;
            goto retry;
        }
    }
    else
        log_entry(LEVEL_ERROR, "[%s] hb is NULL", __func__);

    return 0;
}

/***********************************************************
 * NAME: hb_find_by_devnode
 * DESCRIPTION: search the hashing board list for a matching
 * device node
 *
 * IN:  HB list pointer
 *      device node
 * OUT: HB node pointer or NULL if no match found
 ***********************************************************/
hb_node_t *hb_find_by_devnode(
    struct list *list,
    char *devnode
)
{
    struct list_entry *entry;

    TAILQ_FOREACH(entry, &list->head, next_entry)
    {
        hb_node_t *hb = entry->data;

        if (hb && (strcmp(devnode, hb->devnode) == 0))
        {
            return hb;
        }
    }
    return NULL;
}

/***********************************************************
 * NAME: hb_request
 * DESCRIPTION: 
 *
 * IN:  HB node pointer
 * OUT: 0 - success, -1 - failure
 ***********************************************************/
int hb_request(
    uint8_t request,
    hb_node_t *hb
)
{
    int rc = hb_send_sync_message(request, hb, NULL, 0, NULL, 0);
    if (rc < 0)
    {
        log_entry(LEVEL_ERROR, "[%s] ping failed for HB %s", __func__, hb->devnode);
        return -1;
    }
    return 0;
}

/***********************************************************
 * NAME: hb_update_version
 * DESCRIPTION: 
 *
 * IN:  HB node pointer
 * OUT: 0 - success, -1 - failure
 ***********************************************************/
int hb_update_version(
    hb_node_t *hb
)
{
    int rc;
    hb_version_t rsp;

    rc = hb_send_sync_message(MSG_TYPE_HB_VERSION, hb, NULL, 0, &rsp, sizeof(rsp));
    if (rc < 0)
    {
        log_entry(LEVEL_ERROR, "[%s] failed to get version info for %s", __func__, hb->devnode);
        return -1;
    }
    hb->type = rsp.type;
    sprintf(hb->version, "%c.%c%c", rsp.version[0], rsp.version[1], rsp.version[2]);
    return 0;
}

/***********************************************************
 * NAME: hb_boot_mode
 * DESCRIPTION: 
 *
 * IN:  HB node pointer
 * OUT: 0 - success, -1 - failure
 ***********************************************************/
int hb_boot_mode(
    hb_node_t *hb
)
{
    int rc;
    hb_boot_mode_t req;

    req.magic = HB_BOOT_MODE_MAGIC;
    rc = hb_send_sync_message(MSG_TYPE_HB_BOOT_MODE, hb, &req, sizeof(req), NULL, 0);
    if (rc < 0)
    {
        log_entry(LEVEL_ERROR, "[%s] failed to get version info for %s", __func__, hb->devnode);
        return -1;
    }
    return 0;
}

/***********************************************************
 * NAME: hb_init
 * DESCRIPTION: initialize the hashing board configuration
 *
 * IN:  flag indicating whether HB threads should be started
 * OUT: HB node list
 ***********************************************************/
struct list *hb_init(void)
{
    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    init_list(&hb_list);
    discover_hashing_boards(&hb_list);

    return &hb_list;
}

/***********************************************************
 * NAME: hb_shutdown
 * DESCRIPTION: shutdown the hashing board configuration
 *
 * IN:  HB node list
 * OUT: none
 ***********************************************************/
void hb_shutdown(
    struct list *list
)
{
    struct list_entry *entry;

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    if (list == NULL)
        return;

    TAILQ_FOREACH(entry, &list->head, next_entry)
    {
        hb_node_t *hb = entry->data;

        if (hb)
        {
            hb_close_serial(hb);
            del_entry(list, entry);
        }
    }
}
