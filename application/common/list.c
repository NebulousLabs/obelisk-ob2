/*
 * Copyright (C) 2014-2018 MapleLeaf Software, Inc
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * list.c
 *
 * Dan Jozwiak: djozwiak@mlsw.biz
 * Date Created: 2014-06-12
 *
 * Description: this file contains the code for generic list-related
 * processing. 
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/queue.h>
#include <netinet/in.h>
#include <errno.h>

#include "common.h"
#include "list.h"
#include "logger.h"

/***********************************************************
 * NAME: add_entry
 * DESCRIPTION: add an entry to the list.
 *
 * IN:  list, entry data
 * OUT: entry
 ***********************************************************/
struct list_entry *add_entry(
    struct list *list,
    void *data,
    int len)
{
    struct list_entry *entry;

    entry = (struct list_entry *)calloc(1, sizeof(struct list_entry));
    if (!entry)
    {
        log_entry(LEVEL_ERROR, "[%s] memory allocation failed", __func__);
        return NULL;
    }
    entry->data = calloc(1, len);
    if (!entry->data)
    {
        log_entry(LEVEL_ERROR, "[%s] memory allocation failed", __func__);
        free(entry);
        return NULL;
    }
    memcpy(entry->data, data, len);
    TAILQ_INSERT_TAIL(&list->head, entry, next_entry);
    list->size++;
    return entry;
}

/***********************************************************
 * NAME: del_entry
 * DESCRIPTION: delete a client entry from the client list
 *
 * IN:  list, entry to delete
 * OUT: none
 ***********************************************************/
void del_entry(
    struct list *list,
    struct list_entry *entry)
{
    void *data = entry->data;
    TAILQ_REMOVE(&list->head, entry, next_entry);
    list->size--;
    free(entry);
    free(data);
}

/***********************************************************
 * NAME: init_list
 * DESCRIPTION: initialize the list
 *
 * IN:  list
 * OUT: none
 ***********************************************************/
void init_list(struct list *list)
{
    memset(list, 0, sizeof(struct list));
    TAILQ_INIT(&list->head);
}
