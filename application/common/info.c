/*
 * Copyright (C) 2012-2018 MapleLeaf Software, Inc
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * info.c
 *
 * Dan Jozwiak: djozwiak@mlsw.biz
 * Date Created: 2012-04-13
 *
 * Description: 
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/sysinfo.h>
#include <net/if.h> 
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <libxml/parser.h>
#include <libxml/tree.h>

#include "common.h"
#include "logger.h"
#include "info.h"

/* station_info tags */
#define SID         "sid"
#define RELEASE     "release"
#define DCRRELEASE  "dcrrelease"
#define SIARELEASE  "sizrelease"

/* system_info tags */
#define UPDATEURL       "updateurl"
#define FANSPEED        "fanspeed"
#define REBOOTPERIOD    "rebootperiod"
#define UPDATETO        "updateto"
#define AUTOFAN         "autofan"
#define AUTOFANTEMP     "autofantemp"

static void change_content_number(
    xmlNodePtr current,
    int value
)
{
    char change[5];

    log_entry(LEVEL_DEBUG, "[%s] called - %d", __func__, value);
    sprintf(change, "%d", value);
    xmlNodeSetContent(current, (xmlChar *)change);
}

static void change_content_string(
    xmlNodePtr current,
    char *value
)
{
    log_entry(LEVEL_DEBUG, "[%s] called - %s", __func__, value);
    xmlNodeSetContent(current, (xmlChar *)value);
}

static void add_content_number(
    xmlNodePtr parent,
    char *tag,
    int value
)
{
    char content[5];

    log_entry(LEVEL_DEBUG, "[%s] called - %d", __func__, value);
    sprintf(content, "%d", value);
    xmlNewChild(parent, NULL, (xmlChar *)tag, (xmlChar *)content);
}

static void add_content_string(
    xmlNodePtr parent,
    char *tag,
    char *value
)
{
    log_entry(LEVEL_DEBUG, "[%s] called - %s", __func__, value);
    xmlNewChild(parent, NULL, (xmlChar *)tag, (xmlChar *)value);
}

/***********************************************************
 * NAME: info_update_xml_default_number
 * DESCRIPTION: this function opens the system info XML
 * file and reads its contents to find the specified tag.
 * It then takes the new default value for the tag and writes
 * that to the XML file. This function is used for XML
 * updates that occur as a result of a software update.
 *
 * IN:  file, tag and value
 * OUT: none
 ***********************************************************/
void info_update_xml_default_number(
    char *info_file,
    char *tag,
    int new_default
)
{
    int fd;
    bool xml_changed = false;
    bool tag_found = false;
    xmlDocPtr doc = NULL;
    xmlNodePtr rootElement;
    xmlNodePtr current;

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    /* init XML library parser */
    xmlInitParser();

    /* weird but needed by client code for libxml */
    LIBXML_TEST_VERSION

    /* Allow formatting of the XML file */
    xmlKeepBlanksDefault(0);

    /* Open and lock the XML file */
    fd = open(info_file, O_RDONLY);
    if (fd < 0)
    {
        log_entry(LEVEL_ERROR, "[%s] unable to open %s", __func__, info_file);
        return;
    }

    /* Load the specified XML file */
    doc = xmlReadFile(info_file, NULL, XML_PARSE_RECOVER); 
    if (!doc)
    {
        log_entry(LEVEL_ERROR, "[%s] xmlReadFile error", __func__);
        close(fd);
        return;
    }

    /* Get the root element */
    rootElement = xmlDocGetRootElement(doc);
    for (current = rootElement->children; current != NULL; current = current->next)
    {
        if (strcmp((char *)current->name, tag) == 0)
        {
            change_content_number(current, new_default);
            tag_found = true;
            xml_changed = true;
            break;
        }
    }
    if (!tag_found)
    {
        /* Add a new tag */
        add_content_number(rootElement, tag, new_default);
        xml_changed = true;
    }
    if (xml_changed)
        xmlSaveFormatFileEnc(info_file, doc, "utf-8", 1);
    xmlFreeDoc(doc);
    close(fd);
}

/***********************************************************
 * NAME: info_update_xml_default_string
 * DESCRIPTION: this function opens the system info XML
 * file and reads its contents to find the specified tag.
 * It then takes the new default value for the tag and writes
 * that to the XML file. This function is used for XML
 * updates that occur as a result of a software update.
 *
 * IN:  file, tag and value
 * OUT: none
 ***********************************************************/
void info_update_xml_default_string(
    char *info_file,
    char *tag,
    char *new_default
)
{
    int fd;
    bool xml_changed = false;
    bool tag_found = false;
    xmlDocPtr doc = NULL;
    xmlNodePtr rootElement;
    xmlNodePtr current;

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    /* init XML library parser */
    xmlInitParser();

    /* weird but needed by client code for libxml */
    LIBXML_TEST_VERSION

    /* Allow formatting of the XML file */
    xmlKeepBlanksDefault(0);

    /* Open and lock the XML file */
    fd = open(info_file, O_RDONLY);
    if (fd < 0)
    {
        log_entry(LEVEL_ERROR, "[%s] unable to open %s", __func__, info_file);
        return;
    }

    /* Load the specified XML file */
    doc = xmlReadFile(info_file, NULL, XML_PARSE_RECOVER); 
    if (!doc)
    {
        log_entry(LEVEL_ERROR, "[%s] xmlReadFile error", __func__);
        close(fd);
        return;
    }

    /* Get the root element */
    rootElement = xmlDocGetRootElement(doc);
    for (current = rootElement->children; current != NULL; current = current->next)
    {
        xmlChar *value;

        value = xmlNodeGetContent(current);
        if (strcmp((char *)current->name, tag) == 0)
        {
            change_content_string(current, new_default);
            tag_found = true;
            xml_changed = true;
        }
        if (value)
            xmlFree(value);
    }
    if (!tag_found)
    {
        /* Add a new tag */
        add_content_string(rootElement, tag, new_default);
        xml_changed = true;
    }
    if (xml_changed)
        xmlSaveFormatFileEnc(info_file, doc, "utf-8", 1);
    xmlFreeDoc(doc);
    close(fd);
}

/***********************************************************
 * NAME: get_ipaddr
 * DESCRIPTION: this function gets the IP address of the
 * specified interface.
 *
 * IN:  interface name
 * OUT: IP address string
 ***********************************************************/
static char *get_ipaddr(
    char *ifname
)
{
    int sock;
    struct ifreq ifr;
    char *ipaddr;
    struct sockaddr_in *addr;

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_IP);
    if (sock < 0)
        return NULL;

    ipaddr = (char *)calloc(1, MAX_STATIONIPADDR_LEN);
    if (ipaddr == NULL)
    {
        log_entry(LEVEL_ERROR, "[%s] insufficient memory", __func__);
        goto err;
    }

    memset(&ifr, 0, sizeof(struct ifreq));
    ifr.ifr_addr.sa_family = AF_INET;
    strncpy(ifr.ifr_name, ifname, IFNAMSIZ-1);

    if (ioctl(sock, SIOCGIFADDR, &ifr) < 0)
    {
        log_entry(LEVEL_ERROR, "[%s] ioctl failed: %s", __func__, strerror(errno));
        free(ipaddr);
        goto err;
    }

    close(sock);

    addr = (struct sockaddr_in *)&ifr.ifr_addr;
    strcpy(ipaddr, inet_ntoa(addr->sin_addr));

    return ipaddr;

err:
    close(sock);
    return NULL;
}

/***********************************************************
 * NAME: info_station
 * DESCRIPTION: this function opens the station info XML
 * file and reads its contents.
 *
 * IN:  station info pointer
 * OUT: none
 ***********************************************************/
void info_station(
    char *info_file,
    station_info_t *info
)
{
    int fd;
    xmlDocPtr doc = NULL;
    xmlNodePtr rootElement;
    xmlNodePtr current;

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    /* init XML library parser */
    xmlInitParser();

    /* weird but needed by client code for libxml */
    LIBXML_TEST_VERSION

    /* Allow formatting of the XML file */
    xmlKeepBlanksDefault(0);

    /* Open and lock the XML file */
    fd = open(info_file, O_RDWR);
    if (fd < 0)
    {
        log_entry(LEVEL_ERROR, "[%s] unable to open %s", __func__, info_file);
        return;
    }

    /* Load the specified XML file */
    doc = xmlReadFile(info_file, NULL, XML_PARSE_RECOVER); 
    if (!doc)
    {
        log_entry(LEVEL_ERROR, "[%s] xmlReadFile error", __func__);
        close(fd);
        return;
    }

    /* Get the root element */
    rootElement = xmlDocGetRootElement(doc);
    for (current = rootElement->children; current != NULL; current = current->next)
    {
        xmlChar *value;

        value = xmlNodeGetContent(current);
        if      (strcmp((char *)current->name, SID) == 0)
        {
            strncpy(info->id, (char *)value, MAX_STATIONID_LEN - 1);
        }
        else if (strcmp((char *)current->name, RELEASE) == 0)
        {
            strncpy(info->release, (char *)value, MAX_STATIONRELEASE_LEN - 1);
        }
        else if (strcmp((char *)current->name, DCRRELEASE) == 0)
        {
            strncpy(info->dcrrelease, (char *)value, MAX_HB_RELEASE - 1);
        }
        else if (strcmp((char *)current->name, SIARELEASE) == 0)
        {
            strncpy(info->siarelease, (char *)value, MAX_HB_RELEASE - 1);
        }
        if (value)
            xmlFree(value);
    }
    xmlFreeDoc(doc);
    close(fd);
}

/***********************************************************
 * NAME: info_system
 * DESCRIPTION: this function opens the system info XML
 * file and reads its contents.
 *
 * IN:  station info pointer
 * OUT: none
 ***********************************************************/
void info_system(
    char *info_file,
    system_info_t *info
)
{
    int fd;
    xmlDocPtr doc = NULL;
    xmlNodePtr rootElement;
    xmlNodePtr current;

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    /* init XML library parser */
    xmlInitParser();

    /* weird but needed by client code for libxml */
    LIBXML_TEST_VERSION

    /* Allow formatting of the XML file */
    xmlKeepBlanksDefault(0);

    /* Open and lock the XML file */
    fd = open(info_file, O_RDWR);
    if (fd < 0)
    {
        log_entry(LEVEL_ERROR, "[%s] unable to open %s", __func__, info_file);
        return;
    }

    /* Load the specified XML file */
    doc = xmlReadFile(info_file, NULL, XML_PARSE_RECOVER); 
    if (!doc)
    {
        log_entry(LEVEL_ERROR, "[%s] xmlReadFile error", __func__);
        close(fd);
        return;
    }

    /* Get the root element */
    rootElement = xmlDocGetRootElement(doc);
    for (current = rootElement->children; current != NULL; current = current->next)
    {
        xmlChar *value;

        value = xmlNodeGetContent(current);
        if      (strcmp((char *)current->name, UPDATEURL) == 0)
        {
            strncpy(info->update_url, (char *)value, MAX_URL - 1);
        }
        else if (strcmp((char *)current->name, FANSPEED) == 0)
        {
            info->fanspeed = atoi((char *)value);
        }
        else if (strcmp((char *)current->name, REBOOTPERIOD) == 0)
        {
            info->rebootperiod = atoi((char *)value);
        }
        else if (strcmp((char *)current->name, UPDATETO) == 0)
        {
            strncpy(info->updateto, (char *)value, MAX_STATIONRELEASE_LEN - 1);
        }
        else if (strcmp((char *)current->name, AUTOFAN) == 0)
        {
            if (strcmp((char *)value, "enabled") == 0)
                info->autofan = true;
            else
                info->autofan = false;
        }
        else if (strcmp((char *)current->name, AUTOFANTEMP) == 0)
        {
            info->autofantemp = atof((char *)value);
        }
        if (value)
            xmlFree(value);
    }
    xmlFreeDoc(doc);
    close(fd);
}

/***********************************************************
 * NAME: info_system_update
 * DESCRIPTION: this function opens the system info XML
 * file and reads its contents to compare with the current
 * values in the station configuration. Differences found
 * in the station configuration will be written to the file
 * and then saved to disk.
 *
 * IN:  station config pointer
 * OUT: none
 ***********************************************************/
void info_system_update(
    char *info_file,
    system_info_t *info
)
{
    int fd;
    bool xml_changed = false;
    xmlDocPtr doc = NULL;
    xmlNodePtr rootElement;
    xmlNodePtr current;

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    /* init XML library parser */
    xmlInitParser();

    /* weird but needed by client code for libxml */
    LIBXML_TEST_VERSION

    /* Allow formatting of the XML file */
    xmlKeepBlanksDefault(0);

    /* Open and lock the XML file */
    fd = open(info_file, O_RDONLY);
    if (fd < 0)
    {
        log_entry(LEVEL_ERROR, "[%s] unable to open %s", __func__, info_file);
        return;
    }

    /* Load the specified XML file */
    doc = xmlReadFile(info_file, NULL, XML_PARSE_RECOVER); 
    if (!doc)
    {
        log_entry(LEVEL_ERROR, "[%s] xmlReadFile error", __func__);
        close(fd);
        return;
    }

    /* Get the root element */
    rootElement = xmlDocGetRootElement(doc);
    for (current = rootElement->children; current != NULL; current = current->next)
    {
        xmlChar *value;

        value = xmlNodeGetContent(current);
        if      (strcmp((char *)current->name, UPDATEURL) == 0)
        {
            if (strcmp(info->update_url, (char *)value) != 0)
            {
                change_content_string(current, info->update_url);
                xml_changed = true;
            }
        }
        else if (strcmp((char *)current->name, FANSPEED) == 0)
        {
            if (info->fanspeed != atoi((char *)value))
            {
                change_content_number(current, (int)info->fanspeed);
                xml_changed = true;
            }
        }
        else if (strcmp((char *)current->name, REBOOTPERIOD) == 0)
        {
            if (info->rebootperiod != atoi((char *)value))
            {
                change_content_number(current, (int)info->rebootperiod);
                xml_changed = true;
            }
        }
        else if (strcmp((char *)current->name, UPDATETO) == 0)
        {
            if (strcmp(info->updateto, (char *)value) != 0)
            {
                change_content_string(current, info->updateto);
                xml_changed = true;
            }
        }
        else if (strcmp((char *)current->name, AUTOFAN) == 0)
        {
            char *infovalue = (info->autofan ? "enabled":"disabled");
            if (strcmp(infovalue, (char *)value) != 0)
            {
                change_content_string(current, infovalue);
                xml_changed = true;
            }
        }
        else if (strcmp((char *)current->name, AUTOFANTEMP) == 0)
        {
            if ((int)info->autofantemp != atoi((char *)value))
            {
                change_content_number(current, (int)info->autofantemp);
                xml_changed = true;
            }
        }
        if (value)
            xmlFree(value);
    }
    if (xml_changed)
        xmlSaveFormatFileEnc(info_file, doc, "utf-8", 1);
    xmlFreeDoc(doc);
    close(fd);
    sync();
}

void info_ipaddr(
    station_info_t *info
)
{
    char interface[MAX_IFNAME_LEN] = {"eth0"};
    char *station_ipaddr;
    char path[256];
    int rc;
    struct stat ifinfo;

    /* Verify that the interface exists. */
    sprintf(path, "/sys/class/net/%s", interface);
    rc = stat(path, &ifinfo);
    if (rc)
    {
        log_entry(LEVEL_WARNING, "[%s] unable to find %s: %s", __func__, path, strerror(errno));
        return;
    }

    station_ipaddr = get_ipaddr(interface);
    if (station_ipaddr == NULL)
    {
        log_entry(LEVEL_WARNING, "[%s] unable to get %s IP address", __func__, interface);
        return;
    }
    strcpy(info->ipaddr, station_ipaddr);
    free(station_ipaddr);
}
