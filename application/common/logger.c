/*
 * Copyright (C) 2014-2018 MapleLeaf Software, Inc
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * logger.c
 *
 * Dan Jozwiak: djozwiak@mlsw.biz
 * Date Created: 2014-06-12
 *
 * Description: This file contains functions that manage a log file for the
 * software component that uses it. Note that this is not a centralized logging
 * mechanism. Each component that uses this code will have its own log file.
 *
 */

#include <stdio.h>
#include <stdint.h>
#include <stdarg.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <sys/stat.h>

#include "logger.h"

#ifdef LOGGER_THREADS
#define __USE_GNU
#include <pthread.h>

static pthread_mutex_t log_mutex = PTHREAD_MUTEX_INITIALIZER;
#endif /* LOGGER_THREADS */

static FILE *logfp;
static char *log_filename;
static uint32_t current_log_level;
static uint32_t current_log_limit;

/***********************************************************
 * NAME: level_to_str
 * DESCRIPTION: utility to convert an enumeration to a string.
 *
 * IN:  debug level
 * OUT: string representation of the debug level
 ***********************************************************/
static char *level_to_str(uint8_t debug_level)
{
    char *level = NULL;

    switch(debug_level)
    {
        case LEVEL_CRITICAL: level = "CRITICAL"; break;
        case LEVEL_ERROR:    level = "ERROR"; break;
        case LEVEL_WARNING:  level = "WARNING"; break;
        case LEVEL_INFO:     level = "INFO"; break;
        case LEVEL_DEBUG:    level = "DEBUG"; break;
        default:             level = "UNKNOWN"; break;
    }
    return level;
}

/***********************************************************
 * NAME: log_init
 * DESCRIPTION: this function opens up the file associated
 * with the specified name and sets the limit (maximum size
 * allowed for the log file) and the log level (lowest level
 * allowed for log messages to be written). If no file is
 * specified or if the open fails then all messages will go
 * to stdout. Note that all data (file descriptor, file name,
 * default limit and level are maintained locally so that
 * applications don't have to manage the data.
 *
 * IN:  log file name, log limit and lowest log level
 * OUT: none
 ***********************************************************/
void log_init(char *log_file, uint32_t limit, uint32_t level)
{
    if (logfp && logfp != stdout)
        fclose(logfp);

    current_log_limit = limit;
    current_log_level = level;

    logfp = stdout;
    if (log_file != NULL)
    {
        FILE *fp = fopen(log_file, "a");
        if (fp)
        {
            log_filename = log_file;
            logfp = fp;
            return;
        }
        printf("[%s] fopen failed for %s: %s\n", __func__, log_file, strerror(errno));
        exit(1);
    }
}

/***********************************************************
 * NAME: log_exit
 * DESCRIPTION: close out the log file descriptor.
 *
 * IN:  none
 * OUT: none
 ***********************************************************/
void log_exit(void)
{
    if (logfp)
    {
        fclose(logfp);
        logfp = NULL;
    }
}

/***********************************************************
 * NAME: log_level
 * DESCRIPTION: function that allows an application to change
 * the log level after log initialization (during program
 * operation).
 *
 * IN:  new log level
 * OUT: none
 ***********************************************************/
uint32_t log_level(uint32_t new_level)
{
    if (new_level == 0)
        return current_log_level;
    else if (new_level <= LEVEL_DEBUG)
        current_log_level = new_level;
    return current_log_level;
}

/***********************************************************
 * NAME: log_limit
 * DESCRIPTION: function that allows an application to change
 * the log limit after log initialization (during program
 * operation).
 *
 * IN:  new log limit
 * OUT: none
 ***********************************************************/
uint32_t log_limit(uint32_t new_limit)
{
    if (new_limit == 0)
        return current_log_limit;
    else
        current_log_limit = new_limit;
    return current_log_limit;
}

/***********************************************************
 * NAME: log_reinit
 * DESCRIPTION: this function reopens up the file associated
 * with the specified name and sets the limit (maximum size
 * allowed for the log file) and the log level (lowest level
 * allowed for log messages to be written). The function is
 * called as a result of the log rotation processing.
 *
 * IN:  none
 * OUT: none
 ***********************************************************/
static void log_reinit(void)
{
    if (log_filename)
    {
        int localerrno;
        time_t ts = time(NULL);
        char error_file[256];
        FILE *fp = fopen(log_filename, "a");
        if (fp)
        {
            logfp = fp;
            return;
        }

        /*
         * We have a problem. We're logging to a file (as specified in the
         * log_init() call) but the file open failed. First, save errno so that
         * we can record it, then open up a file in /tmp to store this info
         * and then set the logfp to NULL.
         */
        localerrno = errno;
        sprintf(error_file, "/tmp/logger-error-%lu", ts);
        fp = fopen(error_file, "w");
        if (fp)
        {
            fprintf(fp, "[%s] error opening %s: %s\n", __func__, log_filename, strerror(localerrno));
            fclose(fp);
        }
        logfp = NULL;
    }
}

/***********************************************************
 * NAME: manage_log_file
 * DESCRIPTION: this function checks the current size of the
 * log file and compares it agains the limit. If the limit
 * has been reached the file is closed, saved to a backup
 * file and a new file opened. Note that only one backup is
 * allowed.
 *
 * IN:  none
 * OUT: none
 ***********************************************************/
static void manage_log_file(void)
{
    struct stat info;
    int status;
    char backup[100];

    if (logfp == stdout)
        return;

    status = stat(log_filename, &info);
    if (!status)
    {
        if (info.st_size < current_log_limit)
            return;
    }
    log_exit();
    sprintf(backup, "%s.0", log_filename);
    status = rename(log_filename, backup); /* FIXME: error checking */
    log_reinit();
}

/***********************************************************
 * NAME: log_entry
 * DESCRIPTION: this function logs the specified message
 * based on the current logging configuration. If the
 * specified level is greater then the log level as set by
 * the application the message is just dropped. Otherwise
 * a timestamp and the log level are prepended to the
 * message and the message is written to the log file.
 *
 * IN:  debug level for this message, message format and
 *      subsequent parameters.
 * OUT: none
 ***********************************************************/
void log_entry(uint32_t debug_level, char *format, ...)
{
    va_list args;
    struct timeval tv;
    char *time;

    if (logfp == NULL)
        return;
    if (debug_level > current_log_level)
        return;
#ifdef LOGGER_THREADS
    pthread_mutex_lock(&log_mutex);
#endif /* LOGGER_THREADS */
    manage_log_file();
    gettimeofday(&tv, NULL);
    time = ctime(&tv.tv_sec);
    time[strlen(time) - 1] = 0;
    fprintf(logfp, "%s: %s:", time, level_to_str(debug_level));
    va_start(args, format);
    vfprintf(logfp, format, args);
    va_end(args);
    fprintf(logfp, "\n");
    fflush(logfp);
#ifdef LOGGER_THREADS
    pthread_mutex_unlock(&log_mutex);
#endif /* LOGGER_THREADS */
}
