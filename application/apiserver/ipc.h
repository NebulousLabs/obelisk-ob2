/*
 * Copyright (C) 2014-2018 MapleLeaf Software, Inc
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * ipc.h
 *
 * Dan Jozwiak: djozwiak@mlsw.biz
 * Date Created: 2014-05-20
 *
 * Description: this file contains the contents and data structures used
 * for processing server messages on the TCP socket.
 */

#ifndef __SERVER_H__
#define __SERVER_H__

#ifdef  __cplusplus
extern "C" {
#endif

#include "config.h"

extern void ipc_init(station_config_t *config);
extern void ipc_shutdown(station_config_t *config);

// Actions to send to the control manager
extern void send_software_update_check(bool autoupdate);
extern void send_software_update(void);
extern void send_software_rollback(void);
extern void send_factory_reset(void);
extern void send_cgminer_reset(void);
extern void send_clear_tuning(void);
extern void send_save_performance(void);
extern void send_set_fan_speed_percent(uint8_t fan_speed_percent);
extern void send_set_auto_fan_speed_percent(uint8_t fan_speed_percent);
extern void send_set_auto_fan_temp(float temp);
extern void send_set_auto_fan_mode(uint8_t mode);
extern void send_set_reboot_period(uint32_t reboot_period);
extern void send_set_performance_number(uint8_t hb_index, uint32_t performance_number);
extern void send_set_alternating_leds(bool enabled);
extern void send_set_clock(uint8_t hb_index, uint8_t divider, int8_t bias);
extern void send_set_chip_bias_offset(uint8_t hb_index, uint8_t chip, int8_t offset);

#ifdef  __cplusplus
}
#endif

#endif /* __SERVER_H__ */
