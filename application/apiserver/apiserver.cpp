// Copyright 2018 Obelisk Inc.

#include <iostream>
#include <stdio.h>
#include <unistd.h>
#include <unordered_map>

#include "CgMinerTypes.h"
#include "Crow.h"
#include "HttpStatusCodes.h"
#include "SafeQueue.h"
#include "utils.h"

#ifdef CONFIG_OB2
#include "common.h"
#include "logger.h"
#include "info.h"
#include "ipc.h"
#include "config.h"
#include "stats.h"

static char *log_file;
static bool do_fork = true;
station_config_t station_config;

/***********************************************************
 * NAME: process_signal
 * DESCRIPTION: this is a generic signal handler that can be
 * used to process any Unix signal. Currently not used.
 *
 * IN:  generated signal number
 * OUT: none
 ***********************************************************/
static void process_signal(
    int sig
)
{
    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    switch(sig)
    {
        case SIGUSR1:
        break;
        case SIGUSR2:
        break;
        case SIGHUP:
            if (log_level(0) == LEVEL_INFO)
            {
                log_level(LEVEL_DEBUG);
                log_entry(LEVEL_INFO, "[%s] log level DEBUG", __func__);
            }
            else
            {
                log_level(LEVEL_INFO);
                log_entry(LEVEL_INFO, "[%s] log level INFO", __func__);
            }
        break;
        case SIGINT:
        case SIGQUIT:
        case SIGTERM:
            log_entry(LEVEL_INFO, "[%s] apiserver exiting", __func__);
            ipc_shutdown(&station_config);
            log_exit();
            exit(0);
        break;
    }
}

/***********************************************************
 * NAME: usage
 * DESCRIPTION: this function simply outputs to the console
 * the usage of the station manager.
 *
 * IN:  program name
 * OUT: none
 ***********************************************************/
static void usage(
    char *name
)
{
    printf("Usage:%s\n", name);
    printf("\t-d: don't automatically fork\n"
           "\t-l <log file>: specify full path to log file\n"
          );
    exit(1);
}

/***********************************************************
 * NAME: process_args
 * DESCRIPTION: this function processes any arguments that
 * were passed to the manager on the command line. Current
 * supported arguments are as follows:
 *   -d : allows the user to tell the manager not to 
 *        automatically fork (useful for debugging)
 *   -l : allows the user to specify a log file. If no log
 *        file is specified then output will go to the
 *        console.
 *
 * IN:  argument count, argument list
 * OUT: none
 ***********************************************************/
static void process_args(
    int argc,
    char **argv
)
{
    int c = 0;

    while ((c = getopt(argc, argv, "l:d")) != -1)
    {
        switch (c)
        {
            case 'l':
                log_file = optarg;
            break;
            case 'd':
                do_fork = false;
            break;
            default:
                usage(argv[0]);
            break;
        }
    }
}
#endif /* CONFIG_OB2 */

int main(int argc, char *argv[])
{
#ifdef CONFIG_OB2
    struct sigaction accept_sa;
    struct sigaction ignore_sa;

    process_args(argc, argv);

    /* If the flag is set fork into the background */
    if (do_fork)
    {
        switch(fork())
        {
            case 0:
            break;
            case -1:
                log_entry(LEVEL_ERROR, "[%s]: unable to fork", __func__);
            default:
                exit(0);
        }
    }

#ifdef DEBUG
    log_init(log_file, DEFAULT_LOG_LIMIT, LEVEL_DEBUG);
#else
    log_init(log_file, DEFAULT_LOG_LIMIT, LEVEL_INFO);
#endif /* DEBUG */
    log_entry(LEVEL_INFO, "[%s] apiserver started", __func__);
    CROW_LOG_INFO << "apiserver started"; /* To verify Crow logging */
    
    /* Setup the signal handling for the process */
    accept_sa.sa_handler = process_signal;
    accept_sa.sa_flags = 0;
    sigemptyset(&accept_sa.sa_mask);

    ignore_sa.sa_handler = SIG_IGN;
    ignore_sa.sa_flags = 0;
    sigemptyset(&ignore_sa.sa_mask);

    sigaction(SIGUSR1, &accept_sa, NULL);
    sigaction(SIGUSR2, &accept_sa, NULL);
    sigaction(SIGHUP, &accept_sa, NULL);
    sigaction(SIGINT, &accept_sa, NULL);
    sigaction(SIGQUIT, &accept_sa, NULL);
    sigaction(SIGTERM, &accept_sa, NULL);

    sigaction(SIGPIPE, &ignore_sa, NULL);

    config_init(&station_config);
    info_ipaddr(&station_config.station_info);
    ipc_init(&station_config);
    stats_init();

#else
    // Check if we should run as a daemon
    if (argc > 1 && strcmp(argv[1], "--daemon") == 0)
    {
        daemon(0, 0);
    
        // Since we daemonize ourselves, it is our responsibility to create the pid file for init.d
        // to use for tracking/killing this process.
        pid_t daemon_pid = getpid();
        ostringstream cmd;
        cmd << "echo " << daemon_pid << " > /var/run/apiserver.pid";
        runCmd(cmd.str());
    }

    pthread_t cgMinerThread;
    pthread_create(&cgMinerThread, NULL, runCgMiner, NULL);

    // Create a thread for the autotuning
    pthread_t autotuneThread;
    pthread_create(&autotuneThread, NULL, runAutotune, NULL);
#endif /* CONFIG_OB2 */

    // Force password set (after deleting auth.json)
    // changePassword("admin", "", "admin", AUTH_FILE, true);
    bool isRunning = true;
    while (isRunning)
    {
        try
        {
            runCrow(8080);
            isRunning = false;
        } 
        catch(const std::exception& exc)
        {
            CROW_LOG_ERROR << "CrowMain EXCEPTION: " << exc.what();
            // Catching and continuing here causes Crow to misbehave for some reason, even though
            // we create a new app instance, so we just exit -- no exception has happened from
            // runCrow() though, except ones initiated on purpose for testing.
            return -1;
        }
    }
 
    return 0;
}
