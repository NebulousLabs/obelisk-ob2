#include "common.h"
#include "ipc_messages.h"

#ifndef _STATS_H_
#define _STATS_H_

#ifdef  __cplusplus
extern "C" {
#endif

// Device, Hashboard and Chip stats and knobs
#define NUM_FANS 4
#define NUM_CHIPS_PER_HASHBOARD  32

typedef struct __attribute__ ((packed))
{
    // Stats
    uint64_t good_nonces;
    uint64_t total_nonces;
} chip_stats_t;

typedef struct __attribute__ ((packed))
{
    uint64_t good_nonces;
    uint64_t total_nonces;
    uint64_t pool_nonces;

    chip_stats_t chip_stats[NUM_CHIPS_PER_HASHBOARD];
} hashboard_stats_t;

typedef struct __attribute__ ((packed))
{
    // Stats
    uint32_t fan_rpm[NUM_FANS];
    float pool_difficulty;

    // Knobs
    uint32_t fan_percent[NUM_FANS];  // Do we actually have 4 speed controls or just front/back

    // Hashboards
    hashboard_stats_t hb_stats[MAX_HASHING_BOARDS];
} device_stats_t;


// Functions
extern tlv_ipc_notification_burnin_status_t* allocBurninInfo();
extern void stats_init();

//-------------------------------------------------------------------------------------------------
// Setters
//-------------------------------------------------------------------------------------------------

// Device stats
extern void set_device_stat_fan_rpm(uint8_t fan_index, uint32_t fan_rpm);
extern void set_device_stat_pool_difficulty(float pool_difficulty);
extern void set_device_stat_num_fan_percent(uint8_t fan_index, uint8_t fan_percent);

// Hashboard Stats
extern void set_hb_stat_good_nonces(uint8_t hb_index, uint64_t good_nonces);
extern void set_hb_stat_total_nonces(uint8_t hb_index, uint64_t total_nonces);
extern void set_hb_stat_pool_nonces(uint8_t hb_index, uint64_t pool_nonces);

// Chip Stats
extern void set_chip_stat_good_nonces(uint8_t hb_index, uint8_t chip_index, bool good_nonces);
extern void set_chip_stat_total_nonces(uint8_t hb_index, uint8_t chip_index, bool total_nonces);

// Burnin Status
extern void set_burnin_status(tlv_ipc_notification_burnin_status_t* pstatus);

//-------------------------------------------------------------------------------------------------
// Getters
//-------------------------------------------------------------------------------------------------

// Device Stats
extern uint32_t get_device_stat_fan_rpm(uint8_t fan_index);
extern float get_device_stat_pool_difficulty();
extern uint8_t get_device_stat_fan_percent(uint8_t fan_index);

// Hashboard Stats
extern uint32_t get_hb_stat_good_nonces(uint8_t hb_index);
extern uint32_t get_hb_stat_total_nonces(uint8_t hb_index);
extern uint32_t get_hb_stat_pool_nonces(uint8_t hb_index);

// Chip Stats
extern uint64_t get_chip_stat_good_nonces(uint8_t hb_index, uint8_t chip_index);
extern uint64_t get_chip_stat_total_nonces(uint8_t hb_index, uint8_t chip_index);

// Burnin
extern void get_burnin_status(tlv_ipc_notification_burnin_status_t* pstatus);

#ifdef  __cplusplus
}
#endif

#endif /* _STATS_H_ */
