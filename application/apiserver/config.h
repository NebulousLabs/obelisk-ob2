/*
 * Copyright (C) 2012-2018 MapleLeaf Software, Inc
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * config.h
 *
 * Dan Jozwiak: djozwiak@mlsw.biz
 * Date Created: 2012-04-13
 *
 * Description: Header file for configuration processing.
 */

#ifndef _CONFIG_H_
#define _CONFIG_H_

#ifdef  __cplusplus
extern "C" {
#endif

#include "common.h"
#include "ipc_messages.h"

// Per hashboard info
typedef struct {
    hb_board_type_t type;  // If type is HB_UNKNOWN, then there is no hashboard in this array entry
    uint8_t id;
    char devnode[MAX_HB_DEV_NODE_LEN];
    char version[MAX_FWVERSION_SIZE];
    float intake_temp;
    float exhaust_temp;
    float voltage;
    float current;
    double hashrate_1min;
    double hashrate_5min;
    double hashrate_15min;
    uint64_t last_hash_stat_time_nanos;
    bool is_tuning;
    uint16_t performance_number;
    int8_t relative_voltage;
    int8_t clock_bias;
    uint8_t clock_divider;
    uint32_t noise_counter;
    uint32_t watchdog_counter;
    uint8_t asic_diags[MAX_ASICS_PER_HASHING_BOARD];
    uint8_t chip_bias_offset[MAX_ASICS_PER_HASHING_BOARD];
} hb_info_t;

typedef struct {
    uint64_t accepted;
    uint64_t rejected;
    char status[MAX_POOL_STATUS_LEN];
} pool_info_t;

// Station config
typedef struct
{
    station_info_t station_info;
    system_info_t system_info;
    char latest_firmware_version[MAX_STATIONRELEASE_LEN];
    char rollback_firmware_version[MAX_STATIONRELEASE_LEN];
    float fan_speeds[NUM_FANS];
    int ipc_fd;
    int hb_count;
    hb_info_t hbinfo[MAX_HASHING_BOARDS];
    pool_info_t pool_info[MAX_POOLS];
    float exhaust_temp_setpoint;
    uint8_t fan_mode;
    bool is_tuning;
} station_config_t;


extern void config_init(station_config_t *config);

// Settersf
extern void set_station_info(station_info_t* station_info);
extern void set_system_info(system_info_t* system_info);
extern void set_latest_firmware_version(char *version);
extern void set_rollback_firmware_version(char *version);
extern void set_fan_speed(uint8_t fan_index, float speed);

extern void set_hb_count(uint8_t count);
extern void set_hb_info(uint8_t hb_index, uint8_t id, hb_board_type_t type, char *version, uint8_t *asic_diags);
extern void set_hb_intake_temp(uint8_t hb_index, float temp);
extern void set_hb_exhaust_temp(uint8_t hb_index, float temp);
extern void set_hb_voltage(uint8_t hb_index, float voltage);
extern void set_hb_current(uint8_t hb_index, float current);
extern void set_hb_is_tuning(uint8_t hb_index, bool is_tuning);
extern void set_hb_performance_number(uint8_t hb_index, uint16_t performance_number);
extern void set_hb_clock(uint8_t hb_index, uint8_t divider, int8_t bias);
extern void set_chip_bias_offset(uint8_t hb_index, uint8_t chip_index, int8_t offset);
extern void add_hb_hashes(uint8_t hb_index, uint64_t num_hashes);
extern void set_hb_relative_voltage(uint8_t hb_index, int8_t voltage);
extern void set_hb_noise_counter(uint8_t hb_index, uint32_t counter);
extern void set_hb_watchdog_counter(uint8_t hb_index, uint32_t counter);

extern void set_pool_info(uint8_t pool_index, uint64_t accepted, uint64_t rejected, char *status);

extern void set_system_fan_speed_percent(uint8_t fan_speed);
extern void set_system_reboot_period(uint32_t reboot_period);
extern void set_system_exhaust_temp_setpoint(uint8_t exhaust_temp_setpoint);
extern void set_system_fan_mode(uint8_t fan_mode);
extern void set_system_performance_number(uint16_t performance_number);

// Getters
extern void get_station_info(station_info_t* station_info);
extern void get_system_info(system_info_t* system_info);
extern void get_latest_firmware_version(char *version);
extern void get_rollback_firmware_version(char *version);
extern float get_fan_speed(uint8_t fan_index);
extern uint8_t get_hb_count();

extern uint8_t get_hb_id(uint8_t hb_index) ;
extern hb_board_type_t get_hb_type(uint8_t hb_index);
extern void get_hb_version(uint8_t hb_index, char *version);
extern float get_hb_intake_temp(uint8_t hb_index);
extern float get_hb_exhaust_temp(uint8_t hb_index);
extern float get_hb_voltage(uint8_t hb_index);
extern float get_hb_current(uint8_t hb_index);

extern uint64_t get_pool_accepted(uint8_t pool_index);
extern uint64_t get_pool_rejected(uint8_t pool_index);
extern void get_pool_status(uint8_t pool_index, char *status);

extern double get_hb_hashrate_1min(uint8_t hb_index);
extern double get_hb_hashrate_5min(uint8_t hb_index);
extern double get_hb_hashrate_15min(uint8_t hb_index);

extern uint32_t get_hb_noise_counter(uint8_t hb_index);
extern uint32_t get_hb_watchdog_counter(uint8_t hb_index);
extern void get_hb_asic_diags(uint8_t hb_index, uint8_t *asic_diags);
extern bool get_hb_is_tuning(uint8_t hb_index);
extern uint16_t get_hb_performance_number(uint8_t hb_index);
extern uint8_t get_hb_clock_divider(uint8_t hb_index);
extern int8_t get_hb_clock_bias(uint8_t hb_index);
extern int8_t get_chip_bias_offset(uint8_t hb_index, uint8_t chip_index);

extern uint8_t get_system_fan_speed_percent();
extern uint32_t get_system_reboot_period();
extern uint8_t get_system_exhaust_temp_setpoint();
extern uint8_t get_system_fan_mode();

#ifdef  __cplusplus
}
#endif

#endif /* _CONFIG_H_ */
