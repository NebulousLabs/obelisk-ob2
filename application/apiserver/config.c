/*
 * Copyright (C) 2012-2018 MapleLeaf Software, Inc
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * config.c
 *
 * Dan Jozwiak: djozwiak@mlsw.biz
 * Date Created: 2012-04-13
 *
 * Description: 
 */

#include "common.h"
#include "logger.h"
#include "info.h"
#include "config.h"
#include "stats.h"
#include <pthread.h>
#include <math.h>

// Direct prototype instead of including utils.h, because utils.h is a CPP header
uint64_t asNanos(struct timespec *ts);

static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

static station_config_t* config;

void config_init(
    station_config_t *_config
)
{
    // Stash away a pointer for the functions below to use
    config = _config;

    info_station(STATION_INFO_FILE, &(config->station_info));
    info_system(SYSTEM_INFO_FILE, &(config->system_info));
    config->station_info.release[0] = 'v';
    config->exhaust_temp_setpoint = config->system_info.autofantemp;
    if (config->system_info.autofan)
        config->fan_mode = 0;
    else
        config->fan_mode = 1;

    // Intialize the hashboard info and pool info arrays
    memset(config->hbinfo, 0, MAX_HASHING_BOARDS * sizeof(hb_info_t));
    memset(config->pool_info, 0, MAX_POOLS * sizeof(pool_info_t));

    // Set board ids to invalid values
    for (uint8_t hb_index=0; hb_index<MAX_HASHING_BOARDS; hb_index++) {
        config->hbinfo[hb_index].id = 255;
        log_entry(LEVEL_DEBUG, "config_init(): hb_index=%d id=%d", hb_index, config->hbinfo[hb_index].id);
    }
}


//-------------------------------------------------------------------------------------------------
// Setters
//-------------------------------------------------------------------------------------------------
void set_station_info(station_info_t* station_info) {
    pthread_mutex_lock(&mutex);
    memcpy(&config->station_info, station_info, sizeof(station_info_t));
    pthread_mutex_unlock(&mutex);
}

void set_system_info(system_info_t* system_info) {
    pthread_mutex_lock(&mutex);
    memcpy(&config->system_info, system_info, sizeof(system_info_t));
    pthread_mutex_unlock(&mutex);
}

void set_latest_firmware_version(char *version) {
    pthread_mutex_lock(&mutex);
    strncpy(config->latest_firmware_version, version, MAX_STATIONRELEASE_LEN);
    pthread_mutex_unlock(&mutex);
}

void set_rollback_firmware_version(char *version) {
    pthread_mutex_lock(&mutex);
    strncpy(config->rollback_firmware_version, version, MAX_STATIONRELEASE_LEN);
    pthread_mutex_unlock(&mutex);
}

// The actual fan speed in RPM
void set_fan_speed(uint8_t fan_index, float speed) {
    if (fan_index >= NUM_FANS) {
        return;
    }
    pthread_mutex_lock(&mutex);
    config->fan_speeds[fan_index] = speed;
    pthread_mutex_unlock(&mutex);
}

// Hashboard setters
void set_hb_count(uint8_t count) {
    if (count > MAX_HASHING_BOARDS) {
        return;
    }
    pthread_mutex_lock(&mutex);
    config->hb_count = count;
    pthread_mutex_unlock(&mutex);
}

void set_hb_info(uint8_t hb_index, uint8_t id, hb_board_type_t type, char *version, uint8_t *asic_diags) {
    if (hb_index >= MAX_HASHING_BOARDS) {
        return;
    }

    pthread_mutex_lock(&mutex);
    config->hbinfo[hb_index].id = id;
    config->hbinfo[hb_index].type = type;
    strncpy(config->hbinfo[hb_index].version, version, MAX_FWVERSION_SIZE);
    memcpy(config->hbinfo[hb_index].asic_diags, asic_diags, sizeof(uint8_t) * MAX_ASICS_PER_HASHING_BOARD);
    pthread_mutex_unlock(&mutex);
}

void set_hb_intake_temp(uint8_t hb_index, float temp) {
    if (hb_index >= MAX_HASHING_BOARDS) {
        return;
    }

    pthread_mutex_lock(&mutex);
    config->hbinfo[hb_index].intake_temp = temp;
    pthread_mutex_unlock(&mutex);
}

void set_hb_exhaust_temp(uint8_t hb_index, float temp) {
    if (hb_index >= MAX_HASHING_BOARDS) {
        return;
    }

    pthread_mutex_lock(&mutex);
    config->hbinfo[hb_index].exhaust_temp = temp;
    pthread_mutex_unlock(&mutex);
}

void set_hb_voltage(uint8_t hb_index, float voltage) {
    if (hb_index >= MAX_HASHING_BOARDS) {
        return;
    }

    pthread_mutex_lock(&mutex);
    config->hbinfo[hb_index].voltage = voltage;
    pthread_mutex_unlock(&mutex);
}

void set_hb_current(uint8_t hb_index, float current) {
    if (hb_index >= MAX_HASHING_BOARDS) {
        return;
    }

    pthread_mutex_lock(&mutex);
    config->hbinfo[hb_index].current = current;
    pthread_mutex_unlock(&mutex);
}

void set_hb_is_tuning(uint8_t hb_index, bool is_tuning) {
    if (hb_index >= MAX_HASHING_BOARDS) {
        return;
    }

    pthread_mutex_lock(&mutex);
    config->hbinfo[hb_index].is_tuning = is_tuning;
    pthread_mutex_unlock(&mutex);
}

void set_hb_performance_number(uint8_t hb_index, uint16_t performance_number) {
    if (hb_index >= MAX_HASHING_BOARDS) {
        return;
    }

    pthread_mutex_lock(&mutex);
    config->hbinfo[hb_index].performance_number = performance_number;
    pthread_mutex_unlock(&mutex);
}

void set_hb_clock(uint8_t hb_index, uint8_t divider, int8_t bias) {
    if (hb_index >= MAX_HASHING_BOARDS) {
        return;
    }

    pthread_mutex_lock(&mutex);
    config->hbinfo[hb_index].clock_divider = divider;
    config->hbinfo[hb_index].clock_bias = bias;
    pthread_mutex_unlock(&mutex);
}

void set_chip_bias_offset(uint8_t hb_index, uint8_t chip_index, int8_t offset) {
    if (hb_index >= MAX_HASHING_BOARDS || chip_index >= MAX_ASICS_PER_HASHING_BOARD) {
        return;
    }

    pthread_mutex_lock(&mutex);
    config->hbinfo[hb_index].chip_bias_offset[chip_index] = offset;
    pthread_mutex_unlock(&mutex);
}

void add_hb_good_nonces(uint8_t hb_index, uint64_t num_good_nonces) {
    if (hb_index >= MAX_HASHING_BOARDS) {
        return;
    }

    pthread_mutex_lock(&mutex);
    // TODO: Add to circular queue
    pthread_mutex_unlock(&mutex);
}

void set_pool_info(uint8_t pool_index, uint64_t accepted, uint64_t rejected, char *status) {
    pthread_mutex_lock(&mutex);
    config->pool_info[pool_index].accepted = accepted;
    config->pool_info[pool_index].rejected = rejected;
    strncpy(config->pool_info[pool_index].status, status, MAX_POOL_STATUS_LEN);
    pthread_mutex_unlock(&mutex);
}

// Create an exponentially decaying average over the given interval
void decay_time(double* f, double fadd, double fsecs, double interval)
{
    double ftotal, fprop;

    if (fsecs <= 0) {
        return;
    }

    fprop = 1.0 - 1 / (exp(fsecs / interval));
    ftotal = 1.0 + fprop;
    *f += (fadd / fsecs * fprop);
    *f /= ftotal;
}

void add_hb_hashes(uint8_t hb_index, uint64_t num_hashes) {
    if (hb_index >= MAX_HASHING_BOARDS) {
        return;
    }

    pthread_mutex_lock(&mutex);
    struct timespec now;
    clock_gettime(CLOCK_MONOTONIC, &now);
    uint64_t now_nanos = asNanos(&now);
    uint64_t elapsed = now_nanos - config->hbinfo[hb_index].last_hash_stat_time_nanos;
    double elapsed_secs = ((double)elapsed) / 1000000000.0;
    double gigahashes_done = ((double)num_hashes) / 1000000000.0;
    log_entry(LEVEL_DEBUG, "num_hashes=%llu", num_hashes);
    log_entry(LEVEL_DEBUG, "elapsed_secs=%0.2f giga_hashes_done=%0.2f", elapsed_secs, gigahashes_done);

    decay_time(&config->hbinfo[hb_index].hashrate_1min, gigahashes_done, elapsed_secs, 60.0);
    decay_time(&config->hbinfo[hb_index].hashrate_5min, gigahashes_done, elapsed_secs, 300.0);
    decay_time(&config->hbinfo[hb_index].hashrate_15min, gigahashes_done, elapsed_secs, 900.0);

    log_entry(LEVEL_DEBUG, "hashrate_1min =%0.2f GH/s", config->hbinfo[hb_index].hashrate_1min);
    log_entry(LEVEL_DEBUG, "hashrate_5min =%0.2f GH/s", config->hbinfo[hb_index].hashrate_5min);
    log_entry(LEVEL_DEBUG, "hashrate_15min=%0.2f GH/s", config->hbinfo[hb_index].hashrate_15min);

    config->hbinfo[hb_index].last_hash_stat_time_nanos = now_nanos;
    pthread_mutex_unlock(&mutex);
}

void set_hb_relative_voltage(uint8_t hb_index, int8_t voltage) {
    if (hb_index >= MAX_HASHING_BOARDS) {
        return;
    }

    pthread_mutex_lock(&mutex);
    config->hbinfo[hb_index].relative_voltage = voltage;
    pthread_mutex_unlock(&mutex);
}

void set_hb_clock_bias(uint8_t hb_index, int8_t bias) {
    if (hb_index >= MAX_HASHING_BOARDS) {
        return;
    }

    pthread_mutex_lock(&mutex);
    config->hbinfo[hb_index].clock_bias = bias;
    pthread_mutex_unlock(&mutex);
}

void set_hb_clock_divider(uint8_t hb_index, uint8_t divider) {
    if (hb_index >= MAX_HASHING_BOARDS) {
        return;
    }

    pthread_mutex_lock(&mutex);
    config->hbinfo[hb_index].clock_divider = divider;
    pthread_mutex_unlock(&mutex);
}

void set_hb_noise_counter(uint8_t hb_index, uint32_t counter) {
    if (hb_index >= MAX_HASHING_BOARDS) {
        return;
    }

    pthread_mutex_lock(&mutex);
    config->hbinfo[hb_index].noise_counter = counter;
    pthread_mutex_unlock(&mutex);
}

void set_hb_watchdog_counter(uint8_t hb_index, uint32_t counter) {
    if (hb_index >= MAX_HASHING_BOARDS) {
        return;
    }

    pthread_mutex_lock(&mutex);
    config->hbinfo[hb_index].watchdog_counter = counter;
    pthread_mutex_unlock(&mutex);
}

void set_system_fan_speed_percent(uint8_t fan_speed) {
    pthread_mutex_lock(&mutex);
    config->system_info.fanspeed = fan_speed;
    pthread_mutex_unlock(&mutex);
}

void set_system_exhaust_temp_setpoint(uint8_t exhaust_temp_setpoint) {
    pthread_mutex_lock(&mutex);
    config->exhaust_temp_setpoint = exhaust_temp_setpoint;
    pthread_mutex_unlock(&mutex);
}

void set_system_fan_mode(uint8_t fan_mode) {
    pthread_mutex_lock(&mutex);
    config->fan_mode = fan_mode;
    pthread_mutex_unlock(&mutex);
}

void set_system_reboot_period(uint32_t reboot_period) {
    pthread_mutex_lock(&mutex);
    config->system_info.rebootperiod = reboot_period;
    pthread_mutex_unlock(&mutex);
}


//-------------------------------------------------------------------------------------------------
// Getters
//-------------------------------------------------------------------------------------------------
void get_station_info(station_info_t* station_info) {
    pthread_mutex_lock(&mutex);
    memcpy(station_info, &config->station_info, sizeof(station_info_t));
    pthread_mutex_unlock(&mutex);
}

void get_system_info(system_info_t* system_info) {
    pthread_mutex_lock(&mutex);
    memcpy(system_info, &config->system_info, sizeof(system_info_t));
    pthread_mutex_unlock(&mutex);
}

void get_latest_firmware_version(char *version) {
    pthread_mutex_lock(&mutex);
    strncpy(version, config->latest_firmware_version, MAX_STATIONRELEASE_LEN);
    // Replace 'r' with 'v'
    if (version[0] == 'r') {
        version[0] = 'v';
    }
    pthread_mutex_unlock(&mutex);
}

void get_rollback_firmware_version(char *version) {
    pthread_mutex_lock(&mutex);
    strncpy(version, config->rollback_firmware_version, MAX_STATIONRELEASE_LEN);
    // Replace 'r' with 'v'
    if (version[0] == 'r') {
        version[0] = 'v';
    }
    pthread_mutex_unlock(&mutex);
}

float get_fan_speed(uint8_t fan_index) {
    float result;
    pthread_mutex_lock(&mutex);
    result = config->fan_speeds[fan_index];
    pthread_mutex_unlock(&mutex);
    return result;
}

uint8_t get_hb_count() {
    uint8_t result;
    pthread_mutex_lock(&mutex);
    result = config->hb_count;
    pthread_mutex_unlock(&mutex);
    return result;
}

// Hashboard getters
uint8_t get_hb_id(uint8_t hb_index) {
    uint8_t result;
    pthread_mutex_lock(&mutex);
    result = config->hbinfo[hb_index].id;
    pthread_mutex_unlock(&mutex);
    return result;
}

hb_board_type_t get_hb_type(uint8_t hb_index) {
    hb_board_type_t result;
    pthread_mutex_lock(&mutex);
    result = config->hbinfo[hb_index].type;
    pthread_mutex_unlock(&mutex);
    return result;
}

void get_hb_version(uint8_t hb_index, char *version) {
    pthread_mutex_lock(&mutex);
    strncpy(version, config->hbinfo[hb_index].version, MAX_FWVERSION_SIZE);
    pthread_mutex_unlock(&mutex);
}

float get_hb_intake_temp(uint8_t hb_index) {
    float result;
    pthread_mutex_lock(&mutex);
    result = config->hbinfo[hb_index].intake_temp;
    pthread_mutex_unlock(&mutex);
    return result;
}

float get_hb_exhaust_temp(uint8_t hb_index) {
    float result;
    pthread_mutex_lock(&mutex);
    result = config->hbinfo[hb_index].exhaust_temp;
    pthread_mutex_unlock(&mutex);
    return result;
}

float get_hb_voltage(uint8_t hb_index) {
    float result;
    pthread_mutex_lock(&mutex);
    result = config->hbinfo[hb_index].voltage;
    pthread_mutex_unlock(&mutex);
    return result;
}

float get_hb_current(uint8_t hb_index) {
    float result;
    pthread_mutex_lock(&mutex);
    result = config->hbinfo[hb_index].current;
    pthread_mutex_unlock(&mutex);
    return result;
}

uint64_t get_pool_accepted(uint8_t pool_index) {
    uint64_t result;
    pthread_mutex_lock(&mutex);
    result = config->pool_info[pool_index].accepted;
    pthread_mutex_unlock(&mutex);
    return result;
}

uint64_t get_pool_rejected(uint8_t pool_index) {
    uint64_t result;
    pthread_mutex_lock(&mutex);
    result = config->pool_info[pool_index].rejected;
    pthread_mutex_unlock(&mutex);
    return result;
}

void get_pool_status(uint8_t pool_index, char *status) {
    pthread_mutex_lock(&mutex);
    strncpy(status, config->pool_info[pool_index].status, MAX_POOL_STATUS_LEN);
    pthread_mutex_unlock(&mutex);
}

double get_hb_hashrate_1min(uint8_t hb_index) {
    double result;
    pthread_mutex_lock(&mutex);
    result = config->hbinfo[hb_index].hashrate_1min;
    pthread_mutex_unlock(&mutex);
    return result;
}

double get_hb_hashrate_5min(uint8_t hb_index) {
    double result;
    pthread_mutex_lock(&mutex);
    result = config->hbinfo[hb_index].hashrate_5min;
    pthread_mutex_unlock(&mutex);
    return result;
}

double get_hb_hashrate_15min(uint8_t hb_index) {
    double result;
    pthread_mutex_lock(&mutex);
    result = config->hbinfo[hb_index].hashrate_15min;
    pthread_mutex_unlock(&mutex);
    return result;
}

uint32_t get_hb_noise_counter(uint8_t hb_index) {
    uint32_t result;
    pthread_mutex_lock(&mutex);
    result = config->hbinfo[hb_index].noise_counter;
    pthread_mutex_unlock(&mutex);
    return result;
}

uint32_t get_hb_watchdog_counter(uint8_t hb_index) {
    uint32_t result;
    pthread_mutex_lock(&mutex);
    result = config->hbinfo[hb_index].watchdog_counter;
    pthread_mutex_unlock(&mutex);
    return result;
}

// asic_diags must be at least sizeof(uint8_t) * MAX_ASICS_PER_HASHING_BOARD
void get_hb_asic_diags(uint8_t hb_index, uint8_t *asic_diags) {
    pthread_mutex_lock(&mutex);
    memcpy(asic_diags, config->hbinfo[hb_index].asic_diags, sizeof(uint8_t) * MAX_ASICS_PER_HASHING_BOARD);
    pthread_mutex_unlock(&mutex);
}

bool get_hb_is_tuning(uint8_t hb_index) {
    bool result;
    pthread_mutex_lock(&mutex);
    result = config->hbinfo[hb_index].is_tuning;
    pthread_mutex_unlock(&mutex);
    return result;
}

uint16_t get_hb_performance_number(uint8_t hb_index) {
    uint16_t result;
    pthread_mutex_lock(&mutex);
    result = config->hbinfo[hb_index].performance_number;
    pthread_mutex_unlock(&mutex);
    return result;
}

uint8_t get_hb_clock_divider(uint8_t hb_index) {
    uint8_t result;
    pthread_mutex_lock(&mutex);
    result = config->hbinfo[hb_index].clock_divider;
    pthread_mutex_unlock(&mutex);
    return result;
}

int8_t get_hb_clock_bias(uint8_t hb_index) {
    int8_t result;
    pthread_mutex_lock(&mutex);
    result = config->hbinfo[hb_index].clock_bias;
    pthread_mutex_unlock(&mutex);
    return result;
}

int8_t get_chip_bias_offset(uint8_t hb_index, uint8_t chip_index) {
    int8_t result;
    pthread_mutex_lock(&mutex);
    result = config->hbinfo[hb_index].chip_bias_offset[chip_index];
    pthread_mutex_unlock(&mutex);
    return result;
}

int8_t get_hb_relative_voltage(uint8_t hb_index) {
    int8_t result;
    pthread_mutex_lock(&mutex);
    result = config->hbinfo[hb_index].relative_voltage;
    pthread_mutex_unlock(&mutex);
    return result;
}

// uint32_t get_hb_noise_counter(uint8_t hb_index) {
//     uint32_t result;
//     pthread_mutex_lock(&mutex);
//     result = config->hbinfo[hb_index].noise_counter;
//     pthread_mutex_unlock(&mutex);
//     return result;
// }

// uint32_t get_hb_watchdog_counter(uint8_t hb_index) {
//     uint32_t result;
//     pthread_mutex_lock(&mutex);
//     result = config->hbinfo[hb_index].watchdog_counter;
//     pthread_mutex_unlock(&mutex);
//     return result;
// }


uint8_t get_system_fan_speed_percent() {
    uint8_t result;
    pthread_mutex_lock(&mutex);
    result = config->system_info.fanspeed;
    pthread_mutex_unlock(&mutex);
    return result;
}

uint32_t get_system_reboot_period() {
    uint32_t result;
    pthread_mutex_lock(&mutex);
    result = config->system_info.rebootperiod;
    pthread_mutex_unlock(&mutex);
    return result;
}

uint8_t get_system_exhaust_temp_setpoint() {
    float result;
    pthread_mutex_lock(&mutex);
    result = config->exhaust_temp_setpoint;
    pthread_mutex_unlock(&mutex);
    return result;
}

uint8_t get_system_fan_mode() {
    uint8_t result;
    pthread_mutex_lock(&mutex);
    result = config->fan_mode;
    pthread_mutex_unlock(&mutex);
    return result;
}
