#include "stats.h"
#include "logger.h"
#include <stdlib.h>
#include <pthread.h>
#include <math.h>

static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

device_stats_t stats;

tlv_ipc_notification_burnin_status_t* pburnin_status;

tlv_ipc_notification_burnin_status_t* allocBurninInfo() {
    return calloc(1, sizeof(tlv_ipc_notification_burnin_status_t) + sizeof(tlv_burnin_info_t) * 6);
}

void stats_init() {
    memset(&stats, 0, sizeof(device_stats_t));
    pburnin_status = allocBurninInfo();
}

//-------------------------------------------------------------------------------------------------
// Setters
//-------------------------------------------------------------------------------------------------

// Device stats
void set_device_stat_fan_rpm(uint8_t fan_index, uint32_t fan_rpm) {
    pthread_mutex_lock(&mutex);
    stats.fan_rpm[fan_index] = fan_rpm;
    pthread_mutex_unlock(&mutex);
}

void set_device_stat_pool_difficulty(float pool_difficulty) {
    pthread_mutex_lock(&mutex);
    stats.pool_difficulty = pool_difficulty;
    pthread_mutex_unlock(&mutex);
}

void set_device_stat_num_fan_percent(uint8_t fan_index, uint8_t fan_percent) {
    pthread_mutex_lock(&mutex);
    stats.fan_percent[fan_index] = fan_percent;
    pthread_mutex_unlock(&mutex);
}

// Hashboard Stats
void set_hb_stat_good_nonces(uint8_t hb_index, uint64_t good_nonces) {
    pthread_mutex_lock(&mutex);
    stats.hb_stats[hb_index].good_nonces = good_nonces;
    pthread_mutex_unlock(&mutex);
}

void set_hb_stat_total_nonces(uint8_t hb_index, uint64_t total_nonces) {
    pthread_mutex_lock(&mutex);
    stats.hb_stats[hb_index].total_nonces = total_nonces;
    pthread_mutex_unlock(&mutex);
}

void set_hb_stat_pool_nonces(uint8_t hb_index, uint64_t pool_nonces) {
    pthread_mutex_lock(&mutex);
    stats.hb_stats[hb_index].pool_nonces = pool_nonces;
    pthread_mutex_unlock(&mutex);
}

void set_chip_stat_good_nonces(uint8_t hb_index, uint8_t chip_index, bool good_nonces) {
    pthread_mutex_lock(&mutex);
    stats.hb_stats[hb_index].chip_stats[chip_index].good_nonces = good_nonces;
    pthread_mutex_unlock(&mutex);
}

void set_chip_stat_total_nonces(uint8_t hb_index, uint8_t chip_index, bool total_nonces) {
    pthread_mutex_lock(&mutex);
    stats.hb_stats[hb_index].chip_stats[chip_index].total_nonces = total_nonces;
    pthread_mutex_unlock(&mutex);
}

void set_burnin_status(tlv_ipc_notification_burnin_status_t* pstatus) {
    pthread_mutex_lock(&mutex);
    memcpy(pburnin_status, pstatus, sizeof(tlv_ipc_notification_burnin_status_t) +
           (pstatus->total_hashing_boards * sizeof(tlv_burnin_info_t)));
    pthread_mutex_unlock(&mutex);
}

//-------------------------------------------------------------------------------------------------
// Getters
//-------------------------------------------------------------------------------------------------

// Device Stats
uint32_t get_device_stat_fan_rpm(uint8_t fan_index) {
	uint32_t result;
    pthread_mutex_lock(&mutex);
    result = stats.fan_rpm[fan_index];
    pthread_mutex_unlock(&mutex);
    return result;
}

float get_device_stat_pool_difficulty() {
	float result;
    pthread_mutex_lock(&mutex);
    result = stats.pool_difficulty;
    pthread_mutex_unlock(&mutex);
    return result;
}

uint8_t get_device_stat_fan_percent(uint8_t fan_index) {
	uint8_t result;
    pthread_mutex_lock(&mutex);
    result = stats.fan_percent[fan_index];
    pthread_mutex_unlock(&mutex);
    return result;
}

// Hashboard Stats
uint32_t get_hb_stat_good_nonces(uint8_t hb_index) {
	uint32_t result;
    pthread_mutex_lock(&mutex);
    result = stats.hb_stats[hb_index].good_nonces;
    pthread_mutex_unlock(&mutex);
    return result;
}

uint32_t get_hb_stat_total_nonces(uint8_t hb_index) {
	uint32_t result;
    pthread_mutex_lock(&mutex);
    result = stats.hb_stats[hb_index].total_nonces;
    pthread_mutex_unlock(&mutex);
    return result;
}

uint32_t get_hb_stat_pool_nonces(uint8_t hb_index) {
	uint32_t result;
    pthread_mutex_lock(&mutex);
    result = stats.hb_stats[hb_index].pool_nonces;
    pthread_mutex_unlock(&mutex);
    return result;
}

// Chip Stats
uint64_t get_chip_stat_good_nonces(uint8_t hb_index, uint8_t chip_index) {
	uint64_t result;
    pthread_mutex_lock(&mutex);
    result = stats.hb_stats[hb_index].chip_stats[chip_index].good_nonces;
    pthread_mutex_unlock(&mutex);
    return result;
}

uint64_t get_chip_stat_total_nonces(uint8_t hb_index, uint8_t chip_index) {
	uint64_t result;
    pthread_mutex_lock(&mutex);
    result = stats.hb_stats[hb_index].chip_stats[chip_index].total_nonces;
    pthread_mutex_unlock(&mutex);
    return result;
}

// Burnin
void get_burnin_status(tlv_ipc_notification_burnin_status_t* pstatus) {
    pthread_mutex_lock(&mutex);
    memcpy(pstatus, pburnin_status, sizeof(tlv_ipc_notification_burnin_status_t) +
           (pburnin_status->total_hashing_boards * sizeof(tlv_burnin_info_t)));
    pthread_mutex_unlock(&mutex);
}
