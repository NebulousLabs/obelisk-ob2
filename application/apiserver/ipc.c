/*
 * Copyright (C) 2014-2018 MapleLeaf Software, Inc
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * ipc.c
 *
 * Dan Jozwiak: djozwiak@mlsw.biz
 * Date Created: 2014-05-20
 *
 * Description: this file contains the code to process connections and
 * messages from the server. 
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <sys/socket.h>
#include <sys/queue.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <errno.h>
#define __USE_GNU
#include <pthread.h>

#include "common.h"
#include "ipc_messages.h"
#include "logger.h"
#include "ports.h"
#include "ipc.h"
#include "config.h"
#include "stats.h"


#define THREAD_NAME "aip-server-ipc"

static pthread_t ipc_thread_id;

static station_config_t *config;


static msg_ipc_tlv_t *build_identity(void)
{
    void *msg = NULL;
    size_t msg_len = sizeof(msg_ipc_tlv_t) + sizeof(tlv_ipc_identity_t);

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    msg = calloc(1, msg_len);
    if (msg)
    {
        msg_ipc_tlv_t *tlv = msg;
        tlv_ipc_identity_t *identity = (tlv_ipc_identity_t *)tlv->value;

        tlv->type = ATTR_TYPE_IPC_IDENTITY;
        tlv->len = sizeof(tlv_ipc_identity_t);
        identity->id = IPC_API_SERVER;
    }

    return msg;
}

static msg_ipc_tlv_t *build_registration(void)
{
    void *msg = NULL;
    size_t msg_len = sizeof(msg_ipc_tlv_t) + sizeof(tlv_ipc_notify_registration_t);

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    msg = calloc(1, msg_len);
    if (msg)
    {
        msg_ipc_tlv_t *tlv = msg;
        tlv_ipc_notify_registration_t *reg = (tlv_ipc_notify_registration_t *)tlv->value;

        tlv->type = ATTR_TYPE_IPC_REGISTER_NOTIFICATION;
        tlv->len = sizeof(tlv_ipc_notify_registration_t);
        reg->notifications = NOTIFICATION_HB_TEMP1 |
                             NOTIFICATION_HB_TEMP2 |
                             NOTIFICATION_HB_VOLTAGE |
                             NOTIFICATION_HB_CURRENT |
                             NOTIFICATION_HB_STATS |
                             NOTIFICATION_HB_PERF |
                             NOTIFICATION_CGM_STATS |
                             NOTIFICATION_SWUPDATE |
                             NOTIFICATION_POOL_DIFF |
                             NOTIFICATION_BI_STATUS;
    }

    return msg;
}

static msg_ipc_tlv_t *build_simple_request(uint16_t type)
{
    void *msg = NULL;
    size_t msg_len = sizeof(msg_ipc_tlv_t);

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    msg = calloc(1, msg_len);
    if (msg)
    {
        msg_ipc_tlv_t *tlv = msg;

        tlv->type = type;
    }

    return msg;
}

static void send_simple_request(uint16_t type)
{
    if (config->ipc_fd)
    {
        /* Build and send a request with the specified type */
        msg_ipc_tlv_t *msg = build_simple_request(type);
        if (msg)
        {
            int bytes = send(config->ipc_fd, msg, sizeof(msg_ipc_tlv_t) + msg->len, MSG_NOSIGNAL);
            if (bytes < 0)
                log_entry(LEVEL_ERROR, "[%s] send failed: %s", __func__, strerror(errno));
            else if (bytes == 0)
                log_entry(LEVEL_ERROR, "[%s] sent 0 bytes: %s", __func__, strerror(errno));
            free(msg);
        }
    }
}

static msg_ipc_tlv_t *build_set_fan_speed(uint8_t speed)
{
    void *msg = NULL;
    size_t msg_len = sizeof(msg_ipc_tlv_t) + sizeof(tlv_ipc_fan_speed_t);

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    msg = calloc(1, msg_len);
    if (msg)
    {
        msg_ipc_tlv_t *tlv = msg;
        tlv_ipc_fan_speed_t *fan_speed = (tlv_ipc_fan_speed_t *)tlv->value;

        tlv->type = ATTR_TYPE_IPC_FAN_SPEED;
        tlv->len = sizeof(tlv_ipc_fan_speed_t);
        fan_speed->speed = speed;
    }

    return msg;
}

static msg_ipc_tlv_t *build_set_auto_fan_speed(uint8_t speed)
{
    void *msg = NULL;
    size_t msg_len = sizeof(msg_ipc_tlv_t) + sizeof(tlv_ipc_fan_speed_t);

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    msg = calloc(1, msg_len);
    if (msg)
    {
        msg_ipc_tlv_t *tlv = msg;
        tlv_ipc_fan_speed_t *fan_speed = (tlv_ipc_fan_speed_t *)tlv->value;

        tlv->type = ATTR_TYPE_IPC_AUTO_FAN_SPEED;
        tlv->len = sizeof(tlv_ipc_fan_speed_t);
        fan_speed->speed = speed;
    }

    return msg;
}

static msg_ipc_tlv_t *build_set_auto_fan_temp(float temp)
{
    void *msg = NULL;
    size_t msg_len = sizeof(msg_ipc_tlv_t) + sizeof(tlv_ipc_auto_fan_temp_t);

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    msg = calloc(1, msg_len);
    if (msg)
    {
        msg_ipc_tlv_t *tlv = msg;
        tlv_ipc_auto_fan_temp_t *fan_temp = (tlv_ipc_auto_fan_temp_t *)tlv->value;

        tlv->type = ATTR_TYPE_IPC_AUTO_FAN_TEMP;
        tlv->len = sizeof(tlv_ipc_auto_fan_temp_t);
        fan_temp->temp = temp;
    }

    return msg;
}

static msg_ipc_tlv_t *build_set_auto_fan_mode(uint8_t mode)
{
    void *msg = NULL;
    size_t msg_len = sizeof(msg_ipc_tlv_t) + sizeof(tlv_ipc_auto_fan_mode_t);

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    msg = calloc(1, msg_len);
    if (msg)
    {
        msg_ipc_tlv_t *tlv = msg;
        tlv_ipc_auto_fan_mode_t *fan_mode = (tlv_ipc_auto_fan_mode_t *)tlv->value;

        tlv->type = ATTR_TYPE_IPC_AUTO_FAN_MODE;
        tlv->len = sizeof(tlv_ipc_auto_fan_mode_t);
        if (mode == 0)
            fan_mode->enabled = true;
        else
            fan_mode->enabled = false;
    }

    return msg;
}

static msg_ipc_tlv_t *build_set_performance_number(uint8_t hb_index, uint16_t performance_number)
{
    void *msg = NULL;
    size_t msg_len = sizeof(msg_ipc_tlv_t) + sizeof(tlv_ipc_set_performance_t);

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    msg = calloc(1, msg_len);
    if (msg)
    {
        msg_ipc_tlv_t *tlv = msg;
        tlv_ipc_set_performance_t *set_performance = (tlv_ipc_set_performance_t *)tlv->value;

        tlv->type = ATTR_TYPE_IPC_SET_PERFORMANCE;
        tlv->len = sizeof(tlv_ipc_set_performance_t);
        set_performance->hb_id = hb_index;
        set_performance->performance = performance_number;
    }

    return msg;
}

static msg_ipc_tlv_t *build_set_clock(uint8_t hb_index, uint8_t divider, int8_t bias)
{
    void *msg = NULL;
    size_t msg_len = sizeof(msg_ipc_tlv_t) + sizeof(tlv_ipc_set_clock_t);

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    msg = calloc(1, msg_len);
    if (msg)
    {
        msg_ipc_tlv_t *tlv = msg;
        tlv_ipc_set_clock_t *set_clock = (tlv_ipc_set_clock_t *)tlv->value;

        tlv->type = ATTR_TYPE_IPC_SET_CLOCK;
        tlv->len = sizeof(tlv_ipc_set_clock_t);
        set_clock->hb_id = hb_index;
        set_clock->clock_bias = bias;
        set_clock->clock_divider = divider;
    }

    return msg;
}

static msg_ipc_tlv_t *build_set_chip_bias_offset(uint8_t hb_index, uint8_t chip, int8_t offset)
{
    void *msg = NULL;
    size_t msg_len = sizeof(msg_ipc_tlv_t) + sizeof(tlv_ipc_set_asic_clock_t);

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    msg = calloc(1, msg_len);
    if (msg)
    {
        // TODO: Could change naming of IPC message from SET_ASIC_CLOCK to SET_CHIP_BIAS_OFFSET
        msg_ipc_tlv_t *tlv = msg;
        tlv_ipc_set_asic_clock_t *set_clock = (tlv_ipc_set_asic_clock_t *)tlv->value;

        tlv->type = ATTR_TYPE_IPC_SET_ASIC_CLOCK;
        tlv->len = sizeof(tlv_ipc_set_asic_clock_t);
        set_clock->hb_id = hb_index;
        set_clock->asic = chip;
        set_clock->clock_delta = offset;
    }

    return msg;
}

static msg_ipc_tlv_t *build_set_reboot_period(uint32_t period)
{
    void *msg = NULL;
    size_t msg_len = sizeof(msg_ipc_tlv_t) + sizeof(tlv_ipc_fan_speed_t);

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    msg = calloc(1, msg_len);
    if (msg)
    {
        msg_ipc_tlv_t *tlv = msg;
        tlv_ipc_reboot_period_t *reboot_period = (tlv_ipc_reboot_period_t *)tlv->value;

        tlv->type = ATTR_TYPE_IPC_REBOOT_PERIOD;
        tlv->len = sizeof(tlv_ipc_reboot_period_t);
        reboot_period->period = period;
    }

    return msg;
}

static msg_ipc_tlv_t *build_set_alternating_leds(bool enabled)
{
    void *msg = NULL;
    size_t msg_len = sizeof(msg_ipc_tlv_t) + sizeof(tlv_ipc_alternating_leds_t);

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    msg = calloc(1, msg_len);
    if (msg)
    {
        msg_ipc_tlv_t *tlv = msg;
        tlv_ipc_alternating_leds_t *info = (tlv_ipc_alternating_leds_t *)tlv->value;

        tlv->type = ATTR_TYPE_IPC_ALTERNATE_LEDS;
        tlv->len = sizeof(tlv_ipc_alternating_leds_t);
        info->enabled = enabled;
    }

    return msg;
}

static msg_ipc_tlv_t *build_update_check(bool autoupdate)
{
    void *msg = NULL;
    size_t msg_len = sizeof(msg_ipc_tlv_t) + sizeof(tlv_ipc_update_check_t);

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    msg = calloc(1, msg_len);
    if (msg)
    {
        msg_ipc_tlv_t *tlv = msg;
        tlv_ipc_update_check_t *check = (tlv_ipc_update_check_t *)tlv->value;

        tlv->type = ATTR_TYPE_IPC_CHECK_FOR_UPDATE;
        tlv->len = sizeof(tlv_ipc_update_check_t);
        check->autoupdate = autoupdate;
    }

    return msg;
}

static int process_ipc_tlv(
    station_config_t *config,
    msg_ipc_tlv_t *tlv
)
{
    msg_ipc_tlv_t *msg = NULL;

    // log_entry(LEVEL_DEBUG, "[%s] called", __func__);
    switch (tlv->type)
    {
        case ATTR_TYPE_IPC_CHANGE_NOTIFICATION:
        {
            tlv_ipc_notification_t *change = (tlv_ipc_notification_t *)tlv->value;

            // log_entry(LEVEL_DEBUG, "[%s] received notification change", __func__);
            switch (change->type)
            {
                case NOTIFICATION_HB_TEMP1:
                {
                    tlv_ipc_notification_hb_measurement_t *info = (tlv_ipc_notification_hb_measurement_t *)change->value;
                    // log_entry(LEVEL_DEBUG, "[%s] received temp1 change for HB%d: %.2f", __func__, info->hb_id, info->value);
                    set_hb_intake_temp(info->hb_id, info->value);
                }
                break;

                case NOTIFICATION_HB_TEMP2:
                {
                    tlv_ipc_notification_hb_measurement_t *info = (tlv_ipc_notification_hb_measurement_t *)change->value;
                    // log_entry(LEVEL_DEBUG, "[%s] received temp2 change for HB%d: %.2f", __func__, info->hb_id, info->value);
                    set_hb_exhaust_temp(info->hb_id, info->value);
                }
                break;

                case NOTIFICATION_HB_VOLTAGE:
                {
                    tlv_ipc_notification_hb_measurement_t *info = (tlv_ipc_notification_hb_measurement_t *)change->value;
                    // log_entry(LEVEL_DEBUG, "[%s] received voltage change for HB%d: %.2f", __func__, info->hb_id, info->value);
                    set_hb_voltage(info->hb_id, info->value);
                }
                break;

                case NOTIFICATION_HB_CURRENT:
                {
                    tlv_ipc_notification_hb_measurement_t *info = (tlv_ipc_notification_hb_measurement_t *)change->value;
                    // log_entry(LEVEL_DEBUG, "[%s] received current change for HB%d: %.2f", __func__, info->hb_id, info->value);
                    set_hb_current(info->hb_id, info->value);
                }
                break;

                case NOTIFICATION_HB_STATS:
                {
                    tlv_ipc_notification_hb_stats_t *info = (tlv_ipc_notification_hb_stats_t *)change->value;
                    log_entry(LEVEL_DEBUG, "[%s] received stats for HB%d: %llu", __func__, info->hb_id, info->hashes);
                    add_hb_hashes(info->hb_id, info->hashes);
                    set_hb_relative_voltage(info->hb_id, info->relative_voltage);
                    set_hb_noise_counter(info->hb_id, info->noise_counter);
                    set_hb_watchdog_counter(info->hb_id, info->watchdog_counter);
                }
                break;

                case NOTIFICATION_HB_PERF:
                {
                    tlv_ipc_notification_hb_performance_t *info = (tlv_ipc_notification_hb_performance_t *)change->value;
                    log_entry(LEVEL_INFO, "[%s] received performance change for HB%d: %d, tuning: %s, clock divider: %d, bias: %d",
                              __func__, info->hb_id, info->performance, (info->tuning ? "true":"false"), info->divider, info->bias);

                    set_hb_is_tuning(info->hb_id, info->tuning);
                    set_hb_performance_number(info->hb_id, info->performance);
                    set_hb_clock(info->hb_id, info->divider, info->bias);
                    for (int i=0; i<NUM_CHIPS_PER_HASHBOARD; i++) {
                        set_chip_bias_offset(info->hb_id, i, info->bias_offsets[i]);
                    }
                }
                break;

                case NOTIFICATION_CGM_STATS:
                {
                    tlv_ipc_cgminer_stats_t *info = (tlv_ipc_cgminer_stats_t *)change->value;
                    log_entry(LEVEL_DEBUG, "[%s] received CGM stats:", __func__);
                    log_entry(LEVEL_DEBUG, "[%s] \tpool index: %d", __func__, info->pool_index);
                    log_entry(LEVEL_DEBUG, "[%s] \taccepted: %lld", __func__, info->accepted);
                    log_entry(LEVEL_DEBUG, "[%s] \trejected: %lld", __func__, info->rejected);
                    char status[MAX_POOL_STATUS_LEN];
                    switch (info->status)
                    {
                        case PS_ENABLED:
                            if (info->idle) {
                                log_entry(LEVEL_DEBUG, "[%s] \tstatus: DEAD", __func__);
                                strncpy(status, "Dead", MAX_POOL_STATUS_LEN);
                            } else {
                                log_entry(LEVEL_DEBUG, "[%s] \tstatus: ALIVE", __func__);
                                strncpy(status, "Alive", MAX_POOL_STATUS_LEN);
                            }
                        break;
                        case PS_DISABLED:
                        case PS_REJECTING:
                        default:
                            log_entry(LEVEL_DEBUG, "[%s] \tstatus:", __func__, psstat_to_str(info->status));
                            strncpy(status, psstat_to_str(info->status), MAX_POOL_STATUS_LEN);
                        break;
                    }
                    set_pool_info(info->pool_index, info->accepted, info->rejected, status);
                }
                break;

                case NOTIFICATION_SWUPDATE:
                {
                    tlv_ipc_notification_swupdate_t *info = (tlv_ipc_notification_swupdate_t *)change->value;
                    log_entry(LEVEL_DEBUG, "[%s] received SW update available:%s", __func__, info->latest_release);
                    set_latest_firmware_version(info->latest_release);
                    if (strlen(info->previous_release) > 0)
                        set_rollback_firmware_version(info->previous_release);
                }
                break;

                case NOTIFICATION_POOL_DIFF:
                {
                    tlv_ipc_notification_pool_difficulty_t *info = (tlv_ipc_notification_pool_difficulty_t *)change->value;
                    log_entry(LEVEL_INFO, "[%s] received pool difficulty change: %.2f", __func__, info->value);
                    set_device_stat_pool_difficulty(info->value);
                }
                break;

                case NOTIFICATION_BI_STATUS:
                {
                    tlv_ipc_notification_burnin_status_t *biinfo = (tlv_ipc_notification_burnin_status_t *)change->value;

                    log_entry(LEVEL_INFO, "[%s] received burnin status change: %s (%d hashboards)", __func__,
                        bistatus_to_str(biinfo->miner_status),
                        biinfo->total_hashing_boards);

                    set_burnin_status(biinfo);

                    if ((biinfo->miner_status == BI_PASSED) || (biinfo->miner_status == BI_FAILED))
                    {
                        int i;

                        log_entry(LEVEL_INFO, "[%s] burnin details:", __func__);
                        log_entry(LEVEL_INFO, "[%s] \tminer burnin status: %s:", __func__, bistatus_to_str(biinfo->miner_status));
                        for (i = 0; i < biinfo->total_hashing_boards; ++i)
                        {
                            log_entry(LEVEL_INFO, "[%s] \tboard %d type: %s:", __func__, biinfo->info[i].id, (biinfo->info[i].type == HB_DCR) ? "DCR":"SIA");
                            log_entry(LEVEL_INFO, "[%s] \tboard %d status: %s:", __func__, biinfo->info[i].id, bistatus_to_str(biinfo->info[i].board_status));
                            log_entry(LEVEL_INFO, "[%s] \tboard %d expected hash rate: %.2f:", __func__, biinfo->info[i].id, biinfo->info[i].expected_hash_rate);
                            log_entry(LEVEL_INFO, "[%s] \tboard %d actual hash rate: %.2f:", __func__, biinfo->info[i].id, biinfo->info[i].actual_hash_rate);
                        }
                    }

                }
                break;

            // TODO: Insert handle
            }
        }
        break;

        case ATTR_TYPE_IPC_HB_INFO:
        {
            tlv_ipc_hb_info_t *hbdata = (tlv_ipc_hb_info_t *)tlv->value;
            int i;

            for (i = 0; i < hbdata->count; ++i)
            {
                int id = hbdata->hbinfo[i].id;
                set_hb_info(id,
                            id,
                            hbdata->hbinfo[i].type,
                            hbdata->hbinfo[i].version,
                            hbdata->hbinfo[i].asic_diags);

                char version[MAX_FWVERSION_SIZE];
                get_hb_version(id, version);
                log_entry(LEVEL_DEBUG, "[%s] received HB info: id: %d, type: %s, version: %s",
                          __func__,
                          get_hb_id(id),
                          hbtype_to_str(get_hb_type(id)),
                          hbdata->hbinfo[i].version);
            }
            // Set the count once all the fields are set above
            set_hb_count(hbdata->count);
        }
        break;

        default:
            log_entry(LEVEL_ERROR, "[%s] received unknown TLV type from server:%d", __func__, tlv->type);
            return -1;
        break;
    }

    if (msg)
    {
        if (config->ipc_fd > 0)
        {
            int bytes = send(config->ipc_fd, msg, sizeof(msg_ipc_tlv_t) + msg->len, MSG_NOSIGNAL);
            if (bytes < 0)
                log_entry(LEVEL_ERROR, "[%s] send failed: %s", __func__, strerror(errno));
            else if (bytes == 0)
                log_entry(LEVEL_ERROR, "[%s] sent 0 bytes: %s", __func__, strerror(errno));
        }
        else
            log_entry(LEVEL_WARNING, "[%s] not connected, message will not be sent", __func__);
        free(msg);
    }

    return sizeof(msg_ipc_tlv_t) + tlv->len;
}

/***********************************************************
 * NAME: process_ipc_message
 * DESCRIPTION: process a message from IPC server
 *
 * IN:  server file descriptor
 * OUT: none
 ***********************************************************/
static int process_ipc_message(
    station_config_t *config
)
{
    int bytes_received = 0;
    char buf[MAX_RECV_LEN];
   
    memset(buf, 0, MAX_RECV_LEN);
    bytes_received = recv(config->ipc_fd, buf, MAX_RECV_LEN, 0);
    switch (bytes_received)
    {
        case -1:
        {
            log_entry(LEVEL_ERROR, "[%s] recv error: %s", __func__, strerror(errno));
            return -1;
        }
        break;
        
        case 0:
        {
            log_entry(LEVEL_WARNING, "[%s] far end closed connection", __func__);
            return 0;
        }
        break;
        
        default:
        {
            uint8_t *tmp_tlv;

            /* One or more messages received...process */
            msg_ipc_tlv_t *tlv = (msg_ipc_tlv_t *)buf;
            tmp_tlv = (uint8_t *)tlv;
            while (bytes_received > 0)
            {
                process_ipc_tlv(config, tlv);
                bytes_received -= (sizeof(msg_ipc_tlv_t) + tlv->len);
                if (bytes_received > 0)
                {
                    tmp_tlv += (sizeof(msg_ipc_tlv_t) + tlv->len); /* move to the next message */
                    tlv = (msg_ipc_tlv_t *)tmp_tlv;
                }
            }
        }
    } /* End switch (bytes_received) */
    return 1;
}

/***********************************************************
 * NAME: ipc_server_connect
 * DESCRIPTION: connect to the server (station manager)
 *
 * IN:  none
 * OUT: server file descriptor or -1 on error
 ***********************************************************/
int ipc_server_connect(void)
{
    int opt = 1;
    int fd;
    struct sockaddr_in server_socket;
    msg_ipc_tlv_t *msg;

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    /* Get a tcp file descriptor */
    fd = socket(PF_INET, SOCK_STREAM, 0);
    if (fd < 0)
    {
        log_entry(LEVEL_ERROR, "[%s] failed to create a socket", __func__);
        return -1;
    }

    /* set this so that we can rebind to it after a crash or a manual kill */
    if (setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt)) < 0)
    {
        log_entry(LEVEL_ERROR, "[%s] failed to set socket option: %s", __func__, strerror(errno));
        goto err;
    }

    /* Setup the tcp port information */
    memset(&server_socket, 0, sizeof(server_socket));
    server_socket.sin_family = AF_INET;
    server_socket.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
    server_socket.sin_port = htons(CONTROL_MGR_PORT);

    if (connect(fd, (struct sockaddr *)&server_socket, sizeof(server_socket)) < 0)
    {
        log_entry(LEVEL_ERROR, "[%s] failed to connect: %s", __func__, strerror(errno));
        goto err;
    }

    log_entry(LEVEL_INFO, "[%s] ipc server connection opened", __func__);

    /* Send identity */
    msg = build_identity();
    if (msg)
    {
        int bytes = send(fd, msg, sizeof(msg_ipc_tlv_t) + msg->len, MSG_NOSIGNAL);
        if (bytes < 0)
            log_entry(LEVEL_ERROR, "[%s] send failed: %s", __func__, strerror(errno));
        else if (bytes == 0)
            log_entry(LEVEL_ERROR, "[%s] sent 0 bytes: %s", __func__, strerror(errno));
        free(msg);
    }

    /* Send request for HB configuration */
    msg = build_simple_request(ATTR_TYPE_IPC_HB_INFO);
    if (msg)
    {
        int bytes = send(fd, msg, sizeof(msg_ipc_tlv_t) + msg->len, MSG_NOSIGNAL);
        if (bytes < 0)
            log_entry(LEVEL_ERROR, "[%s] send failed: %s", __func__, strerror(errno));
        else if (bytes == 0)
            log_entry(LEVEL_ERROR, "[%s] sent 0 bytes: %s", __func__, strerror(errno));
        free(msg);
    }

    /* Send notification registration */
    msg = build_registration();
    if (msg)
    {
        int bytes = send(fd, msg, sizeof(msg_ipc_tlv_t) + msg->len, MSG_NOSIGNAL);
        if (bytes < 0)
            log_entry(LEVEL_ERROR, "[%s] send failed: %s", __func__, strerror(errno));
        else if (bytes == 0)
            log_entry(LEVEL_ERROR, "[%s] sent 0 bytes: %s", __func__, strerror(errno));
        free(msg);
    }

    return fd;
err:
    close(fd);
    return -1;
}

/***********************************************************
 * NAME: ipc_server_connect
 * DESCRIPTION: connect to the server (station manager)
 *
 * IN:  none
 * OUT: server file descriptor or -1 on error
 ***********************************************************/
void ipc_server_disconnect(
    station_config_t *config
)
{
    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    if (config->ipc_fd > 0)
    {
        close(config->ipc_fd);
        config->ipc_fd = -1;
        log_entry(LEVEL_INFO, "[%s] ipc server connection closed", __func__);
    }
}

/***********************************************************
 * NAME: process_ipc_signal
 * DESCRIPTION: 
 *
 * IN:  generated signal number
 * OUT: none
 ***********************************************************/
static void process_ipc_signal(int sig)
{
    switch(sig)
    {
        case SIGTERM:
            log_entry(LEVEL_INFO, "[%s] %s thread exiting...", __func__, THREAD_NAME);
            ipc_server_disconnect(config);
        break;
    }
}

/***********************************************************
 * NAME: ipc_thread
 * DESCRIPTION: 
 *
 * IN:  data passed during thread creation
 * OUT: none
 ***********************************************************/
static void *ipc_thread(
    void *data
)
{
    sigset_t mask;
    sigset_t orig_mask;
    struct sigaction act;

    log_entry(LEVEL_DEBUG, "[%s] %s thread started", __func__, THREAD_NAME);

    pthread_setname_np(pthread_self(), THREAD_NAME);

    config = data;
    memset (&act, 0, sizeof(act));
    act.sa_handler = process_ipc_signal;
    /* This thread should shut down on SIGTERM. */
    if (sigaction(SIGTERM, &act, NULL))
        log_entry(LEVEL_ERROR, "[%s] sigaction failed: %s", __func__, strerror(errno));

    sigemptyset(&mask);
    sigaddset(&mask, SIGTERM);

    if (sigprocmask(SIG_BLOCK, &mask, &orig_mask) < 0)
        log_entry(LEVEL_ERROR, "[%s] sigprocmask failed: %s", __func__, strerror(errno));

    config->ipc_fd = ipc_server_connect();

    while(true)
    {
        int status;
        int track_fd;
        fd_set socket_set;
        struct timeval timeout;

        track_fd = 0;
        FD_ZERO(&socket_set);
        memset(&timeout, 0, sizeof(timeout));

        if (config->ipc_fd > 0)
        {
            FD_SET(config->ipc_fd, &socket_set);
            if (config->ipc_fd > track_fd)
                track_fd = config->ipc_fd;
        }
        else
        {
            timeout.tv_sec = 30;
            timeout.tv_usec = 0;
        }

        /* wait for activity on the file descriptors */
        if (timeout.tv_sec == 0)
            status = select(track_fd + 1, &socket_set, NULL, NULL, NULL);
        else
            status = select(track_fd + 1, &socket_set, NULL, NULL, &timeout);
        if (status < 0)
        {
            log_entry(LEVEL_DEBUG, "[%s] select error: %s", __func__, strerror(errno));
            continue;
        }
        else if (status == 0)
        {
            log_entry(LEVEL_DEBUG, "[%s] select timeout", __func__);
            /*
             * If we're not connected to the control manager then
             * attempt to reconnect.
             */
            if (config->ipc_fd < 0)
                config->ipc_fd = ipc_server_connect();
            continue;
        }

        if (FD_ISSET(config->ipc_fd, &socket_set))
        {
            int status = process_ipc_message(config);
            if (status <= 0)
                ipc_server_disconnect(config);
        }
    }
    return NULL;
}

void ipc_init(
    station_config_t *config
)
{
    int rc;

    rc = pthread_create(&ipc_thread_id, NULL, ipc_thread, config);
    if (rc < 0)
        log_entry(LEVEL_ERROR, "[%s] failed to create thread: %s", __func__, strerror(errno));
        
}

void ipc_shutdown(
    station_config_t *config
)
{
    pthread_kill(ipc_thread_id, SIGTERM);
    close(config->ipc_fd);
}

// Wrappers to expose actions for apiserver to call
void send_software_update_check(bool autoupdate) {
    log_entry(LEVEL_DEBUG, "Check for software update");

    if (config->ipc_fd)
    {
        /* Build and send a request with the specified type */
        msg_ipc_tlv_t *msg = build_update_check(autoupdate);
        if (msg)
        {
            int bytes = send(config->ipc_fd, msg, sizeof(msg_ipc_tlv_t) + msg->len, MSG_NOSIGNAL);
            if (bytes < 0)
                log_entry(LEVEL_ERROR, "[%s] send failed: %s", __func__, strerror(errno));
            else if (bytes == 0)
                log_entry(LEVEL_ERROR, "[%s] sent 0 bytes: %s", __func__, strerror(errno));
            free(msg);
        }
    }
}

void send_software_update() {
    log_entry(LEVEL_DEBUG, "Starting a software update");
    send_simple_request(ATTR_TYPE_IPC_UPDATE);
}

void send_software_rollback() {
    log_entry(LEVEL_DEBUG, "Starting a software rollback");
    send_simple_request(ATTR_TYPE_IPC_ROLLBACK);
}

void send_factory_reset() {
    log_entry(LEVEL_DEBUG, "Perform a factory reset");
    send_simple_request(ATTR_TYPE_IPC_FACTORY_RESET);
}

void send_cgminer_reset() {
    log_entry(LEVEL_DEBUG, "Send cgminer reset");
    send_simple_request(ATTR_TYPE_IPC_RESTART_CGM);
}

void send_set_fan_speed_percent(uint8_t fan_speed_percent) {
    log_entry(LEVEL_DEBUG, "Set fan speed: %d%%", fan_speed_percent);

    if (config->ipc_fd)
    {
        /* Build and send a request with the specified type */
        msg_ipc_tlv_t *msg = build_set_fan_speed(fan_speed_percent);
        if (msg)
        {
            int bytes = send(config->ipc_fd, msg, sizeof(msg_ipc_tlv_t) + msg->len, MSG_NOSIGNAL);
            if (bytes < 0)
                log_entry(LEVEL_ERROR, "[%s] send failed: %s", __func__, strerror(errno));
            else if (bytes == 0)
                log_entry(LEVEL_ERROR, "[%s] sent 0 bytes: %s", __func__, strerror(errno));
            free(msg);
        }
    }
}

void send_set_auto_fan_speed_percent(uint8_t fan_speed_percent) {
    log_entry(LEVEL_DEBUG, "Set auto fan speed: %d%%", fan_speed_percent);

    if (config->ipc_fd)
    {
        /* Build and send a request with the specified type */
        msg_ipc_tlv_t *msg = build_set_auto_fan_speed(fan_speed_percent);
        if (msg)
        {
            int bytes = send(config->ipc_fd, msg, sizeof(msg_ipc_tlv_t) + msg->len, MSG_NOSIGNAL);
            if (bytes < 0)
                log_entry(LEVEL_ERROR, "[%s] send failed: %s", __func__, strerror(errno));
            else if (bytes == 0)
                log_entry(LEVEL_ERROR, "[%s] sent 0 bytes: %s", __func__, strerror(errno));
            free(msg);
        }
    }
}

void send_set_auto_fan_temp(float temp) {
    log_entry(LEVEL_DEBUG, "Set fan temp: %f%%", temp);

    if (config->ipc_fd)
    {
        /* Build and send a request with the specified type */
        msg_ipc_tlv_t *msg = build_set_auto_fan_temp(temp);
        if (msg)
        {
            int bytes = send(config->ipc_fd, msg, sizeof(msg_ipc_tlv_t) + msg->len, MSG_NOSIGNAL);
            if (bytes < 0)
                log_entry(LEVEL_ERROR, "[%s] send failed: %s", __func__, strerror(errno));
            else if (bytes == 0)
                log_entry(LEVEL_ERROR, "[%s] sent 0 bytes: %s", __func__, strerror(errno));
            free(msg);
        }
    }
}

void send_set_auto_fan_mode(uint8_t mode) {
    log_entry(LEVEL_DEBUG, "Set fan temp: %d%%", mode);

    if (config->ipc_fd)
    {
        /* Build and send a request with the specified type */
        msg_ipc_tlv_t *msg = build_set_auto_fan_mode(mode);
        if (msg)
        {
            int bytes = send(config->ipc_fd, msg, sizeof(msg_ipc_tlv_t) + msg->len, MSG_NOSIGNAL);
            if (bytes < 0)
                log_entry(LEVEL_ERROR, "[%s] send failed: %s", __func__, strerror(errno));
            else if (bytes == 0)
                log_entry(LEVEL_ERROR, "[%s] sent 0 bytes: %s", __func__, strerror(errno));
            free(msg);
        }
    }
}

void send_set_performance_number(uint8_t hb_index, uint32_t performance_number) {
    log_entry(LEVEL_DEBUG, "Send set performance number hb[%u] =  %u", hb_index, performance_number);

    if (config->ipc_fd)
    {
        /* Build and send a request with the specified type */
        msg_ipc_tlv_t *msg = build_set_performance_number(hb_index, performance_number);
        if (msg)
        {
            int bytes = send(config->ipc_fd, msg, sizeof(msg_ipc_tlv_t) + msg->len, MSG_NOSIGNAL);
            if (bytes < 0)
                log_entry(LEVEL_ERROR, "[%s] send failed: %s", __func__, strerror(errno));
            else if (bytes == 0)
                log_entry(LEVEL_ERROR, "[%s] sent 0 bytes: %s", __func__, strerror(errno));
            free(msg);
        }
    }
}

void send_set_clock(uint8_t hb_index, uint8_t divider, int8_t bias) {
    log_entry(LEVEL_DEBUG, "Send set clock hb[%u]: divider: %d, bias: %d", hb_index, divider, bias);

    if (config->ipc_fd)
    {
        /* Build and send a request with the specified type */
        msg_ipc_tlv_t *msg = build_set_clock(hb_index, divider, bias);
        if (msg)
        {
            int bytes = send(config->ipc_fd, msg, sizeof(msg_ipc_tlv_t) + msg->len, MSG_NOSIGNAL);
            if (bytes < 0)
                log_entry(LEVEL_ERROR, "[%s] send failed: %s", __func__, strerror(errno));
            else if (bytes == 0)
                log_entry(LEVEL_ERROR, "[%s] sent 0 bytes: %s", __func__, strerror(errno));
            free(msg);
        }
    }
}

void send_set_chip_bias_offset(uint8_t hb_index, uint8_t chip, int8_t offset) {
    log_entry(LEVEL_DEBUG, "Send set set bias offset hb[%u]: chip: %d, offset: %d", hb_index, chip, offset);

    if (config->ipc_fd)
    {
        /* Build and send a request with the specified type */
        msg_ipc_tlv_t *msg = build_set_chip_bias_offset(hb_index, chip, offset);
        if (msg)
        {
            int bytes = send(config->ipc_fd, msg, sizeof(msg_ipc_tlv_t) + msg->len, MSG_NOSIGNAL);
            if (bytes < 0)
                log_entry(LEVEL_ERROR, "[%s] send failed: %s", __func__, strerror(errno));
            else if (bytes == 0)
                log_entry(LEVEL_ERROR, "[%s] sent 0 bytes: %s", __func__, strerror(errno));
            free(msg);
        }
    }
}

void send_set_reboot_period(uint32_t reboot_period) {
    log_entry(LEVEL_DEBUG, "Set reboot intervalt o %d secs", reboot_period);

    if (config->ipc_fd)
    {
        /* Build and send a request with the specified type */
        msg_ipc_tlv_t *msg = build_set_reboot_period(reboot_period);
        if (msg)
        {
            int bytes = send(config->ipc_fd, msg, sizeof(msg_ipc_tlv_t) + msg->len, MSG_NOSIGNAL);
            if (bytes < 0)
                log_entry(LEVEL_ERROR, "[%s] send failed: %s", __func__, strerror(errno));
            else if (bytes == 0)
                log_entry(LEVEL_ERROR, "[%s] sent 0 bytes: %s", __func__, strerror(errno));
            free(msg);
        }
    }
}

void send_clear_tuning() {
    log_entry(LEVEL_DEBUG, "Clear the tuning parameters");
    send_simple_request(ATTR_TYPE_IPC_CLEAR_PERFORMANCE);
}

void send_save_performance() {
    log_entry(LEVEL_DEBUG, "Save the performance parameters");
    send_simple_request(ATTR_TYPE_IPC_SAVE_PERFORMANCE);
}

void send_set_alternating_leds(bool enabled) {
    log_entry(LEVEL_DEBUG, "Set alternating LEDs %s", (enabled ? "on":"off"));
    if (config->ipc_fd)
    {
        /* Build and send a request with the specified type */
        msg_ipc_tlv_t *msg = build_set_alternating_leds(enabled);
        if (msg)
        {
            int bytes = send(config->ipc_fd, msg, sizeof(msg_ipc_tlv_t) + msg->len, MSG_NOSIGNAL);
            if (bytes < 0)
                log_entry(LEVEL_ERROR, "[%s] send failed: %s", __func__, strerror(errno));
            else if (bytes == 0)
                log_entry(LEVEL_ERROR, "[%s] sent 0 bytes: %s", __func__, strerror(errno));
            free(msg);
        }
    }
}
