// Copyright 2018 Obelisk Inc.

#include <boost/bind.hpp>
#include <sys/reboot.h>
#include <unistd.h>
#include <pthread.h>

#include "common.h"
#include "Handlers.h"
#include "CgMinerTypes.h"
#include "Crow.h" // Get the log levels
#include "HttpStatusCodes.h"
#include "SafeQueue.h"
#include "base64.h"
#include "utils.h"
#include "config.h"
#include "stats.h"
#include "logger.h"
#include "ipc.h"

using namespace std;
using namespace crow;

// TODO: Move these to an appropriate header
#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))

// This is the queue we use to send cgminer requests
extern SafeQueue<CgMiner::Request> gRequestQueue;

void sendError(string error, int code, crow::response &resp)
{
  json::wvalue jsonErr = json::load("{}");
  jsonErr["error"] = error;
  string jsonStr = json::dump(jsonErr);
  resp.code = code;
  resp.write(jsonStr);
  resp.end();
}

void sendJson(string json, crow::response &resp)
{
  resp.add_header("Content-Type", "application/json");
  resp.code = 200;
  resp.write(json);
  resp.end();
}

void sendCgMinerCmds(CgMiner::Commands commands, CgMiner::RequestCallback callback)
{
  CgMiner::Request cgMinerReq{commands, callback};
  gRequestQueue.enqueue(cgMinerReq);
}

void sendCgMinerCmd(string command, string param, CgMiner::RequestCallback callback)
{
  CgMiner::Commands cmds;
  cmds.push_back({command, param});

  sendCgMinerCmds(cmds, callback);
}

bool isCgMinerError(json::rvalue json) { return json["STATUS"][0]["STATUS"].s() == "E"; }

// TODO: This is stupid!
json::rvalue to_rvalue(json::wvalue &v) { return json::load(json::dump(v)); }

// Find the index of the entry with the specified name
// the jsonArr must be an array
int indexOfEntryWithId(json::rvalue jsonArr, string id)
{
  if (jsonArr.t() == json::type::List)
  {
    for (unsigned int i = 0; i < jsonArr.size(); i++)
    {
      json::rvalue entry = jsonArr[i];
      if (entry.has(id))
      {
        return i;
      }
    }
  }
  return -1;
}

int findIndexByFieldValue(json::rvalue jsonArr, string fieldName, int value)
{
  if (jsonArr.t() == json::type::List)
  {
    for (unsigned int i = 0; i < jsonArr.size(); i++)
    {
      json::rvalue entry = jsonArr[i];
      if (entry.has(fieldName) && entry[fieldName].i() == value)
      {
        return i;
      }
    }
  }
  return -1;
}

json::rvalue makeSystemInfoEntry(string name, string value)
{
  json::wvalue obj = json::load("{}");
  obj["name"] = name;
  obj["value"] = value;
  return to_rvalue(obj);
}

// Structure to store data from cgminer

#define MAX_HASHRATE_HISTORY_ENTRIES (60) // We keep one hour of history for now
hashrate_t hashrate_history_secs[MAX_HASHRATE_HISTORY_ENTRIES];
int num_hashrate_entries = 0;

// Timer callback to poll for updated hashrates
void pollForHashrate()
{
  // See if we need to move the old data first
  int entry_index = num_hashrate_entries;
  if (num_hashrate_entries >= MAX_HASHRATE_HISTORY_ENTRIES)
  {
    // Need to slide the other entries down first, then set new data into the last entry
    memmove(hashrate_history_secs, &hashrate_history_secs[1],
            sizeof(hashrate_t) * (MAX_HASHRATE_HISTORY_ENTRIES - 1));
    entry_index = MAX_HASHRATE_HISTORY_ENTRIES - 1;
  }
  else
  {
    num_hashrate_entries++;
  }

  // Get the hashrates
  time_t now = time(0);
  hashrate_history_secs[entry_index].time = now;
  for (int i = 0; i < MAX_HASHING_BOARDS; i++)
  {
    if (get_hb_id(i) != 255)
    {
      uint64_t hashrate = get_hb_hashrate_5min(i);
      hashrate_history_secs[entry_index].hashrates[i] = hashrate;
    }
  }
}

#define MIN_VALID_EXHAUST_TEMP_SETPOINT 50
#define MAX_VALID_EXHAUST_TEMP_SETPOINT 75

#define MIN_ADJUSTMENT -5
#define MAX_ADJUSTMENT 5

// Very simple fan control loop

// If system exhaust_fan_setpoint is between 50-80, then the loop runs,
// otherwise the fanspeed is used.
float last_hb_temp[2] = {100.0, 100.0};

void fanControl()
{
  log_entry(LEVEL_DEBUG, "fanControl() =============================================================");

  uint8_t num_hashboards = get_hb_count();
  if (num_hashboards > 2)
  {
    // No fans if an immersion unit, so can skip the rest
    return;
  }

  uint8_t curr_fan_speed_percent = get_system_fan_speed_percent();

  // See if any board is tuning - can happen that only one board is tuning, for example, if someone
  // inserts a new hashboard in the second slot to go alongside an already-tuned board.
  bool is_tuning = false;
  for (uint8_t h = 0; h < num_hashboards; h++)
  {
    if (get_hb_is_tuning(h))
    {
      is_tuning = true;
    }
  }

  if (is_tuning)
  {
    // Force fan speed to 85 during tuning
    if (curr_fan_speed_percent != TUNING_FAN_SPEED) {
      send_set_auto_fan_speed_percent(TUNING_FAN_SPEED);
    }
    return;
  }

  uint8_t fan_mode = get_system_fan_mode();
  if (fan_mode == 1)
  {
    return;
  }

  // Automatic fan speed control
  float temp_setpoint = (float)get_system_exhaust_temp_setpoint();

  log_entry(LEVEL_DEBUG, "setpoint: %0.fC", temp_setpoint);
  int8_t adjustments[2];

  for (uint32_t h = 0; h < num_hashboards; h++)
  {
    float curr_temp = get_hb_exhaust_temp(h);
    if (last_hb_temp[h] == 0)
    {
      // Ensure delta is zero first time through
      last_hb_temp[h] = curr_temp;
    }

    float temp_delta = curr_temp - last_hb_temp[h];
    log_entry(LEVEL_DEBUG, "hb[%d] curr_temp: %0.2fC  temp_delta: %0.2fC", h, curr_temp, temp_delta);

    // if board is +/- 0.5 degrees of the setpoint, do nothing
    if (curr_temp >= temp_setpoint - 1.0 && curr_temp <= temp_setpoint + 1.0)
    {
      // Do nothing
      adjustments[h] = 0;
    }
    else if (curr_temp < temp_setpoint)
    {
      // Board is too cool, we can slow down the fans to warm it up
      // We use the difference in degrees to decide how much to adjust the fan.
      if (temp_delta > 0.1)
      {
        // Moving in the right direction, so just wait for the fan change to settle again
        adjustments[h] = 0;
      }
      else
      {
        adjustments[h] = -(int8_t)(temp_setpoint - curr_temp);
      }
    }
    else
    {
      // Board is too warm, we can speed up the fans to cool it down.
      // We use the difference in degrees to decide how much to adjust the fan.
      if (temp_delta < -0.1)
      {
        // Moving in the right direction, so just wait for the fan change to settle again
        adjustments[h] = 0;
      }
      else
      {
        adjustments[h] = (int8_t)(curr_temp - temp_setpoint) * 2;
      }
    }

    // Save for calculating next delta
    last_hb_temp[h] = curr_temp;
  }

  int8_t adjustment = MIN_ADJUSTMENT;
  for (uint32_t h = 0; h < num_hashboards; h++)
  {
    // Give precedence to more cooling if the boards disagree on what to do
    // TODO: May need this to be smarter.
    adjustment = MAX(adjustment, adjustments[h]);
  }

  // Restrict max adjustment so it doesn't ramp too fast when temp setpoint is
  // first reached on the way up, otherwise it cools the chips too quickly and
  // they do not like that.
  adjustment = MIN(MAX(adjustment, MIN_ADJUSTMENT), MAX_ADJUSTMENT);

  // Restrict to valid range
  uint8_t new_fan_speed_percent = MIN(MAX(curr_fan_speed_percent + adjustment, MIN_FAN_SPEED), MAX_FAN_SPEED);

  // Don't send anything unless there is a change
  if (new_fan_speed_percent != curr_fan_speed_percent)
  {
    set_system_fan_speed_percent(new_fan_speed_percent);
    send_set_auto_fan_speed_percent(new_fan_speed_percent);
  }
  else
  {
    log_entry(LEVEL_DEBUG, "No change to fan speed: %u%%", new_fan_speed_percent);
  }
  log_entry(LEVEL_DEBUG, "=============================================================");
}

uint64_t periodicTicks = 0;
void runPeriodicActions()
{
  periodicTicks++;

  if (periodicTicks % 10 == 0)
  {
    fanControl();
  }

  if (periodicTicks % 60 == 0)
  {
    pollForHashrate();
  }
}

//--------------------------------------------------------------------------------------------------
// INVENTORY HANDLERS
//--------------------------------------------------------------------------------------------------

void getInventoryVersions(string path, query_string &urlParams, const crow::request &req,
                          crow::response &resp)
{
  station_info_t station_info;
  get_station_info(&station_info);

  char latest_version[MAX_STATIONRELEASE_LEN];
  get_latest_firmware_version(latest_version);

  char rollback_version[MAX_STATIONRELEASE_LEN];
  get_rollback_firmware_version(rollback_version);

  json::wvalue jsonResp = json::load("{}");
  jsonResp["cgminerVersion"] = "4.10.0";
  jsonResp["activeFirmwareVersion"] = station_info.release;
  jsonResp["latestFirmwareVersion"] = latest_version;
  jsonResp["rollbackFirmwareVersion"] = rollback_version;

  sendJson(json::dump(jsonResp), resp);
}

void getInventorySystem(string path, query_string &urlParams, const crow::request &req,
                        crow::response &resp)
{
  string osName = getOSName();
  string osVersion = getOSVersion();
  json::wvalue jsonResp = json::load("{}");
  jsonResp["osName"] = osName;
  jsonResp["osVersion"] = osVersion;
  sendJson(json::dump(jsonResp), resp);
}

void getInventoryAsicCount(string path, query_string &urlParams, const crow::request &req,
                           crow::response &resp)
{
  sendCgMinerCmd("asccount", "",
                 [&](CgMiner::Response cgMinerResp) { sendJson(cgMinerResp.json, resp); });
}

void getInventoryDevices(string path, query_string &urlParams, const crow::request &req,
                         crow::response &resp)
{
  sendCgMinerCmd("devs", "",
                 [&](CgMiner::Response cgMinerResp) { sendJson(cgMinerResp.json, resp); });
}

//--------------------------------------------------------------------------------------------------
// CONFIG GET HANDLERS
//--------------------------------------------------------------------------------------------------

void getConfigPools(string path, query_string &urlParams, const crow::request &req,
                    crow::response &resp)
{
  json::rvalue conf = readCgMinerConfig();

  if (!conf.has("pools"))
  {
    json::rvalue result = json::load("[]");
    sendJson(json::dump(result), resp);
    return;
  }

  json::rvalue pools = conf["pools"];
  json::wvalue result = json::load("[]");
  for (unsigned int i = 0; i < pools.size(); i++)
  {
    json::rvalue entry = pools[i];
    json::wvalue resEntry = json::load("{}");
    resEntry["worker"] = entry["user"].s();
    resEntry["url"] = entry["url"].s();
    resEntry["password"] = entry["pass"].s();
    result[i] = to_rvalue(resEntry);
  }
  sendJson(json::dump(result), resp);
}

void getConfigNetwork(string path, query_string &urlParams, const crow::request &req,
                      crow::response &resp)
{
  json::wvalue jsonResp = json::load("{}");
  jsonResp["hostname"] = getHostname();
  jsonResp["ipAddress"] = getIpV4(INTF_NAME);
  jsonResp["subnetMask"] = getSubnetMaskV4(INTF_NAME);
  jsonResp["gateway"] = getDefaultGateway(INTF_NAME);
  jsonResp["dnsServer"] = getDns(INTF_NAME);
  jsonResp["dhcpEnabled"] = getDhcpEnabled(INTF_NAME);
  jsonResp["macAddress"] = getMACAddr(INTF_NAME);
  string jsonStr = json::dump(jsonResp);

  sendJson(jsonStr, resp);
}

void getConfigSystem(string path, query_string &urlParams, const crow::request &req,
                     crow::response &resp)
{
  json::wvalue jsonResp = json::load("{}");
  jsonResp["timezone"] = getTimezone();
  string jsonStr = json::dump(jsonResp);

  sendJson(jsonStr, resp);
}

void getConfigMining(string path, query_string &urlParams, const crow::request &req,
                     crow::response &resp)
{
  json::wvalue jsonResp = json::load("{}");
  jsonResp["rebootIntervalSecs"] = get_system_reboot_period();
  jsonResp["fanMode"] = get_system_fan_mode();
  jsonResp["fanSpeedPercent"] = get_system_fan_speed_percent();
  jsonResp["exhaustSetpoint"] = get_system_exhaust_temp_setpoint();

  /*
export interface ChipMiningConfig {
  biasOffset: number
}

export interface BoardMiningConfig {
  id: number
  performanceNumber: number
  clockDivider: number
  clockBias: number
  chipConfig: ChipMiningConfig[]
}

export interface MiningConfig {
  rebootIntervalSecs: number
  fanMode: number
  fanSpeedPercent: number
  exhaustSetpoint: number
  hbConfig: BoardMiningConfig[]
}*/

  // Array of performance numbers from hashboards
  json::wvalue perfArr = json::load("[]");
  int index = 0;
  for (int i = 0; i < MAX_HASHING_BOARDS; i++)
  {
    uint8_t id = get_hb_id(i);
    if (id != 255)
    {
      json::wvalue entry = json::load("{}");
      entry["id"] = id + 1;
      entry["performanceNumber"] = get_hb_performance_number(id);
      entry["clockDivider"] =get_hb_clock_divider(id);
      entry["clockBias"] = get_hb_clock_bias(id);

      json::wvalue chips = json::load("[]");
      for (int chip=0; chip< NUM_CHIPS_PER_HASHBOARD; chip++) {
        json::wvalue chipEntry = json::load("{}");
        chipEntry["biasOffset"] = get_chip_bias_offset(id, chip);

        chips[chip] = to_rvalue(chipEntry);
      }
      entry["chipConfig"] = to_rvalue(chips);

      perfArr[index++] = to_rvalue(entry);
    }
  }
  jsonResp["hbConfig"] = to_rvalue(perfArr);

  string jsonStr = json::dump(jsonResp);

  sendJson(jsonStr, resp);
}

void getConfigConfig(string path, query_string &urlParams, const crow::request &req,
                     crow::response &resp)
{
  sendCgMinerCmd("config", "",
                 [&](CgMiner::Response cgMinerResp) { sendJson(cgMinerResp.json, resp); });
}

//--------------------------------------------------------------------------------------------------
// CONFIG SET HANDLERS
//--------------------------------------------------------------------------------------------------
void setConfigPools(string path, json::rvalue &args, const crow::request &req,
                    crow::response &resp)
{
  if (args.t() != json::type::List)
  {
    resp.code = HttpStatus_BadRequest;
    resp.end();
    return;
  }

  // Read .cgminer/cgminer.conf
  json::rvalue conf = readCgMinerConfig();
  // Create a wvalue from the rvalue
  json::wvalue newConf;
  json::wvalue newPools = json::load("[]");
  try
  {
    newConf = conf;
  }
  catch (...)
  {
    newConf = json::load("{}");
  }

  // Build up new pools entries as an array
  int numEntries = 0;
  for (unsigned int i = 0; i < args.size(); i++)
  {
    json::rvalue poolEntry = args[i];
    string url = poolEntry["url"].s();
    string user = poolEntry["worker"].s();
    string pass = poolEntry["password"].s();

    // Don't include entries that have empty URLs, because CGminer will crash, of course
    if (url.length() != 0)
    {
      json::wvalue entry = json::load("{}");
      entry["url"] = url;
      entry["user"] = user;
      entry["pass"] = pass;
      newPools[numEntries] = to_rvalue(entry);
      numEntries++;
    }
  }

  // Set new pools entry into the wvalue
  newConf["pools"] = to_rvalue(newPools);

  // Write out config file
  writeCgMinerConfig(newConf);

  CROW_LOG_ERROR << "setConfigPools() - Resetting cgminer!";
  sendCgMinerCmd("quit", "", [&](CgMiner::Response cgMinerResp) {});
  resp.end();
  send_cgminer_reset();

  backupConfigData();
}

void setConfigNetwork(string path, json::rvalue &args, const crow::request &req,
                      crow::response &resp)
{
  if (args.t() != json::type::Object)
  {
    resp.code = HttpStatus_BadRequest;
    resp.end();
    return;
  }

  bool dhcpEnabled = args["dhcpEnabled"].b();
  string hostname = args["hostname"].s();
  string ipAddress = args["ipAddress"].s();
  string subnetMask = args["subnetMask"].s();
  string defaultGateway = args["gateway"].s();
  string dnsServer = args["dnsServer"].s();

  if (!setNetworkInfo(INTF_NAME, hostname, ipAddress, subnetMask, defaultGateway, dnsServer,
                      dhcpEnabled))
  {
    resp.code = HttpStatus_BadRequest;
    resp.end();
    return;
  }

  resp.end();
  backupConfigData();
}

void setConfigSystem(string path, json::rvalue &args, const crow::request &req,
                     crow::response &resp)
{
  if (args.t() != json::type::Object)
  {
    resp.code = HttpStatus_BadRequest;
    resp.end();
    return;
  }

  string timezone = args["timezone"].s();
  setTimezone(timezone);

  resp.end();
  backupConfigData();
}

void setConfigMining(string path, json::rvalue &args, const crow::request &req,
                     crow::response &resp)
{
  if (args.t() != json::type::Object)
  {
    resp.code = HttpStatus_BadRequest;
    resp.end();
    return;
  }

  // Set the values locally and then send them remotely
  uint8_t fan_mode = args["fanMode"].i();
  uint8_t fan_speed = args["fanSpeedPercent"].i();
  float fan_temp = args["exhaustSetpoint"].i();
  uint32_t reboot_period = args["rebootIntervalSecs"].i();

  if (fan_mode != get_system_fan_mode())
  {
    set_system_fan_mode(fan_mode);
    send_set_auto_fan_mode(fan_mode);
  }

  if (fan_temp != get_system_exhaust_temp_setpoint())
  {
    set_system_exhaust_temp_setpoint(fan_temp);
    send_set_auto_fan_temp(fan_temp);
  }

  if (fan_speed != get_system_fan_speed_percent())
  {
    set_system_fan_speed_percent(fan_speed);
    if (fan_mode == 1)
      send_set_fan_speed_percent(fan_speed);
  }

  if (reboot_period != get_system_reboot_period())
  {
    set_system_reboot_period(reboot_period);
    send_set_reboot_period(reboot_period);
  }

  json::rvalue hbConfig = args["hbConfig"];
  for (uint8_t index = 0; index < hbConfig.size(); index++)
  {
    uint8_t hb_index = hbConfig[index]["id"].i() - 1;
    if (hb_index < 0 || hb_index >= MAX_HASHING_BOARDS) {
      continue;
    }

    uint16_t performance_number = hbConfig[index]["performanceNumber"].i();
    if (performance_number != get_hb_performance_number(hb_index))
    {
      // set_hb_performance_number(hb_index, performance_number);
      send_set_performance_number(hb_index, performance_number);
    }

    // Clock/Bias
    uint8_t clock_divider = hbConfig[index]["clockDivider"].i();
    int8_t clock_bias = hbConfig[index]["clockBias"].i();
    CROW_LOG_ERROR << "setConfigMining() - HB[" << to_string(hb_index) << "] received clockDivider=" << to_string(clock_divider) << " clockBias=" << to_string(clock_bias);
    CROW_LOG_ERROR << "setConfigMining() - HB[" << to_string(hb_index) << "] current  clockDivider=" << to_string(get_hb_clock_divider(hb_index)) << " clockBias=" << to_string(get_hb_clock_bias(hb_index));

    if ((clock_divider != get_hb_clock_divider(hb_index)) || (clock_bias != get_hb_clock_bias(hb_index))) {
      // set_hb_clock(hb_index, clock_divider, clock_bias);
      CROW_LOG_ERROR << "HB[" << to_string(hb_index) << "] Clocks are different!";
      send_set_clock(hb_index, clock_divider, clock_bias);
    }

    // For each chip, check the bias offset
    json::rvalue chipConfig = hbConfig[index]["chipConfig"];
    uint8_t count = MIN(NUM_CHIPS_PER_HASHBOARD, chipConfig.size());
    for (uint8_t chip_index = 0; chip_index < count; chip_index++) {
      int8_t bias_offset = chipConfig[chip_index]["biasOffset"].i();

      if (bias_offset != get_chip_bias_offset(hb_index, chip_index)) {
        // set_chip_bias_offset(hb_index, chip_index, bias_offset);
        send_set_chip_bias_offset(hb_index, chip_index, bias_offset);
      }
    }
  }

  // Send one "save performance" request at the end - previously it was taking forever because it saved after
  // each clock, bias or bias offset request was sent, and this included both writing out the whole file, 
  // and backing up the config file, which required remounting the file system as read/write, copying the
  // file and then remounting the file system as read only.  This all took a looong time.
  bool saveHbConfig = false;  // TODO: Make this true by default so UI saves are still done.
  if (args.has("saveHbConfig")) {
    saveHbConfig = args["saveHbConfig"].b();
  }

  if (saveHbConfig) {
    send_save_performance();
  }

  resp.end();
}

//--------------------------------------------------------------------------------------------------
// STATUS HANDLERS (CGMINER)
//--------------------------------------------------------------------------------------------------
void getStatusDashboard(string path, query_string &urlParams, const crow::request &req,
                        crow::response &resp)
{
  // Prepare our JSON response outline
  json::wvalue jsonResp = json::load("{}");
  json::wvalue hashrateArr = json::load("[]");
  json::wvalue hashboardArr = json::load("[]");
  json::wvalue poolArr = json::load("[]");
  json::wvalue systemArr = json::load("[]");

  // Get hashboard info and encode it
  uint8_t hb = 0;
  for (int i = 0; i < MAX_HASHING_BOARDS; i++)
  {
    if (get_hb_id(i) != 255)
    {
      json::wvalue entry = json::load("{}");

      char hb_version[MAX_FWVERSION_SIZE];
      get_hb_version(i, hb_version);

      entry["boardNum"] = i + 1;
      entry["status"] = get_hb_is_tuning(i) ? "Autotuning" : "Alive";
      entry["firmwareVersion"] = hb_version;
      entry["intakeTemp"] = formatFloat(get_hb_intake_temp(i), 1);
      entry["exhaustTemp"] = formatFloat(get_hb_exhaust_temp(i), 1);
      entry["exhaustTempSetpoint"] = formatFloat(get_system_exhaust_temp_setpoint(), 1);
      
      entry["hashrate1min"] = formatFloat(get_hb_hashrate_1min(i), 1);
      entry["hashrate5min"] = formatFloat(get_hb_hashrate_5min(i), 1);
      entry["hashrate15min"] = formatFloat(get_hb_hashrate_15min(i), 1);
      
      entry["goodNonces"] = get_hb_stat_good_nonces(i);
      entry["totalNonces"] = get_hb_stat_total_nonces(i);
      
      entry["performanceNumber"] = get_hb_performance_number(i);
      entry["clockDivider"] = get_hb_clock_divider(i);
      entry["clockBias"] = get_hb_clock_bias(i);
      entry["voltage"] = formatFloat(get_hb_voltage(i), 1);
      entry["current"] = formatFloat(get_hb_current(i), 1);

      entry["isTuning"] = get_hb_is_tuning(i);
      entry["noiseCounter"] = get_hb_noise_counter(i);
      entry["watchdogCounter"] = get_hb_watchdog_counter(i);

      // Iterate over each chip
      json::wvalue chips = json::load("[]");
      for (int chip=0; chip< NUM_CHIPS_PER_HASHBOARD; chip++) {
        json::wvalue chipEntry = json::load("{}");

        chipEntry["goodNonces"] = get_chip_stat_good_nonces(i, chip);
        chipEntry["totalNonces"] = get_chip_stat_total_nonces(i, chip);
        chipEntry["biasOffset"] = get_chip_bias_offset(i, chip);

        chips[chip] = to_rvalue(chipEntry);
      }
      entry["chipStatus"] = to_rvalue(chips);

      hashboardArr[hb] = to_rvalue(entry);
      hb++;
    }
  }
  jsonResp["hashboardStatus"] = to_rvalue(hashboardArr);

  // Hashrate graph data
  for (int e = 0; e < num_hashrate_entries; e++)
  {
    json::wvalue entry = json::load("{}");
    entry["time"] = hashrate_history_secs[e].time;
    uint32_t total = 0;
    uint8_t hb = 0;
    for (int i = 0; i < MAX_HASHING_BOARDS; i++)
    {
      if (get_hb_id(i) != 255)
      {
        uint32_t value = hashrate_history_secs[e].hashrates[i];
        entry["Board " + to_string(i + 1)] = value;
        total += value;
      }
    }
    entry["Total"] = total;
    hashrateArr[e] = to_rvalue(entry);
    hb++;
  }
  jsonResp["hashrateData"] = to_rvalue(hashrateArr);

  // Handle pools
  json::rvalue conf = readCgMinerConfig();

  if (conf.error())
    log_entry(LEVEL_ERROR, "[%s] could not read CGMiner config file", __func__);
  else
  {
    if (conf.has("pools"))
    {
      json::rvalue pools = conf["pools"];
      for (unsigned int i = 0; i < pools.size(); i++)
      {
        json::rvalue entry = pools[i];
        json::wvalue resEntry = json::load("{}");
        resEntry["worker"] = entry["user"].s();
        resEntry["url"] = entry["url"].s();
        resEntry["accepted"] = (uint32_t)get_pool_accepted(i);
        resEntry["rejected"] = (uint32_t)get_pool_rejected(i);

        char pool_status[MAX_POOL_STATUS_LEN];
        get_pool_status(i, pool_status);
        resEntry["status"] = pool_status;

        poolArr[i] = to_rvalue(resEntry);
      }
    }
    jsonResp["poolStatus"] = to_rvalue(poolArr);
  }

  // Local system info
  station_info_t station_info;
  get_station_info(&station_info);

  int i = 0;
  systemArr[i++] = makeSystemInfoEntry("Model", getModel());
  systemArr[i++] = makeSystemInfoEntry("Software Version", station_info.release);
  systemArr[i++] = makeSystemInfoEntry("Free Memory", to_string(getFreeMemory()) + " MB");
  systemArr[i++] = makeSystemInfoEntry("Total Memory", to_string(getTotalMemory()) + " MB");
  systemArr[i++] = makeSystemInfoEntry("Uptime", getUptime() + " (HH:MM)");
  systemArr[i++] = makeSystemInfoEntry("Pool Difficulty", formatFloat(get_device_stat_pool_difficulty(), 1));
  systemArr[i++] = makeSystemInfoEntry("Fan Speed", to_string(get_system_fan_speed_percent()) + "%");

  // TODO: When fan tach is added, remove the following comments so that fan
  //       RPMs are sent to the UI.
  // for (int f=0; f<NUM_FANS; f++) {
  //   systemArr[i] = makeSystemInfoEntry("Fan " + to_string(f + 1) +
  //     "  Speed", to_string((uint32_t)get_fan_speed(i)) + " RPM");
  //     i++;
  // }

  jsonResp["systemInfo"] = to_rvalue(systemArr);

  string str = json::dump(jsonResp);
  log_entry(LEVEL_DEBUG, "getStatusDashboard() complete");
  // CROW_LOG_DEBUG << "jsonStr=" << str;
  sendJson(str, resp);
}

void getStatusBurnin(string path, query_string &urlParams, const crow::request &req,
                     crow::response &resp)
{
  json::wvalue jsonResp = json::load("{}");
  json::wvalue boardStatusArr = json::load("[]");

  tlv_ipc_notification_burnin_status_t *pburninStatus = allocBurninInfo();
  get_burnin_status(pburninStatus);

  jsonResp["minerStatus"] = bistatus_to_str(pburninStatus->miner_status);

  for (unsigned int i = 0; i < pburninStatus->total_hashing_boards; i++)
  {
    json::wvalue hbEntry = json::load("{}");
    hbEntry["type"] = hbtype_to_str(pburninStatus->info[i].type);
    hbEntry["status"] = bistatus_to_str(pburninStatus->info[i].board_status);
    hbEntry["expectedHashRate"] = pburninStatus->info[i].expected_hash_rate;
    hbEntry["actualHashRate"] = pburninStatus->info[i].actual_hash_rate;

    boardStatusArr[i] = to_rvalue(hbEntry);
  }
  jsonResp["boardInfo"] = to_rvalue(boardStatusArr);

  string str = json::dump(jsonResp);
  log_entry(LEVEL_DEBUG, "getStatusBurnin() complete");

  sendJson(str, resp);

  free(pburninStatus);
}

void getStatusDiagnostics(string path, query_string &urlParams, const crow::request &req,
                          crow::response &resp)
{
  string diagnostics = getDiagnostics();
  resp.write(diagnostics);
  resp.end();
}

void getStatusDeviceDetails(string path, query_string &urlParams, const crow::request &req,
                            crow::response &resp)
{
  sendCgMinerCmd("devdetails", "",
                 [&](CgMiner::Response cgMinerResp) { sendJson(cgMinerResp.json, resp); });
}

void getStatusSummary(string path, query_string &urlParams, const crow::request &req,
                      crow::response &resp)
{
  sendCgMinerCmd("summary", "",
                 [&](CgMiner::Response cgMinerResp) { sendJson(cgMinerResp.json, resp); });
}

void getStatusAsic(string path, query_string &urlParams, const crow::request &req,
                   crow::response &resp)
{
  sendCgMinerCmd("asc", urlParams.get("n"),
                 [&](CgMiner::Response cgMinerResp) { sendJson(cgMinerResp.json, resp); });
}

void getStatusStats(string path, query_string &urlParams, const crow::request &req,
                    crow::response &resp)
{
  sendCgMinerCmd("stats", "",
                 [&](CgMiner::Response cgMinerResp) { sendJson(cgMinerResp.json, resp); });
}

void getStatusCoinMining(string path, query_string &urlParams, const crow::request &req,
                         crow::response &resp)
{
  sendCgMinerCmd("coin", "",
                 [&](CgMiner::Response cgMinerResp) { sendJson(cgMinerResp.json, resp); });
}

void getStatusLockStats(string path, query_string &urlParams, const crow::request &req,
                        crow::response &resp)
{
  sendCgMinerCmd("lockstats", "",
                 [&](CgMiner::Response cgMinerResp) { sendJson(cgMinerResp.json, resp); });
}

//--------------------------------------------------------------------------------------------------
// STATUS HANDLERS (API SERVER)
//--------------------------------------------------------------------------------------------------

void getStatusMemory(string, query_string &urlParams, const crow::request &req,
                     crow::response &resp)
{
  int freeMemory = getFreeMemory();
  int totalMemory = getTotalMemory();
  json::wvalue jsonResp = json::load("{}");
  jsonResp["freeMemory"] = to_string(freeMemory) + " MB";
  jsonResp["totalMemory"] = to_string(totalMemory) + " MB";
  string jsonStr = json::dump(jsonResp);
  sendJson(jsonStr, resp);
}

//--------------------------------------------------------------------------------------------------
// ACTION HANDLERS
//--------------------------------------------------------------------------------------------------

void actionChangePassword(string path, json::rvalue &args, const crow::request &req,
                          crow::response &resp)
{
  string username = args["username"].s();
  string oldPassword = args["oldPassword"].s();
  string newPassword = args["newPassword"].s();

  if (!changePassword(username, oldPassword, newPassword, AUTH_FILE, false))
  {
    resp.code = HttpStatus_Unauthorized;
    resp.end();
    return;
  }

  resp.end();
}

void actionRestartApiServer(string path, json::rvalue &args, const crow::request &req,
                            crow::response &resp)
{
  resp.end();

  // TODO: Do we need to wait here before exiting so that the success code is sent?
  // We're not actually calling this at the moment anyway...

  // Exit the API Server.  The watchdog should kick in to restart us
  CROW_LOG_DEBUG << "Calling exit()";
  exit(EXIT_SUCCESS);
}

void actionFactoryReset(string path, json::rvalue &args, const crow::request &req,
                        crow::response &resp)
{
  send_factory_reset();
  resp.end();
}

void actionSoftwareUpdate(string path, json::rvalue &args, const crow::request &req,
                          crow::response &resp)
{
  send_software_update();
  resp.end();
}

void actionSoftwareRollback(string path, json::rvalue &args, const crow::request &req,
                            crow::response &resp)
{
  send_software_rollback();
  resp.end();
}

void actionReboot(string path, json::rvalue &args, const crow::request &req, crow::response &resp)
{
  CROW_LOG_DEBUG << "actionReboot() called";
  resp.end();

  // Reboot the entire computeren

  // This works even when not running the app under sudo
  doReboot();

  // The following way did not work unless the server was run as "sudo ./bin/apiserver"
  //
  // sync();
  // setuid(0);
  // int ret = reboot(RB_AUTOBOOT);
  // CROW_LOG_DEBUG << "ret=" << ret << " errno=" << errno;
}

void actionClearStats(string path, json::rvalue &args, const crow::request &req,
                      crow::response &resp)
{
  sendCgMinerCmd("zero", "all,false", [&](CgMiner::Response cgMinerResp) { resp.end(); });
}

void actionEnableAsic(string path, json::rvalue &args, const crow::request &req,
                      crow::response &resp)
{
  string asicNum = args["n"].s();
  if (args["enabled"].s() == "true")
  {

    sendCgMinerCmd("ascenable", asicNum, [&](CgMiner::Response cgMinerResp) { resp.end(); });
  }
  else
  {
    sendCgMinerCmd("ascdisable", asicNum, [&](CgMiner::Response cgMinerResp) { resp.end(); });
  }
}

void actionClearTuning(string path, json::rvalue &args, const crow::request &req,
                       crow::response &resp)
{
  send_clear_tuning();
  resp.end();
}

void actionCheckForSoftwareUpdate(string path, json::rvalue &args, const crow::request &req,
                                  crow::response &resp)
{
  bool autoupdate = false;
  if (args.has("autoupdate"))
  {
    autoupdate = args["autoupdate"].b();
  }
  send_software_update_check(autoupdate);
  resp.end();
}

void actionIdentifyWithLEDs(string path, json::rvalue &args, const crow::request &req,
                            crow::response &resp)
{
  bool enabled = true;

  if (args.has("enabled"))
  {
    enabled = args["enabled"].b();
  }

  send_set_alternating_leds(enabled);
  resp.end();
}

// inventory/ - things about the miner that "just are" and cannot be modified by set requests
//              (e.g., number of boards, number of ASICs per board, serial numbers, etc.)
// config/    - things about the miner that can be modified and read back (e.g., pool
// settings,getIP
//              IP address, DHCP enabled, etc.).
// status/    - things about the miner that cannot be specifically set, but that do change
// over time
//              (e.g., control board temperature, hashrates, uptime, etc.)
map<string, PathHandlerForGet> pathHandlerMapForGet = {
    // Inventory
    {"inventory/versions", getInventoryVersions},
    {"inventory/devices", getInventoryDevices},
    {"inventory/asicCount", getInventoryAsicCount},
    {"inventory/system", getInventorySystem},

    // Config
    {"config/config", getConfigConfig},
    {"config/pools", getConfigPools},
    {"config/network", getConfigNetwork},
    {"config/system", getConfigSystem},
    {"config/mining", getConfigMining},

    // Status
    {"status/dashboard", getStatusDashboard},
    {"status/diagnostics", getStatusDiagnostics},
    {"status/memory", getStatusMemory},
    {"status/devDetails", getStatusDeviceDetails},
    {"status/summary", getStatusSummary},
    {"status/asic", getStatusAsic},
    {"status/stats", getStatusStats},
    {"status/coinMining", getStatusCoinMining},
    {"status/lockStats", getStatusLockStats},
    {"status/burnin", getStatusBurnin},
};

map<string, PathHandlerForSet> pathHandlerMapForSet = {
    {"config/network", setConfigNetwork},
    {"config/system", setConfigSystem},
    {"config/mining", setConfigMining},
    {"config/pools", setConfigPools},
};

map<string, PathHandlerForAction> pathHandlerMapForAction = {
    {"changePassword", actionChangePassword},
    {"restartApiServer", actionRestartApiServer},
    {"reboot", actionReboot},
    {"clearStats", actionClearStats},
    {"enableAsic", actionEnableAsic},
    {"factoryReset", actionFactoryReset},
    {"softwareUpdate", actionSoftwareUpdate},
    {"softwareRollback", actionSoftwareRollback},
    {"resetTuningParameters", actionClearTuning},
    {"checkForSoftwareUpdate", actionCheckForSoftwareUpdate},
    {"identifyWithLEDs", actionIdentifyWithLEDs}};

void handleGet(string &path, const crow::request &req, crow::response &resp)
{
  CROW_LOG_DEBUG << "GET: " << path;
  crow::query_string urlParams = req.url_params;

  auto it = pathHandlerMapForGet.find(path);
  if (it == pathHandlerMapForGet.end())
  {
    // Not found
    sendError("Path not found", HttpStatus_NotFound, resp);
    return;
  }

  auto handler = it->second;
  handler(path, urlParams, req, resp);
}

void handleSet(string &path, const crow::request &req, crow::response &resp)
{
  json::rvalue args = json::load(req.body);

  auto it = pathHandlerMapForSet.find(path);
  if (it == pathHandlerMapForSet.end())
  {
    // Not found
    sendError("Path not found", HttpStatus_NotFound, resp);
    return;
  }

  auto handler = it->second;
  handler(path, args, req, resp);
}

void handleAction(string &path, const crow::request &req, crow::response &resp)
{
  json::rvalue args = json::load(req.body);

  auto it = pathHandlerMapForAction.find(path);
  if (it == pathHandlerMapForAction.end())
  {
    // Not found
    sendError("Path not found", HttpStatus_NotFound, resp);
    return;
  }

  auto handler = it->second;
  handler(path, args, req, resp);
}

void handleInfo(const crow::request &req, crow::response &resp)
{
  CROW_LOG_DEBUG << "INFO";
  station_info_t station_info;
  get_station_info(&station_info);

  tlv_ipc_notification_burnin_status_t burnin_info;
  get_burnin_status(&burnin_info);

  char latest_version[MAX_STATIONRELEASE_LEN];
  get_latest_firmware_version(latest_version);

  char rollback_version[MAX_STATIONRELEASE_LEN];
  get_rollback_firmware_version(rollback_version);

  string macAddress = getMACAddr(INTF_NAME);
  string ipAddress = getIpV4(INTF_NAME);
  string firmwareVersion = station_info.release;
  string hostname = getHostname();

  json::wvalue jsonResp = json::load("{}");
  jsonResp["macAddress"] = macAddress;
  jsonResp["ipAddress"] = ipAddress;
  jsonResp["hostname"] = hostname;
  jsonResp["model"] = getModel();
  jsonResp["vendor"] = "Obelisk";
  jsonResp["firmwareVersion"] = firmwareVersion;
  jsonResp["latestFirmwareVersion"] = latest_version;
  jsonResp["rollbackFirmwareVersion"] = rollback_version;

  // Include burnin-info if burnin is/was active
  switch (burnin_info.miner_status)
  {
  case BI_INPROGRESS:
  case BI_INACTIVE:
  case BI_FAILED:
  case BI_PASSED:
  {
    json::wvalue burnin = json::load("{}");

    burnin["deviceStatus"] = bistatus_to_str(burnin_info.miner_status);

    json::wvalue boardInfoEntries = json::load("[]");
    for (int i = 0; i < burnin_info.total_hashing_boards; i++)
    {
      json::wvalue hbEntry = json::load("{}");
      hbEntry["type"] = hbtype_to_str(burnin_info.info[i].type);
      hbEntry["status"] = bistatus_to_str(burnin_info.info[i].board_status);
      hbEntry["expectedHashrate"] = burnin_info.info[i].expected_hash_rate;
      hbEntry["actualHashrate"] = burnin_info.info[i].actual_hash_rate;
      boardInfoEntries[i] = to_rvalue(hbEntry);
    }
    burnin["hashboardStatus"] = to_rvalue(boardInfoEntries);
    jsonResp["burninStatus"] = to_rvalue(burnin);
  }
  break;

  default:
    break;
  }

  sendJson(json::dump(jsonResp), resp);
}
