/*
 * Copyright (C) 2014-2018 MapleLeaf Software, Inc
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * common.h
 *
 * Dan Jozwiak: djozwiak@mlsw.biz
 * Date Created: 2014-06-13
 *
 * Description: 
 */

#ifndef __COMMON_H__
#define __COMMON_H__

#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>

#ifdef  __cplusplus
extern "C" {
#endif

#define FRONTEND_SERVER	"71.255.123.50"

#define PROJECT_DIR     "ob2"
#define LOG_DIR         "/var/log/ob2"
#define DATALOG_DIR     "datalogs"
#define UPDATES_DIR     "/var/etc/ob2/updates"

#define FACTORY_STATION_INFO_FILE   "/usr/local/ob2/data/factory_system_info"
#define STATION_INFO_FILE           "/var/etc/ob2/station_info"
#define SYSTEM_INFO_FILE            "/var/etc/ob2/system_info"
#define HB_INFO_FILE                "/var/etc/ob2/hb_info"
#define SIA_FW_FILE                 "/usr/local/ob2/data/SC1B-SIA.bin"
#define DCR_FW_FILE                 "/usr/local/ob2/data/SC1B-DCR.bin"
#define UPDATE_SCR                  "/usr/local/ob2/bin/update"
#define ROLLBACK_SCR                "/usr/local/ob2/bin/rollback"
#define BACKUP_FILE                 "/usr/local/ob2/backup/ob2-backup.tgz"
#define NEW_XML_DEFAULTS_FILE       "/var/etc/ob2/new_xml_defaults"

#define VERSION_FILE    "versions.xml"

#define UPDATE_PREFIX   "ob2-update"
#define UPDATE_SUFFIX   ".pkg"

#define ACTIVE_CONSOLE  "/sys/devices/virtual/tty/console/active"

/* Device paths */
#define INPUT_DEVICE    "/dev/input/event0"
#define LED1            "/sys/class/leds/fp_led1/brightness"
#define LED2            "/sys/class/leds/fp_led2/brightness"

#define HB_DEV_NODE_BASE "/dev/ttyO"

/* Message source/destination ID's */
#define EP_TYPE_START       1
#define EP_TYPE_SERVER      (EP_TYPE_START)
#define EP_TYPE_STATION     (EP_TYPE_START + 1)
#define EP_TYPE_AGENT       (EP_TYPE_START + 2)

#define MAX_STATIONID_LEN           12
#define MAX_STATIONINFO_LEN         64
#define MAX_STATIONIPADDR_LEN       16
#define MAX_STATIONRELEASE_LEN      10
#define MAX_RELEASE                 14
#define MAX_HB_RELEASE               5
#define MAX_IPADDR_LEN              16
#define MAX_IFNAME_LEN               6
#define MAX_FWVERSION_SIZE           6
#define MAX_HB_DEV_NODE_LEN         11
#define MAX_POOL_STATUS_LEN         20

#define MAX_HASHING_BOARDS           6
#define MAX_ASICS_PER_HASHING_BOARD 32
#define NUM_FANS                     4
#define MAX_POOLS                    3

#define MIN_FAN_SPEED               40
#define MAX_FAN_SPEED              100
#define STARTUP_FAN_SPEED           MIN_FAN_SPEED
#define TUNING_FAN_SPEED            85
#define FAN_SPEED_DELTA              5
#define MIN_AUTOFAN_TEMP            50.0
#define MAX_AUTOFAN_TEMP            75.0

#define MIN_DCR_HASHRATE            (900.0)
#define MIN_SIA_HASHRATE            (350.0)

#define MAX_URL 128

#define MAX_UPDATE_INFO 256

#define HB_ASIC_COUNT               32

typedef uint32_t magic_t;
typedef uint16_t token_t;

typedef struct __attribute__ ((packed))
{
    char id[MAX_STATIONID_LEN];
    char release[MAX_STATIONRELEASE_LEN];
    char dcrrelease[MAX_HB_RELEASE];
    char siarelease[MAX_HB_RELEASE];
    char ipaddr[MAX_IPADDR_LEN];
    time_t mfgdate;
} station_info_t;

typedef struct __attribute__ ((packed))
{
    char update_url[MAX_URL];
    uint32_t rebootperiod;
    uint8_t fanspeed;
    char updateto[MAX_STATIONRELEASE_LEN];
    bool autofan;
    float autofantemp;
} system_info_t;

typedef enum
{
    FAN_STATUS_FAILED,
    FAN_STATUS_GOOD,

    FAN_STATUS_END
} fan_status_t;

typedef enum
{
    BUTTON_UNKNOWN,
    BUTTON_PRESSED,
    BUTTON_RELEASED,

    BUTTON_END
} button_state_t;

typedef enum
{
    LED_OFF,
    LED_GREEN,
    LED_RED,
    LED_BOTH,

    LED_END
} led_t;

static inline const char *led_to_str(led_t state)
{
    static char *unknown_enum = "UNKNOWN";
    static const char *strings[] =
    {
        "OFF",
        "GREEN",
        "RED",
        "BOTH",
    };
    if (state < LED_END)
        return strings[state];
    return unknown_enum;
}

typedef enum __attribute__ ((packed))
{
    HB_UNKNOWN,
    HB_DCR,
    HB_SIA,

    HB_END
} hb_board_type_t;

static inline const char *hbtype_to_str(hb_board_type_t type)
{
    static const char *strings[] =
    {
        "Unknown",
        "DCR",
        "SIA",
    };
    return strings[type];
}

#ifdef  __cplusplus
}
#endif

#endif /* __COMMON_H__ */
