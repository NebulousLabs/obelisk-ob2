/*
 * Copyright (C) 2014-2018 MapleLeaf Software, Inc
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * ipc_messages.h
 *
 * Dan Jozwiak: djozwiak@mlsw.biz
 * Date Created: 2014-06-14
 *
 * Description: 
 */

#ifndef __IPC_MESSAGES_H__
#define __IPC_MESSAGES_H__

#ifdef  __cplusplus
extern "C" {
#endif

#include <time.h>

#include "common.h"

#define MAX_RECV_LEN    1024

/* cgminer related definitions */
#define DCR_NUM_VREGS      16
#define DCR_NUM_MREGS      13
#define SIA_NUM_MREGS      10
#define MAX_NONCE_FIFO_LEN  8
#define DCR_HASH_MULT       4294976296ULL
#define SIA_HASH_MULT       1099511627776ULL

/* Message flags */
#define FLAG_COMPRESSED     0x01

/* Various attributes used to exchange data with the local server */
#define ATTR_TYPE_IPC_START         300
#define ATTR_TYPE_IPC_IDENTITY      (ATTR_TYPE_IPC_START + 0)
#define ATTR_TYPE_IPC_ACK           (ATTR_TYPE_IPC_START + 1)

/* Attributes for the control manager processing */
#define ATTR_TYPE_IPC_CONTROLMGR_START        400
#define ATTR_TYPE_IPC_REGISTER_NOTIFICATION   (ATTR_TYPE_IPC_CONTROLMGR_START + 0)
#define ATTR_TYPE_IPC_UNREGISTER_NOTIFICATION (ATTR_TYPE_IPC_CONTROLMGR_START + 1)
#define ATTR_TYPE_IPC_CHANGE_NOTIFICATION     (ATTR_TYPE_IPC_CONTROLMGR_START + 2)
#define ATTR_TYPE_IPC_HB_INFO                 (ATTR_TYPE_IPC_CONTROLMGR_START + 3)
#define ATTR_TYPE_IPC_RUN_DCR_TEST_JOB        (ATTR_TYPE_IPC_CONTROLMGR_START + 4)
#define ATTR_TYPE_IPC_ABORT_DCR_TEST_JOB      (ATTR_TYPE_IPC_CONTROLMGR_START + 5)
#define ATTR_TYPE_IPC_RUN_SIA_TEST_JOB        (ATTR_TYPE_IPC_CONTROLMGR_START + 6)
#define ATTR_TYPE_IPC_ABORT_SIA_TEST_JOB      (ATTR_TYPE_IPC_CONTROLMGR_START + 7)
#define ATTR_TYPE_IPC_FACTORY_RESET           (ATTR_TYPE_IPC_CONTROLMGR_START + 8)
#define ATTR_TYPE_IPC_FAN_SPEED               (ATTR_TYPE_IPC_CONTROLMGR_START + 9)
#define ATTR_TYPE_IPC_REBOOT_PERIOD           (ATTR_TYPE_IPC_CONTROLMGR_START + 10)
#define ATTR_TYPE_IPC_RESTART_CGM             (ATTR_TYPE_IPC_CONTROLMGR_START + 11)
#define ATTR_TYPE_IPC_BURNIN_STATUS           (ATTR_TYPE_IPC_CONTROLMGR_START + 12)
#define ATTR_TYPE_IPC_CHECK_FOR_UPDATE        (ATTR_TYPE_IPC_CONTROLMGR_START + 13)
#define ATTR_TYPE_IPC_CLEAR_PERFORMANCE       (ATTR_TYPE_IPC_CONTROLMGR_START + 14)
#define ATTR_TYPE_IPC_SET_PERFORMANCE         (ATTR_TYPE_IPC_CONTROLMGR_START + 15)
#define ATTR_TYPE_IPC_AUTO_FAN_MODE           (ATTR_TYPE_IPC_CONTROLMGR_START + 16)
#define ATTR_TYPE_IPC_AUTO_FAN_SPEED          (ATTR_TYPE_IPC_CONTROLMGR_START + 17)
#define ATTR_TYPE_IPC_AUTO_FAN_TEMP           (ATTR_TYPE_IPC_CONTROLMGR_START + 18)
#define ATTR_TYPE_IPC_ALTERNATE_LEDS          (ATTR_TYPE_IPC_CONTROLMGR_START + 19)
#define ATTR_TYPE_IPC_SET_CLOCK               (ATTR_TYPE_IPC_CONTROLMGR_START + 20)
#define ATTR_TYPE_IPC_SET_ASIC_CLOCK          (ATTR_TYPE_IPC_CONTROLMGR_START + 21)
#define ATTR_TYPE_IPC_SAVE_PERFORMANCE        (ATTR_TYPE_IPC_CONTROLMGR_START + 22)

/* Attributes for the api server processing */
#define ATTR_TYPE_IPC_APISERVER_START       500

/* Attributes for the update manager processing */
#define ATTR_TYPE_IPC_UPDATEMGR_START       600
#define ATTR_TYPE_IPC_UPDATE                (ATTR_TYPE_IPC_UPDATEMGR_START + 0)
#define ATTR_TYPE_IPC_ROLLBACK              (ATTR_TYPE_IPC_UPDATEMGR_START + 1)

/* Attributes for the cgminer processing */
#define ATTR_TYPE_IPC_CGMINER_START         700
#define ATTR_TYPE_IPC_DCR_WORK_REQ          (ATTR_TYPE_IPC_CGMINER_START + 0)
#define ATTR_TYPE_IPC_DCR_NONCE             (ATTR_TYPE_IPC_CGMINER_START + 1)
#define ATTR_TYPE_IPC_DCR_POOL_DIFFICULTY   (ATTR_TYPE_IPC_CGMINER_START + 2)
#define ATTR_TYPE_IPC_DCR_HASH_LIMIT        (ATTR_TYPE_IPC_CGMINER_START + 3)
#define ATTR_TYPE_IPC_SIA_WORK_REQ          (ATTR_TYPE_IPC_CGMINER_START + 4)
#define ATTR_TYPE_IPC_SIA_NONCE             (ATTR_TYPE_IPC_CGMINER_START + 5)
#define ATTR_TYPE_IPC_SIA_POOL_DIFFICULTY   (ATTR_TYPE_IPC_CGMINER_START + 6)
#define ATTR_TYPE_IPC_SIA_HASH_LIMIT        (ATTR_TYPE_IPC_CGMINER_START + 7)
#define ATTR_TYPE_IPC_JOB_COMPLETE          (ATTR_TYPE_IPC_CGMINER_START + 8)
#define ATTR_TYPE_IPC_WORK_FAIL             (ATTR_TYPE_IPC_CGMINER_START + 9)
#define ATTR_TYPE_IPC_POOL_STATS            (ATTR_TYPE_IPC_CGMINER_START + 10)

/* Available notifications that clients can register for */
typedef enum
{
    NOTIFICATION_HB_TEMP1   = 0x00000001,
    NOTIFICATION_HB_TEMP2   = 0x00000002,
    NOTIFICATION_HB_VOLTAGE = 0x00000004,
    NOTIFICATION_HB_CURRENT = 0x00000008,
    NOTIFICATION_HB_STATS   = 0x00000010,
    NOTIFICATION_CGM_STATS  = 0x00000020,
    NOTIFICATION_SWUPDATE   = 0x00000040,
    NOTIFICATION_HB_PERF    = 0x00000080,
    NOTIFICATION_POOL_DIFF  = 0x00000100,
    NOTIFICATION_BI_STATUS  = 0x00000200,
    NOTIFICATION_HB_CLOCK   = 0x00000400,

    NOTIFICATION_END
} notification_t;

typedef enum  __attribute__ ((packed))
{
    IPC_CONTROL_MANAGER = 1,
    IPC_UI_MANAGER,
    IPC_UPDATE_MANAGER,
    IPC_CGMINER_DCR,
    IPC_CGMINER_SIA,
    IPC_API_SERVER,
    IPC_TEST,
    IPC_BURNIN_MANAGER,

    IPC_END
} ipc_id_t;

static inline const char *id_to_str(ipc_id_t id)
{
    static char *unknown_enum = "UNKNOWN";
    static const char *id_strings[] =
    {
        "unused",
        "Control Manager",
        "UI Manager",
        "Update Manager",
        "CGMiner DCR",
        "CGMiner SIA",
        "API Server",
        "Test",
        "Burnin Manager",
    };
    if (id < IPC_END)
        return id_strings[id];
    return unknown_enum;
}

typedef enum  __attribute__ ((packed))
{
	PS_DISABLED,
	PS_ENABLED,
	PS_REJECTING,

    PS_END
} pool_status_t;

static inline const char *psstat_to_str(pool_status_t id)
{
    static char *unknown_enum = "UNKNOWN";
    static const char *psstat_strings[] =
    {
        "DISABLED",
        "ENABLED",
        "REJECTING",
    };
    if (id < PS_END)
        return psstat_strings[id];
    return unknown_enum;
}

typedef enum  __attribute__ ((packed))
{
    BI_UNKNOWN,
    BI_INACTIVE,
    BI_INPROGRESS,
    BI_FAILED,
    BI_PASSED,

    BI_END
} burnin_status_t;

static inline const char *bistatus_to_str(burnin_status_t id)
{
    static char *unknown_enum = "UNKNOWN";
    static const char *burnin_strings[] =
    {
        "UNKNOWN",
        "INACTIVE",
        "INPROGRESS",
        "FAILED",
        "PASSED",
    };
    if (id < BI_END)
        return burnin_strings[id];
    return unknown_enum;
}
/*
 * Data structures that define messages
 * passed to/from the server
 */
typedef struct __attribute__ ((packed))
{
    ipc_id_t id;
} tlv_ipc_identity_t;

typedef struct __attribute__ ((packed))
{
    char info[MAX_UPDATE_INFO];
} tlv_ipc_update_info_t;

typedef struct __attribute__ ((packed))
{
    uint8_t speed;
} tlv_ipc_fan_speed_t;

typedef struct __attribute__ ((packed))
{
    uint32_t period;
} tlv_ipc_reboot_period_t;

typedef struct __attribute__ ((packed))
{
    bool autoupdate;
} tlv_ipc_update_check_t;

typedef struct __attribute__ ((packed))
{
    uint32_t notifications;
} tlv_ipc_notify_registration_t;

typedef struct __attribute__ ((packed))
{
    uint8_t id;
    hb_board_type_t type;
    burnin_status_t board_status;
    double expected_hash_rate;
    double actual_hash_rate;
} tlv_burnin_info_t;

typedef struct __attribute__ ((packed))
{
    burnin_status_t miner_status;
    uint8_t total_hashing_boards;
    tlv_burnin_info_t info[];
} tlv_ipc_burnin_status_t;

typedef struct __attribute__ ((packed))
{
    uint8_t total_hashing_boards;
} tlv_ipc_burnin_start_t;

typedef struct __attribute__ ((packed))
{
    hb_board_type_t type;
    uint8_t id;
    char devnode[MAX_HB_DEV_NODE_LEN];
    char version[MAX_FWVERSION_SIZE];
    uint8_t asic_diags[MAX_ASICS_PER_HASHING_BOARD];
} tlv_hb_info_t;

typedef struct __attribute__ ((packed))
{
    uint8_t count;
    tlv_hb_info_t hbinfo[];
} tlv_ipc_hb_info_t;

typedef struct __attribute__ ((packed))
{
    uint8_t hb_id;
    uint16_t performance;
} tlv_ipc_set_performance_t;

typedef struct __attribute__ ((packed))
{
    uint8_t hb_id;
    int8_t clock_bias;
    uint8_t clock_divider;
} tlv_ipc_set_clock_t;

typedef struct __attribute__ ((packed))
{
    uint8_t hb_id;
    uint8_t asic;
    int8_t clock_delta;
} tlv_ipc_set_asic_clock_t;

typedef struct __attribute__ ((packed))
{
    bool enabled;
} tlv_ipc_auto_fan_mode_t;

typedef struct __attribute__ ((packed))
{
    float temp;
} tlv_ipc_auto_fan_temp_t;

typedef struct __attribute__ ((packed))
{
    bool enabled;
} tlv_ipc_alternating_leds_t;

typedef struct __attribute__ ((packed))
{
    char url[MAX_URL];
} tlv_ipc_update_request_t;

typedef struct __attribute__ ((packed))
{
    uint8_t  job_id;
    bool clean;
    double difficulty;
    uint32_t vreg[DCR_NUM_VREGS];
    uint32_t mreg[DCR_NUM_MREGS];
} tlv_ipc_cgminer_dcr_work_req_t;

typedef struct __attribute__ ((packed))
{
    uint32_t limit;
} tlv_ipc_cgminer_dcr_hash_limit_t;

typedef struct __attribute__ ((packed))
{
    uint8_t  job_id;
    uint32_t nonce;
    uint32_t m5;
} tlv_ipc_cgminer_dcr_nonce_t;

typedef struct __attribute__ ((packed))
{
    uint8_t  job_id;
    bool clean;
    double difficulty;
    uint64_t mreg[SIA_NUM_MREGS];
} tlv_ipc_cgminer_sia_work_req_t;

typedef struct __attribute__ ((packed))
{
    uint8_t  job_id;
    uint64_t nonce;
} tlv_ipc_cgminer_sia_nonce_t;

typedef struct __attribute__ ((packed))
{
    uint8_t job_id;
} tlv_ipc_cgminer_job_t;

typedef struct __attribute__ ((packed))
{
    double difficulty;
} tlv_ipc_cgminer_difficulty_t;

typedef struct __attribute__ ((packed))
{
    int pool_index;
    int64_t accepted;
    int64_t rejected;
    pool_status_t status;
    bool idle;
} tlv_ipc_cgminer_stats_t;

typedef struct __attribute__ ((packed))
{
    uint16_t type;
    uint16_t len;
    uint8_t value[];
} msg_ipc_tlv_t;

/* notifications */

typedef struct __attribute__ ((packed))
{
    uint8_t hb_id;
    float value;
} tlv_ipc_notification_hb_measurement_t;

typedef struct __attribute__ ((packed))
{
    uint8_t hb_id;
    uint64_t hashes;
    int8_t relative_voltage;
    uint32_t noise_counter;
    uint32_t watchdog_counter;
} tlv_ipc_notification_hb_stats_t;

typedef struct __attribute__ ((packed))
{
    char latest_release[MAX_STATIONRELEASE_LEN];;
    char previous_release[MAX_STATIONRELEASE_LEN];
} tlv_ipc_notification_swupdate_t;

typedef struct __attribute__ ((packed))
{
    uint8_t hb_id;
    bool tuning;
    uint32_t performance;
    uint8_t divider;
    int8_t bias;
    int8_t bias_offsets[HB_ASIC_COUNT];
} tlv_ipc_notification_hb_performance_t;

typedef struct __attribute__ ((packed))
{
    float value;
} tlv_ipc_notification_pool_difficulty_t;

typedef struct __attribute__ ((packed))
{
    burnin_status_t miner_status;
    uint8_t total_hashing_boards;
    tlv_burnin_info_t info[];
} tlv_ipc_notification_burnin_status_t;

typedef struct __attribute__ ((packed))
{
    uint32_t type;
    uint8_t value[];
} tlv_ipc_notification_t;

#ifdef  __cplusplus
}
#endif

#endif /* __IPC_MESSAGES__ */
