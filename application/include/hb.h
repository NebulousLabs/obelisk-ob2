/*
 * Copyright (C) 2018-2019 MapleLeaf Software, Inc
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * hb.h
 *
 * Dan Jozwiak: djozwiak@mlsw.biz
 * Date Created: 2018-10-17
 *
 * Description: this file contains the contents and data structures used
 * for hashing board communications.
 */

#ifndef _HB_H_
#define _HB_H_

#include "common.h"

/* HB message types */
#define MSG_TYPE_HB_START         0x00
#define MSG_TYPE_HB_NAK           (MSG_TYPE_HB_START + 0x00)
#define MSG_TYPE_HB_NOP           (MSG_TYPE_HB_START + 0x01)
#define MSG_TYPE_HB_VERSION       (MSG_TYPE_HB_START + 0x02)
#define MSG_TYPE_HB_BOOT_MODE     (MSG_TYPE_HB_START + 0x03)
#define MSG_TYPE_HB_PWR_UP        (MSG_TYPE_HB_START + 0x04)
#define MSG_TYPE_HB_PWR_DN        (MSG_TYPE_HB_START + 0x05)
#define MSG_TYPE_HB_SET_VOLT      (MSG_TYPE_HB_START + 0x06)
#define MSG_TYPE_HB_GET_VOLT      (MSG_TYPE_HB_START + 0x07)
#define MSG_TYPE_HB_GET_CURR      (MSG_TYPE_HB_START + 0x08)
#define MSG_TYPE_HB_GET_TEMP1     (MSG_TYPE_HB_START + 0x09)
#define MSG_TYPE_HB_GET_TEMP2     (MSG_TYPE_HB_START + 0x0A)
#define MSG_TYPE_HB_SET_CLKS      (MSG_TYPE_HB_START + 0x0B)
#define MSG_TYPE_HB_START_JOB     (MSG_TYPE_HB_START + 0x0C)
#define MSG_TYPE_HB_ABORT_JOB     (MSG_TYPE_HB_START + 0x0D)
#define MSG_TYPE_HB_CHECK_JOB     (MSG_TYPE_HB_START + 0x0E)
#define MSG_TYPE_HB_JREG_WRITE    (MSG_TYPE_HB_START + 0x0F)
#define MSG_TYPE_HB_JMAN_WRITE    (MSG_TYPE_HB_START + 0x10)
#define MSG_TYPE_HB_GET_PROC_ID   (MSG_TYPE_HB_START + 0x11)
#define MSG_TYPE_HB_SET_POOLD     (MSG_TYPE_HB_START + 0x12)
#define MSG_TYPE_HB_SET_PERF      (MSG_TYPE_HB_START + 0x13)
#define MSG_TYPE_HB_GET_ADIAG     (MSG_TYPE_HB_START + 0x14)
#define MSG_TYPE_HB_SET_STATS     (MSG_TYPE_HB_START + 0x15)
#define MSG_TYPE_HB_WDOG_RESET    (MSG_TYPE_HB_START + 0x16)
#define MSG_TYPE_HB_SET_ASIC_CLK  (MSG_TYPE_HB_START + 0x17)
#define MSG_TYPE_HB_GOOD_NONCES   (MSG_TYPE_HB_START + 0x18)
#define MSG_TYPE_HB_BAD_NONCES    (MSG_TYPE_HB_START + 0x19)

#define MSG_TYPE_HB_DCR_ASYNC_START   0x80
#define MSG_TYPE_HB_DCR_NONCE     (MSG_TYPE_HB_DCR_ASYNC_START + 0)
#define MSG_TYPE_HB_DCR_JOB_DONE  (MSG_TYPE_HB_DCR_ASYNC_START + 1)
#define MSG_TYPE_HB_DCR_BAD_JOB   (MSG_TYPE_HB_DCR_ASYNC_START + 2)

#define MSG_TYPE_HB_SIA_ASYNC_START   0xC0
#define MSG_TYPE_HB_SIA_NONCE     (MSG_TYPE_HB_SIA_ASYNC_START + 0)
#define MSG_TYPE_HB_SIA_JOB_DONE  (MSG_TYPE_HB_SIA_ASYNC_START + 1)
#define MSG_TYPE_HB_SIA_BAD_JOB   (MSG_TYPE_HB_SIA_ASYNC_START + 2)

/* HB job commands */
#define HB_JOB_CMD_START            0x00
#define HB_JOB_CMD_JOB_ID           (HB_JOB_CMD_START + 0x00)
#define HB_JOB_CMD_JOB_HASH1_START  (HB_JOB_CMD_START + 0x01)
#define HB_JOB_CMD_JOB_HASH1_END    (HB_JOB_CMD_START + 0x02)
#define HB_JOB_CMD_JOB_HASH2_START  (HB_JOB_CMD_START + 0x03)
#define HB_JOB_CMD_JOB_HASH2_END    (HB_JOB_CMD_START + 0x04)

#define CSUM_SIZE   sizeof(uint8_t)

#define MAX_PAYLOAD         128
#define MAX_HB_MSG_LEN      (sizeof(hb_tlv_t) + MAX_PAYLOAD + CSUM_SIZE)
#define MAX_HB_THREAD_NAME  10

#define VERSION_SIZE    3

#define APPLICATION_START_PAGE  4

#define HB_BOOT_MODE_MAGIC  0x03c0

#define HB_DCR_TYPE 0x32
#define HB_SIA_TYPE 0x64

#define HB_DCR_REGISTERS    32
#define HB_DCR_V_REG_START   0
#define HB_DCR_M_REG_START  16
#define HB_DCR_LIM_REG      28
#define HB_DCR_MATCH_REG    29
#define HB_DCR_LBR_REG      30
#define HB_DCR_UBR_REG      31

#define HB_SIA_REGISTERS    10
#define HB_SIA_M_REG_START   0

#define HB_DEFAULT_PERFORMANCE  1950UL
#define HB_FIRST_PERFORMANCE    1950UL
#define HB_INC_PERFORMANCE        50UL
#define HB_DEC_PERFORMANCE        50UL
#define HB_INC_RAMP              100UL
#define HB_DCR_MAX_PERFORMANCE  2600UL
#define HB_SIA_MAX_PERFORMANCE  2600UL
#define HB_MIN_PERFORMANCE      HB_DEFAULT_PERFORMANCE

#define HB_MIN_BIAS         -5
#define HB_MAX_BIAS          5

#define HB_ASIC_CLOCK_DELTA_MIN     -3
#define HB_ASIC_CLOCK_DELTA_MAX      3


typedef enum
{
    JOB_IDLE,
    JOB_PENDING,
    JOB_ACTIVE,
    JOB_STOP,
    JOB_COMPLETE,

    JOB_END
} hb_job_state_t;

typedef struct
{
    double best_hashrate;
    double performance;
} best_performance_t;

typedef struct
{
    hb_job_state_t a1state;
    hb_job_state_t a2state;
    uint8_t id;
    uint32_t total_nonces;
    uint32_t good_nonces;
    uint32_t pool_nonces;
    uint32_t total_crc_errors;
    double last_hashrate_5min;
    double current_hashrate_5min;
    uint64_t last_hash_stat_time_nanos;
    best_performance_t perf_info;
    time_t start;
    time_t end;
} hb_job_info_t;

/* HB types */
typedef struct
{
    pthread_t thread_id;
    pthread_mutex_t mutex;
    char name[MAX_HB_THREAD_NAME];
    char devnode[MAX_HB_DEV_NODE_LEN];
    int fd;
    uint8_t id;
    hb_board_type_t type;
    char version[MAX_FWVERSION_SIZE];
    bool powered;
    float temp1;
    float temp2;
    float voltage;
    float current;
    int8_t relative_voltage;
    int8_t clock_bias;
    uint8_t clock_divider;
    uint32_t current_performance;
    int8_t bias_offsets[HB_ASIC_COUNT];
    uint32_t max_performance;
    uint32_t peak_performance;
    uint32_t noise_counter;
    uint32_t watchdog_counter;
    uint8_t asic_diags[MAX_ASICS_PER_HASHING_BOARD];
    bool asic_diags_valid;
    bool clean_occurred;
    bool tuned;
    hb_job_info_t job_info;
} hb_node_t;

typedef struct __attribute__ ((packed))
{
    uint8_t type;
    uint8_t version[3];
} hb_version_t;

typedef struct __attribute__ ((packed))
{
    uint16_t magic;
} hb_boot_mode_t;

typedef struct __attribute__ ((packed))
{
    uint32_t temp;
} hb_temp_t;

typedef struct __attribute__ ((packed))
{
    uint8_t asic[MAX_ASICS_PER_HASHING_BOARD];
} hb_asic_diag_status_t;

typedef struct __attribute__ ((packed))
{
    uint32_t signature;
} hb_signature_t;

typedef struct __attribute__ ((packed))
{
    int8_t voltage;
} hb_set_voltage_t;

typedef struct __attribute__ ((packed))
{
    int8_t bias;
    uint8_t divider;
} hb_set_clock_t;

typedef struct __attribute__ ((packed))
{
    uint8_t asic;
    int8_t clock_delta;
} hb_set_asic_clock_t;

typedef struct __attribute__ ((packed))
{
    uint32_t value;
} hb_set_performance_t;

typedef struct __attribute__ ((packed))
{
    uint8_t state;
} hb_set_stats_state_t;

typedef struct __attribute__ ((packed))
{
    uint32_t value;
} hb_measurement_t;

typedef struct __attribute__ ((packed))
{
    uint64_t value;
} hb_difficulty_t;

typedef struct __attribute__ ((packed))
{
    uint8_t busy1;
    uint8_t busy2;
    uint32_t jobs;
    uint32_t nonces;
    uint32_t good_nonces;
    uint32_t pool_nonces;
    uint8_t noise;
    int32_t voltage;
    int32_t bias;
    uint32_t divider;
#ifdef SUPPORT_NOISE
    uint8_t noise;
#endif
} hb_check_job_t;

typedef struct __attribute__ ((packed))
{
    uint8_t reg;
    uint32_t value;
} hb_dcr_register_t;

typedef struct __attribute__ ((packed))
{
    uint8_t reg;
    uint64_t value;
} hb_sia_register_t;

typedef struct __attribute__ ((packed))
{
    uint8_t job_id;
} hb_bad_job_t;

typedef struct __attribute__ ((packed))
{
    uint8_t job_id;
    double difficulty;
    uint32_t registers[HB_DCR_REGISTERS];
} hb_dcr_work_req_t;

typedef struct __attribute__ ((packed))
{
    uint8_t job_id;
    double difficulty;
    uint64_t registers[HB_SIA_REGISTERS];
} hb_sia_work_req_t;

typedef struct __attribute__ ((packed))
{
    uint8_t cmd;
    uint32_t value;
} hb_command_t;

typedef struct __attribute__ ((packed))
{
    uint8_t job;
    uint32_t m5;
    uint32_t nonce;
} hb_dcr_nonce_t;

typedef struct __attribute__ ((packed))
{
    uint8_t job;
    uint64_t nonce;
} hb_sia_nonce_t;

typedef struct __attribute__ ((packed))
{
    uint8_t type;
    uint8_t len;
    uint8_t value[];
} hb_tlv_t;

extern uint8_t hb_generate_checksum(void *data);
extern struct list *hb_init(void);
extern void hb_shutdown(struct list *list);
extern int  hb_open_serial(char *interface);
extern void hb_close_serial(hb_node_t *hb);
extern int  hb_send_sync_message(uint8_t type, hb_node_t *hb, void *req, int req_len, void *rsp, int rsp_len);
extern hb_node_t *hb_find_by_devnode(struct list *list, char *devnode);
extern int  hb_request(uint8_t request, hb_node_t *hb);
extern int  hb_update_version(hb_node_t *hb);
extern int  hb_boot_mode(hb_node_t *hb);

#endif /* _HB_H_ */
