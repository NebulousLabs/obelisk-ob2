/*
 * Copyright (C) 2014-2018 MapleLeaf Software, Inc
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * list.h
 *
 * Dan Jozwiak: djozwiak@mlsw.biz
 * Date Created: 2014-06-12
 *
 * Description: this file contains the contants and data structures used
 * for list-related processing.
 */

#ifndef _LIST_H_
#define _LIST_H_

#include <sys/queue.h>

#ifdef  __cplusplus
extern "C" {
#endif

struct list_entry
{
    void *data;
   
    TAILQ_ENTRY(list_entry) next_entry;
};

struct list
{
    int size;
    
    TAILQ_HEAD(, list_entry) head;
};

extern void init_list(struct list *list);
extern void destroy_list(struct list *list);
extern struct list_entry *add_entry(struct list *list, void *data, int len);
extern void del_entry(struct list *list, struct list_entry *entry);

#ifdef  __cplusplus
}
#endif

#endif /* _LIST_H_ */
