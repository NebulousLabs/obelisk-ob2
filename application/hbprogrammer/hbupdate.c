/*
 * Copyright (C) 2018 MapleLeaf Software, Inc
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * hbupdate.c
 *
 * Dan Jozwiak: djozwiak@mlsw.biz
 * Date Created: 2018-10-21
 *
 * Description: 
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <termios.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <libxml/parser.h>
#include <libxml/tree.h>

#include "init.h"
#include "utils.h"
#include "serial.h"
#include "stm32.h"
#include "port.h"
#include "parser.h"
#include "binary.h"

#include "common.h"
#include "logger.h"
#include "list.h"
#include "hb.h"
#include "hbupdate.h"

#define RELEASE  "release"
#define SIA_VER  "siarelease"
#define DCR_VER  "dcrrelease"

typedef enum
{
    MODE_NONE,
    MODE_APP,
    MODE_BL,

    MODE_END
} hb_mode_t;

typedef struct
{
    char devnode[MAX_HB_DEV_NODE_LEN];
    bool initialized;
    hb_board_type_t type;
    hb_mode_t mode;
    stm32_t *stm;
    void *p_st;
    parser_t *parser;
    struct port_interface *port;
    struct port_options port_opts;
} hb_bl_node_t;

static hb_bl_node_t hb_nodes[MAX_HASHING_BOARDS];
static struct list *hb_app_list;
static char console[11];

extern stm32_t *stm;
extern void *p_st;
extern parser_t *parser;
extern struct port_interface *port;
extern struct port_options port_opts;

extern char init_flag;
extern char *gpio_seq;
extern int retry;

extern uint32_t flash_page_to_addr(int page);
extern int flash_addr_to_page_ceil(uint32_t addr);

#if 0
static void msg_dump(
    void *data,
    int len
)
{
    int i, j;
    int current = 0;
    uint8_t *buf = (uint8_t *)data;
    char log_info[256];

    memset(log_info, 0, sizeof(log_info));
    for (i = 0; current < len; i += 16)
    {
        char value[5];
        sprintf(log_info, "%04x: ", i);
        for (j = 0; (j < 16) && (current < len); ++j)
        {
            sprintf(value, "%02x ", buf[current++]);
            strcat(log_info, value);
        }
        log_entry(LEVEL_DEBUG, "[%s] %s", __func__, log_info);
        memset(log_info, 0, sizeof(log_info));
    }
}
#endif
/* returns the lower address of flash page "page" */
static uint32_t hbupdate_flash_page_to_addr(
    hb_bl_node_t *node,
    int page
)
{
    int i;
    uint32_t addr, *psize;

    addr = node->stm->dev->fl_start;
    psize = node->stm->dev->fl_ps;

    for (i = 0; i < page; i++) {
        addr += psize[0];
        if (psize[1])
            psize++;
    }

    return addr;
}

/* returns the first page whose start addr is >= "addr" */
static int hbupdate_flash_addr_to_page_ceil(
    hb_bl_node_t *node,
    uint32_t addr
)
{
    int page;
    uint32_t *psize;

    if (!(addr >= node->stm->dev->fl_start && addr <= node->stm->dev->fl_end))
        return 0;

    page = 0;
    addr -= node->stm->dev->fl_start;
    psize = node->stm->dev->fl_ps;

    while (addr >= psize[0]) {
        addr -= psize[0];
        page++;
        if (psize[1])
            psize++;
    }

    return addr ? page + 1 : page;
}

static char *get_active_console(void)
{
    FILE *active;
    char value[6];
    int rc;

    active = fopen(ACTIVE_CONSOLE, "r");
    if (active == NULL)
    {
        log_entry(LEVEL_ERROR, "[%s] unable to determine active console", __func__);
        return NULL;
    }
    rc = fscanf(active, "%s", value);
    if (rc == 0)
    {
        log_entry(LEVEL_WARNING, "[%s] unable to read active console: %s", __func__, strerror(errno));
        fclose(active);
        return NULL;
    }
    fclose(active);
    sprintf(console, "/dev/%s", value);
    return console;
}

static void init_bl_node(
    hb_bl_node_t *node,
    char *devnode
)
{
    strcpy(node->devnode, devnode);
    memcpy(&node->port_opts, &port_opts, sizeof(port_opts));
    node->port_opts.device = node->devnode;
    node->initialized = true;
}

static hb_board_type_t get_board_type(
    hb_bl_node_t *hb
)
{
    return HB_SIA; // FIXME
}

static int open_bl_connection(
    hb_bl_node_t *hb
)
{
    if (port_open(&hb->port_opts, &hb->port) != PORT_ERR_OK)
    {
        log_entry(LEVEL_ERROR, "[%s] failed to open port: %s", __func__, port_opts.device);
        return -1;
    }

    log_entry(LEVEL_DEBUG, "[%s] interface %s: %s", __func__, hb->devnode, hb->port->get_cfg_str(hb->port));

    if (init_flag && init_bl_entry(hb->port, gpio_seq))
    {
        log_entry(LEVEL_ERROR, "[%s] failed to send boot enter sequence", __func__);
        hb->port->close(hb->port);
        hb->port = NULL;
        return -1;
    }

    hb->port->flush(hb->port);

    hb->stm = stm32_init(hb->port, init_flag);
    if (hb->stm == NULL)
    {
        log_entry(LEVEL_WARNING, "[%s] failed stm32_init...HB may not be present", __func__);
        hb->port->close(hb->port);
        hb->port = NULL;
        return -1;
    }
    return 0;
}

static void close_bl_connection(
    hb_bl_node_t *hb
)
{
    if (hb->stm)
    {
        stm32_close(hb->stm);
        hb->stm = NULL;
    }
    if (hb->port)
    {
        hb->port->close(hb->port);
        hb->port = NULL;
    }
}

static int open_parser(
    hb_bl_node_t *hb
)
{
    parser_err_t perr;
    char *filename = NULL;

    hb->parser = &PARSER_BINARY;
    hb->p_st = hb->parser->init();
    if (hb->p_st == NULL)
    {
        log_entry(LEVEL_ERROR, "[%s] failed to initialize parser: %s", __func__, parser->name);
        return -1;
    }

    switch (hb->type)
    {
        case HB_SIA:
            filename = SIA_FW_FILE;
        break;
        case HB_DCR:
            filename = DCR_FW_FILE;
        break;
        default:
            log_entry(LEVEL_ERROR, "[%s] unknown HB type: %d", __func__, hb->type);
            hb->parser = NULL;
            return -1;
        break;
    }
    perr = hb->parser->open(hb->p_st, filename, 0);
    if (perr != PARSER_ERR_OK)
    {
        log_entry(LEVEL_ERROR, "[%s] %s parser failed to open %s", __func__, parser->name, filename);
        hb->parser = NULL;
        return -1;
    }
    return 0;
}

static void close_parser(
    hb_bl_node_t *hb
)
{
    if (hb->p_st)
    {
        hb->parser->close(hb->p_st);
        hb->p_st = NULL;
        hb->parser = NULL;
    }
}

static void hbupdate_discover(
    struct list *list
)
{
    int i;
    char *console_device;

    memset(hb_nodes, 0, sizeof(hb_nodes));

    /*
     * First call hb_init(). It will do a discovery of all HBs that
     * are in application mode.
     */
    hb_app_list = hb_init();

    /* Update the Linux console port in case console is active */
    console_device = get_active_console();

    /*
     * Now let's loop through the list of serial device nodes to either
     * identify HBs that have already been found or boards that may be
     * in bootloader mode.
     */
    for (i = 0; i < MAX_HASHING_BOARDS; ++i)
    {
        char device[MAX_HB_DEV_NODE_LEN];
        hb_node_t *hb;

        sprintf(device, "%s%d", HB_DEV_NODE_BASE, i);
        init_bl_node(&hb_nodes[i], device);

        /* Avoid probing the device node if the Linux console owns it */
        if (console_device && strcmp(console_device, device) == 0)
        {
            hb_nodes[i].mode = MODE_NONE;
            continue;
        }

        hb = hb_find_by_devnode(hb_app_list, device);
        if (hb)
        {
            hb_nodes[i].type = hb->type;
            hb_nodes[i].mode = MODE_APP;
        }
        else
        {
            int rc;
            /*
             * No node found in application mode...let's see if it's in
             * bootloader mode.
             */
            rc = open_bl_connection(&hb_nodes[i]);
            if (rc < 0)
                hb_nodes[i].mode = MODE_NONE;
            else
            {
                hb_nodes[i].type = get_board_type(&hb_nodes[i]);
                hb_nodes[i].mode = MODE_BL;
            }
            close_bl_connection(&hb_nodes[i]);
        }
    }
    
    return;
}

/***********************************************************
 * NAME: verify_hb
 * DESCRIPTION: this function  
 *
 * IN:  HB node pointer
 * OUT: none
 ***********************************************************/
static void verify_hb(
    hb_node_t *hb,
    char *release
)
{
    int rc;

    /* Give the HB a chance to boot the application... */
    sleep(20);

    /* ...and then ping to make sure that it's there... */
    rc = hb_request(MSG_TYPE_HB_NOP, hb);
    if (rc < 0)
        return;

    /* ...and now get the current version... */
    rc = hb_update_version(hb);
    if (rc < 0)
        return;

    /* ...and finally compare it to what it should be. */
    if (strcmp(release, hb->version) == 0)
        log_entry(LEVEL_INFO, "[%s] HB on %s updated successfully", __func__, hb->devnode);
    else
        log_entry(LEVEL_ERROR, "[%s] HB on %s update failed: expected %s, got %s",
                  __func__, hb->devnode, release, hb->version);
}

/***********************************************************
 * NAME: write_hb_flash
 * DESCRIPTION: this function  
 *
 * IN:  start address
 *      end address
 *      first page to program
 *      number of pages to program
 *      verify flag
 * OUT: none
 ***********************************************************/
static void write_hb_flash(
    hb_bl_node_t *bl,
    uint32_t start,
    uint32_t end,
    int first_page,
    int num_pages,
    bool verify
)
{
    stm32_err_t s_err;
    uint32_t addr;
    unsigned int offset = 0;
    unsigned int r;
    unsigned int size;
    unsigned int max_wlen;
    unsigned int max_rlen;
    unsigned int len;
    int	failed = 0;
    uint8_t	buffer[256];

    max_wlen = bl->port_opts.tx_frame_max - 2;	/* skip len and crc */
    max_wlen &= ~3;	/* 32 bit aligned */

    max_rlen = bl->port_opts.rx_frame_max;
    max_rlen = max_rlen < max_wlen ? max_rlen : max_wlen;
    size = bl->parser->size(bl->p_st);

    log_entry(LEVEL_DEBUG, "[%s] erasing memory", __func__);
    s_err = stm32_erase_memory(bl->stm, first_page, num_pages);
    if (s_err != STM32_ERR_OK)
    {
        log_entry(LEVEL_ERROR, "[%s] failed to erase memory", __func__);
        return;
    }

    addr = start;
    while (addr < end && offset < size)
    {
        uint32_t left = end - addr;
        len = max_wlen > left ? left : max_wlen;
        len = len > size - offset ? size - offset : len;

        if (bl->parser->read(bl->p_st, buffer, &len) != PARSER_ERR_OK)
        {
            log_entry(LEVEL_ERROR, "[%s] failed to read firmware file", __func__);
            return;
        }

        if (len == 0)
        {
            log_entry(LEVEL_DEBUG, "[%s] programming complete", __func__);
            return;
        }

again:
        s_err = stm32_write_memory(bl->stm, addr, buffer, len);
        if (s_err != STM32_ERR_OK)
        {
            log_entry(LEVEL_ERROR, "[%s] failed to write memory at address 0x%08x\n", __func__, addr);
            return;
        }

        if (verify)
        {
            uint8_t compare[len];
            unsigned int offset, rlen;

            offset = 0;
            while (offset < len)
            {
                rlen = len - offset;
                rlen = rlen < max_rlen ? rlen : max_rlen;
                s_err = stm32_read_memory(bl->stm, addr + offset, compare + offset, rlen);
                if (s_err != STM32_ERR_OK)
                {
                    log_entry(LEVEL_ERROR, "[%s] failed to read memory at address 0x%08x\n", __func__, addr + offset);
                    return;
                }
                offset += rlen;
            }

            for (r = 0; r < len; ++r)
            {
                if (buffer[r] != compare[r])
                {
                    if (failed == retry)
                    {
                        log_entry(LEVEL_ERROR, "[%s] failed to verify at address 0x%08x, expected 0x%02x and found 0x%02x\n",
                                  __func__, (uint32_t)(addr + r), buffer [r], compare[r]);
                        return;
                    }
                    ++failed;
                    goto again;
                }
            }

            failed = 0;
        }

        addr	+= len;
        offset	+= len;

        log_entry(LEVEL_DEBUG, "[%s] wrote %saddress 0x%08x (%.2f%%) ",
                  __func__,  verify ? "and verified " : "", addr, (100.0f / size) * offset);
    }
}

/***********************************************************
 * NAME: program_hb
 * DESCRIPTION: this function  
 *
 * IN:  HB node pointer
 *      HB bootloader node pointer
 * OUT: none
 ***********************************************************/
static int program_hb(
    hb_node_t *hb,
    hb_bl_node_t *bl
)
{
    uint32_t start;
    uint32_t end;
    int	first_page;
    int num_pages;
    int rc;
    int rval = 0;

    if (hb && (bl->mode == MODE_APP))
    {
        rc = hb_boot_mode(hb);
        if (rc < 0)
        {
            log_entry(LEVEL_ERROR, "[%s] failed to send HB %d to boot mode", __func__, hb->id);
            rval = -1;
            goto close;
        }
        sleep(1); /* Give the HB time to get into the bootloader */
    }

    rc = open_parser(bl);
    if (rc < 0)
    {
        log_entry(LEVEL_ERROR, "[%s] failed to open parser", __func__);
        rval = -1;
        goto close;
    }

    rc = open_bl_connection(bl);
    if (rc < 0)
    {
        log_entry(LEVEL_ERROR, "[%s] failed to open bootloader connection", __func__);
        rval = -1;
        goto close;
    }

    log_entry(LEVEL_DEBUG, "[%s] Version      : 0x%02x", __func__, bl->stm->bl_version);
    log_entry(LEVEL_DEBUG, "[%s] Device ID    : 0x%04x (%s)", __func__, bl->stm->pid, bl->stm->dev->name);
    log_entry(LEVEL_DEBUG, "[%s] - RAM        : Up to %dKiB  (%db reserved by bootloader)", __func__, (bl->stm->dev->ram_end - 0x20000000) / 1024, bl->stm->dev->ram_start - 0x20000000);
    log_entry(LEVEL_DEBUG, "[%s] - Flash      : Up to %dKiB (size first sector: %dx%d)", __func__, (bl->stm->dev->fl_end - bl->stm->dev->fl_start ) / 1024, bl->stm->dev->fl_pps, bl->stm->dev->fl_ps[0]);
    log_entry(LEVEL_DEBUG, "[%s] - Option RAM : %db", __func__, bl->stm->dev->opt_end - bl->stm->dev->opt_start + 1);
    log_entry(LEVEL_DEBUG, "[%s] - System RAM : %dKiB", __func__, (bl->stm->dev->mem_end - bl->stm->dev->mem_start) / 1024);
    log_entry(LEVEL_DEBUG, "[%s] - RAM start  : 0x%08x", __func__, bl->stm->dev->ram_start);
    log_entry(LEVEL_DEBUG, "[%s] - RAM end    : 0x%08x", __func__, bl->stm->dev->ram_end);
    log_entry(LEVEL_DEBUG, "[%s] - Flash start: 0x%08x", __func__, bl->stm->dev->fl_start);
    log_entry(LEVEL_DEBUG, "[%s] - Flash end  : 0x%08x", __func__, bl->stm->dev->fl_end);

    first_page = APPLICATION_START_PAGE;
    start = hbupdate_flash_page_to_addr(bl, first_page);
    end = bl->stm->dev->fl_end;
    num_pages = hbupdate_flash_addr_to_page_ceil(bl, end) - first_page;

    write_hb_flash(bl, start, end, first_page, num_pages, false);

    if (init_bl_exit(bl->stm, bl->port, gpio_seq))
        log_entry(LEVEL_ERROR, "[%s] reset failed", __func__);
    else
        log_entry(LEVEL_INFO, "[%s] reset complete", __func__);

close:
    close_parser(bl);
    close_bl_connection(bl);
    return rval;
}

/***********************************************************
 * NAME: get_version_info
 * DESCRIPTION: this function opens the versions XML
 * file and reads its contents to extract the current
 * software release, SIA firmware version and DCR firmware
 * version.
 *
 * IN:  station info pointer
 * OUT: none
 ***********************************************************/
static int get_version_info(
    char **release,
    char **sia_fw_version,
    char **dcr_fw_version
)
{
    int fd;
    xmlDocPtr doc = NULL;
    xmlNodePtr rootElement;
    xmlNodePtr current;

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    /* init XML library parser */
    xmlInitParser();

    /* weird but needed by client code for libxml */
    LIBXML_TEST_VERSION

    /* Allow formatting of the XML file */
    xmlKeepBlanksDefault(0);

    /* Open and lock the XML file */
    fd = open(STATION_INFO_FILE, O_RDWR);
    if (fd < 0)
    {
        log_entry(LEVEL_ERROR, "[%s] unable to open %s", __func__, STATION_INFO_FILE);
        return -1;
    }

    /* Load the specified XML file */
    doc = xmlReadFile(STATION_INFO_FILE, NULL, XML_PARSE_RECOVER); 
    if (!doc)
    {
        log_entry(LEVEL_ERROR, "[%s] xmlReadFile error", __func__);
        close(fd);
        return -1;
    }

    /* Get the root element */
    rootElement = xmlDocGetRootElement(doc);
    for (current = rootElement->children; current != NULL; current = current->next)
    {
        xmlChar *value;

        value = xmlNodeGetContent(current);
        if      (strcmp((char *)current->name, RELEASE) == 0)
        {
            *release = (char *)value;
        }
        else if (strcmp((char *)current->name, SIA_VER) == 0)
        {
            *sia_fw_version = (char *)value;
        }
        else if (strcmp((char *)current->name, DCR_VER) == 0)
        {
            *dcr_fw_version = (char *)value;
        }
    }
    xmlFreeDoc(doc);
    close(fd);
    return 0;
}

/***********************************************************
 * NAME: hbupdate_process
 * DESCRIPTION: processes all device nodes to make sure that
 * hashing boards that are connected, in either application
 * mode or bootloader mode, are running the latest firmware
 * version.
 *
 * IN:  none
 * OUT: none
 ***********************************************************/
void hbupdate_process(void)
{
    int i;
    int rc;
    char *current_release = NULL;
    char *sia_release = NULL;
    char *dcr_release = NULL;

    rc = get_version_info(&current_release, &sia_release, &dcr_release);
    if (rc < 0)
    {
        log_entry(LEVEL_ERROR, "[%s] failed to get version info", __func__);
        return;
    }
    else if ((current_release == NULL) ||
             (dcr_release == NULL) ||
             (sia_release == NULL))
    {
        log_entry(LEVEL_ERROR, "[%s] incomplete version info", __func__);
        return;
    }

    for (i = 0; i < MAX_HASHING_BOARDS; ++i)
    {
        switch (hb_nodes[i].mode)
        {
            case MODE_APP:
            {
                hb_node_t *hb = hb_find_by_devnode(hb_app_list, hb_nodes[i].devnode);
                if (hb)
                {
                    switch (hb->type)
                    {
                        case HB_SIA:
                            if (strcmp(sia_release, hb->version) != 0)
                            {
                                log_entry(LEVEL_INFO, "[%s] SIA FW mismatch...programming HB %d: current: %s, actual: %s",
                                          __func__, hb->id, sia_release, hb->version);
                                rc = program_hb(hb, &hb_nodes[i]);
                                if (rc < 0)
                                    log_entry(LEVEL_ERROR, "[%s] programming failed for HB %d", __func__, hb->id);
                                else
                                    verify_hb(hb, sia_release);
                            }
                            else
                                log_entry(LEVEL_DEBUG, "[%s] SIA FW matches for HB %d",
                                          __func__, hb->id);
                        break;
                        case HB_DCR:
                            if (strcmp(dcr_release, hb->version) != 0)
                            {
                                log_entry(LEVEL_INFO, "[%s] DCR FW mismatch...programming HB %d: current: %s, actual: %s",
                                          __func__, hb->id, dcr_release, hb->version);
                                rc = program_hb(hb, &hb_nodes[i]);
                                if (rc < 0)
                                    log_entry(LEVEL_ERROR, "[%s] programming failed for HB %d", __func__, hb->id);
                                else
                                    verify_hb(hb, dcr_release);
                            }
                            else
                                log_entry(LEVEL_DEBUG, "[%s] DCR FW matches for HB %d",
                                          __func__, hb->id);
                        break;
                        default:
                            log_entry(LEVEL_DEBUG, "[%s] unknown HB type: %d", __func__, hb->type);
                        break;
                    }
                }
            }
            break;

            case MODE_BL:
            {
                rc = program_hb(NULL, &hb_nodes[i]);
                if (rc < 0)
                    log_entry(LEVEL_ERROR, "[%s] programming failed for HB on %s", __func__, hb_nodes[i].devnode);
                else
                {
                    hb_node_t hb;

                    /*
                     * Setup a temporary HB node so that we can verify
                     * the programming operation.
                     */
                    strcpy(hb.devnode, hb_nodes[i].devnode);
                    hb.type = hb_nodes[i].type;
                    hb.fd = hb_open_serial(hb.devnode);
                    if (hb.fd < 0)
                        log_entry(LEVEL_ERROR, "[%s] unable to verify programming HB on %s", __func__, hb_nodes[i].devnode);
                    else
                    {
                        switch (hb.type)
                        {
                            case HB_DCR: verify_hb(&hb, dcr_release); break;
                            case HB_SIA: verify_hb(&hb, sia_release); break;
                            default: break;
                        }
                        hb_close_serial(&hb);
                    }
                }
            }
            break;

            case MODE_NONE:
            default:
                /* Nothing to do */
            break;
        }
    }
}

/***********************************************************
 * NAME: hbupdate_init
 * DESCRIPTION: initialize the hashing board configuration
 *
 * IN:  none
 * OUT: hb list
 ***********************************************************/
void hbupdate_init(void)
{
    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    hbupdate_discover(hb_app_list);
}

/***********************************************************
 * NAME: hbupdate_shutdown
 * DESCRIPTION: shutdown the hashing board configuration
 *
 * IN:  none
 * OUT: none
 ***********************************************************/
void hbupdate_shutdown(void)
{
    int i;

    for (i = 0; i < MAX_HASHING_BOARDS; ++i)
        close_bl_connection(&hb_nodes[i]);

    hb_shutdown(hb_app_list);
}
