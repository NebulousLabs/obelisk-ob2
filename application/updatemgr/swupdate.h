/*
 * Copyright (C) 2009-2018 MapleLeaf Software, Inc
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * swupdate.h
 *
 * Dan Jozwiak: djozwiak@mlsw.biz
 * Date Created: 2009-10-20
 *
 * Description: 
 */
#ifndef __SWUPDATE_H__
#define __SWUPDATE_H__

#define MAX_FILENAME    256
#define KEYDIR      "/usr/local/ob2/keys"
#define TMP_DIR     "/tmp"

#define VERSION_INDEX_SIZE      3   /* 3 digit version format */
#define VERSION_MAJOR_INDEX     0
#define VERSION_MINOR_INDEX     1
#define VERSION_POINT_INDEX     2

/* Need these keys for decrypting */
#define PRIV_KEY    KEYDIR"/upgrade.prv"
#define SECRET_KEY  KEYDIR"/secret.key"

/* This key is the decrypted secret key name. */
#define DECRYPT_KEY TMP_DIR"/secret.decryp"

#define SOFTWARE_UPDATE_CMD UPDATE_TEMP_DIR"/update"

/* Names for dynamically generated script files */
#define RELEASE_FILE        "/usr/local/ob2/etc/ob2-release"

/* The minimum length for a valid version string N.N.N */
#define SWUPDATE_MIN_VER_LEN 5

#define SWUPDATE_MAX_PATH   128
#define SWUPDATE_MAX_CMD    256
#define SWUPDATE_WRK_BUFSZ  256
#define SWUPDATE_TMP_ERRSZ  256

extern void initiate_update(char *update_url);
extern void initiate_rollback(void);

#endif /* __SWUPDATE_H__ */


