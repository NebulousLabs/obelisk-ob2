/*
 * Copyright (C) 2009-2018 MapleLeaf Software, Inc
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * swupdate.c
 *
 * Dan Jozwiak: djozwiak@mlsw.biz
 * Date Created: 2009-10-20
 *
 * Description: 
 */

/* Standard Linux headers */
#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <time.h>

#include <sys/mman.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/mount.h>
#include <unistd.h>
#include <sys/vfs.h>
#include <zlib.h>
#include <libxml/parser.h>
#include <libxml/tree.h>

#include "common.h"
#include "swupdate.h"
#include "logger.h"
#include "xmlparser.h"

#define MAX_UPDATE_RETRIES  3

/********************************************************
 * NAME: get_file
 * DESCRIPTION: Download file from the specified URL
 *
 * IN: URL and local file path
 * OUT: 
 *********************************************************/
static int get_file(
    char *url,
    char *local
)
{
    char cmd[512];

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    sprintf(cmd, "/usr/bin/wget -O %s %s > /dev/null 2>&1", local, url);
    return system(cmd);
}

/***********************************************************
 * NAME: convert_version()
 * DESCRIPTION: Get the integer values (in decimal) from
 *              the passed in version number string.
 *
 * IN:  Pointer to version string to extract point value from
 *      Pointer to integer array to store the integer values
 * OUT: Point value from version string, or -1 for error
 ***********************************************************/
static int convert_version(
    const char *version_string,
    int version_value[]
)
{
    char buf[256];
    char *pch;
    int i = 0;

    strcpy(buf, version_string);

    pch = strtok(buf, ".");
    while (pch != NULL)
    {
        version_value[i] = atoi(pch);
        pch = strtok (NULL, ".");
        i++;
    }

    /* Make sure we parsed the string correctly... */
    if (i != VERSION_INDEX_SIZE)    
        return -1;

    return 0;
}

/***********************************************************
 * NAME: is_update_viable()
 * DESCRIPTION: Compare two version number strings to 
 * determine if the release version is a viable upgrade 
 * for the current version
 *
 * IN:  Pointer to current version
 *      Pointer to release version
 *      Pointer to previous version 
 * OUT: true if current_version can be updated to release_version. 
 *      false otherwise
 ***********************************************************/
static bool is_update_viable(
    const char *current_version, 
    const char *release_version, 
    const char *previous_version
)
{
    int current_version_value[VERSION_INDEX_SIZE];
    int release_version_value[VERSION_INDEX_SIZE];
    int previous_version_value[VERSION_INDEX_SIZE];

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    /* Convert the version to integer. Ignore the alpha character at index 0 */
    if (convert_version(&current_version[1], current_version_value) < 0)
    {
        log_entry(LEVEL_ERROR, "[%s] error parsing the current version", __func__);
        return false;
    }

    /* Convert the version to integer. Ignore the alpha character at index 0 */
    if (convert_version(&release_version[1], release_version_value) < 0)
    {
        log_entry(LEVEL_ERROR, "[%s] error parsing the release version", __func__);
        return false;
    }

    /* Convert the version to integer. Ignore the alpha character at index 0 */
    if (convert_version(&previous_version[1], previous_version_value) < 0)
    {
        log_entry(LEVEL_ERROR, "[%s] error parsing the previous version", __func__);
        return false;
    }

    /* 
     * Compare versions to see if this update version is viable.
     * Update is Viable if any of the following cases are true:
     * - current major = (release major -1) AND current version = previous version
     * - current minor = (release minor -1)
     * - current point < release point version
     */

    /***** Compare Major versions *****/
    if (current_version_value[VERSION_MAJOR_INDEX] == (release_version_value[VERSION_MAJOR_INDEX] - 1))
    {
        /* 
         * The release is a major revision ahead. But we can only update 
         * if the current version is the previous version. 
         * 
         * Example:
         * - Current version is r3.3.7
         * - Checking to see if r4.0.0 is viable
         * - r4.0.0 is viable IF r3.3.7 is in the versions database as the previous
         *   version. That is, r3.3.7 is the latest version of r3.x.x
         */
        if (strcmp(current_version, previous_version) == 0)
        {
            log_entry(LEVEL_DEBUG, "[%s] current version is behind by one major revision and there are no minor or point release updates. This update is viable.", __func__);
            return true;
        }
        else
            return false;
    }
    else if (current_version_value[VERSION_MAJOR_INDEX] > release_version_value[VERSION_MAJOR_INDEX])
    {
        /* The current is ahead of the release. This versions database is out-of-date */
        log_entry(LEVEL_ERROR, "[%s] current version is ahead of the release version", __func__);
        return false;
    }
    else if (current_version_value[VERSION_MAJOR_INDEX] < release_version_value[VERSION_MAJOR_INDEX])
    {
        /* The current is behind by multiple major revisions. This is not viable */
        log_entry(LEVEL_ERROR, "[%s] current version is behind by multiple major versions", __func__);
        return false;
    }

    /***** Compare Minor versions *****/
    if (current_version_value[VERSION_MINOR_INDEX] == (release_version_value[VERSION_MINOR_INDEX] - 1))
    {
        /* 
         * The release is a minor revision ahead. But we can only update 
         * if the current version is the previous version. 
         * 
         * Example:
         * - Current version is r1.0.0
         * - Checking to see if r1.1.0 is viable
         * - r1.1.0 is viable IF r1.0.0 is in the versions database as the previous
         *   version. That is, r1.0.0 is the latest version of r1.0.x and there are 
         *   no point releases to update to first (r1.0.1, r1.0.2, etc).
         */
        if (strcmp(current_version, previous_version) == 0)
        {
            log_entry(LEVEL_DEBUG, "[%s] current version is behind by one minor revision and there are no point release updates. This update is viable.", __func__);
            return true;
        }
        else
            return false;
    }
    else if (current_version_value[VERSION_MINOR_INDEX] > release_version_value[VERSION_MINOR_INDEX])
    {
        /* The current is ahead of the release. This versions database is out-of-date */
        log_entry(LEVEL_ERROR, "[%s] current version is ahead of the release version", __func__);
        return false;
    }
    else if (current_version_value[VERSION_MINOR_INDEX] < release_version_value[VERSION_MINOR_INDEX])
    {
        /* The current is behind by multiple minor revisions. This is not viable */
        log_entry(LEVEL_ERROR, "[%s] current version is behind by multiple minor versions", __func__);
        return false;
    }

    /***** Compare Point versions *****/
    if (current_version_value[VERSION_POINT_INDEX] < release_version_value[VERSION_POINT_INDEX])
    {
        /* The current is behind by one or more point revision and the Major and Minor revisions are the same. Viable update */
        log_entry(LEVEL_DEBUG, "[%s] current version is behind by point release(s). Viable update.", __func__);
        return true;
    }
    else if (current_version_value[VERSION_POINT_INDEX] > release_version_value[VERSION_POINT_INDEX])
    {
        /* The current is ahead of the release. This versions database is out-of-date */
        log_entry(LEVEL_ERROR, "[%s] current version is ahead of the release version", __func__);
        return false;
    }

    /* We should never get here.. but this would mean versions are the same */
    log_entry(LEVEL_DEBUG, "[%s] release version (%s) is not a viable update for current version (%s)", __func__, release_version, current_version);
    return false;
}

/***********************************************************
 * NAME: is_update_required()
 * DESCRIPTION: Compare two version strings to determine if
 * the system is out-of-date and if an update is required
 *
 * IN:  Pointer to current version
 *      Pointer to release version
 * OUT: true if release_version is greater than current_version
 *      false otherwise
 ***********************************************************/
static bool is_update_required(
    const char *current_version,
    const char *release_version
)
{
    int current_version_value[VERSION_INDEX_SIZE];
    int release_version_value[VERSION_INDEX_SIZE];
    int i;

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    if (strcmp(current_version, release_version) == 0)
    {
        log_entry(LEVEL_DEBUG, "[%s] versions are equal", __func__);
        return false;
    }

    /* Convert the version to integer. Ignore the alpha character at index 0 */
    if (convert_version(&current_version[1], current_version_value) < 0)
    {
        log_entry(LEVEL_ERROR, "[%s] error occurred parsing the current version", __func__);
        return false;
    }
    
    /* Convert the version to integer. Ignore the alpha character at index 0 */
    if (convert_version(&release_version[1], release_version_value) < 0)
    {
        log_entry(LEVEL_ERROR, "[%s] error occurred parsing the release version", __func__);
        return false;
    }
    
    /* Compare versions to see if an update is required */
    for (i = 0; i < VERSION_INDEX_SIZE; i++)
    {
        if (current_version_value[i] < release_version_value[i])
            return true;
    }
    
    return false;
}

/***********************************************************
 * NAME: decrypt_package()
 * DESCRIPTION: Decrypts the file.
 *
 * IN:  Pointer to pathname of encrypted package file
 *      Pointer to path where decrypted package file will be
 *      placed.
 * OUT: 0 on success, error code otherwise
 ***********************************************************/
static int decrypt_package(
    char *encrypted_package,
    const char *decrypted_package_path
)
{
    int rc;
    char cmd[512];
    struct stat info;

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    /*
     * Verify that we have the private and encrypted secret keys
     * present.
     */
    rc = stat(PRIV_KEY, &info);
    if (rc)
    {
        log_entry(LEVEL_ERROR, "[%s] unable to find private key: %s", __func__, strerror(errno));
        return -1;
    }

    rc = stat(SECRET_KEY, &info);
    if (rc)
    {
        log_entry(LEVEL_ERROR, "[%s] unable to find secret key: %s", __func__, strerror(errno));
        return -1;
    }

    /* Build command to decrypt the encrypted private key using the RSA private key */
    sprintf(cmd, "openssl rsautl -decrypt -inkey %s -in %s -out %s", PRIV_KEY, SECRET_KEY, DECRYPT_KEY);

    rc = system(cmd);
    if (rc)
    {
        log_entry(LEVEL_ERROR, "[%s] failed to decrypt secret key",__func__);
        return -1;
    }

    /*
     * Make sure that the key is only readable by the owner
     * May be overkill for embedded use, so can disuss
     * removing.
     */
    sprintf(cmd, "chmod 600 %s", DECRYPT_KEY);
    rc = system(cmd);
    if (rc)
    {
        log_entry(LEVEL_ERROR, "[%s] chmod 600 of decrypt key failed", __func__);
        return -1;
    }

    /* Need to make sure that the decrypted package path exists */
    sprintf(cmd, "mkdir -p %s", decrypted_package_path);
    rc = system(cmd);

    /* Build command to decrypt the packge wrapper and extract the package file using the decrypted secret key */
    sprintf(cmd, "dd if=%s | openssl enc -d -blowfish -pass file:%s -md md5 | tar xzf - -C %s", encrypted_package,
            DECRYPT_KEY, decrypted_package_path);
    rc = system(cmd);
    if (rc)
    {
        log_entry(LEVEL_ERROR, "[%s] failed to decrypt package file %s to %s",__func__, encrypted_package, decrypted_package_path);
        unlink(DECRYPT_KEY);
        return -1;
    }

    unlink(DECRYPT_KEY);
    /*
     * The calling function will be attempting to extract the package contents, so if there was
     * some kind of failure that was not detected during decryption, e.g. a badly built package
     * the extraction process will return a failure code.  So we do not need to jump though the
     * hoops here to use tar -t to test the decrypted and extracted package file..
     */

    return 0;
}

/***********************************************************
 * NAME: get_current_version()
 * DESCRIPTION: Retrieves the current version from the release
 * file in etc
 *
 * IN:  Pointer to current version string buffer
 * OUT: 0 if successful, -1 otherwise
 ***********************************************************/
static int get_current_version(
    char *current_version
)
{
    char buf[256];
    FILE *fp;

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    fp = fopen(RELEASE_FILE, "r");
    if (fp == NULL)
    {
        log_entry(LEVEL_DEBUG, "[%s] unable to open release file for reading", __func__);
        log_entry(LEVEL_ERROR, "[%s] unable to retrieve current version", __func__);
        return -1;
    }

    while (fgets(buf, sizeof(buf), fp) != NULL)
    {
        if (strstr(buf, "Release: ") != NULL)
        {
            /* Split the line and retrieve our tag */
            char *versionTag = strtok(buf, ":");

            versionTag = strtok(NULL, ": "); /* This is our release tag */
            strtok(versionTag, "\n");        /* Removing the trailing newline */

            strncpy(current_version, versionTag, strlen(versionTag));

            log_entry(LEVEL_DEBUG, "[%s] current software version: %s", __func__, current_version);
            fclose(fp);
            return 0;
        }
    }

    log_entry(LEVEL_ERROR, "[%s] unable to retrieve current version", __func__);
    fclose(fp);
    return -1;
}

/***********************************************************
 * NAME: get_release_version()
 * DESCRIPTION: Retrieves the latest release version from the 
 * versions database
 *
 * IN:  Pointer to current software versions database file
 *      Pointer to release version to store the data retrieved
 * OUT: 0 if successful, -1 otherwise
 ***********************************************************/
static int get_release_version(
    char *versions_file,
    char *release_version
)
{
    xmlNodePtr version_node;
    xmlNodePtr root;
    xmlDocPtr  doc;

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    /* Parse the update versions database file, get the version number */
    doc = xmlReadFile(versions_file, NULL, XML_PARSE_RECOVER);
    if (doc == NULL)
    {
        /* If no versions database file then we cannot update */
        log_entry(LEVEL_DEBUG, "[%s] unable to read versions database...", __func__);
        return -1;
    }

    root = xmlDocGetRootElement(doc);
    if (root == NULL)
    {
        /* There is a problem reading the versions database file */
        log_entry(LEVEL_ERROR, "[%s] unable to read root element of the versions database", __func__);
        goto err;
    }

    version_node = get_node_by_name(root, "version");
    if (version_node == NULL)
    {
        /* There is a problem reading the versions database file */
        log_entry(LEVEL_ERROR, "[%s] unable to read version element of the current versions database", __func__);
        goto err;
    }

    while (version_node)
    {
        char *current_release_version;

        /* Now get release_version */
        current_release_version = get_node_content_by_name(version_node, "releaseVersion");
        if (current_release_version)
        {
            if (strlen(current_release_version) >= SWUPDATE_MIN_VER_LEN)
            {
                strcpy(release_version, current_release_version);
                xmlFree(current_release_version);

                break;
            }
            else
            {
                /* There is a problem reading the versions database file */
                log_entry(LEVEL_ERROR, "[%s] corrupt versions database file. Bad version read", __func__);
                xmlFree(current_release_version);
                goto err;
            }
        }
        else
        {
            log_entry(LEVEL_ERROR, "[%s] corrupt versions database file. Unable to read release version", __func__);
            goto err;
        }

        version_node = xmlNextElementSibling(version_node);
    }

    log_entry(LEVEL_DEBUG, "[%s] latest release version is %s", __func__, release_version);
    log_entry(LEVEL_DEBUG, "[%s] finished processing versions database. Cleaning up...", __func__);

    xmlFreeDoc(doc);
    return 0;

err:
    xmlFreeDoc(doc);
    return -1;
}

/***********************************************************
 * NAME: get_viable_version()
 * DESCRIPTION: Processes the versions database to find
 * a viable update for the current version
 *
 * IN:  Pointer to current software versions database file
 *      Pointer to current version
 *      Pointer to store viable version if found
 * OUT: 0 if successful, -1 otherwise
 ***********************************************************/
static int get_viable_version(
    char *versions_file, 
    char *current_version, 
    char *viable_version,
    char *viable_package
)
{
    xmlNodePtr version_node;
    xmlNodePtr root;
    xmlDocPtr  doc;

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    /* Parse the update versions database, get the version number */
    doc = xmlReadFile(versions_file, NULL, XML_PARSE_RECOVER);
    if (doc == NULL )
    {
        /* If no versions database then we cannot update */
        log_entry(LEVEL_DEBUG, "[%s] unable to read versions database", __func__);
        return -1;
    }

    root = xmlDocGetRootElement(doc);
    if (root == NULL)
    {
        /* There is a problem reading the versions database */
        log_entry(LEVEL_ERROR, "[%s] unable to read root element of the version database", __func__);
        goto err;
    }

    version_node = get_node_by_name(root, "version");
    if (version_node == NULL)
    {
        /* There is a problem reading the versions database */
        log_entry(LEVEL_ERROR, "[%s] unable to read version element of the current versions database", __func__);
        goto err;
    }

    while (version_node)
    {
        char *previous_version;
        char *current_release_version;
        char *current_release_package;

        /* Now get release_version */
        current_release_version = get_node_content_by_name(version_node, "releaseVersion");
        current_release_package = get_node_content_by_name(version_node, "upgradeFile");
        previous_version = get_node_content_by_name(version_node, "previousVersion");

        if ((current_release_version) && (previous_version))
        {
            if (strlen(current_release_version) >= SWUPDATE_MIN_VER_LEN)
            {
                /* See if this is a viable update */
                log_entry(LEVEL_DEBUG, "[%s] checking viability of release version: %s", __func__, current_release_version);
                if (is_update_viable(current_version, current_release_version, previous_version))
                {
                    /* This is a viable update package */
                    strcpy(viable_version, current_release_version);

                    if (current_release_package && (strlen(current_release_package) > 0))
                        strcpy(viable_package, current_release_package);
                    else
                    {
                        log_entry(LEVEL_ERROR, "[%s] release package for version %s empty", __func__, viable_version);
                        strcpy(viable_package, "");
                    }
                    /* Clean up and return */
                    if (current_release_package)
                        xmlFree(current_release_package);
                    xmlFree(current_release_version);
                    xmlFree(previous_version);
                    goto out;
                }
                if (current_release_version)
                    xmlFree(current_release_version);
                if (current_release_package)
                    xmlFree(current_release_package);
                if (previous_version)
                    xmlFree(previous_version);
            }
            else
            {
                /* There is a problem reading the versions database */
                log_entry(LEVEL_ERROR, "[%s] corrupt versions database...bad version read", __func__);
                if (current_release_version)
                    xmlFree(current_release_version);
                if (current_release_package)
                    xmlFree(current_release_package);
                if (previous_version)
                    xmlFree(previous_version);
                goto err;
            }
        }
        else
        {
            log_entry(LEVEL_ERROR, "[%s] corrupt versions database...unable to read release version", __func__);
            goto err;
        }

        version_node = xmlNextElementSibling(version_node);
    }

    log_entry(LEVEL_WARNING, "[%s] no viable update version was found in the versions database.", __func__);

out:
    xmlFreeDoc(doc);
    return 0;

err:
    xmlFreeDoc(doc);
    return -1;
}

/***********************************************************
 * NAME: install_update()
 * DESCRIPTION: Install an update package.
 *
 * IN:  Pointer to update package file pathname.
 * OUT: 0 for success or error code on failure.
 ***********************************************************/
static int install_update(
    char *update_package
)
{
    int rc;
    uint32_t ret = 0;
    struct stat info;
    FILE *fp = NULL;
    uint8_t *buffer = NULL;
    uint32_t file_crc;
    uint32_t calculated_crc;
    pid_t pid;
    char *tmp;
    char cmd[512];
    char *argv[4];
    char encrypted_filename[MAX_FILENAME];
    char decrypted_package_file[MAX_FILENAME];
    char extracted_files_path[MAX_FILENAME] = TMP_DIR;

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    /* Get information about the file...we need size */
    ret = stat(update_package, &info);
    if (ret < 0)
    {
        log_entry(LEVEL_ERROR, "[%s] unable to stat package %s: %s", __func__, update_package, strerror(errno));
        return -1;
    }

    if (info.st_size == 0)
    {
        log_entry(LEVEL_ERROR, "[%s] package %s is empty", __func__, update_package);
        return -1;
    }

    /* Open the package */
    fp = fopen(update_package, "rb");
    if (fp == NULL)
    {
        log_entry(LEVEL_ERROR, "[%s] unable to open package %s: %s", __func__, update_package, strerror(errno));
        return -1;
    }

    /* Allocate a buffer to read the file into and do the read */
    buffer = (uint8_t*)calloc(1, info.st_size);
    if (buffer == NULL)
    {
        log_entry(LEVEL_ERROR, "[%s] insufficient memory", __func__);
        return -1;
    }

    ret = fread(buffer, 1, info.st_size, fp);
    if (ret != info.st_size)
    {
        log_entry(LEVEL_ERROR, "[%s] unable to read package: %s", __func__, update_package);
        fclose(fp);
        goto err;
    }

    /* Get the CRC from the file and close the file */
    file_crc = *(uint32_t *)(buffer + (info.st_size - sizeof(uint32_t)));
    fclose(fp);

    /* Compute the CRC on the file proper, excluding the CRC at end. */
    calculated_crc = crc32(0L, Z_NULL, 0);
    calculated_crc = crc32(calculated_crc, buffer, info.st_size - sizeof(uint32_t));

    /* ...and now compare to make sure they're good */
    if (file_crc != calculated_crc)
    {
        log_entry(LEVEL_ERROR, "[%s] CRC error for package: %s", __func__, update_package);
        goto err;
    }

    log_entry(LEVEL_INFO, "[%s] update package %s valid", __func__, update_package);

    /*
     * Now we need to strip the CRC from the file. We'll do this by
     * saving it to a new file with a .enc file extension.
     * Search for the file extension and replace the extension.
     */
    strcpy(encrypted_filename, update_package);
    tmp = strchr(encrypted_filename, '-');
    if (tmp)
        sprintf(tmp, ".enc");
    else
        strcat(encrypted_filename, ".enc");

    /* Save the file without CRC */
    fp = fopen(encrypted_filename, "wb");
    if (fp == NULL)
    {
        log_entry(LEVEL_ERROR, "[%s] unable to open file: %s", __func__, encrypted_filename);
        goto err;
    }

    ret = fwrite(buffer, 1, info.st_size - sizeof(uint32_t), fp);
    if (ret != (info.st_size - sizeof(uint32_t)))
    {
        log_entry(LEVEL_ERROR, "[%s] unable to save the file package without the CRC", __func__);
        fclose(fp);
        /* remove the stripped encrypted file if present */
        unlink(encrypted_filename);
        goto err;
    }

    fclose(fp);
    free(buffer);

    /* sync to make sure that data is written to disk (best effort) */
    rc = system("sync");
    if (rc < 0)
        log_entry(LEVEL_WARNING, "[%s] sync failed", __func__, strerror(errno));

    log_entry(LEVEL_DEBUG, "[%s] package '%s' CRC calculated successfully stripped", __func__, update_package);

    /* Decrypt the .enc file */
    strcat(extracted_files_path, "/");
    ret = decrypt_package(encrypted_filename, TMP_DIR);
    if (ret)
    {
        /* Decryption failed...remove the stripped encrypted file */
        unlink(encrypted_filename);
        log_entry(LEVEL_ERROR, "[%s] failed to decrypt the package", __func__);
        return -1;
    }

    /* We've sucessfully decrypted...remove the stripped encrypted file and sync. */
    unlink(encrypted_filename);
    rc = system("sync");
    if (rc < 0)
        log_entry(LEVEL_WARNING, "[%s] sync failed", __func__, strerror(errno));

    log_entry(LEVEL_DEBUG, "[%s] package %s successfully decrypted", __func__, update_package);

    sprintf(decrypted_package_file, "%s/%s.tgz", TMP_DIR, UPDATE_PREFIX);

    /* Extract the backup script to our temp directory. */
    sprintf(cmd, "tar -xz -f %s -C %s usr/local/ob2/bin/backup", decrypted_package_file, TMP_DIR);
    rc = system(cmd);
    if (rc < 0)
    {
        log_entry(LEVEL_ERROR, "[%s] failed to extract backup script", __func__);
        return -1;
    }

    /* Extract the update script to our temp directory. */
    sprintf(cmd, "tar -xz -f %s -C %s usr/local/ob2/bin/update", decrypted_package_file, TMP_DIR);
    ret = system(cmd);
    if (ret)
    {
        log_entry(LEVEL_ERROR, "[%s] failed to extract update script", __func__);
        return -1;
    }

    /*
     * Fork off a process that will run the update script. That process
     * will shutdown all application processes. Then the update script
     * will perform a backup using the dobackup script and finally will
     * extract the update package file.
     */
    sprintf(cmd, "%s%s", TMP_DIR, UPDATE_SCR);
    argv[0] = "/bin/sh";
    argv[1] = cmd;
    argv[2] = NULL;

    pid = fork();
    switch (pid)
    {
        case -1:
            log_entry(LEVEL_ERROR, "[%s] fork of %s failed\n", __func__, argv[0]);
        break;

        case 0:
        {
            execvp(argv[0], argv);
        }
        break;

        default:
        break;
    }

    log_entry(LEVEL_DEBUG, "[%s] update process started...", __func__);
    return 0;

err:
    free(buffer);
    return -1;
}

/***********************************************************
 * NAME: process_versions_file
 * DESCRIPTION: Processes the versions file. Obtains
 * the current version of software on the system as well as
 * the latest release version from the database and determines
 * if an update is required. If so, it will search the versions
 * database for a viable update version.
 * 
 * IN:  Pointer to versions database
 *      Pointer to store viable version if found
 * OUT: 0 to indicate an update should be done
 *      -1 on error or if no update is possible
 ***********************************************************/
static int process_versions_file(
    char *versions_file,
    char *viable_version,
    char *viable_package,
    char *mount_point
)
{
    char release_version[256] = {0};
    char current_version[256] = {0};

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    /* Get our current software version */
    if (get_current_version(current_version) < 0)
    {
        log_entry(LEVEL_ERROR, "[%s] no current version...aborting update.", __func__);
        return -1;
    }

    /* Get the latest software version */
    if (get_release_version(versions_file, release_version) < 0)
    {
        log_entry(LEVEL_ERROR, "[%s] no release version...aborting update.", __func__);
        return -1;
    }

    /* Check to see if we should update based on the versions database */
    log_entry(LEVEL_DEBUG, "[%s] comparing current version (%s) to release version (%s)", __func__, current_version, release_version);

    /* Pass in the version strings excluding the prefix character */
    if (is_update_required(current_version, release_version) == false)
    {
        log_entry(LEVEL_WARNING, "[%s] no update required...system is up-to-date.", __func__);
        if (mount_point)
            umount(mount_point);
        return -1;
    }

    /* Yes, the system is out-of-date...*/
    log_entry(LEVEL_DEBUG, "[%s] system out-of-date", __func__);

    /* Determine what next viable version is */
    if (get_viable_version(versions_file, current_version, viable_version, viable_package) < 0)
    {
        log_entry(LEVEL_ERROR, "[%s] no viable update version...aborting update.", __func__);
        return -1;
    }

    return 0;
}

/***********************************************************
 * NAME: initiate_update()
 * DESCRIPTION: This function is called when the update manager
 * receives a message from the control manager that an is
 * available.
 * 
 * IN:  none
 * OUT: none
 ***********************************************************/
void initiate_update(
    char *update_url
)
{
    char local_update_package[SWUPDATE_WRK_BUFSZ] = {0};
    char remote_update_package[SWUPDATE_WRK_BUFSZ] = {0};
    char local_versions_file[SWUPDATE_WRK_BUFSZ] = {0};
    char remote_versions_file[SWUPDATE_WRK_BUFSZ] = {0};
    char viable_version[SWUPDATE_WRK_BUFSZ] = {0};
    char viable_package[SWUPDATE_WRK_BUFSZ] = {0};
    uint8_t retry_count;
    struct stat info;
    int rc;

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    sprintf(remote_versions_file, "%s/%s", update_url, VERSION_FILE);
    sprintf(local_versions_file, "%s/%s", UPDATES_DIR, VERSION_FILE);
    log_entry(LEVEL_INFO, "[%s] downloading versions DB: %s", __func__, remote_versions_file);
    retry_count = 0;

retry_db_get:
    rc = get_file(remote_versions_file, local_versions_file);
    if (rc < 0)
    {
        log_entry(LEVEL_WARNING, "[%s] versions DB download failed: retry %d", __func__, retry_count);
        ++retry_count;
        if (retry_count < MAX_UPDATE_RETRIES)
            goto retry_db_get;
        else
        {
            log_entry(LEVEL_ERROR, "[%s] failed to get versions DB", __func__);
            return;
        }
    }
    rc = stat(local_versions_file, &info);
    if ((rc < 0) || (info.st_size == 0))
    {
        log_entry(LEVEL_ERROR, "[%s] versions DB empty or not found", __func__);
        return;
    }
    
    /* 
     * Process the versions database to determine if we need an update. 
     * If so, the viable version will be populated with the update we
     * need. Download this file from the server and kick off an installation.
     */
    if (process_versions_file(local_versions_file, viable_version, viable_package, NULL) < 0)
        return;
    else if (strlen(viable_package) == 0)
    {
        log_entry(LEVEL_ERROR, "[%s] viable package empty...aborting update", __func__);
        return;
    }

    log_entry(LEVEL_INFO, "[%s] viable update version found in the versions database: %s", __func__, viable_version);

    /* Download update package from server */
    sprintf(remote_update_package, "%s/%s", update_url, viable_package);
    sprintf(local_update_package, "%s/%s", UPDATES_DIR, viable_package);
    log_entry(LEVEL_INFO, "[%s] downloading update package: %s", __func__, remote_update_package);
    retry_count = 0;

retry_pkg_get:
    rc = get_file(remote_update_package, local_update_package);
    if (rc < 0)
    {
        log_entry(LEVEL_WARNING, "[%s] package download failed: retry %d", __func__, retry_count);
        ++retry_count;
        if (retry_count < MAX_UPDATE_RETRIES)
            goto retry_pkg_get;
        else
        {
            log_entry(LEVEL_ERROR, "[%s] package download failed", __func__);
            return;
        }
    }
    rc = stat(local_update_package, &info);
    if ((rc < 0) || (info.st_size == 0))
    {
        log_entry(LEVEL_ERROR, "[%s] package %s empty or not found", __func__, local_update_package);
        return;
    }

    /* Install update package */
    log_entry(LEVEL_INFO, "[%s] installing update package: %s", __func__, local_update_package);    
    retry_count = 0;

retry_pkg_install:
    if (install_update(local_update_package) < 0)
    {
        log_entry(LEVEL_WARNING, "[%s] installation of update failed: retry %d", __func__, retry_count);
        ++retry_count;
        if (retry_count < MAX_UPDATE_RETRIES)
            goto retry_pkg_install;
        else
            log_entry(LEVEL_ERROR, "[%s] installation of update failed", __func__);
    }
}

/***********************************************************
 * NAME: initiate_ota_rollback()
 * DESCRIPTION: This function is called when the UM receives
 * a message from the SM that an OTA rollback command was received
 * from the server. This function will simply initiate the
 * software rollback operation.
 * 
 * IN:  none
 * OUT: none
 ***********************************************************/
void initiate_rollback(void)
{
    pid_t pid;
    char cmd[512];
    char *argv[4];

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    /*
     * Fork off a process that will run the rollback script. That process
     * will shutdown all application processes and will proceed with the
     * rollback operation.
     */
    sprintf(cmd, "%s", ROLLBACK_SCR);
    argv[0] = "/bin/sh";
    argv[1] = cmd;
    argv[2] = NULL;

    pid = fork();
    switch (pid)
    {
        case -1:
            log_entry(LEVEL_ERROR, "[%s] fork of %s failed\n", __func__, argv[0]);
        break;

        case 0:
        {
            execvp(argv[0], argv);
        }
        break;

        default:
        break;
    }

    log_entry(LEVEL_DEBUG, "[%s] rollback process started...", __func__);
}
