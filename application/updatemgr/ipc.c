/*
 * Copyright (C) 2014-2018 MapleLeaf Software, Inc
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * ipc.c
 *
 * Dan Jozwiak: djozwiak@mlsw.biz
 * Date Created: 2014-05-20
 *
 * Description: this file contains the code to process connections and
 * messages from the server. 
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/queue.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <errno.h>

#include "common.h"
#include "ipc_messages.h"
#include "ipc.h"
#include "logger.h"
#include "swupdate.h"
#include "ports.h"

static msg_ipc_tlv_t *build_identity(void)
{
    void *msg = NULL;
    size_t msg_len = sizeof(msg_ipc_tlv_t) + sizeof(tlv_ipc_identity_t);

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    msg = calloc(1, msg_len);
    if (msg)
    {
        msg_ipc_tlv_t *tlv = msg;
        tlv_ipc_identity_t *identity = (tlv_ipc_identity_t *)tlv->value;

        tlv->type = ATTR_TYPE_IPC_IDENTITY;
        tlv->len = sizeof(tlv_ipc_identity_t);
        identity->id = IPC_UPDATE_MANAGER;
    }

    return msg;
}

static int process_ipc_tlv(
    int fd,
    msg_ipc_tlv_t *tlv
)
{
    msg_ipc_tlv_t *msg = NULL;

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);
    switch (tlv->type)
    {
        case ATTR_TYPE_IPC_UPDATE:
        {
            tlv_ipc_update_request_t *info = (tlv_ipc_update_request_t *)tlv->value;

            log_entry(LEVEL_INFO, "[%s] received update notification", __func__);
            initiate_update(info->url);
        }
        break;

        case ATTR_TYPE_IPC_ROLLBACK:
        {
            log_entry(LEVEL_INFO, "[%s] received rollback notification", __func__);
            initiate_rollback();
        }
        break;

        default:
            log_entry(LEVEL_ERROR, "[%s] received unknown TLV type from server:%d", __func__, tlv->type);
            return -1;
        break;
    }

    if (msg)
    {
        if (fd > 0)
        {
            int bytes = send(fd, msg, sizeof(msg_ipc_tlv_t) + msg->len, MSG_NOSIGNAL);
            if (bytes < 0)
                log_entry(LEVEL_ERROR, "[%s] send failed: %s", __func__, strerror(errno));
            else if (bytes == 0)
                log_entry(LEVEL_ERROR, "[%s] sent 0 bytes: %s", __func__, strerror(errno));
        }
        else
            log_entry(LEVEL_WARNING, "[%s] not connected, message will not be sent", __func__);
        free(msg);
    }

    return sizeof(msg_ipc_tlv_t) + tlv->len;
}

/***********************************************************
 * NAME: process_ipc_message
 * DESCRIPTION: process a message from IPC server
 *
 * IN:  server file descriptor
 * OUT: none
 ***********************************************************/
int process_ipc_message(
    int fd
)
{
    int bytes_received = 0;
    char buf[MAX_RECV_LEN];
   
    memset(buf, 0, MAX_RECV_LEN);
    bytes_received = recv(fd, buf, MAX_RECV_LEN, 0);
    switch (bytes_received)
    {
        case -1:
        {
            log_entry(LEVEL_ERROR, "[%s] recv error: %s", __func__, strerror(errno));
            return -1;
        }
        break;
        
        case 0:
        {
            log_entry(LEVEL_WARNING, "[%s] far end closed connection", __func__);
            return 0;
        }
        break;
        
        default:
        {
            uint8_t *tmp_tlv;

            /* One or more messages received...process */
            msg_ipc_tlv_t *tlv = (msg_ipc_tlv_t *)buf;
            tmp_tlv = (uint8_t *)tlv;
            while (bytes_received > 0)
            {
                process_ipc_tlv(fd, tlv);
                bytes_received -= (sizeof(msg_ipc_tlv_t) + tlv->len);
                if (bytes_received > 0)
                {
                    tmp_tlv += (sizeof(msg_ipc_tlv_t) + tlv->len); /* move to the next message */
                    tlv = (msg_ipc_tlv_t *)tmp_tlv;
                }
            }
        }
    } /* End switch (bytes_received) */
    return 1;
}

/***********************************************************
 * NAME: ipc_server_connect
 * DESCRIPTION: connect to the server (station manager)
 *
 * IN:  none
 * OUT: server file descriptor or -1 on error
 ***********************************************************/
void ipc_server_connect(int *fd)
{
    int opt = 1;
    struct sockaddr_in server_socket;
    msg_ipc_tlv_t *msg;

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    /* Get a tcp file descriptor */
    *fd = socket(PF_INET, SOCK_STREAM, 0);
    if (*fd < 0)
    {
        log_entry(LEVEL_ERROR, "[%s] failed to create a socket", __func__);
        return;
    }

    /* set this so that we can rebind to it after a crash or a manual kill */
    if (setsockopt(*fd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt)) < 0)
    {
        log_entry(LEVEL_ERROR, "[%s] failed to set socket option: %s", __func__, strerror(errno));
        goto err;
    }

    /* Setup the tcp port information */
    memset(&server_socket, 0, sizeof(server_socket));
    server_socket.sin_family = AF_INET;
    server_socket.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
    server_socket.sin_port = htons(CONTROL_MGR_PORT);

    if (connect(*fd, (struct sockaddr *)&server_socket, sizeof(server_socket)) < 0)
    {
        log_entry(LEVEL_ERROR, "[%s] failed to connect: %s", __func__, strerror(errno));
        goto err;
    }

    log_entry(LEVEL_INFO, "[%s] ipc server connection opened", __func__);

    /* Send identity */
    msg = build_identity();
    if (msg)
    {
        int bytes = send(*fd, msg, sizeof(msg_ipc_tlv_t) + msg->len, MSG_NOSIGNAL);
        if (bytes < 0)
            log_entry(LEVEL_ERROR, "[%s] send failed: %s", __func__, strerror(errno));
        else if (bytes == 0)
            log_entry(LEVEL_ERROR, "[%s] sent 0 bytes: %s", __func__, strerror(errno));
        free(msg);
    }

    return;
err:
    close(*fd);
    *fd = -1;
}

/***********************************************************
 * NAME: ipc_server_connect
 * DESCRIPTION: connect to the server (station manager)
 *
 * IN:  none
 * OUT: server file descriptor or -1 on error
 ***********************************************************/
void ipc_server_disconnect(
    int *fd
)
{
    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    if (*fd > 0)
    {
        close(*fd);
        *fd = -1;
        log_entry(LEVEL_INFO, "[%s] ipc server connection closed", __func__);
    }
}
