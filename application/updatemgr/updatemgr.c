/*
 * Copyright (C) 2009-2018 MapleLeaf Software, Inc
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * updatemgr.c
 *
 * Dan Jozwiak: djozwiak@mlsw.biz
 * Date Created: 2009-10-20
 *
 * Description: This file is the central point of processing for the Software
 * Update Manager.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/mount.h>
#include <sys/time.h>
#include <sys/inotify.h>
#include <netinet/in.h>
#include <errno.h>
#include <net/if.h>

#include "common.h"
#include "ipc.h"
#include "logger.h"

static int ipc_fd;

static char *log_file;
static bool do_fork = true;

/***********************************************************
 * NAME: process_signal
 * DESCRIPTION: this is a generic signal handler that can be
 * used to process any Unix signal. Currently not used.
 *
 * IN:  generated signal number
 * OUT: none
 ***********************************************************/
static void process_signal(int sig)
{
    switch(sig)
    {
        case SIGUSR1:
        break;
        case SIGUSR2:
        break;
        case SIGHUP:
            if (log_level(0) == LEVEL_INFO)
            {
                log_level(LEVEL_DEBUG);
                log_entry(LEVEL_INFO, "[%s] log level DEBUG", __func__);
            }
            else
            {
                log_level(LEVEL_INFO);
                log_entry(LEVEL_INFO, "[%s] log level INFO", __func__);
            }
        break;
        case SIGINT:
        case SIGQUIT:
        case SIGTERM:
            log_entry(LEVEL_INFO, "[%s] update manager exiting", __func__);
            ipc_server_disconnect(&ipc_fd);
            log_exit();
            exit(0);
        break;
    }
}

/***********************************************************
 * NAME: usage
 * DESCRIPTION: this function simply outputs to the console
 * the usage of the SW Update Manager.
 *
 * IN:  program name
 * OUT: none
 ***********************************************************/
static void usage(char *name)
{
    printf("Usage:%s\n", name);
    printf("\t-d: don't automatically fork\n"
           "\t-l <log file>: specify full path to log file\n"
          );
    exit(1);
}

/***********************************************************
 * NAME: process_args
 * DESCRIPTION: this function processes any arguments that
 * were passed to the manager on the command line. Current
 * supported arguments are as follows:
 *   -d : allows the user to tell the manager not to 
 *        automatically fork (useful for debugging)
 *   -l : allows the user to specify a log file. If no log
 *        file is specified then output will go to the
 *        console.
 *
 * IN:  argument count, argument list
 * OUT: none
 ***********************************************************/
static void
process_args(int argc, char**argv)
{
    int c = 0;

    while ((c = getopt(argc, argv, "l:d")) != -1)
    {
        switch (c)
        {
            case 'l':
                log_file = optarg;
            break;
            case 'd':
                do_fork = false;
            break;
            default:
                usage(argv[0]);
            break;
        }
    }
}

/***********************************************************
 * NAME: main
 * DESCRIPTION: this is the entry point for the WiFi
 * manager. 
 *
 * IN:  argument count, argument list
 * OUT: exit status
 ***********************************************************/
int main(int argc, char *argv[])
{
    struct sigaction accept_sa;
    struct sigaction ignore_sa;

    process_args(argc, argv);

#ifdef DEBUG
    log_init(log_file, DEFAULT_LOG_LIMIT, LEVEL_DEBUG);
#else
    log_init(log_file, DEFAULT_LOG_LIMIT, LEVEL_INFO);
#endif /* DEBUG */

    log_entry(LEVEL_INFO, "[%s]: update manager started", __func__);

    /* If the flag is set fork into the background */
    if (do_fork)
    {
        switch(fork())
        {
            case 0:
            break;
            case -1:
                log_entry(LEVEL_ERROR, "[%s]: unable to fork", __func__);
            default:
                exit(0);
        }
    }
    
    /* Setup our server connection */
    ipc_server_connect(&ipc_fd);

    /* Setup the signal handling for the process */
    accept_sa.sa_handler = process_signal;
    accept_sa.sa_flags = 0;
    sigemptyset(&accept_sa.sa_mask);

    ignore_sa.sa_handler = SIG_IGN;
    ignore_sa.sa_flags = 0;
    sigemptyset(&ignore_sa.sa_mask);

    sigaction(SIGUSR1, &accept_sa, NULL);
    sigaction(SIGUSR2, &accept_sa, NULL);
    sigaction(SIGHUP, &accept_sa, NULL);
    sigaction(SIGINT, &accept_sa, NULL);
    sigaction(SIGQUIT, &accept_sa, NULL);
    sigaction(SIGTERM, &accept_sa, NULL);

    sigaction(SIGPIPE, &ignore_sa, NULL);

    while (1)
    {
        fd_set socket_set;
        int status;
        int track_fd;
        struct timeval timeout;

        track_fd = 0;
        FD_ZERO(&socket_set);
        memset(&timeout, 0, sizeof(timeout));

        if (ipc_fd > 0)
        {
            FD_SET(ipc_fd, &socket_set);
            if (ipc_fd > track_fd)
                track_fd = ipc_fd;
        }
        else
        {
            timeout.tv_sec = 30;
            timeout.tv_usec = 0;
        }

        /* wait for activity on the file descriptors */
        if (timeout.tv_sec == 0)
            status = select(track_fd + 1, &socket_set, (fd_set*)0, (fd_set*)0, NULL);
        else
            status = select(track_fd + 1, &socket_set, (fd_set*)0, (fd_set*)0, &timeout);
       
        if (status < 0) /* select() error */
        {
            log_entry(LEVEL_DEBUG, "[%s] select error: %s", __func__, strerror(errno));
            continue;
        }
        else if (status == 0) /* select() timeout */
        {
            /*
             * If we're not connected to the control manager then
             * attempt to reconnect.
             */
            if (ipc_fd < 0)
                ipc_server_connect(&ipc_fd);
            continue;
        }

        if (FD_ISSET(ipc_fd, &socket_set))
        {
            int status = process_ipc_message(ipc_fd);
            if (status <= 0)
                ipc_server_disconnect(&ipc_fd);
        }
    }
}
