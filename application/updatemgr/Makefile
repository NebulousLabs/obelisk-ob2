#
# Copyright (C) 2009-2018 MapleLeaf Software, Inc
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. The name of the author may not be used to endorse or promote products
#    derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
# OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
# NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
# THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

TOP = ../..

SOURCES  = updatemgr.c
SOURCES += ipc.c
SOURCES += swupdate.c
SOURCES += xmlparser.c

SOURCES += logger.c

VPATH  = $(TOP)/application/common

CFLAGS  = --sysroot=$(TOP)/rootfs
CFLAGS += -Wall
CFLAGS += -fno-omit-frame-pointer
CFLAGS += -march=armv7-a
CFLAGS += -mfpu=neon
CFLAGS += -mfloat-abi=hard
CFLAGS += -mcpu=cortex-a8
CFLAGS += -I$(TOP)/application/include
CFLAGS += -I$(TOP)/rootfs/usr/include/libxml2

LDFLAGS  = -pg -Wl,-Map,$(MAP)
LDFLAGS += --sysroot=$(TOP)/rootfs

LIBS = -lz -lxml2

TOOLS            = /opt/
CROSS_COMPILE   ?= arm-poky-linux-gnueabi-
CC               = $(CROSS_COMPILE)gcc
EXECUTABLE       = updatemgr
TARGETDIR        = arm

ifeq ($(findstring debug,$(MAKECMDGOALS)),debug)
OBJDIR = $(TARGETDIR)/debug
CFLAGS += -g -DDEBUG
LDFLAGS += -g
STRIP =
else
OBJDIR = $(TARGETDIR)/release
CFLAGS += -pg -O2
STRIP = $(CROSS_COMPILE)strip
endif

PROGRAMDIR  = $(OBJDIR)
INSTALL_DIR	= $(TOP)/rootfs/usr/local/ob2/bin/
PROGRAM		= $(PROGRAMDIR)/$(EXECUTABLE)
MAP		    = $(PROGRAMDIR)/$(EXECUTABLE).map

OBJECTS = $(addprefix $(OBJDIR)/,$(SOURCES:.c=.o))

RM := rm -rf

all: $(PROGRAM)

debug: $(PROGRAM)

# Tool invocations
$(PROGRAM): $(OBJECTS) FORCE
	@echo 'Building target: $@'
	@echo 'Invoking: GCC C Linker'
	@[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(CC) $(LDFLAGS) -o $@ $(OBJECTS) $(LIBS)
	@echo 'Finished building target: $@'
	@echo ' '

# Other Targets
clean: FORCE
	@$(RM) $(TARGETDIR)

$(OBJDIR)/%.o:  %.c
	@rm -f $@
	@[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(CC) $(CFLAGS) -c -MMD -MP -o $@ $<

ifneq ($(MAKECMDGOALS),clean)
-include $(OBJECTS:.o=.d)
endif

install: $(PROGRAM)
	@echo 'Installing $(PROGRAM)...'
	@mkdir -p $(INSTALL_DIR)
	@cp -f $(PROGRAM) $(INSTALL_DIR)
	@[ -z $(STRIP) ] || $(STRIP) $(INSTALL_DIR)/$(EXECUTABLE)
	@echo 'Installation complete'

.PHONY: all clean install FORCE
.SECONDARY:
