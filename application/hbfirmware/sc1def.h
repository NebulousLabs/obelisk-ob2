//***********************************************************************
//**                                                                   **
//** TITLE: SC1DEF.H                                                   **
//**                                                                   **
//** Orchid Technologies Engineering and Consulting, Inc.              **
//**                                                                   **
//** Written and Developed by:                                         **
//** Orchid Technologies Engineering and Consulting, Inc.              **
//** 147 Main Street                                                   **
//** Maynard, Ma.  01754                                               **
//** TEL: 978-461-2000                                                 **
//**                                                                   **
//**                                                                   **
//** This Source Code is the property of Orchid Technologies           **
//** Engineering and Consulting, Inc.  Copyright 2003, 2004, 2005,     **
//** 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016  **
//** 2017, 2018                                                        **
//**                                                                   **
//** Orchid Technologies Engineering and Consulting, Inc. reserves all **
//** rights and title to this software.  This software shall not be    **
//** reverse engineered, copied or used in any way without the express **
//** written permission of Orchid Technologies Engineering and         **
//** Consulting, Inc.                                                  **
//**                                                                   **
//**                                                                   **
//** NOTICE: Copyright (C) 2003, 2004, 2005, 2006, 2007, 2008, 2009,   **
//** 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018              **
//**                                                                   **
//** Orchid Technologies Engineering and Consulting, Inc.              **
//** All Rights Reserved.                                              **
//**                                                                   **
//**                                                                   **
//** Revision History:                                                 **
//**                                                                   **
//** ORCHID     11/01/04    V1.0    Original                           **
//**                                                                   **
//** ORCHID     04/15/05    V1.1    Adapted for Use                    **
//**                                                                   **
//** ORCHID     05/01/05    V1.2    Adapted for Use                    **
//**                                                                   **
//** ORCHID     11/01/06    V2.0    Adapted as MEDCORE                 **
//**                                                                   **
//** ORCHID     03/01/09    V3.0    Adapted as RMCORE for HS82117      **
//**                                Platform                           **
//**                                                                   **
//** ORCHID     05/01/09    V4.0    Adapted as A7CORE for NXP2194      **
//**                                Platform                           **
//**                                                                   **
//** ORCHID     04/01/10    V5.0    Adapted as M3COR for NXP17xx       **
//**                                                                   **
//** ORCHID     01/01/11    V6.0    Adapted as S3COR for STM32C10x     **
//**                                                                   **
//** ORCHID     12/01/14    V7.0    Adapted as MG1   for STM32F40x     **
//**                                                                   **
//** ORCHID     12/01/15    V8.0    Adapted as CH1   for STM32F40x     **
//**                                                                   **
//** ORCHID     04/15/16    V9.0    Adapted as SL3   for STM32F103     **
//**                                                                   **
//** ORCHID     05/05/18    V10.0   Adapted as FC1   for STM32F103     **
//**                                                                   **
//** ORCHID     07/15/18    V11.0   Adapted as SC1   for STM32F101     **
//***********************************************************************

//VERSION INCREMENT
//VERSION INCREMENT
//Increment the version number here:
#define VERSION 0x3530                 //Version 3.50

//DCR or SC1 Selection
//Select Only One Version
//#define HASH32      //32Bit DCR Selection
#define HASH64        //64Bit SC1 Selection

//Define this for SPI LED Activity Flashing
#define SPI_LED

//This is to compile in helpful debug code.  This option should be set
//to 1 for debug and to 0 for normal operation.
#define  DEBUG 0           //Debug = 1, Normal = 0

//Define bit test macros
#define _bset(A, B)         (A |= (0x01<<(B)))
#define _bres(A, B)         (A &= ~(0x01<<(B)))
#define _btst(A, B)         ((A) & (0x01<<(B)))

//General Definitions
#define READY  0
#define BUSY   1

//General Flags
#define FREADY 3           //Data Flags Ready Set
#define PREADY 2           //Print only flag - for diags
#define DREADY 1           //Data only flag for normal processing
#define NREADY 0           //Data not ready

//SCI (UART) Definitions
#define  SCI_BUF_SIZE            200   //Ring Buffer Size Tx and Rx
#define  SCI_TIE                 0x80
#define  SCI_TCIE                0x04
#define  SCI_RIE                 0x40
#define  SC1_PROT_MAX_SIZE       40    //Maximum size for protocol Rx Message

//SPI Definitions
#define  SPI_BUF_SIZE            15     //Ring Buffer Size Tx and Rx
#define  SPI_SPIF                0x80
#define  SPI_SPTEF               0x20

//ADC Constants
#define  ADC_VSTANDARD_0         9.52        //Power Supply Startup Level - Build 00
#define  ADC_VSTANDARD_1         9.57        //Power Supply Startup Level - Build 01
#define  AVOLT                   0.00040283  //Initial value
#define  VOLT10_0_00             95          //Power Supply Setting for 10.0 Volts Output - Build 00
#define  VOLT10_0_01             80          //Power Supply Setting for 10.0 Volts Output - Build 01
#define  VOLT10_25_01            95          //Power Supply Setting for 10.0 Volts Output - Build 01
#define  VOLT10_5_01             110         //Power Supply Setting for 10.5 Volts Output - Build 01
#define  VOLT11_0_01             123         //Power Supply Setting for 11.0 Volts Output - Build 01
#define  VOLT_MAX                128         //128 Steps to Max
#define  VOLT_MIN                -150        //128 Steps to Min

//Interrupt Priority
#define  INTPRI_HI               0x00   //Used for Highest Priority
#define  INTPRI_MED              0x20   //Used for Medium  Priority
#define  INTPRI_LOW              0x40   //Used for Low     Priority

//ASIC Messages - MODES
#define AM_LW   0x00            //Local Write - shifted
#define AM_LR   0x40            //Local Read  - shifted
#define AM_CW   0x80            //Chip  Write - shifted
#define AM_SW   0xC0            //System Write- shifted

#ifdef HASH32
#define NUMBER_OF_ENGINES       4096        //Total Number of Engines, 32 Chips x 128 Engines per Chip
#define NUMBER_OF_ENGINES_SPI   2048        //Total Number of Engines in an SPI Chain
#define ECF_SIZE                256         //Engine control Flag Array Size, 16 Chips x 128 Engines per chip / 8 (three birs per flag packed into 24 bit entities)
#define RAW_RESULT_QUEUE_SIZE   50          //Raw Result Queue Size
#define AR_V00  0x00            //V0 Register
#define AR_V01  0x01            //V1 Register
#define AR_V02  0x02            //V2 Register
#define AR_V03  0x03            //V3 Register
#define AR_V04  0x04            //V4 Register
#define AR_V05  0x05            //V5 Register
#define AR_V06  0x06            //V6 Register
#define AR_V07  0x07            //V7 Register
#define AR_V08  0x08            //V8 Register
#define AR_V09  0x09            //V9 Register
#define AR_V10  0x0A            //V10 Register
#define AR_V11  0x0B            //V11 Register
#define AR_V12  0x0C            //V12 Register
#define AR_V13  0x0D            //V13 Register
#define AR_V14  0x0E            //V14 Register
#define AR_V15  0x0F            //V15 Register
#define AR_M0   0x10            //M0  Register
#define AR_M1   0x11            //M1  Register
#define AR_M2   0x12            //M2  Register
#define AR_M4   0x14            //M4  Register
#define AR_M5   0x15            //M5  Register
#define AR_M6   0x16            //M6  Register
#define AR_M7   0x17            //M7  Register
#define AR_M8   0x18            //M8  Register
#define AR_M9   0x19            //M9  Register
#define AR_MAT  0x1A            //Match Register
#define AR_FCR  0x1C            //FCR Register
#define AR_UBR  0x1D            //Upper Bound Register
#define AR_LBR  0x1E            //Lower Bound Register
#define AR_LIM  0x1F            //Limits Register
#define AR_ECR  0x20            //ECR Register
#define AR_M10  0x21            //M10  Register
#define AR_M11  0x22            //M11  Register
#define AR_M12  0x23            //M12  Register
#define AR_OCR2 0x2E            //OCR Register
#define AR_OCR1 0x2F            //OCR Register
#define AR_EDR1 0x40            //Engine Done Reg
#define AR_EDR2 0x41            //Engine Done Reg
#define AR_EDR3 0x42            //Engine Done Reg
#define AR_EDR4 0x43            //Engine Done Reg
#define AR_EBR1 0x44            //Engine Busy Reg
#define AR_EBR2 0x45            //Engine Busy Reg
#define AR_EBR3 0x46            //Engine Busy Reg
#define AR_EBR4 0x47            //Engine Busy Reg
#define AR_IVEC 0x48            //Enterrupt vector reg
#define AR_FDR0 0x40            //Engine Fifo Reg
#define AR_FDR1 0x41            //Engine Fifo Reg
#define AR_FDR2 0x42            //Engine Fifo Reg
#define AR_FDR3 0x43            //Engine Fifo Reg
#define AR_FDR4 0x44            //Engine Fifo Reg
#define AR_FDR5 0x45            //Engine Fifo Reg
#define AR_FDR6 0x46            //Engine Fifo Reg
#define AR_FDR7 0x47            //Engine Fifo Reg
#define AR_POR0 0x48            //Engine Pipeline out 0
#define AR_POR1 0x49            //Engine Pipeline out 1
#define AR_ICR  0x4A            //Engine Int Chain
#define AR_FSR  0x4B            //Fifo Status Register
#define AR_ESR  0x4E            //Engine Status Register
#endif

#ifdef HASH64
#define NUMBER_OF_ENGINES       2048        //Total Number of Engines, 32 Chips x 64 Engines per Chip
#define NUMBER_OF_ENGINES_SPI   1024        //Total Number of Engines in an SPI Chain
#define ECF_SIZE                128         //Engine control Flag Array Size, 16 Chips x 64 Engines per chip / 8 (three birs per flag packed into 24 bit entities)
#define RAW_RESULT_QUEUE_SIZE   50          //Raw Result Queue Size
#define SIA_INCREMENT           1009        //SIA NONCE Increment
#define SIA_INCREMENT_HEX       0x00000000000003F1  //1009 Hex
#define AR_V00  0x00            //V0 Register
#define AR_V01  0x01            //V1 Register
#define AR_V02  0x02            //V2 Register
#define AR_V03  0x03            //V3 Register
#define AR_V04  0x04            //V4 Register
#define AR_V05  0x05            //V5 Register
#define AR_V06  0x06            //V6 Register
#define AR_V07  0x07            //V7 Register
#define AR_V08  0x08            //V8 Register
#define AR_V09  0x09            //V9 Register
#define AR_V10  0x0A            //V10 Register
#define AR_V11  0x0B            //V11 Register
#define AR_V12  0x0C            //V12 Register
#define AR_V13  0x0D            //V13 Register
#define AR_V14  0x0E            //V14 Register
#define AR_V15  0x0F            //V15 Register
#define AR_M0   0x10            //M0  Register
#define AR_M1   0x11            //M1  Register
#define AR_M2   0x12            //M2  Register
#define AR_M3   0x13            //M3  Register
#define AR_M5   0x15            //M5  Register
#define AR_M6   0x16            //M6  Register
#define AR_M7   0x17            //M7  Register
#define AR_M8   0x18            //M8  Register
#define AR_M9   0x19            //M9  Register
#define AR_MAT0 0x1A            //Match Register
#define AR_MAT8 0x1B            //Match Register
#define AR_FCR  0x1C            //FCR Register
#define AR_UBR  0x1D            //Upper Bound Register
#define AR_LBR  0x1E            //Lower Bound Register
#define AR_LIM  0x1F            //Limits Register
#define AR_ECR  0x20            //ECR Register
#define AR_STP  0x21            //Step Register
#define AR_OCR  0x2F            //OCR Register
#define AR_EDR  0x40            //Engine Done Reg
#define AR_EBR  0x41            //Engine Busy Reg
#define AR_IVEC 0x42            //Interrupt vector reg
#define AR_FDR0 0x40            //Engine Fifo Reg
#define AR_FDR1 0x41            //Engine Fifo Reg
#define AR_FDR2 0x42            //Engine Fifo Reg
#define AR_FDR3 0x43            //Engine Fifo Reg
#define AR_FDR4 0x44            //Engine Fifo Reg
#define AR_FDR5 0x45            //Engine Fifo Reg
#define AR_FDR6 0x46            //Engine Fifo Reg
#define AR_FDR7 0x47            //Engine Fifo Reg
#define AR_POR0 0x48            //Engine Pipeline out 0
#define AR_POR1 0x49            //Engine Pipeline out 1
#define AR_ICR  0x4A            //Engine Int Chain
#define AR_FSR  0x4B            //Fifo Status Register
#define AR_ESR  0x4E            //Engine Status Register
#endif

//ASIC Message Structure Type Defines
struct amess{
    unsigned char mode;     //mode
    unsigned char ch_addr;  //chip address
    unsigned char co_addr;  //core address
    unsigned char rg_addr;  //register address
    unsigned int  h_data;   //high data
    unsigned int  l_data;   //low  data
    };

//ASIC Message Structure Type Defines
//This is for newer format data in 64Bit style
struct amess64{
    unsigned char mode;     //mode
    unsigned char ch_addr;  //chip address
    unsigned char co_addr;  //core address
    unsigned char rg_addr;  //register address
    unsigned long long data;//64Bit Data
    };

//Engine Job Control Management
//This is for job control, each engine gets three bits:
//000: Not Busy
//001: Job 1
//010: Job 2
//011: Job 3
//100: Job 4
//101: Job 5
//110: Job 6
//111: Job Invalid - to be cleared and updated
struct ecfm {
    unsigned char e2;   //high byte
    unsigned char e1;   //middle byte
    unsigned char e0;   //low byt
    };

//Global State Machine STATES
#define  STATE0 0x00
#define  STATE1 0x01
#define  STATE2 0x02
#define  STATE3 0x03
#define  STATE4 0x04
#define  STATE5 0x05
#define  STATE6 0x06
#define  STATE7 0x07
#define  STATE8 0x08
#define  STATE9 0x09
#define  STATE10 0x10
#define  STATE11 0x11
#define  STATE12 0x12
#define  STATE13 0x13
#define  STATE14 0x14
#define  STATE15 0x15
#define  STATE16 0x16
#define  STATE17 0x17
#define  STATE18 0x18
#define  STATE19 0x19
#define  STATE20 0x20
#define  STATE21 0x21
#define  STATE22 0x22
#define  STATE23 0x23
#define  STATE24 0x24
#define  STATE25 0x25
#define  STATE26 0x26
#define  STATE27 0x27
#define  STATE28 0x28
#define  STATE29 0x29
#define  STATE30 0x30
#define  STATE31 0x31
#define  STATE32 0x32
#define  STATE33 0x33
#define  STATE34 0x34
#define  STATE35 0x35
#define  STATE36 0x36
#define  STATE37 0x37
#define  STATE38 0x38
#define  STATE39 0x39
#define  STATE40 0x40
#define  STATE41 0x41
#define  STATE42 0x42
#define  STATE43 0x43
#define  STATE44 0x44
#define  STATE45 0x45
#define  STATE46 0x46
#define  STATE47 0x47
#define  STATE48 0x48
#define  STATE49 0x49
#define  STATE50 0x50
#define  STATE51 0x51
#define  STATE52 0x52
#define  STATE53 0x53
#define  STATE54 0x54
#define  STATE55 0x55
#define  STATE56 0x56
#define  STATE57 0x57
#define  STATE58 0x58
#define  STATE59 0x59
#define  STATE60 0x60
#define  STATE61 0x61
#define  STATE62 0x62
#define  STATE63 0x63
#define  STATE64 0x64
#define  STATE65 0x65
#define  STATE66 0x66
#define  STATE67 0x67
#define  STATE68 0x68
#define  STATE69 0x69
#define  STATE_ERROR 0x98
#define  STATE_IDLE  0x99
#define  ESTATE_IDLE 0xFFF

//SC1 Protocol Commands
#define SC1_NACK        0x00
#define SC1_NOP         0x01
#define SC1_SWVERSION   0x02    //Return SW Version and Board Type
#define SC1_GOLOADER    0x03    //Enter STM32 Loader
#define SC1_PWR_UP      0x04    //Power Up and Initialize Array
#define SC1_PWR_DN      0x05    //Power Down and Stop Array
#define SC1_SET_VOLT    0x06    //Set Voltage Level
#define SC1_GET_VOLT    0x07    //Get Voltage Level, returns 32bit float
#define SC1_GET_CURR    0x08    //Get Current Level, returns 32bit float
#define SC1_GET_TEMP1   0x09    //Get Temperature 1, returns 32bit float
#define SC1_GET_TEMP2   0x0A    //Get Temperature 2, returns 32bit float
#define SC1_SET_CLKS    0x0B    //Set ASIC Clocking, Increment or Decrement Only
#define SC1_START_JOB   0x0C    //Start Hashing Job
#define SC1_ABORT_JOB   0x0D    //Abort Hashing Job
#define SC1_CHECK_JOB   0x0E    //Check Hashing Job, return status
#define SC1_JREG_WRITE  0x0F    //Job Register Write
#define SC1_JMAN_WRITE  0x10    //Job Management Write
#define SC1_PROC_ID     0x11    //Get Processor ID
#define SC1_SET_POOLD   0x12    //Set Pool Difficulty
#define SC1_SET_PERF    0x13    //Set Performance Servo
#define SC1_GET_ADIAG   0x14    //Get ASIC Power On Diagnostic Result
#define SC1_SET_SMSG    0x15    //Set Status Message Start / Stop
#define SC1_IWDG_RESET  0x16    //Watchdog Reset Message
#define SC1_SET_CLK_DELTA 0x17  //Set ASIC Device Clock Delta
#define SC1_GNONCE_CNT  0x18    //Good Nonce per device count
#define SC1_BNONCE_CNT  0x19    //Bad  Nonce per device count
#define SC1_DCR_NONCE   0x80    //Unsolicited DCR NONCE Message
#define SC1_DCR_DONE    0x81    //Unsolicited DCR DONE  Message
#define SC1_DCR_JNACK   0x82    //Unsolicited DCR Bad Job Submit Message
#define SC1_SIA_NONCE   0xC0    //Unsolicited SIA NONCE Message
#define SC1_SIA_DONE    0xC1    //Unsolicited SIA DONE  Message
#define SC1_SIA_JNACK   0xC2    //Unsolicited SIA Bad Job Submit Message
//
//
//Now Start Defines for Engine Job Management
//Now Start Defines for Engine Job Management
//
#ifdef HASH32
//Job Definition Structure
struct jobman{
    unsigned char current_jqn;  //Job queue number 1 to 6
    unsigned char jid;          //Job ID for copy to job_id when submitted
    unsigned int  mreg[35];     //V, M, Control Regs in JM_RADR Order
    unsigned char job_id[8];    //Unique job ID
    unsigned int  hash1_start;
    unsigned int  hash1_end;
    unsigned int  hash2_start;
    unsigned int  hash2_end;
    unsigned int  mreg_q[8][15];    //Que storage for nonce checking only
    unsigned int  vreg_q[8][16];    //Que storage for nonce checking only
    };

//ASIC Set Job Structure, one per SPI string needed
struct asic_set_job{
    unsigned char  state;
    unsigned int   timer;
    unsigned char  busy_flag;
    unsigned char  new_job_flag;
    unsigned char  jqn;
    unsigned int   hash_start;
    unsigned int   hash_end;
    unsigned int   current_job;
    unsigned int   current_engine;
    unsigned char  current_reg_index;
    unsigned char  txbuf[SPI_BUF_SIZE];
    unsigned char  rxbuf[SPI_BUF_SIZE];
    struct amess   am;
    };

//ASIC Get Result Structure, one per SPI string needed
struct asic_get_result{
    unsigned char state;
    unsigned int  current_engine;
    unsigned int  current_result_inptr;
    unsigned int  current_result_count;
    unsigned int  current_fifo_status;
    unsigned int  current_fifo_status_index;
    unsigned int  current_m5_reg;
    unsigned char txbuf[SPI_BUF_SIZE];
    unsigned char rxbuf[SPI_BUF_SIZE];
    struct amess  am;
    };

//ASIC Check Hash32 Result
struct asic_h32_check{
    unsigned char state;
    unsigned int  m_reg[16];
    unsigned int  current_outptr1;
    unsigned int  current_outptr2;
    };

//ASIC Raw Result Structure, type define - array of raw results
struct raw_result_queue{
    unsigned short engine;
    unsigned char  job_q;
    unsigned int   mreg;
    unsigned int   nonce;
    };

//DCR Statistics
struct dcr_statistics {
    int total_jobs;
    int total_nonces;
    int total_good_nonces;
    int total_pool_nonces;
    short dtotal_jobs;
    short dtotal_nonces;
    short dtotal_good_nonces;
    short dtotal_pool_nonces;
    };

//ASIC Chip Statistics
struct asic_stat {
    unsigned short asic_nonce_count[32];      //Good Nonces per Chip
    unsigned short asic_bad_nonce_count[32];  //Bad  Nonces per Chip
             short asic_clock_delta[32];      //+ or - clock adjust
    };

// Common C implementation of 32-bit bitwise right rotation
#define ROTR32(x, y)    (((x) >> (y)) | ((x) << (32 - (y))))

// Implementation of Blake-256's round function G - iterations
// of which make up the main compression function. Found in
// section 2.1.2 of the specification.
#define G(a, b, c, d, msg0, msg1, const1, const0)   do { \
        a = a + b + (msg0 ^ const1); \
        d = ROTR32(d ^ a, 16); \
        c = c + d; \
        b = ROTR32(b ^ c, 12); \
        a = a + b + (msg1 ^ const0); \
        d = ROTR32(d ^ a, 8); \
        c = c + d; \
        b = ROTR32(b ^ c, 7); \
    } while (0)


#endif //HASH32

#ifdef HASH64
//Job Definition Structure
struct jobman{
    unsigned char       current_jqn;  //Job queue number 1 to 6
    unsigned char       jid;          //Job ID for copy to job_id when submitted
    unsigned long long  mreg[10];     //Only M-Regs for SIA JM_RADR Order, others are hard coded
    unsigned char       job_id[8];    //Unique job ID
    unsigned int        hash1_start;
    unsigned int        hash1_end;
    unsigned int        hash2_start;
    unsigned int        hash2_end;
    unsigned long long  mreg_q[7][10];  //Que storage for nonce checking only
    };

//ASIC Set Job Structure, one per SPI string needed
struct asic_set_job{
    unsigned char      state;
    unsigned int       timer;
    unsigned char      busy_flag;
    unsigned char      new_job_flag;
    unsigned char      jqn;
    unsigned int       hash_start;
    unsigned int       hash_end;
    unsigned int       current_job;             //Upper 32 bits of range
    unsigned int       current_engine;
    unsigned char      current_reg_index;
    unsigned char      txbuf[SPI_BUF_SIZE];
    unsigned char      rxbuf[SPI_BUF_SIZE];
    struct amess64     am;
    };

//ASIC Get Result Structure, one per SPI string needed
struct asic_get_result{
    unsigned char      state;
    unsigned short     timer;
    unsigned int       current_engine;
    unsigned int       current_result_inptr;
    unsigned int       current_result_count;
    unsigned long long current_fifo_status;
    unsigned int       current_fifo_status_index;
    unsigned char      txbuf[SPI_BUF_SIZE];
    unsigned char      rxbuf[SPI_BUF_SIZE];
    struct amess64     am;
    };

//ASIC Raw Result Structure, type define - array of raw results
struct raw_result_queue{
    unsigned short     engine;
    unsigned char      job_q;
    unsigned long long nonce;
    };

//ASIC Check Hash32 Result
struct asic_h64_check{
    unsigned char state;
    unsigned long long  m_reg[11];
    unsigned int        current_outptr1;
    unsigned int        current_outptr2;
    };

//DCR Statistics
struct dcr_statistics {
    int total_jobs;
    int total_nonces;
    int total_good_nonces;
    int total_pool_nonces;
    short dtotal_jobs;
    short dtotal_nonces;
    short dtotal_good_nonces;
    short dtotal_pool_nonces;
    };

//ASIC Chip Statistics
struct asic_stat {
    unsigned short asic_nonce_count[32];      //Good Nonces per Chip
    unsigned short asic_bad_nonce_count[32];  //Bad  Nonces per Chip
             short asic_clock_delta[32];      //+ or - clock adjust
    };

//SIA64 Hash Support
//SIA64 Hash Support
//SIA64 Hash Support

#define BLAKE2B_BLOCKBYTES      128
#define BLAKE2B_OUTBYTES        64
#define BLAKE2B_KEYBYTES        64
#define BLAKE2B_SALTBYTES       16
#define BLAKE2B_PERSONALBYTES   16

struct blake2b_state {
    unsigned long long h[8];
    unsigned long long t[2];
    unsigned long long f[2];
    unsigned char      buf[BLAKE2B_BLOCKBYTES];
    int                buflen;
    int                outlen;
    unsigned char      last_node;
    };

struct blake2b_param {
    unsigned char digest_length; /* 1 */
    unsigned char key_length;    /* 2 */
    unsigned char fanout;        /* 3 */
    unsigned char depth;         /* 4 */
    unsigned int  leaf_length;   /* 8 */
    unsigned int  node_offset;   /* 12 */
    unsigned int  xof_length;    /* 16 */
    unsigned char node_depth;    /* 17 */
    unsigned char inner_length;  /* 18 */
    unsigned char reserved[14];  /* 32 */
    unsigned char salt[BLAKE2B_SALTBYTES]; /* 48 */
    unsigned char personal[BLAKE2B_PERSONALBYTES];  /* 64 */
    };

#define G(r,i,a,b,c,d)                      \
  do {                                      \
    a = a + b + m[blake2b_sigma[r][2*i+0]]; \
    d = rotr64(d ^ a, 32);                  \
    c = c + d;                              \
    b = rotr64(b ^ c, 24);                  \
    a = a + b + m[blake2b_sigma[r][2*i+1]]; \
    d = rotr64(d ^ a, 16);                  \
    c = c + d;                              \
    b = rotr64(b ^ c, 63);                  \
  } while(0)

#define ROUND(r)                    \
  do {                              \
    G(r,0,v[ 0],v[ 4],v[ 8],v[12]); \
    G(r,1,v[ 1],v[ 5],v[ 9],v[13]); \
    G(r,2,v[ 2],v[ 6],v[10],v[14]); \
    G(r,3,v[ 3],v[ 7],v[11],v[15]); \
    G(r,4,v[ 0],v[ 5],v[10],v[15]); \
    G(r,5,v[ 1],v[ 6],v[11],v[12]); \
    G(r,6,v[ 2],v[ 7],v[ 8],v[13]); \
    G(r,7,v[ 3],v[ 4],v[ 9],v[14]); \
  } while(0)

#endif //HASH64

