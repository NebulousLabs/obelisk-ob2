//***********************************************************************
//**                                                                   **
//** TITLE: SC1COM.C                                                   **
//**                                                                   **
//** Orchid Technologies Engineering and Consulting, Inc.              **
//**                                                                   **
//** Written and Developed by:                                         **
//** Orchid Technologies Engineering and Consulting, Inc.              **
//** 147 Main Street                                                   **
//** Maynard, Ma.  01754                                               **
//** TEL: 978-461-2000                                                 **
//**                                                                   **
//**                                                                   **
//** This Source Code is the property of Orchid Technologies           **
//** Engineering and Consulting, Inc.  Copyright 2003, 2004, 2005,     **
//** 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016  **
//** 2017, 2018                                                        **
//**                                                                   **
//** Orchid Technologies Engineering and Consulting, Inc. reserves all **
//** rights and title to this software.  This software shall not be    **
//** reverse engineered, copied or used in any way without the express **
//** written permission of Orchid Technologies Engineering and         **
//** Consulting, Inc.                                                  **
//**                                                                   **
//**                                                                   **
//** NOTICE: Copyright (C) 2003, 2004, 2005, 2006, 2007, 2008, 2009,   **
//** 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018              **
//**                                                                   **
//** Orchid Technologies Engineering and Consulting, Inc.              **
//** All Rights Reserved.                                              **
//**                                                                   **
//**                                                                   **
//** Revision History:                                                 **
//**                                                                   **
//** ORCHID     11/01/04    V1.0    Original                           **
//**                                                                   **
//** ORCHID     04/15/05    V1.1    Adapted for Use                    **
//**                                                                   **
//** ORCHID     05/01/05    V1.2    Adapted for Use                    **
//**                                                                   **
//** ORCHID     11/01/06    V2.0    Adapted as MEDCORE                 **
//**                                                                   **
//** ORCHID     03/01/09    V3.0    Adapted as RMCORE for HS82117      **
//**                                Platform                           **
//**                                                                   **
//** ORCHID     05/01/09    V4.0    Adapted as A7CORE for NXP2194      **
//**                                Platform                           **
//**                                                                   **
//** ORCHID     04/01/10    V5.0    Adapted as M3COR for NXP17xx       **
//**                                                                   **
//** ORCHID     01/01/11    V6.0    Adapted as S3COR for STM32C10x     **
//**                                                                   **
//** ORCHID     12/01/14    V7.0    Adapted as MG1   for STM32F40x     **
//**                                                                   **
//** ORCHID     12/01/15    V8.0    Adapted as CH1   for STM32F40x     **
//**                                                                   **
//** ORCHID     04/15/16    V9.0    Adapted as SL3   for STM32F103     **
//**                                                                   **
//** ORCHID     05/05/18    V10.0   Adapted as FC1   for STM32F103     **
//**                                                                   **
//** ORCHID     07/15/18    V11.0   Adapted as SC1   for STM32F101     **
//***********************************************************************

//Include Files
#include "arm_comm.h"
#include "iostm32f10xxE.h"
#include "sc1def.h"            /* Control Defines    */
#include "sc1lib.h"            /* Library Prototypes */
#include "sc1com.h"            /* Communications     */
#include "sc1app.h"            /* Application        */
#include "sc1prot.h"           /* Protocol Support   */
#include "sc1mem.h"            /* Global Memory      */
#include <stdio.h>
#ifdef HASH32
#include "sc1app32.h"          /* Hash32 Job Manager */
#endif
#ifdef HASH64
#include "sc1app64.h"          /* Hash64 Job Manager */
#endif
// These routines support UART2 Tx/Rx
// These routines support UART2 Tx/Rx
// These routines support UART2 Tx/Rx

//comm_getc0
//This routine reads bytes from the input data buffer and updates the
//ring buffer pointers.  This routine is blocking, use comm_rxtestc to
//determine if data is available in the buffers.
unsigned char comm_getc0(void)
{
   unsigned char indata;

   while(rxring_count0 == 0);     //Wait for data

   __disable_interrupt();
   rxring_count0--;               //Update RxRing Status
   indata = *rxring_outptr0++;    //Read the data from the ring
   if(rxring_outptr0 >= &sci_rxbuffer0[SCI_BUF_SIZE])
   rxring_outptr0 = sci_rxbuffer0; //Update RxRing Pointer
   __enable_interrupt();

   return(indata);
}

//comm_getline0
//This routine gets an input line from SCI0.  This routine echoes
//the input characters and recognizes back space.  <CR> ends the
//line input function.
void comm_getline0(void)
{
   unsigned char indat;

   //Should we be idle
   if(comm_lineflag0 == STATE_IDLE)
   return;

   //Is the line buffer availabe?
   if(comm_lineflag0 == STATE2)
   return;

   //Should we send prompt
   if(comm_lineflag0 == STATE0)
   {
   comm_putc0('S');
   comm_putc0('C');
   #ifdef HASH32
   comm_putc0('3');
   comm_putc0('2');
   #endif
   #ifdef HASH64
   comm_putc0('6');
   comm_putc0('4');
   #endif
   comm_putc0('>');
   comm_lineflag0 = STATE1;
   }

   //Is there a character available?
   if((comm_testrx0()) != 1)  //Is a character available
   return;                    //No, We're done

   //Get character input
   indat = comm_getc0();

   //Is there room?
   if(comm_linecount0 > SCI_BUF_SIZE)
   indat = 0x0D;              //No More Room, Force <CR>

   switch(indat)
   {
   case 0x08:  //BackSpace
               if(comm_linecount0 > 0)
               {
               comm_putc0(0x08);
               comm_putc0(0x20);
               comm_putc0(0x08);
               comm_linecount0--;
               }
               break;

   case 0x0D:  //Carriage Return
               comm_linebuf0[comm_linecount0++] = 0x00;
               comm_lineflag0 = STATE2;
               comm_putc0(0x0D);
               comm_putc0(0x0A);
               break;

   case 0x0A:  //Line Feed - Ignore
               break;

   default:    //Normal Character Entry
               comm_putc0(indat);
               comm_linebuf0[comm_linecount0++] = indat;
               break;
   }
}


//comm_procline0
//This routine processes the line inputs
void comm_procline0(void)
{
   if(comm_procline0_state == STATE_IDLE)
   return;

   //Prepare to receive new line
   if(comm_lineflag0 == STATE2)
   {

   //Call Application Command Processor
   sc1_procline0();

   if(comm_lineflag0 != STATE_IDLE)
   comm_lineflag0 = STATE0;
   comm_linecount0 = 0;
   }
}


//comm_putc0
//This routine write bytes to the output data buffer and updates the
//ring buffer pointers.  This routine is blocking, use comm_txtestc to
//determine if write space is available in the buffers.
void comm_putc0(unsigned char outdata)
{
   while(txring_count0 >= SCI_BUF_SIZE);     //Wait for data buffer space

   __disable_interrupt();

   //Put Character on the Ring, Enable Interrupts
   txring_count0++;                //Update TxRing Status
  *txring_inptr0++ = outdata;     //Write Data to the TxRing
   if(txring_inptr0 >= &sci_txbuffer0[SCI_BUF_SIZE])
   txring_inptr0 = sci_txbuffer0;  //Update TxRing Pointer

   //Enable Tx Interrupts
   USART2_CR1 |= 0x80;

   __enable_interrupt();
}

//comm_testrx0
//This routine checks if the receive character buffer is Empty
unsigned char comm_testrx0(void)
{
   unsigned char result;

   result = 1;
   if(rxring_count0 == 0)
   result = 0;

   return(result);
}


//comm_testtx0
//This routine checks if the transmit character buffer is Full
unsigned char comm_testtx0(void)
{
   unsigned char result;

   result = 1;
   if(txring_count0 == SCI_BUF_SIZE)
   result = 0;

   return(result);
}


//comm_sprint0
//This routine prints a string to the RS232 Port
void comm_sprint0 (unsigned char *data)
{
   while(*data != 0x00)
   comm_putc0(*data++);
}


//comm_flushrx0
//This routine flushes the receive character buffer
void comm_flushrx0(void)
{
   rxring_inptr0  = sci_rxbuffer0;
   rxring_outptr0 = sci_rxbuffer0;
   rxring_count0  = 0x00;
}


// UART2_IRQHandler
// UART Interrupt Service Routine.  This routine places Rx Data
// into the Rxbuffer and sends data from the Txbuffer.
void USART2_IRQHandler(void)
{
   irq_sci_status0 = USART2_SR;              //Get the UART Interrupt Status
   irq_sci_status0 = irq_sci_status0 & 0xA0; //Mask Bits
   //Is Transmit Data Register Empty?
   if(irq_sci_status0 & 0x80)
   {
      //Do We have more data?
      if(txring_count0)
      {
         USART2_DR = *txring_outptr0++;      //output next data byte
         txring_count0--;                    //Update TxRing Status
         if(txring_outptr0 >= &sci_txbuffer0[SCI_BUF_SIZE])
         txring_outptr0 = sci_txbuffer0;     //Update TxRing Pointer
      }

      if(txring_count0 == 0)
      {
         USART2_CR1 &= ~0x80;                //Stop Xmit IRQ's
      }
   }

   //Do the Receive Data
   if(irq_sci_status0 & 0x20)                 //Is there valid data?
   {
      if(rxring_count0 < SCI_BUF_SIZE)        //Is there a place to put it?
      {
         *rxring_inptr0++ = USART2_DR;        //Store the RxData, Mask high data bit when running with parity!!
         rxring_count0++;                     //Update RxRing Status
         if(rxring_inptr0 >= &sci_rxbuffer0[SCI_BUF_SIZE])
         rxring_inptr0 = sci_rxbuffer0;       //Update RxRing Pointer
      }
      else
      {
         irq_sci_status0 = USART2_DR;         //Read Data, ignore it, flags clear
      }
   }

   //Clear Interrupts
   __enable_interrupt();
   NVIC_ClrPend(NVIC_USART2);
}


// These routines support UART1 Tx/Rx
// These routines support UART1 Tx/Rx
// These routines support UART1 Tx/Rx

//comm_getc1
//This routine reads bytes from the input data buffer and updates the
//ring buffer pointers.  This routine is blocking, use comm_rxtestc to
//determine if data is available in the buffers.
unsigned char comm_getc1(void)
{
   unsigned char indata;

   while(rxring_count1 == 0);     //Wait for data

   __disable_interrupt();
   rxring_count1--;               //Update RxRing Status
   indata = *rxring_outptr1++;    //Read the data from the ring
   if(rxring_outptr1 >= &sci_rxbuffer1[SCI_BUF_SIZE])
   rxring_outptr1 = sci_rxbuffer1; //Update RxRing Pointer
   __enable_interrupt();

   return(indata);
}

//comm_putc1
//This routine write bytes to the output data buffer and updates the
//ring buffer pointers.  This routine is blocking, use comm_txtestc to
//determine if write space is available in the buffers.
void comm_putc1(unsigned char outdata)
{
   while(txring_count1 >= SCI_BUF_SIZE);     //Wait for data buffer space

   __disable_interrupt();

   //Put Character on the Ring, Enable Interrupts
   txring_count1++;                //Update TxRing Status
  *txring_inptr1++ = outdata;     //Write Data to the TxRing
   if(txring_inptr1 >= &sci_txbuffer1[SCI_BUF_SIZE])
   txring_inptr1 = sci_txbuffer1;  //Update TxRing Pointer

   //Enable Tx Interrupts
   USART1_CR1 |= 0x80;

   __enable_interrupt();
}

//comm_testrx1
//This routine checks if the receive character buffer is Empty
unsigned char comm_testrx1(void)
{
   unsigned char result;

   result = 1;
   if(rxring_count1 == 0)
   result = 0;

   return(result);
}


//comm_testtx1
//This routine checks if the transmit character buffer is Full
unsigned char comm_testtx1(void)
{
   unsigned char result;

   result = 1;
   if(txring_count1 == SCI_BUF_SIZE)
   result = 0;

   return(result);
}


//comm_sprint1
//This routine prints a string to the RS232 Port
void comm_sprint1 (unsigned char *data)
{
   while(*data != 0x00)
   comm_putc1(*data++);
}


//comm_flushrx1
//This routine flushes the receive character buffer
void comm_flushrx1(void)
{
   rxring_inptr1  = sci_rxbuffer1;
   rxring_outptr1 = sci_rxbuffer1;
   rxring_count1  = 0x00;
}


// UART1_IRQHandler
// UART Interrupt Service Routine.  This routine places Rx Data
// into the Rxbuffer and sends data from the Txbuffer.
void USART1_IRQHandler(void)
{
   irq_sci_status1 = USART1_SR;              //Get the UART Interrupt Status
   irq_sci_status1 = irq_sci_status1 & 0xA0; //Mask Bits
   //Is Transmit Data Register Empty?
   if(irq_sci_status1 & 0x80)
   {
      //Do We have more data?
      if(txring_count1)
      {
         USART1_DR = *txring_outptr1++;      //output next data byte
         txring_count1--;                    //Update TxRing Status
         if(txring_outptr1 >= &sci_txbuffer1[SCI_BUF_SIZE])
         txring_outptr1 = sci_txbuffer1;     //Update TxRing Pointer
      }

      if(txring_count1 == 0)
      {
         USART1_CR1 &= ~0x80;                //Stop Xmit IRQ's
      }
   }

   //Do the Receive Data
   if(irq_sci_status1 & 0x20)                 //Is there valid data?
   {
      if(rxring_count1 < SCI_BUF_SIZE)        //Is there a place to put it?
      {
         *rxring_inptr1++ = USART1_DR;        //Store the RxData, Mask high data bit when running with parity!!
         rxring_count1++;                     //Update RxRing Status
         if(rxring_inptr1 >= &sci_rxbuffer1[SCI_BUF_SIZE])
         rxring_inptr1 = sci_rxbuffer1;       //Update RxRing Pointer

         //Queue Protocol
         if(service_sc1_rcvr_state != STATE_IDLE)
         service_sc1_rcvr_timer = 3;
      }
      else
      {
         irq_sci_status1 = USART1_DR;         //Read Data, ignore it, flags clear
      }
   }

   //Clear Interrupts
   __enable_interrupt();
   NVIC_ClrPend(NVIC_USART1);
}

//SPI Drivers
//SPI Drivers
//SPI Drivers

//comm_spi_dma_sprint1
//This routine prints a string to the SPI Port at high speed.
//This routine reads DMA data back as well.
//The first parameter is the string length, the second is a pointer
//to the source data.  This routine sets chip selects low.  The ISR will return the
//chip select to the high state once all data is sent.  Be careful
//about chip select toggling too quickly if the routine is called too
//fast.  Message size is limited to SPI_BUF_SIZE.  Be certain that the
//destination location can accept the data to be received.
void comm_spi_dma_sprint1(unsigned short count, unsigned char *indata, unsigned char *outdata)
{
   //Wait if busy
   while(spi1_ready_flag == BUSY);

   //Now we're busy!
   #ifdef SPI_LED
   GPIOA_ODR &= ~0x00000800;    //LED On
   #endif
   spi1_ready_flag = BUSY;

   //Setup the DMA Channel - Tx
   DMA_CPAR3  = 0x4001300C;                 //Setup SPI1 Tx Destination Address
   DMA_CMAR3  = (unsigned long) indata;     //Memory Address of Source Data
   DMA_CNDTR3 = count;                      //Setup nuber bytes to transfer
   DMA_CCR3   = 0x00003090;                 //Setup transfer

   //Setup the DMA Channel - Rx
   DMA_CPAR2  = 0x4001300C;                 //Setup SPI1 Rx Destination Address
   DMA_CMAR2  = (unsigned long) outdata;    //Memory Address of Source Data
   DMA_CNDTR2 = count;                      //Setup nuber bytes to transfer
   DMA_CCR2   = 0x00003082;                 //Setup transfer

   //Receive it... DMA
   DMA_CCR2 |= 0x00000001;           //DMA Go!

   //Send it... DMA
   DMA_CCR3 |= 0x00000001;           //DMA Go!

   //Set Chip Select Low
   GPIOA_BSRR = 0x00100000;          //SS# PA4 Low

   //Send it!
   SPI1_CR2 |= 0x0007;               //MASTER, DMA Tx Enable, DMA Rx Enable
   SPI1_CR1 |= 0x0040;               //Enable SPI1

   #ifdef SPI_LED
   GPIOA_ODR |= 0x00000800;    //LED Off
   #endif
}


// dma1_isr
// DMA1 Interrupt Service Routine.  This routine completes the
// DMA transfer of data over SPI1.  This is the Rx DMA Interrupt.
void DMA1_Channel2_IRQHandler(void)
{
    //If we get here, we know that DMA completed...
    //So all we do is stop the SPI1 System

    //Stop the DMA
    DMA_CCR3 &= ~0x00000001;     //DMA Stop - Tx
    DMA_CCR2 &= ~0x00000001;     //DMA Stop - Rx

    //Clear DMA Flags
    DMA_IFCR = 0x00000FF0;       //Clear Channel 2,3 Flags

    //Disable DMA
    SPI1_CR2 &= ~0x0003;         //MASTER, DMA Tx Enable, DMA Rx Enable

    //All Done, Now we're Ready Again
    spi1_ready_flag = READY;

    //Stop SPI2, bring chip select hi...
    SPI1_CR1 &= ~0x0040;         //MASTER DONE, Disable SPI

    //Clear Interrupts
    NVIC_ClrPend(NVIC_DMA_CH2);

    GPIOA_BSRR = 0x00000010;     //SS# PA4 High
}

//comm_spi_dma_sprint2
//This routine prints a string to the SPI Port at high speed.
//This routine reads DMA data back as well.
//The first parameter is the string length, the second is a pointer
//to the source data.  This routine sets chip selects low.  The ISR will return the
//chip select to the high state once all data is sent.  Be careful
//about chip select toggling too quickly if the routine is called too
//fast.  Message size is limited to SPI_BUF_SIZE.  Be certain that the
//destination location can accept the data to be received.
void comm_spi_dma_sprint2(unsigned short count, unsigned char *indata, unsigned char *outdata)
{
   //Wait if busy
   while(spi2_ready_flag == BUSY);

   //Now we're busy!
   #ifdef SPI_LED
   GPIOA_ODR &= ~0x00000800;    //LED On
   #endif
   spi2_ready_flag = BUSY;

   //Setup the DMA Channel - Tx
   DMA_CPAR5  = 0x4000380C;                 //Setup SPI2 Tx Destination Address
   DMA_CMAR5  = (unsigned long) indata;     //Memory Address of Source Data
   DMA_CNDTR5 = count;                      //Setup nuber bytes to transfer
   DMA_CCR5   = 0x00003090;                 //Setup transfer

   //Setup the DMA Channel - Rx
   DMA_CPAR4  = 0x4000380C;                 //Setup SPI2 Rx Destination Address
   DMA_CMAR4  = (unsigned long) outdata;    //Memory Address of Source Data
   DMA_CNDTR4 = count;                      //Setup nuber bytes to transfer
   DMA_CCR4   = 0x00003082;                 //Setup transfer

   //Receive it... DMA
   DMA_CCR4 |= 0x00000001;           //DMA Go!

   //Send it... DMA
   DMA_CCR5 |= 0x00000001;           //DMA Go!

   //Set Chip Select Low
   GPIOB_BSRR = 0x10000000;          //SS# PB12 Low

   //Send it!
   SPI2_CR2 |= 0x0007;               //MASTER, DMA Tx Enable, DMA Rx Enable
   SPI2_CR1 |= 0x0040;               //Enable SPI1

   #ifdef SPI_LED
   GPIOA_ODR |= 0x00000800;    //LED Off
   #endif
}


// dma2_isr
// DMA1 Interrupt Service Routine.  This routine completes the
// DMA transfer of data over SPI1.  This is the Rx DMA Interrupt.
void DMA1_Channel4_IRQHandler(void)
{
    //If we get here, we know that DMA completed...
    //So all we do is stop the SPI1 System

    //Stop the DMA
    DMA_CCR5 &= ~0x00000001;     //DMA Stop - Tx
    DMA_CCR4 &= ~0x00000001;     //DMA Stop - Rx

    //Clear DMA Flags
    DMA_IFCR = 0x000FF000;       //Clear Channel 4,5 Flags

    //Disable DMA
    SPI2_CR2 &= ~0x0003;         //MASTER, DMA Tx Enable, DMA Rx Enable

    //All Done, Now we're Ready Again
    spi2_ready_flag = READY;

    //Stop SPI2, bring chip select hi...
    SPI2_CR1 &= ~0x0040;         //MASTER DONE, Disable SPI

    //Clear Interrupts
    NVIC_ClrPend(NVIC_DMA_CH4);

    GPIOB_BSRR = 0x00001000;     //SS# PB12 High
}

