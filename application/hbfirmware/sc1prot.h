/***********************************************************************/
//**                                                                   **
//** TITLE: SC1PROT.H                                                  **
//**                                                                   **
//** Orchid Technologies Engineering and Consulting, Inc.              **
//**                                                                   **
//** Written and Developed by:                                         **
//** Orchid Technologies Engineering and Consulting, Inc.              **
//** 147 Main Street                                                   **
//** Maynard, Ma.  01754                                               **
//** TEL: 978-461-2000                                                 **
//**                                                                   **
//**                                                                   **
//** This Source Code is the property of Orchid Technologies           **
//** Engineering and Consulting, Inc.  Copyright 2003, 2004, 2005,     **
//** 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016  **
//** 2017, 2018                                                        **
//**                                                                   **
//** Orchid Technologies Engineering and Consulting, Inc. reserves all **
//** rights and title to this software.  This software shall not be    **
//** reverse engineered, copied or used in any way without the express **
//** written permission of Orchid Technologies Engineering and         **
//** Consulting, Inc.                                                  **
//**                                                                   **
//**                                                                   **
//** NOTICE: Copyright (C) 2003, 2004, 2005, 2006, 2007, 2008, 2009,   **
//** 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018              **
//**                                                                   **
//** Orchid Technologies Engineering and Consulting, Inc.              **
//** All Rights Reserved.                                              **
//**                                                                   **
//**                                                                   **
//** Revision History:                                                 **
//**                                                                   **
//** ORCHID     11/01/04    V1.0    Original                           **
//**                                                                   **
//** ORCHID     04/15/05    V1.1    Adapted for Use                    **
//**                                                                   **
//** ORCHID     05/01/05    V1.2    Adapted for Use                    **
//**                                                                   **
//** ORCHID     11/01/06    V2.0    Adapted as MEDCORE                 **
//**                                                                   **
//** ORCHID     03/01/09    V3.0    Adapted as RMCORE for HS82117      **
//**                                Platform                           **
//**                                                                   **
//** ORCHID     05/01/09    V4.0    Adapted as A7CORE for NXP2194      **
//**                                Platform                           **
//**                                                                   **
//** ORCHID     04/01/10    V5.0    Adapted as M3COR for NXP17xx       **
//**                                                                   **
//** ORCHID     01/01/11    V6.0    Adapted as S3COR for STM32C10x     **
//**                                                                   **
//** ORCHID     12/01/14    V7.0    Adapted as MG1   for STM32F40x     **
//**                                                                   **
//** ORCHID     12/01/15    V8.0    Adapted as CH1   for STM32F40x     **
//**                                                                   **
//** ORCHID     04/15/16    V9.0    Adapted as SL3   for STM32F103     **
//**                                                                   **
//** ORCHID     05/05/18    V10.0   Adapted as FC1   for STM32F103     **
//**                                                                   **
//** ORCHID     07/15/18    V11.0   Adapted as SC1   for STM32F101     **
//***********************************************************************
void start_sc1_xmit(unsigned char *);
void service_sc1_rcvr(void);
void sc1_proc_message(void);
void sc1_do_msg_nop(void);
void sc1_do_msg_swversion(void);
void sc1_do_msg_loader(void);
void sc1_do_msg_pwr_up(void);
void sc1_do_msg_pwr_dn(void);
void sc1_do_msg_set_volt(void);
void sc1_do_msg_get_volt(void);
void sc1_do_msg_get_curr(void);
void sc1_do_msg_get_temp1(void);
void sc1_do_msg_get_temp2(void);
void sc1_do_msg_setclk(void);
void sc1_do_msg_start_job(void);
void sc1_do_msg_abort_job(void);
void sc1_do_msg_check_job(void);
void sc1_do_msg_jreg_write(void);
void sc1_do_msg_jman_write(void);
void send_sc1_jobdone(unsigned char);
void sc1_do_msg_proc_id(void);
void sc1_do_msg_set_poold(void);
void sc1_do_msg_set_perf(void);
void sc1_do_msg_get_adiag(void);
void sc1_do_smsg(void);
void sc1_do_msg_iwdg(void);
void sc1_do_msg_set_clock_delta(void);
void sc1_do_msg_good_nonces_per_device(void);
void sc1_do_msg_bad_nonces_per_device(void);

#ifdef HASH32
void send_sc1_nonce(unsigned char, unsigned int, unsigned int);
#endif

#ifdef HASH64
void send_sc1_nonce(unsigned char, long long);
#endif