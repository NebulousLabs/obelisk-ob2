//***********************************************************************
//**                                                                   **
//** TITLE: SC1CST.H                                                   **
//**                                                                   **
//** Orchid Technologies Engineering and Consulting, Inc.              **
//**                                                                   **
//** Written and Developed by:                                         **
//** Orchid Technologies Engineering and Consulting, Inc.              **
//** 147 Main Street                                                   **
//** Maynard, Ma.  01754                                               **
//** TEL: 978-461-2000                                                 **
//**                                                                   **
//**                                                                   **
//** This Source Code is the property of Orchid Technologies           **
//** Engineering and Consulting, Inc.  Copyright 2003, 2004, 2005,     **
//** 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016  **
//** 2017, 2018. 2019                                                  **
//**                                                                   **
//** Orchid Technologies Engineering and Consulting, Inc. reserves all **
//** rights and title to this software.  This software shall not be    **
//** reverse engineered, copied or used in any way without the express **
//** written permission of Orchid Technologies Engineering and         **
//** Consulting, Inc.                                                  **
//**                                                                   **
//**                                                                   **
//** NOTICE: Copyright (C) 2003, 2004, 2005, 2006, 2007, 2008, 2009,   **
//** 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019        **
//**                                                                   **
//** Orchid Technologies Engineering and Consulting, Inc.              **
//** All Rights Reserved.                                              **
//**                                                                   **
//**                                                                   **
//** Revision History:                                                 **
//**                                                                   **
//** ORCHID     11/01/04    V1.0    Original                           **
//**                                                                   **
//** ORCHID     04/15/05    V1.1    Adapted for Use                    **
//**                                                                   **
//** ORCHID     05/01/05    V1.2    Adapted for Use                    **
//**                                                                   **
//** ORCHID     11/01/06    V2.0    Adapted as MEDCORE                 **
//**                                                                   **
//** ORCHID     03/01/09    V3.0    Adapted as RMCORE for HS82117      **
//**                                Platform                           **
//**                                                                   **
//** ORCHID     05/01/09    V4.0    Adapted as A7CORE for NXP2194      **
//**                                Platform                           **
//**                                                                   **
//** ORCHID     04/01/10    V5.0    Adapted as M3COR for NXP17xx       **
//**                                                                   **
//** ORCHID     01/01/11    V6.0    Adapted as S3COR for STM32C10x     **
//**                                                                   **
//** ORCHID     12/01/14    V7.0    Adapted as MG1   for STM32F40x     **
//**                                                                   **
//** ORCHID     12/01/15    V8.0    Adapted as CH1   for STM32F40x     **
//**                                                                   **
//** ORCHID     04/15/16    V9.0    Adapted as SL3   for STM32F103     **
//**                                                                   **
//** ORCHID     05/05/18    V10.0   Adapted as FC1   for STM32F103     **
//**                                                                   **
//** ORCHID     07/15/18    V11.0   Adapted as SC1   for STM32F101     **
//***********************************************************************

//OTEC Declaration
const char otec_declaration[] = {"M4CORE V11.00  Written by Orchid Technologies Engineering and Consulting, Inc.  Copyright (c) 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019.  All Rights Reserved.  "};

//Now we define the CONSTANT Tables.
//These are the CRC16 Lookup Tables

const unsigned char CRC16_LookupHigh[] = {
   0x00, 0x10, 0x20, 0x30, 0x40, 0x50, 0x60, 0x70,
   0x81, 0x91, 0xA1, 0xB1, 0xC1, 0xD1, 0xE1, 0xF1};

const unsigned char CRC16_LookupLow[] = {
   0x00, 0x21, 0x42, 0x63, 0x84, 0xA5, 0xC6, 0xE7,
   0x08, 0x29, 0x4A, 0x6B, 0x8C, 0xAD, 0xCE, 0xEF};

//This array performs a simple chip address lookup translation
//This translates Chip Number 0x00 through 0x1F (32 chips)
//to their actual address which is described below:
//Devices 0x00-0x07 Are on SPI 1, These are the 'A' Devices
//Devices 0x08-0x0F Are on SPI 2, These are the 'A' Devices
//Devices 0x20-0x27 Are on SPI 1, These are the 'B' Devices
//Devices 0x28-0x2F Are on SPI 2, These are the 'B' Devices
const unsigned char chip_addr[] = {
    0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
    0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F,
    0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27,
    0x28, 0x29, 0x2A, 0x2B, 0x2C, 0x2D, 0x2E, 0x2F};

//This array contains the first set of test data form the service_ac
//Array Check routine
const unsigned int chip_data1[] = {
    0x84572938,     //Chip 0
    0xA5C339C4,     //Chip 1
    0x9EA852B0,     //Chip 2
    0x39ACF84B,     //Chip 3
    0x67C0F35A,     //Chip 4
    0x472BCA0F,     //Chip 5
    0x937CF35A,     //Chip 6
    0x10390C5A,     //Chip 7
    0x315CD645,     //Chip 8
    0xDFEBA532,     //Chip 9
    0x7801CA5B,     //Chip A
    0x2503CDA5,     //Chip B
    0x69BCE43A,     //Chip C
    0xAA320F0F,     //Chip D
    0x2357BDCE,     //Chip E
    0xD3B6C1A8,     //Chip F
    0x0430CCA3,     //Chip 10
    0xC53C0BEA,     //Chip 11
    0x53BAC443,     //Chip 12
    0x31CB0A0F,     //Chip 13
    0x043C0021,     //Chip 14
    0x5A430C23,     //Chip 15
    0x506CFF01,     //Chip 16
    0xF3089FC0,     //Chip 17
    0x892CBA33,     //Chip 18
    0x0948C231,     //Chip 19
    0x8941BEAC,     //Chip 1A
    0xDEADBEEF,     //Chip 1B
    0xBEEFDEAD,     //Chip 1C
    0x3223DEBC,     //Chip 1D
    0xB3EFA5C1,     //Chip 1E
    0xA53367C5,     //Chip 1F
    };

//This array contains the second set of test data form the service_ac
//Arrach Check routine
const unsigned int chip_data2[] = {
    0x84564EEF,     //Chip 0
    0x55AC5467,     //Chip 1
    0x0CE33BAA,     //Chip 2
    0x3443CBBB,     //Chip 3
    0x3214AC33,     //Chip 4
    0x643CBAF0,     //Chip 5
    0x045576C3,     //Chip 6
    0x363C35AE,     //Chip 7
    0xDAEC3465,     //Chip 8
    0x400C3256,     //Chip 9
    0x1238CEAB,     //Chip A
    0x7548C443,     //Chip B
    0x987696CC,     //Chip C
    0xCCBED436,     //Chip D
    0x12587649,     //Chip E
    0x9565CEAA,     //Chip F
    0x8564576B,     //Chip 10
    0x5465BCBE,     //Chip 11
    0xACBEF557,     //Chip 12
    0xD47DBC97,     //Chip 13
    0xDA535BCC,     //Chip 14
    0x3CF74BCC,     //Chip 15
    0x452DFE63,     //Chip 16
    0x856744CC,     //Chip 17
    0xAAC45388,     //Chip 18
    0x84645CB4,     //Chip 19
    0x437CEFFF,     //Chip 1A
    0x246547FC,     //Chip 1B
    0x74CBEE43,     //Chip 1C
    0x8345CEDD,     //Chip 1D
    0xA646CCE4,     //Chip 1E
    0xCBEDF544,     //Chip 1F
    };

#ifdef HASH64
//ASIC Startup Reset1 Messages
//These messages are globally written to the ASIC and then
//read back an optional number of times to verify proper programming.
//These messages are provided specifically for service_asic_reset.
struct amess AM_RESET1[] = {
    AM_SW, 0x00, 0x00, AR_FCR , 0x00000000, 0x000000FF,     //Fifo Control Reg - Mask all Interrupts
    AM_SW, 0x00, 0x00, AR_OCR,  0x00000000, 0x007FFF88,     //OCR to Divide by 8, Engines Enabled
    AM_SW, 0x00, 0x00, AR_ECR,  0x00000000, 0x00000000,     //ECR to Zero
    AM_SW, 0x00, 0x00, AR_LIM,  0x00000000, 0x00000031,     //LIM to 0x31
    AM_SW, 0x00, 0x00, AR_LBR,  0x00000000, 0x00000000,     //LBR to Zero
    AM_SW, 0x00, 0x00, AR_UBR,  0x00000000, 0x00000000,     //UBR to Zero
    AM_SW, 0x00, 0x00, AR_V00,  0x6A09E667, 0xF2BDC928,     //V Reg
    AM_SW, 0x00, 0x00, AR_V01,  0xBB67AE85, 0x84CAA73B,     //V Reg
    AM_SW, 0x00, 0x00, AR_V02,  0x3C6EF372, 0xFE94F82B,     //V Reg
    AM_SW, 0x00, 0x00, AR_V03,  0xA54FF53A, 0x5F1D36F1,     //V Reg
    AM_SW, 0x00, 0x00, AR_V04,  0x510E527F, 0xADE682D1,     //V Reg
    AM_SW, 0x00, 0x00, AR_V05,  0x9B05688C, 0x2B3E6C1F,     //V Reg
    AM_SW, 0x00, 0x00, AR_V06,  0x1F83D9AB, 0xFB41BD6B,     //V Reg
    AM_SW, 0x00, 0x00, AR_V07,  0x5BE0CD19, 0x137E2179,     //V Reg
    AM_SW, 0x00, 0x00, AR_V08,  0x6A09E667, 0xF3BCC908,     //V Reg
    AM_SW, 0x00, 0x00, AR_V09,  0xBB67AE85, 0x84CAA73B,     //V Reg
    AM_SW, 0x00, 0x00, AR_V10,  0x3C6EF372, 0xFE94F82B,     //V Reg
    AM_SW, 0x00, 0x00, AR_V11,  0xA54FF53A, 0x5F1D36F1,     //V Reg
    AM_SW, 0x00, 0x00, AR_V12,  0x510E527F, 0xADE68281,     //V Reg
    AM_SW, 0x00, 0x00, AR_V13,  0x9B05688C, 0x2B3E6C1F,     //V Reg
    AM_SW, 0x00, 0x00, AR_V14,  0xE07C2654, 0x04BE4294,     //V Reg
    AM_SW, 0x00, 0x00, AR_V15,  0x5BE0CD19, 0x137E2179,     //V Reg
    AM_SW, 0x00, 0x00, AR_MAT0, 0x6A09E667, 0xF2BDC928,     //Match Register
    AM_SW, 0x00, 0x00, AR_MAT8, 0x00000000, 0xFFFFFFFF,     //Match Register
//  AM_SW, 0x00, 0x00, AR_STP,  0x00000000, 0x00000001,     //Step Register
    AM_SW, 0x00, 0x00, AR_STP,  0x00000000, 0x000003F1,     //Step Register - 1009
    AM_SW, 0x00, 0x00, AR_M0,   0x00000000, 0x00000000,     //M Reg
    AM_SW, 0x00, 0x00, AR_M1,   0x00000000, 0x00000000,     //M Reg
    AM_SW, 0x00, 0x00, AR_M2,   0x00000000, 0x00000000,     //M Reg
    AM_SW, 0x00, 0x00, AR_M3,   0x00000000, 0x00000000,     //M Reg
    AM_SW, 0x00, 0x00, AR_M5,   0x00000000, 0x00000000,     //M Reg
    AM_SW, 0x00, 0x00, AR_M6,   0x00000000, 0x00000000,     //M Reg
    AM_SW, 0x00, 0x00, AR_M7,   0x00000000, 0x00000000,     //M Reg
    AM_SW, 0x00, 0x00, AR_M8,   0x00000000, 0x00000000,     //M Reg
    AM_SW, 0x00, 0x00, AR_M9,   0x00000000, 0x00000000,     //M Reg
    AM_SW, 0x00, 0x00, AR_ECR,  0x00000000, 0x00000009,     //ECR Reset
    AM_SW, 0xFF, 0xFF, 0xFF,    0x00000000, 0x00000000      //All Done
    };

//ASIC Startup Reset2 Messages
//These messages are globally written to the ASIC and then
//read back an optional number of times to verify proper programming.
//These messages are provided specifically for service_asic_reset.
struct amess AM_RESET2[] = {
    AM_SW, 0x00, 0x00, AR_ECR,  0x00000000, 0x00000000,     //ECR Start Engines
//  AM_SW, 0x00, 0x00, AR_OCR,  0x00000000, 0x007FFF88,     //OCR to Divide by 8, All  Engines Enabled
//  AM_SW, 0x00, 0x00, AR_OCR,  0x00000002, 0x00003F88,     //OCR to Divide by 8, Some Engines Enabled, Enable CLKOUT
//  AM_SW, 0x00, 0x00, AR_OCR,  0x00000002, 0xC07FFFA0,     //OCR to Divide by 2, All  Engines Enabled +2 Bias, enable CLKOUT
    AM_SW, 0x00, 0x00, AR_OCR,  0x00000002, 0x007FFFA0,     //OCR to Divide by 2, All  Engines Enabled +0 Bias, enable CLKOUT
//  AM_SW, 0x00, 0x00, AR_OCR,  0x00000000, 0x000FFFA0,     //OCR to Divide by 2, Some  Engines Enabled
//  AM_SW, 0x00, 0x00, AR_OCR,  0x00000000, 0x007FFF90,     //OCR to Divide by 4, All  Engines Enabled
//  AM_SW, 0x00, 0x00, AR_OCR,  0x00000000, 0x007FFFC0,     //OCR to Divide by 1, All  Engines Enabled
//  AM_SW, 0x00, 0x00, AR_FCR , 0x00000000, 0x00000000,     //Fifo Control Reg - UnMask all Interrupts
    AM_SW, 0xFF, 0xFF, 0xFF,    0x00000000, 0x00000000      //All Done
    };

//ASIC Read Status Message
//This message performs a read of the chip's status register
//There are read messages which read the entire done and busy data of a given device
struct amess AM_STATUS1[] = {
    AM_LR, 0x00, 0x80, AR_EDR,  0x00000000, 0x00000000,     //EDR Read
    AM_LR, 0x00, 0x80, AR_EBR,  0x00000000, 0x00000000,     //EBR Read
    AM_LR, 0x00, 0x80, AR_IVEC, 0x00000000, 0x00000000,     //Interrupt Vector
    AM_SW, 0xFF, 0xFF, 0xFF,    0x00000000, 0x00000000      //All Done
    };

//Generic Local Write VALID Hi
struct amess64 AM_VALID_HI = {
    AM_LW, 0x00, 0x00, AR_ECR,  0x0000000000000004,     //ECR Reg Valid Data High
    };

//Generic Local Write VALID Low
struct amess64 AM_VALID_LO = {
    AM_LW, 0x00, 0x00, AR_ECR,  0x0000000000000000,     //ECR Reg Valid Data Low
    };

//Generic Local Write READ_READY Hi
struct amess64 AM_RREADY_HI = {
    AM_LW, 0x00, 0x00, AR_ECR,  0x0000000000000002,     //ECR Reg Read Complete High
    };

//Generic Local Write READ_READY Low
struct amess64 AM_RREADY_LO = {
    AM_LW, 0x00, 0x00, AR_ECR,  0x0000000000000000,     //ECR Reg Read Complete Low
    };

//Generic Local Write RESET Hi
struct amess64 AM_RESET_HI = {
    AM_LW, 0x00, 0x00, AR_ECR,  0x0000000000000008,     //ECR Reg Read Complete High
    };

//Generic Local Write READ_READY Low
struct amess64 AM_RESET_LO = {
    AM_LW, 0x00, 0x00, AR_ECR,  0x0000000000000000,     //ECR Reg Read Complete Low
    };

//Generic Local Read ESR Register for DONE Bit
struct amess64 AM_READ_ESR = {
    AM_LR, 0x00, 0x00, AR_ESR, 0x0000000000000000,
    };

//Generic Local Read FSR Register for FIFO Status Bits
struct amess64 AM_READ_FSR = {
    AM_LR, 0x00, 0x00, AR_FSR, 0x0000000000000000,
    };

//Generic Local Read M5 Register Read
struct amess64 AM_READ_M5 = {
    AM_LR, 0x00, 0x00, AR_M5, 0x0000000000000000,
    };

//ASIC Job Manager Test Data for Code Development Only
//ASIC Test Message 1 - This data cooresponds to below registers
//This test message is intended to program M registers and LB/UB
//registers with Sample Input #1 from the Blake2B data sheet.
unsigned long long JM_MREG[] =  {
    0x1E63000000000000,     //M Reg 0
    0xF78B797E5D753F6F,     //M Reg 1
    0xE0E21B09975ADACE,     //M Reg 2
    0x923C26B1B437E95D,     //M Reg 3
    0x0000000000000000,     //M Reg 4 - Placeholder
    0x0000000056307FC8,     //M Reg 5
    0x1023C71D7FB4A5EB,     //M Reg 6
    0x459779BFC18FFED4,     //M Reg 7
    0x10EEA15DE88CAF87,     //M Reg 8
    0xCC2F2CEDD68DB7C6,     //M Reg 9
    0x6A09E667F2BDC928,     //V Reg 0
    0xBB67AE8584CAA73B,     //V Reg 1
    0x3C6EF372FE94F82B,     //V Reg 2
    0xA54FF53A5F1D36F1,     //V Reg 3
    0x510E527FADE682D1,     //V Reg 4
    0x9B05688C2B3E6C1F,     //V Reg 5
    0x1F83D9ABFB41BD6B,     //V Reg 6
    0x5BE0CD19137E2179,     //V Reg 7
    0x6A09E667F3BCC908,     //V Reg 8
    0xBB67AE8584CAA73B,     //V Reg 9
    0x3C6EF372FE94F82B,     //V Reg 10
    0xA54FF53A5F1D36F1,     //V Reg 11
    0x510E527FADE68281,     //V Reg 12
    0x9B05688C2B3E6C1F,     //V Reg 13
    0xE07C265404BE4294,     //V Reg 14
    0x5BE0CD19137E2179,     //V Reg 15
    0x6A09E667F2BDC928,     //Match Register
    0x00000000FFFFFFFF,     //Match Register
    SIA_INCREMENT_HEX,      //Step Register = 1009
//  0x0000000000000001,     //Step Register = 1
    0x0000000000000000      //All Done
    };

//ASIC Job Manager Test Data for Code Development Only
//ASIC Test Message 1  -  These register addresses coorespond to above data
//This test message is intended to program M registers and LB/UB
//registers with Sample Input #1 from the Blake2B data sheet.
unsigned char JM_RADR[] =  {
    AR_M0,   //M Reg
    AR_M1,   //M Reg
    AR_M2,   //M Reg
    AR_M3,   //M Reg
    AR_M5,   //M Reg - This is the M4 Register Placeholder...
    AR_M5,   //M Reg
    AR_M6,   //M Reg
    AR_M7,   //M Reg
    AR_M8,   //M Reg
    AR_M9,   //M Reg
    AR_V00,
    AR_V01,
    AR_V02,
    AR_V03,
    AR_V04,
    AR_V05,
    AR_V06,
    AR_V07,
    AR_V08,
    AR_V09,
    AR_V10,
    AR_V11,
    AR_V12,
    AR_V13,
    AR_V14,
    AR_V15,
    AR_MAT0,
    AR_MAT8,
    AR_STP,
    0xFF     //All Done
    };

//The following is the SIA64 Hashing Algorithm
//The following is the SIA64 Hashing Algorithm
//The following is the SIA64 Hashing Algorithm

//Test example
unsigned char testOneHeader[] = {
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x34, 0x6f,
        0x8b, 0xb8, 0x8b, 0xf6, 0x7e, 0x19, 0x4a, 0xc2,
        0x06, 0x4b, 0xb4, 0x16, 0x5f, 0x90, 0x04, 0x5d,
        0x84, 0x46, 0x97, 0xab, 0x37, 0x1a, 0xd0, 0x5f,
        0x98, 0x33, 0x17, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x7c, 0x17, 0x7e, 0x55, 0x00, 0x00, 0x00, 0x00,
        0x89, 0x4e, 0x20, 0x2a, 0x44, 0x1b, 0x79, 0xa7,
        0x67, 0x6b, 0xb5, 0x7f, 0x61, 0xd7, 0x7f, 0x8c,
        0xfb, 0x6c, 0x3d, 0xca, 0x6d, 0xd6, 0x0c, 0xa8,
        0xc8, 0x92, 0x4d, 0xbb, 0xcb, 0x74, 0x8c, 0x6b
        };

unsigned long long blake2b_IV[8] =
{
  0x6a09e667f3bcc908, 0xbb67ae8584caa73b,
  0x3c6ef372fe94f82b, 0xa54ff53a5f1d36f1,
  0x510e527fade682d1, 0x9b05688c2b3e6c1f,
  0x1f83d9abfb41bd6b, 0x5be0cd19137e2179
};

unsigned char blake2b_sigma[12][16] =
{
  {  0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15 } ,
  { 14, 10,  4,  8,  9, 15, 13,  6,  1, 12,  0,  2, 11,  7,  5,  3 } ,
  { 11,  8, 12,  0,  5,  2, 15, 13, 10, 14,  3,  6,  7,  1,  9,  4 } ,
  {  7,  9,  3,  1, 13, 12, 11, 14,  2,  6,  5, 10,  4,  0, 15,  8 } ,
  {  9,  0,  5,  7,  2,  4, 10, 15, 14,  1, 11, 12,  6,  8,  3, 13 } ,
  {  2, 12,  6, 10,  0, 11,  8,  3,  4, 13,  7,  5, 15, 14,  1,  9 } ,
  { 12,  5,  1, 15, 14, 13,  4, 10,  0,  7,  6,  3,  9,  2,  8, 11 } ,
  { 13, 11,  7, 14, 12,  1,  3,  9,  5,  0, 15,  4,  8,  6,  2, 10 } ,
  {  6, 15, 14,  9, 11,  3,  0,  8, 12,  2, 13,  7,  1,  4, 10,  5 } ,
  { 10,  2,  8,  4,  7,  6,  1,  5, 15, 11,  9, 14,  3, 12, 13 , 0 } ,
  {  0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15 } ,
  { 14, 10,  4,  8,  9, 15, 13,  6,  1, 12,  0,  2, 11,  7,  5,  3 }
};

#endif  //HASH64



#ifdef HASH32
//ASIC Startup Reset1 Messages
//These messages are globally written to the ASIC and then
//read back an optional number of times to verify proper programming.
//These messages are provided specifically for service_asic_reset.
struct amess AM_RESET1[] = {
    AM_SW, 0x00, 0x00, AR_FCR , 0x000000FF, 0x00000000,     //Fifo Control Reg - Mask all Interrupts
    AM_SW, 0x00, 0x00, AR_OCR1, 0x007FFF88, 0x00000000,     //OCR1 to Divide by 8, Engines Enabled
    AM_SW, 0x00, 0x00, AR_OCR2, 0x00000000, 0x00000000,     //OCR2 to Zero - enable clk out
    AM_SW, 0x00, 0x00, AR_ECR,  0x00000000, 0x00000000,     //ECR to Zero
    AM_SW, 0x00, 0x00, AR_LIM,  0x00000038, 0x00000000,     //LIM to 0x38
    AM_SW, 0x00, 0x00, AR_LBR,  0x00000000, 0x00000000,     //LBR to Zero
    AM_SW, 0x00, 0x00, AR_UBR,  0x00000000, 0x00000000,     //UBR to Zero
    AM_SW, 0x00, 0x00, AR_V00,  0x51F87D90, 0x00000000,     //V Reg
    AM_SW, 0x00, 0x00, AR_V01,  0x660D07D4, 0x00000000,     //V Reg
    AM_SW, 0x00, 0x00, AR_V02,  0xA697C2B7, 0x00000000,     //V Reg
    AM_SW, 0x00, 0x00, AR_V03,  0xC52BB4EF, 0x00000000,     //V Reg
    AM_SW, 0x00, 0x00, AR_V04,  0xBB2C0B5E, 0x00000000,     //V Reg
    AM_SW, 0x00, 0x00, AR_V05,  0x41B8CB4C, 0x00000000,     //V Reg
    AM_SW, 0x00, 0x00, AR_V06,  0x9254A116, 0x00000000,     //V Reg
    AM_SW, 0x00, 0x00, AR_V07,  0xE2B2CC34, 0x00000000,     //V Reg
    AM_SW, 0x00, 0x00, AR_V08,  0x243F6A88, 0x00000000,     //V Reg
    AM_SW, 0x00, 0x00, AR_V09,  0x85A308D3, 0x00000000,     //V Reg
    AM_SW, 0x00, 0x00, AR_V10,  0x13198A2E, 0x00000000,     //V Reg
    AM_SW, 0x00, 0x00, AR_V11,  0x03707344, 0x00000000,     //V Reg
    AM_SW, 0x00, 0x00, AR_V12,  0xA4093D82, 0x00000000,     //V Reg
    AM_SW, 0x00, 0x00, AR_V13,  0x299F3470, 0x00000000,     //V Reg
    AM_SW, 0x00, 0x00, AR_V14,  0x082EFA98, 0x00000000,     //V Reg
    AM_SW, 0x00, 0x00, AR_V15,  0xEC4E6C89, 0x00000000,     //V Reg
    AM_SW, 0x00, 0x00, AR_MAT,  0xE2B2CC34, 0x00000000,     //Match Register
    AM_SW, 0x00, 0x00, AR_M0,   0x00000000, 0x00000000,     //M Reg
    AM_SW, 0x00, 0x00, AR_M1,   0x00000000, 0x00000000,     //M Reg
    AM_SW, 0x00, 0x00, AR_M2,   0x00000000, 0x00000000,     //M Reg
    AM_SW, 0x00, 0x00, AR_M4,   0x00000000, 0x00000000,     //M Reg
    AM_SW, 0x00, 0x00, AR_M5,   0x00000000, 0x00000000,     //M Reg
    AM_SW, 0x00, 0x00, AR_M6,   0x00000000, 0x00000000,     //M Reg
    AM_SW, 0x00, 0x00, AR_M7,   0x00000000, 0x00000000,     //M Reg
    AM_SW, 0x00, 0x00, AR_M8,   0x00000000, 0x00000000,     //M Reg
    AM_SW, 0x00, 0x00, AR_M9,   0x00000000, 0x00000000,     //M Reg
    AM_SW, 0x00, 0x00, AR_M10,  0x00000000, 0x00000000,     //M Reg
    AM_SW, 0x00, 0x00, AR_M11,  0x00000000, 0x00000000,     //M Reg
    AM_SW, 0x00, 0x00, AR_M12,  0x00000000, 0x00000000,     //M Reg
    AM_SW, 0x00, 0x00, AR_ECR,  0x00000009, 0x00000000,     //ECR Reset
    AM_SW, 0xFF, 0xFF, 0xFF,    0x00000000, 0x00000000      //All Done
    };

//ASIC Startup Reset2 Messages
//These messages are globally written to the ASIC and then
//read back an optional number of times to verify proper programming.
//These messages are provided specifically for service_asic_reset.
struct amess AM_RESET2[] = {
    AM_SW, 0x00, 0x00, AR_ECR , 0x00000000, 0x00000000,     //ECR Start Engines
//  AM_SW, 0x00, 0x00, AR_OCR1, 0x007FFF88, 0x00000000,     //OCR1 to Divide by 8, All  Engines Enabled
//  AM_SW, 0x00, 0x00, AR_OCR1, 0x0003FF88, 0x00000000,     //OCR1 to Divide by 8, Some Engines Enabled
    AM_SW, 0x00, 0x00, AR_OCR1, 0xC07FFFA0, 0x00000000,     //OCR1 to Divide by 2, All  Engines Enabled, +2 Bias
//  AM_SW, 0x00, 0x00, AR_OCR1, 0x000FFFA0, 0x00000000,     //OCR1 to Divide by 2, Some  Engines Enabled
//  AM_SW, 0x00, 0x00, AR_OCR1, 0x007FFF90, 0x00000000,     //OCR1 to Divide by 4, All  Engines Enabled
//  AM_SW, 0x00, 0x00, AR_OCR1, 0x007FFFC0, 0x00000000,     //OCR1 to Divide by 1, All  Engines Enabled
//  AM_SW, 0x00, 0x00, AR_FCR , 0x00000000, 0x00000000,     //Fifo Control Reg - UnMask all Interrupts
    AM_SW, 0x00, 0x00, AR_OCR2, 0x00000002, 0x00000000,     //OCR2 to Zero - enable clk out
    AM_SW, 0xFF, 0xFF, 0xFF,    0x00000000, 0x00000000      //All Done
    };

//ASIC Read Status Message
//This message performs a read of the chip's status register
//There are read messages which read the entire done and busy data of a given device
struct amess AM_STATUS1[] = {
    AM_LR, 0x00, 0x80, AR_EDR1, 0x00000000, 0x00000000,     //EDR Read
    AM_LR, 0x00, 0x80, AR_EDR2, 0x00000000, 0x00000000,     //EDR Read
    AM_LR, 0x00, 0x80, AR_EDR3, 0x00000000, 0x00000000,     //EDR Read
    AM_LR, 0x00, 0x80, AR_EDR4, 0x00000000, 0x00000000,     //EDR Read
    AM_LR, 0x00, 0x80, AR_EBR1, 0x00000000, 0x00000000,     //EBR Read
    AM_LR, 0x00, 0x80, AR_EBR2, 0x00000000, 0x00000000,     //EBR Read
    AM_LR, 0x00, 0x80, AR_EBR3, 0x00000000, 0x00000000,     //EBR Read
    AM_LR, 0x00, 0x80, AR_EBR4, 0x00000000, 0x00000000,     //EBR Read
    AM_LR, 0x00, 0x80, AR_IVEC, 0x00000000, 0x00000000,     //Interrupt Vector
    AM_SW, 0xFF, 0xFF, 0xFF,    0x00000000, 0x00000000      //All Done
    };

//Generic Local Write VALID Hi
struct amess AM_VALID_HI = {
    AM_LW, 0x00, 0x00, AR_ECR,  0x00000004, 0x00000000,     //ECR Reg Valid Data High
    };

//Generic Local Write VALID Low
struct amess AM_VALID_LO = {
    AM_LW, 0x00, 0x00, AR_ECR,  0x00000000, 0x00000000,     //ECR Reg Valid Data Low
    };

//Generic Local Write READ_READY Hi
struct amess AM_RREADY_HI = {
    AM_LW, 0x00, 0x00, AR_ECR,  0x00000002, 0x00000000,     //ECR Reg Read Complete High
    };

//Generic Local Write READ_READY Low
struct amess AM_RREADY_LO = {
    AM_LW, 0x00, 0x00, AR_ECR,  0x00000000, 0x00000000,     //ECR Reg Read Complete Low
    };

//Generic Local Write RESET Hi
struct amess AM_RESET_HI = {
    AM_LW, 0x00, 0x00, AR_ECR,  0x00000008, 0x00000000,     //ECR Reset
    };

//Generic Local Write RESET Low
struct amess AM_RESET_LO = {
    AM_LW, 0x00, 0x00, AR_ECR,  0x00000000, 0x00000000,     //ECR Reset
    };

//Generic Local Read ESR Register for DONE Bit
struct amess AM_READ_ESR = {
    AM_LR, 0x00, 0x00, AR_ESR, 0x00000000, 0x00000000,
    };

//Generic Local Read FSR Register for FIFO Status Bits
struct amess AM_READ_FSR = {
    AM_LR, 0x00, 0x00, AR_FSR, 0x00000000, 0x00000000,
    };

//Generic Local Read M5 Register Read
struct amess AM_READ_M5 = {
    AM_LR, 0x00, 0x00, AR_M5, 0x00000000, 0x00000000,
    };


//ASIC Job Manager Test Data for Code Development Only
//ASIC Test Message 1 - This data cooresponds to below registers
//This test message is intended to program M registers and LB/UB
//registers with Sample Input #1 from the Blake256 data sheet.
unsigned int JM_MREG[] =  {
    0x51F87D90,   //V Reg
    0x660D07D4,   //V Reg
    0xA697C2B7,   //V Reg
    0xC52BB4EF,   //V Reg
    0xBB2C0B5E,   //V Reg
    0x41B8CB4C,   //V Reg
    0x9254A116,   //V Reg
    0xE2B2CC34,   //V Reg
    0x243F6A88,   //V Reg -- Even though these are fixed we re-write them anyway
    0x85A308D3,   //V Reg
    0x13198A2E,   //V Reg
    0x03707344,   //V Reg
    0xA4093D82,   //V Reg
    0x299F3470,   //V Reg
    0x082EFA98,   //V Reg
    0xEC4E6C89,   //V Reg
    0x6A5C0200,   //M Reg 0
    0x39200000,   //M Reg 1
    0x74017B59,   //M Reg 2
    0x00000005,   //M Reg 4
    0x00000000,   //M Reg 5
    0x00000000,   //M Reg 6
    0x00000000,   //M Reg 7
    0x00000000,   //M Reg 8
    0x00000000,   //M Reg 9
    0x00000000,   //M Reg 10
    0x00000000,   //M Reg 11
    0x04000000,   //M Reg 12
    0x00000038,   //LIM to 0x38
    0xE2B2CC34,   //Match Register
    0x00000000,   //LBR
    0xFFFFFFFF,   //Full Range UBR
//    0x64EE1D70,   //LBR
//    0x64EE1DB0,   //Full Range UBR
    0x00000000    //Dummy
    };

//ASIC Job Manager Engine Register Order in Memory
//These addresses correspond to the location of the job-specific V and M
//register data in the job memory structure.  These are fixed and contain
//the entire V and M register set.  NOTE: THE ORDER IS FIXED AND IMPORTANT
//AS THE REGISTER LOCATIONS ARE RELIED UPON BY OTHER CODE ELEMENTS.  DO
//NOT CHANGE THE REGISTER ORDER....  Last data entry is indicated by 0xFF.
unsigned char JM_RADR[] =  {
    AR_V00,  //V Reg
    AR_V01,  //V Reg
    AR_V02,  //V Reg
    AR_V03,  //V Reg
    AR_V04,  //V Reg
    AR_V05,  //V Reg
    AR_V06,  //V Reg
    AR_V07,  //V Reg
    AR_V08,  //V Reg
    AR_V09,  //V Reg
    AR_V10,  //V Reg
    AR_V11,  //V Reg
    AR_V12,  //V Reg
    AR_V13,  //V Reg
    AR_V14,  //V Reg
    AR_V15,  //V Reg
    AR_M0,   //M Reg
    AR_M1,
    AR_M2,
    AR_M4,
    AR_M5,
    AR_M6,
    AR_M7,
    AR_M8,
    AR_M9,
    AR_M10,
    AR_M11,
    AR_M12,
    AR_LIM,  //LIM to 0x38
    AR_MAT,  //Match Register
    AR_LBR,
    AR_UBR,
    0xFF
    };

// Blake-256 constants, taken verbatim from the final
// spec, "SHA-3 proposal BLAKE" version 1.3, published
// on December 16, 2010 - section 2.1.1.
unsigned int BLAKE256_CONSTS[16] = {
    0x243F6A88, 0x85A308D3, 0x13198A2E, 0x03707344,
    0xA4093822, 0x299F31D0, 0x082EFA98, 0xEC4E6C89,
    0x452821E6, 0x38D01377, 0xBE5466CF, 0x34E90C6C,
    0xC0AC29B7, 0xC97C50DD, 0x3F84D5B5, 0xB5470917
};

// Sigma values - permutations of 0 -15, used in Blake-256.
// Found in the aforementioned spec in section 2.1.2.
unsigned char BLAKE256_SIGMA[10][16] = {
    {  0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15 },
    { 14, 10,  4,  8,  9, 15, 13,  6,  1, 12,  0,  2, 11,  7,  5,  3 },
    { 11,  8, 12,  0,  5,  2, 15, 13, 10, 14,  3,  6,  7,  1,  9,  4 },
    {  7,  9,  3,  1, 13, 12, 11, 14,  2,  6,  5, 10,  4,  0, 15,  8 },
    {  9,  0,  5,  7,  2,  4, 10, 15, 14,  1, 11, 12,  6,  8,  3, 13 },
    {  2, 12,  6, 10,  0, 11,  8,  3,  4, 13,  7,  5, 15, 14,  1,  9 },
    { 12,  5,  1, 15, 14, 13,  4, 10,  0,  7,  6,  3,  9,  2,  8, 11 },
    { 13, 11,  7, 14, 12,  1,  3,  9,  5,  0, 15,  4,  8,  6,  2, 10 },
    {  6, 15, 14,  9, 11,  3,  0,  8, 12,  2, 13,  7,  1,  4, 10,  5 },
    { 10,  2,  8,  4,  7,  6,  1,  5, 15, 11,  9, 14,  3, 12, 13 , 0 }
};

#endif  //HASH32
