//***********************************************************************
//**                                                                   **
//** TITLE: SC1MEM.H                                                   **
//**                                                                   **
//** Orchid Technologies Engineering and Consulting, Inc.              **
//**                                                                   **
//** Written and Developed by:                                         **
//** Orchid Technologies Engineering and Consulting, Inc.              **
//** 147 Main Street                                                   **
//** Maynard, Ma.  01754                                               **
//** TEL: 978-461-2000                                                 **
//**                                                                   **
//**                                                                   **
//** This Source Code is the property of Orchid Technologies           **
//** Engineering and Consulting, Inc.  Copyright 2003, 2004, 2005,     **
//** 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016  **
//** 2017, 2018                                                        **
//**                                                                   **
//** Orchid Technologies Engineering and Consulting, Inc. reserves all **
//** rights and title to this software.  This software shall not be    **
//** reverse engineered, copied or used in any way without the express **
//** written permission of Orchid Technologies Engineering and         **
//** Consulting, Inc.                                                  **
//**                                                                   **
//**                                                                   **
//** NOTICE: Copyright (C) 2003, 2004, 2005, 2006, 2007, 2008, 2009,   **
//** 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018              **
//**                                                                   **
//** Orchid Technologies Engineering and Consulting, Inc.              **
//** All Rights Reserved.                                              **
//**                                                                   **
//**                                                                   **
//** Revision History:                                                 **
//**                                                                   **
//** ORCHID     11/01/04    V1.0    Original                           **
//**                                                                   **
//** ORCHID     04/15/05    V1.1    Adapted for Use                    **
//**                                                                   **
//** ORCHID     05/01/05    V1.2    Adapted for Use                    **
//**                                                                   **
//** ORCHID     11/01/06    V2.0    Adapted as MEDCORE                 **
//**                                                                   **
//** ORCHID     03/01/09    V3.0    Adapted as RMCORE for HS82117      **
//**                                Platform                           **
//**                                                                   **
//** ORCHID     05/01/09    V4.0    Adapted as A7CORE for NXP2194      **
//**                                Platform                           **
//**                                                                   **
//** ORCHID     04/01/10    V5.0    Adapted as M3COR for NXP17xx       **
//**                                                                   **
//** ORCHID     01/01/11    V6.0    Adapted as S3COR for STM32C10x     **
//**                                                                   **
//** ORCHID     12/01/14    V7.0    Adapted as MG1   for STM32F40x     **
//**                                                                   **
//** ORCHID     12/01/15    V8.0    Adapted as CH1   for STM32F40x     **
//**                                                                   **
//** ORCHID     04/15/16    V9.0    Adapted as SL3   for STM32F103     **
//**                                                                   **
//** ORCHID     05/05/18    V10.0   Adapted as FC1   for STM32F103     **
//**                                                                   **
//** ORCHID     07/15/18    V11.0   Adapted as SC1   for STM32F101     **
//***********************************************************************

//State Variables

//RTC Interrupt Memory Declarations
//These are used to run the internal RTC Services and should
//not be modified by application firmware.  The flags are
//used in the rtc_event routine to queue various events on
//their respective intervals in the foreground.
extern volatile unsigned char rtc_delay;               //Used for short blocking delays in various routines
extern volatile unsigned short rtc_running;            //Base Fractional second timer to esablish seconds count
extern volatile unsigned char rtc_seconds;             //Seconds

//SCI0 Storage
extern unsigned char sci_txbuffer0[SCI_BUF_SIZE+2];
extern unsigned char sci_rxbuffer0[SCI_BUF_SIZE+2];
extern unsigned char irq_sci_status0;
extern unsigned char *txring_inptr0;
extern unsigned char *txring_outptr0;
extern unsigned char *rxring_inptr0;
extern unsigned char *rxring_outptr0;
extern unsigned short txring_count0;
extern unsigned short rxring_count0;
extern unsigned char  comm_lineflag0;
extern unsigned short comm_linecount0;
extern unsigned char  comm_linebuf0[SCI_BUF_SIZE+2];

//SCI1 Storage
extern unsigned char sci_txbuffer1[SCI_BUF_SIZE+2];
extern unsigned char sci_rxbuffer1[SCI_BUF_SIZE+2];
extern unsigned char irq_sci_status1;
extern unsigned char *txring_inptr1;
extern unsigned char *txring_outptr1;
extern unsigned char *rxring_inptr1;
extern unsigned char *rxring_outptr1;
extern unsigned short txring_count1;
extern unsigned short rxring_count1;

//SPI1 Storage
extern unsigned char spi1_txbuffer[SPI_BUF_SIZE+2]; //Global Transmit Buffer
extern unsigned char spi1_rxbuffer[SPI_BUF_SIZE+2]; //Global Receive  Buffer
extern unsigned char spi1_ready_flag;               //SPI Ready Flag

//SPI2 Storage
extern unsigned char spi2_txbuffer[SPI_BUF_SIZE+2]; //Global Transmit Buffer
extern unsigned char spi2_rxbuffer[SPI_BUF_SIZE+2]; //Global Receive  Buffer
extern unsigned char spi2_ready_flag;               //SPI Ready Flag

//Protocol Support
extern unsigned char  service_sc1_rcvr_state;
extern unsigned char  service_sc1_rcvr_timer;
extern unsigned char  service_sc1_rcvr_flag;
extern unsigned char  service_sc1_rcvr_index;
extern unsigned char  service_sc1_rcvr_csum;
extern unsigned char  service_sc1_rcvr_noise;  //Protocol Noise Flag if Set to 0xFF
extern unsigned char  service_sc1_rcvr_buffer[SC1_PROT_MAX_SIZE];
extern unsigned char  service_sc1_xmit_buffer[SC1_PROT_MAX_SIZE];

//Application Globals
extern unsigned char comm_procline0_state;
extern unsigned char show_morse_version_timer;
extern unsigned char  service_pwrset_state;
extern unsigned char  service_pwrset_timer;
extern unsigned char  service_pwrset_upcnt;
extern unsigned char  service_pwrset_dncnt;

extern unsigned char  service_ac_state;
extern unsigned int   service_ac_testdata_hr;
extern unsigned int   service_ac_testdata_lr;
extern unsigned char  service_ac_chip;
extern unsigned char  service_ac_pass_count;
extern unsigned short service_ac_timer;
extern unsigned char  service_ac_pass[];
extern unsigned char  service_ac_fail[];
extern unsigned char  service_ac_flag;
extern          float service_ac_adc_vscale;
extern struct amess   service_ac_msg;

extern unsigned char  service_asic_reset_state;
extern unsigned char  service_asic_reset_timer;
extern struct amess   *service_asic_reset_msgp;

extern unsigned char  service_asic_status_state;
extern struct amess   *service_asic_status_msgp;
extern unsigned char  service_asic_status_chip;
extern unsigned char  service_asic_status_mcnt;
extern unsigned int   service_asic_status_status_l[32][10];
extern unsigned int   service_asic_status_status_h[32][10];

extern unsigned char  service_adc_state;
extern unsigned char  service_adc_timer;
extern unsigned char  service_adc_chan;
extern unsigned char  service_adc_cflag;
extern unsigned int   service_adc_rdata[20];

extern unsigned char  service_show_adc_state;
extern unsigned char  service_show_adc_timer;

extern unsigned char  service_setclk_state;
extern          int   service_setclk_bias;
extern          int   service_setclk_divisor;
extern struct amess   service_setclk_am;

extern unsigned char  service_diff_setclk_state;
extern          int   service_diff_setclk_bias;
extern          int   service_diff_setclk_divisor;
extern          int   service_diff_setclk_bias2;
extern          int   service_diff_setclk_divisor2;
extern          int   service_diff_setclk_index;
extern struct amess   service_diff_setclk_am;

//Application Messages
extern struct amess AM_RESET1[];
extern struct amess AM_RESET2[];
extern struct amess AM_STATUS1[];

extern unsigned char  service_perf_state;
extern unsigned char  service_perf_avgcnt;
extern          int   service_perf_pwrset;
extern          int   service_perf_pwrpoint;   //actual set point 0 to 127
extern          int   service_perf_avgcur;
extern          int   service_perf_setcur;
extern          int   service_perf_bias;
extern          int   service_perf_divide;

extern unsigned char  service_unsoc_msg_state;
extern unsigned short service_unsoc_msg_timer;
extern unsigned short hw_build_level;
extern unsigned char  service_kick_iwdg_timer;

//Engine Job Control
extern struct ecfm ecf1[];     //SPI1 Engine Conrol Flags, 3 bits per engine
extern struct ecfm ecf2[];     //SPI1 Engine Conrol Flags, 3 bits per engine

#ifdef HASH32
extern struct amess AM_VALID_HI;
extern struct amess AM_VALID_LO;
extern struct amess AM_RREADY_HI;
extern struct amess AM_RREADY_LO;
extern struct amess AM_RESET_HI;
extern struct amess AM_RESET_LO;
extern struct amess AM_READ_ESR;
extern struct amess AM_READ_FSR;
extern struct amess AM_READ_M5;

extern struct jobman jm;
extern struct asic_set_job asj1;
extern struct asic_set_job asj2;
extern struct asic_get_result agr1;
extern struct asic_get_result agr2;
extern struct raw_result_queue rrq1[];
extern struct raw_result_queue rrq2[];
extern struct asic_h32_check  ahc;
extern unsigned char dcr_target_difficulty[32];
extern struct dcr_statistics  dstat;
extern struct asic_stat as;

extern unsigned int JM_MREG[];
extern unsigned char JM_RADR[];

//These support Hashing Check Function
//This is the BLAKE256 Fixed Constants - V Registers
extern unsigned int BLAKE256_CONSTS[];
extern unsigned char BLAKE256_SIGMA[10][16];

#endif  //HASH32

#ifdef HASH64
extern struct amess64 AM_VALID_HI;
extern struct amess64 AM_VALID_LO;
extern struct amess64 AM_RREADY_HI;
extern struct amess64 AM_RREADY_LO;
extern struct amess64 AM_RESET_HI;
extern struct amess64 AM_RESET_LO;
extern struct amess64 AM_READ_ESR;
extern struct amess64 AM_READ_FSR;
extern struct amess64 AM_READ_M5;

extern struct jobman jm;
extern struct asic_set_job asj1;
extern struct asic_set_job asj2;
extern struct asic_get_result agr1;
extern struct asic_get_result agr2;
extern struct raw_result_queue rrq1[];
extern struct raw_result_queue rrq2[];
extern struct asic_h64_check  ahc;
extern unsigned char sia_target_difficulty[32];
extern struct dcr_statistics  dstat;
extern struct asic_stat as;

extern unsigned long long JM_MREG[];
extern unsigned char JM_RADR[];

//SIA HASH64 Support
extern unsigned char testOneHeader[];
extern unsigned long long blake2b_IV[];
extern unsigned char blake2b_sigma[12][16];

#endif  //HASH64

extern const unsigned char chip_addr[];
extern const unsigned int  chip_data1[];
extern const unsigned int  chip_data2[];


