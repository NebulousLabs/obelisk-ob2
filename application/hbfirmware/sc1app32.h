//***********************************************************************
//**                                                                   **
//** TITLE: SC1APP32.H                                                 **
//**                                                                   **
//** Orchid Technologies Engineering and Consulting, Inc.              **
//**                                                                   **
//** Written and Developed by:                                         **
//** Orchid Technologies Engineering and Consulting, Inc.              **
//** 147 Main Street                                                   **
//** Maynard, Ma.  01754                                               **
//** TEL: 978-461-2000                                                 **
//**                                                                   **
//**                                                                   **
//** This Source Code is the property of Orchid Technologies           **
//** Engineering and Consulting, Inc.  Copyright 2003, 2004, 2005,     **
//** 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016  **
//** 2017, 2018                                                        **
//**                                                                   **
//** Orchid Technologies Engineering and Consulting, Inc. reserves all **
//** rights and title to this software.  This software shall not be    **
//** reverse engineered, copied or used in any way without the express **
//** written permission of Orchid Technologies Engineering and         **
//** Consulting, Inc.                                                  **
//**                                                                   **
//**                                                                   **
//** NOTICE: Copyright (C) 2003, 2004, 2005, 2006, 2007, 2008, 2009,   **
//** 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018              **
//**                                                                   **
//** Orchid Technologies Engineering and Consulting, Inc.              **
//** All Rights Reserved.                                              **
//**                                                                   **
//**                                                                   **
//** Revision History:                                                 **
//**                                                                   **
//** ORCHID     11/01/04    V1.0    Original                           **
//**                                                                   **
//** ORCHID     04/15/05    V1.1    Adapted for Use                    **
//**                                                                   **
//** ORCHID     05/01/05    V1.2    Adapted for Use                    **
//**                                                                   **
//** ORCHID     11/01/06    V2.0    Adapted as MEDCORE                 **
//**                                                                   **
//** ORCHID     03/01/09    V3.0    Adapted as RMCORE for HS82117      **
//**                                Platform                           **
//**                                                                   **
//** ORCHID     05/01/09    V4.0    Adapted as A7CORE for NXP2194      **
//**                                Platform                           **
//**                                                                   **
//** ORCHID     04/01/10    V5.0    Adapted as M3COR for NXP17xx       **
//**                                                                   **
//** ORCHID     01/01/11    V6.0    Adapted as S3COR for STM32C10x     **
//**                                                                   **
//** ORCHID     12/01/14    V7.0    Adapted as MG1   for STM32F40x     **
//**                                                                   **
//** ORCHID     12/01/15    V8.0    Adapted as CH1   for STM32F40x     **
//**                                                                   **
//** ORCHID     04/15/16    V9.0    Adapted as SL3   for STM32F103     **
//**                                                                   **
//** ORCHID     05/05/18    V10.0   Adapted as FC1   for STM32F103     **
//**                                                                   **
//** ORCHID     07/15/18    V11.0   Adapted as SC1   for STM32F101     **
//***********************************************************************

void submit_job(void);
void start_job_services(void);
void stop_job_services(void);
void abort_job_services(void);
void clear_nonce_count(void);
void clear_clock_deltas(void);

void start_service_asic_set_job1(void);
void service_asci_set_job1(void);
void engine_xlat1(struct amess *, unsigned int);
void engine_nonce_increment1(unsigned int, unsigned char);
unsigned int read_data32(unsigned char *);
void invalidate_ecf1(unsigned int);
unsigned int get_ecf1(unsigned int);
void set_ecf1(unsigned int, unsigned char);
void start_service_asic_get_result1(void);
void service_asci_get_result1(void);

void start_service_asic_set_job2(void);
void service_asci_set_job2(void);
void engine_xlat2(struct amess *, unsigned int);
void engine_nonce_increment2(unsigned int, unsigned char);
void invalidate_ecf2(unsigned int);
unsigned int get_ecf2(unsigned int);
void set_ecf2(unsigned int, unsigned char);
void start_service_asic_get_result2(void);
void service_asci_get_result2(void);
void start_service_ahc(void);
void service_ahc(void);

//This routine is taken from Obelisk and modified to work with more
//efficient data structures for STM32 ARM Environment
void dcrBlake256CompressBlock(unsigned int *, unsigned int *, unsigned int *, unsigned int);
unsigned char dcrMidstateMeetsMinimumTarget(unsigned int *, unsigned int *);
void dcrComputeTarget(unsigned char *target, unsigned long long difficulty);



