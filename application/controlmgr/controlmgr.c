/*
 * Copyright (C) 2012-2018 MapleLeaf Software, Inc
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * controlmgr.c
 *
 * Dan Jozwiak: djozwiak@mlsw.biz
 * Date Created: 2012-04-12
 *
 * Description: 
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>
#include <sys/time.h>

#include "common.h"
#include "list.h"
#include "logger.h"
#include "info.h"
#include "ipc.h"
#include "fan.h"
#include "update.h"
#include "hbmonitor.h"
#include "swbutton.h"
#include "led.h"
#include "utils.h"

static char *log_file;
static bool do_fork = true;
static bool verbose = false;
static struct list *client_list;
static char *info_file = STATION_INFO_FILE;
static station_config_t station_config;

/***********************************************************
 * NAME: process_signal
 * DESCRIPTION: this is a generic signal handler that can be
 * used to process any Unix signal. Currently not used.
 *
 * IN:  generated signal number
 * OUT: none
 ***********************************************************/
static void process_signal(
    int sig
)
{
    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    switch(sig)
    {
        case SIGUSR1:
            log_entry(LEVEL_INFO, "[%s] usage: %d", __func__, get_current_rss());
        break;
        case SIGUSR2:
        break;
        case SIGHUP:
            if (log_level(0) == LEVEL_INFO)
            {
                log_level(LEVEL_DEBUG);
                log_entry(LEVEL_INFO, "[%s] log level DEBUG", __func__);
            }
            else
            {
                log_level(LEVEL_INFO);
                log_entry(LEVEL_INFO, "[%s] log level INFO", __func__);
            }
        break;
        case SIGINT:
        case SIGQUIT:
        case SIGTERM:
            log_entry(LEVEL_INFO, "[%s] control manager exiting", __func__);
            swbutton_shutdown(&station_config);
            led_shutdown(&station_config);
            update_shutdown(&station_config);
            ipc_server_shutdown(&station_config);
            hbmonitor_shutdown(&station_config);
            fan_shutdown(&station_config);
            log_exit();
            exit(0);
        break;
    }
}

/***********************************************************
 * NAME: usage
 * DESCRIPTION: this function simply outputs to the console
 * the usage of the station manager.
 *
 * IN:  program name
 * OUT: none
 ***********************************************************/
static void usage(
    char *name
)
{
    printf("Usage:%s\n", name);
    printf("\t-d: don't automatically fork\n"
           "\t-l <log file>: specify full path to log file\n"
           "\t-i <info file>: specify full path to station info file\n"
          );
    exit(1);
}

/***********************************************************
 * NAME: process_args
 * DESCRIPTION: this function processes any arguments that
 * were passed to the manager on the command line. Current
 * supported arguments are as follows:
 *   -d : allows the user to tell the manager not to 
 *        automatically fork (useful for debugging)
 *   -l : allows the user to specify a log file. If no log
 *        file is specified then output will go to the
 *        console.
 *
 * IN:  argument count, argument list
 * OUT: none
 ***********************************************************/
static void process_args(
    int argc,
    char **argv
)
{
    int c = 0;

    while ((c = getopt(argc, argv, "l:i:dv")) != -1)
    {
        switch (c)
        {
            case 'i':
                info_file = optarg;
            break;
            case 'l':
                log_file = optarg;
            break;
            case 'd':
                do_fork = false;
            break;
            case 'v':
                verbose = true;
            break;
            default:
                usage(argv[0]);
            break;
        }
    }
}

/***********************************************************
 * NAME: main
 * DESCRIPTION: this is the entry point for the station
 * manager. 
 *
 * IN:  argument count, argument list
 * OUT: exit status
 ***********************************************************/
int main(int argc, char *argv[])
{
    struct sigaction accept_sa;
    struct sigaction ignore_sa;
    bool time_synced = false;

    process_args(argc, argv);

    memset(&station_config, 0, sizeof(station_config_t));

#ifdef DEBUG
    log_init(log_file, DEFAULT_LOG_LIMIT, LEVEL_DEBUG);
#else
    log_init(log_file, DEFAULT_LOG_LIMIT, LEVEL_ERROR);
#endif /* DEBUG */
    if (verbose)
        log_level(LEVEL_DEBUG);
    log_entry(LEVEL_INFO, "[%s]: control manager started", __func__);

    /* If the flag is set fork into the background */
    if (do_fork)
    {
        switch(fork())
        {
            case 0:
            break;
            case -1:
                log_entry(LEVEL_ERROR, "[%s]: unable to fork", __func__);
            default:
                exit(0);
        }
    }

    process_new_xml_defaults();
    config_init(&station_config);
    station_config.ipc_fd = -1;
    station_config.internet_connection = true; /* Assume initial Internet connection */
    info_ipaddr(&station_config.station_info);

    client_list = ipc_init();
    ipc_server_init(&station_config);

    led_init(&station_config);
    swbutton_init(&station_config);
    fan_init(&station_config);
    hbmonitor_init(&station_config);
    update_init(&station_config);

    /* Setup the signal handling for the process */
    accept_sa.sa_handler = process_signal;
    accept_sa.sa_flags = 0;
    sigemptyset(&accept_sa.sa_mask);

    ignore_sa.sa_handler = SIG_IGN;
    ignore_sa.sa_flags = 0;
    sigemptyset(&ignore_sa.sa_mask);

    sigaction(SIGUSR1, &accept_sa, NULL);
    sigaction(SIGUSR2, &accept_sa, NULL);
    sigaction(SIGHUP, &accept_sa, NULL);
    sigaction(SIGINT, &accept_sa, NULL);
    sigaction(SIGQUIT, &accept_sa, NULL);
    sigaction(SIGTERM, &accept_sa, NULL);

    sigaction(SIGPIPE, &ignore_sa, NULL);

    led_set(&station_config, LED_GREEN, RATE_NONE, false);

    while (true)
    {
        int status;
        int track_fd;
        fd_set fds;
        struct list_entry *entry;
        struct timeval timeout;
        time_t current;

        track_fd = 0;
        FD_ZERO(&fds);
        memset(&timeout, 0, sizeof(timeout));

        current = time(NULL);

        if (station_config.ipc_fd > 0)
        {
            // log_entry(LEVEL_DEBUG, "[%s] listening for clients: %d",
            //          __func__, station_config.ipc_fd);
            FD_SET(station_config.ipc_fd, &fds);
            if (station_config.ipc_fd > track_fd)
                track_fd = station_config.ipc_fd;
        }

        TAILQ_FOREACH(entry, &client_list->head, next_entry)
        {
            ipc_info_t *client = entry->data;
            if (client && (client->fd > 0))
            {
                FD_SET(client->fd, &fds);
                if (client->fd > track_fd)
                    track_fd = client->fd;
                // log_entry(LEVEL_DEBUG, "[%s] listening for %s client",
                //           __func__, (client->id == 0) ? "not registered":id_to_str(client->id));
            }
            else {
                // log_entry(LEVEL_ERROR, "[%s] invalid file descriptor: %s client",
                //           __func__, (client->id == 0) ? "not registered":id_to_str(client->id));
            }
        }

        if (time_synced && (station_config.system_info.rebootperiod > 0))
        {
            station_config.time_until_reboot = (station_config.reboot_start_time + station_config.system_info.rebootperiod) - current;
            if (station_config.time_until_reboot <= 0)
            {
                hbmonitor_shutdown(&station_config);
                system_reboot(&station_config);
            }
            timeout.tv_sec = station_config.time_until_reboot;
            // log_entry(LEVEL_DEBUG, "[%s] time remaining to reboot: %d", __func__, station_config.time_until_reboot);
        }
        else
            timeout.tv_sec = 2;

        if (station_config.button_fd > 0)
        {
            // log_entry(LEVEL_DEBUG, "[%s] listening for button presses: %d",
            //           __func__, station_config.button_fd);
            FD_SET(station_config.button_fd, &fds);
            if (station_config.button_fd > track_fd)
                track_fd = station_config.button_fd;

            if (station_config.button_state == BUTTON_PRESSED)
                timeout.tv_sec = FACTORY_RESET_TIME;
        }

        /* Wait for activity on the file descriptors or timeout if used. */
        if (timeout.tv_sec > 0)
            status = select(track_fd + 1, &fds, NULL, NULL, &timeout);
        else
            status = select(track_fd + 1, &fds, NULL, NULL, NULL);
        current = time(NULL);
        if (status < 0)
        {
            // log_entry(LEVEL_DEBUG, "[%s] select error: %s", __func__, strerror(errno));
            continue;
        }
        else if (status == 0)
        {
            // log_entry(LEVEL_DEBUG, "[%s] select timeout", __func__);
            goto process_timeout;
        }

        /* Check for connections from clients */
        if ((station_config.ipc_fd > 0) && FD_ISSET(station_config.ipc_fd, &fds))
        {
            ipc_accept_connection(&station_config);
        }

        /* Check for messages from clients */
        TAILQ_FOREACH(entry, &client_list->head, next_entry)
        {
            ipc_info_t *client = entry->data;
            if (client == NULL)
            {
                // log_entry(LEVEL_ERROR, "[%s] entry->data == NULL", __func__);
                continue;
            }

            if (FD_ISSET(client->fd, &fds))
            {
                int status = ipc_process_message(&station_config, client);
                if (status <= 0)
                {
                    close(client->fd);
                    del_entry(client_list, entry);
                }
            }
        }

        /* Check for button presses */
        if ((station_config.button_fd > 0) && FD_ISSET(station_config.button_fd, &fds))
        {
            swbutton_process_event(&station_config);
        }
        continue;

process_timeout:
        if (station_config.button_state == BUTTON_PRESSED)
            swbutton_check(&station_config);

        if (time_synced)
        {
            if ((station_config.system_info.rebootperiod > 0) &&
                (current - station_config.reboot_start_time) <= station_config.system_info.rebootperiod)
            {
                hbmonitor_shutdown(&station_config);
                system_reboot(&station_config);
            }
        }
        else
        {
            // log_entry(LEVEL_INFO, "[%s] check time sync", __func__);
            time_synced = is_time_synced();
            if (time_synced)
            {
                if (station_config.system_info.rebootperiod > 0)
                {
                    station_config.reboot_start_time = time(NULL);
                    station_config.time_until_reboot = station_config.system_info.rebootperiod;
                }
            }
            else {
                // log_entry(LEVEL_INFO, "[%s] time is not syncd", __func__);
            }
        }
    }
    log_exit();
}
