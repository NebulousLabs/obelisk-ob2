/*
 * Copyright (C) 2018 MapleLeaf Software, Inc
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * update.c
 *
 * Dan Jozwiak: djozwiak@mlsw.biz
 * Date Created: 2018-11-07
 *
 * Description: 
 */

#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <signal.h>
#include <sys/stat.h>
#include <libxml/parser.h>
#include <libxml/tree.h>
#define __USE_GNU
#include <pthread.h>

#include "common.h"
#include "logger.h"
#include "config.h"
#include "info.h"
#include "ipc.h"
#include "hbmonitor.h"
#include "update.h"

#define VERSION_TAG     "version"
#define DATE_TAG        "date"
#define RELEASE_TAG     "releaseVersion"
#define PREVIOUS_TAG    "previousVersion"

#define VERSION_INDEX_SIZE      3   /* 3 digit version format */
#define VERSION_MAJOR_INDEX     0
#define VERSION_MINOR_INDEX     1
#define VERSION_POINT_INDEX     2

#define THREAD_NAME "update-monitor"

#define MONITOR_PERIOD  (12 * 60 * 60) /* seconds */

static pthread_t update_thread_id;
static station_config_t *config;
static const char *backup_file = BACKUP_FILE;

/********************************************************
 * NAME: get_file
 * DESCRIPTION: Download file from the specified URL
 *
 * IN: URL and local file path
 * OUT: 
 *********************************************************/
static int get_file(
    char *url,
    char *local
)
{
    char cmd[512];

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    sprintf(cmd, "/usr/bin/wget -O %s %s > /dev/null 2>&1", local, url);
    return system(cmd);
}

/********************************************************
 * NAME: download_versions_file
 * DESCRIPTION: download the versions file from the server
 *
 * IN: station configuration
 * OUT: 0 on success, -1 on failure;
 *********************************************************/
static char *download_versions_file(
    station_config_t *config
)
{
    int rc;
    char *local_versions_file;
    char remote_versions_file[256];

    local_versions_file = calloc(1, strlen(UPDATES_DIR) + strlen(VERSION_FILE) + 2);
    if (local_versions_file == NULL)
    {
        log_entry(LEVEL_ERROR, "[%s] insufficient memory", __func__);
        return NULL;
    }

    /* Download the versions database to the local file system */
    sprintf(local_versions_file, "%s/%s", UPDATES_DIR, VERSION_FILE);
    sprintf(remote_versions_file, "%s/%s", config->system_info.update_url, VERSION_FILE);
    log_entry(LEVEL_DEBUG, "[%s] downloading the versions database from %s...", __func__, remote_versions_file);
    rc = get_file(remote_versions_file, local_versions_file);
    if (rc < 0)
    {
        log_entry(LEVEL_ERROR, "[%s] get_file() failed", __func__);
        free(local_versions_file);
        return NULL;
    }
    return local_versions_file;
}

static bool update_available(
    uint8_t *current,
    uint8_t *latest
)
{
    if      (latest[VERSION_MAJOR_INDEX] > current[VERSION_MAJOR_INDEX])
        return true;
    else if (latest[VERSION_MINOR_INDEX] > current[VERSION_MINOR_INDEX])
        return true;
    else if (latest[VERSION_POINT_INDEX] > current[VERSION_POINT_INDEX])
        return true;
    return false;
}

/***********************************************************
 * NAME: convert_version()
 * DESCRIPTION: Get the version values (in decimal) from
 * the version number string.
 *
 * IN:  version string
 *      array to store version values
 * OUT: 0 on success or -1 for error
 ***********************************************************/
static int convert_version(
    const char *version_string,
    uint8_t version_value[]
)
{
    char buf[256];
    char *pch;
    int i = 0;

    strcpy(buf, version_string);

    pch = strtok(buf, ".");
    while (pch != NULL)
    {
        version_value[i] = atoi(pch);
        pch = strtok (NULL, ".");
        i++;
    }

    /* Make sure we parsed the string correctly... */
    if (i != VERSION_INDEX_SIZE)    
        return -1;

    return 0;
}

/***********************************************************
 * NAME: get_latest_release
 * DESCRIPTION: this function opens the versions XML
 * file and reads its contents to find the latest release
 * available.
 *
 * IN:  versions file
 * OUT: none
 ***********************************************************/
static char *get_latest_release(
    char *versions_file
)
{
    int fd;
    xmlDocPtr doc = NULL;
    xmlNodePtr rootElement;
    xmlNodePtr current;
    char *rval = NULL;

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    /* init XML library parser */
    xmlInitParser();

    /* weird but needed by client code for libxml */
    LIBXML_TEST_VERSION

    /* Allow formatting of the XML file */
    xmlKeepBlanksDefault(0);

    /* Open and lock the XML file */
    fd = open(versions_file, O_RDWR);
    if (fd < 0)
    {
        log_entry(LEVEL_ERROR, "[%s] unable to open %s", __func__, versions_file);
        return NULL;
    }

    /* Load the specified XML file */
    doc = xmlReadFile(versions_file, NULL, XML_PARSE_RECOVER); 
    if (!doc)
    {
        log_entry(LEVEL_ERROR, "[%s] xmlReadFile error for %s", __func__, versions_file);
        close(fd);
        return NULL;
    }

    /* Get the root element */
    rootElement = xmlDocGetRootElement(doc);
    for (current = rootElement->children; current != NULL; current = current->next)
    {
        if (strcmp((char *)current->name, VERSION_TAG) == 0)
        {
            xmlNodePtr entry;
            for (entry = current->children; entry != NULL; entry = entry->next)
            {
                xmlChar *value;

                value = xmlNodeGetContent(entry);
                if (strcmp((char *)entry->name, RELEASE_TAG) == 0)
                {
                    rval = (char *)value;
                    goto out;
                }
                if (value)
                    xmlFree(value);
            }
        }
    }
out:
    xmlFreeDoc(doc);
    close(fd);
    return rval;
}

/***********************************************************
 * NAME: get_previous_release
 * DESCRIPTION: this function opens the versions XML
 * file and reads its contents to find the previous release
 * associated with the current release
 *
 * IN:  versions file, current release
 * OUT: none
 ***********************************************************/
static char *get_previous_release(
    char *versions_file,
    char *current_release
)
{
    int fd;
    xmlDocPtr doc = NULL;
    xmlNodePtr rootElement;
    xmlNodePtr current;
    char *rval = NULL;

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    /* init XML library parser */
    xmlInitParser();

    /* weird but needed by client code for libxml */
    LIBXML_TEST_VERSION

    /* Allow formatting of the XML file */
    xmlKeepBlanksDefault(0);

    /* Open and lock the XML file */
    fd = open(versions_file, O_RDWR);
    if (fd < 0)
    {
        log_entry(LEVEL_ERROR, "[%s] unable to open %s", __func__, versions_file);
        return NULL;
    }

    /* Load the specified XML file */
    doc = xmlReadFile(versions_file, NULL, XML_PARSE_RECOVER); 
    if (!doc)
    {
        log_entry(LEVEL_ERROR, "[%s] xmlReadFile error for %s", __func__, versions_file);
        close(fd);
        return NULL;
    }

    /* Get the root element */
    rootElement = xmlDocGetRootElement(doc);
    for (current = rootElement->children; current != NULL; current = current->next)
    {
        if (strcmp((char *)current->name, VERSION_TAG) == 0)
        {
            xmlNodePtr entry;
            for (entry = current->children; entry != NULL; entry = entry->next)
            {
                xmlChar *value;

                if (strcmp((char *)entry->name, RELEASE_TAG) == 0)
                {
                    value = xmlNodeGetContent(entry);
                    if (strcmp((char *)value, current_release) == 0)
                    {
                        xmlNodePtr tag;

                        xmlFree(value);
                        for (tag = entry->next; tag != NULL; tag = tag->next)
                        {
                            value = xmlNodeGetContent(tag);
                            if (strcmp((char *)tag->name, PREVIOUS_TAG) == 0)
                            {
                                rval = (char *)value;
                                goto out;
                            }
                        }
                    }
                    else
                    {
                        xmlFree(value);
                        goto next_tag;
                    }
                }
            }
        }
next_tag:
        continue;
    }
out:
    xmlFreeDoc(doc);
    close(fd);
    return rval;
}

/***********************************************************
 * NAME: process_update_signal
 * DESCRIPTION: 
 *
 * IN:  generated signal number
 * OUT: none
 ***********************************************************/
static void process_update_signal(int sig)
{
    switch(sig)
    {
        case SIGTERM:
            log_entry(LEVEL_INFO, "[%s] %s thread exiting...", __func__, THREAD_NAME);
        break;
    }
}

/***********************************************************
 * NAME: update_monitor_thread
 * DESCRIPTION: 
 *
 * IN:  data passed during thread creation
 * OUT: none
 ***********************************************************/
static void *update_monitor_thread(
    void *data
)
{
    sigset_t mask;
    sigset_t orig_mask;
    struct sigaction act;

    log_entry(LEVEL_DEBUG, "[%s] %s thread started", __func__, THREAD_NAME);

    pthread_setname_np(pthread_self(), THREAD_NAME);

    config = data;
    memset (&act, 0, sizeof(act));
    act.sa_handler = process_update_signal;
    /* This thread should shut down on SIGTERM. */
    if (sigaction(SIGTERM, &act, NULL))
        log_entry(LEVEL_ERROR, "[%s] sigaction failed: %s", __func__, strerror(errno));

    sigemptyset(&mask);
    sigaddset(&mask, SIGTERM);

    if (sigprocmask(SIG_BLOCK, &mask, &orig_mask) < 0)
        log_entry(LEVEL_ERROR, "[%s] sigprocmask failed: %s", __func__, strerror(errno));

    while (true)
    {
        int rc;

        rc = update_check(config, false);
        if ((rc < 0) || (strlen(config->system_info.updateto) > 0))
            sleep(60);
        else
            sleep(MONITOR_PERIOD);
    }
    return NULL;
}

int update_check(
    station_config_t *config,
    bool autoupdate
)
{
    char *local_versions_file;
    char *latest_release = NULL;
    int rval = 0;

    /*
     * First, see if we are in the middle of an update cycle. If so,
     * just kick off the update.
     */
    if (strlen(config->system_info.updateto) > 0)
    {
        if (strcmp(config->latest_release, config->system_info.updateto) != 0)
        {
            log_entry(LEVEL_INFO, "[%s] doing next update", __func__);
            hbmonitor_shutdown(config);
            sleep(2);
            ipc_start_swupdate(config->system_info.update_url);
            return 0;
        }
        else
        {
            /*
             * System is at the latest release...clear the updateto
             * release and return to normal update checking.
             */
            log_entry(LEVEL_INFO, "[%s] all updates complete", __func__);
            strcpy(config->system_info.updateto, "");
            info_system_update(SYSTEM_INFO_FILE, &(config->system_info));
        }
    }

    local_versions_file = download_versions_file(config);
    if (local_versions_file == NULL)
    {
        log_entry(LEVEL_ERROR, "[%s] failed to downloads versions file", __func__);
        return -1;
    }

    latest_release = get_latest_release(local_versions_file);
    if (latest_release)
    {
        int rc;
        uint8_t current_version_info[VERSION_INDEX_SIZE];
        uint8_t latest_version_info[VERSION_INDEX_SIZE];

        log_entry(LEVEL_INFO, "[%s] latest release found: %s", __func__, latest_release);

        rc = convert_version(&config->station_info.release[1], current_version_info);
        if (rc < 0)
        {
            log_entry(LEVEL_ERROR, "[%s] converting current version: %s", __func__, config->station_info.release);
            free(latest_release);
            goto out;
        }
        rc = convert_version(&latest_release[1], latest_version_info);
        if (rc < 0)
        {
            log_entry(LEVEL_ERROR, "[%s] converting current version: %s", __func__, latest_release);
            free(latest_release);
            goto out;
        }

        if (update_available(current_version_info, latest_version_info))
        {
            log_entry(LEVEL_INFO, "[%s] update available - current: %s, latest: %s", __func__, config->station_info.release, latest_release);

            if (strcmp(config->latest_release, latest_release) != 0)
            {
                strcpy(config->latest_release, latest_release);
                if (autoupdate)
                {
                    log_entry(LEVEL_INFO, "[%s] autoupdate started", __func__);
                    strcpy(config->system_info.updateto, latest_release);
                    info_system_update(SYSTEM_INFO_FILE, &(config->system_info));
                    hbmonitor_shutdown(config);
                    sleep(2);
                    ipc_start_swupdate(config->system_info.update_url);
                }
                else
                    ipc_notify_swupdate(latest_release, config->previous_release);
            }
        }
        else
            log_entry(LEVEL_INFO, "[%s] release check complete...no update required", __func__);

        free(latest_release);
    }
    else
    {
        log_entry(LEVEL_ERROR, "[%s] latest release not found", __func__);
        rval = -1;
    }
out:
    free(local_versions_file);
    return rval;
}

void update_init(
    station_config_t *config
)
{
    int rc;
    char *previous_release;
    char local_versions_file[256];

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    strcpy(config->latest_release, config->station_info.release);
    sprintf(local_versions_file, "%s/%s", UPDATES_DIR, VERSION_FILE);
    previous_release = get_previous_release(local_versions_file, config->station_info.release);
    /*
     * r1.0.0 and r1.0.1 do not qualify as previous releases for
     * rollback so leave the previous release NULL if either of these
     * show up.
     */
    if (previous_release &&
        (strcmp(previous_release, "r1.0.0") != 0) &&
        (strcmp(previous_release, "r1.0.1") != 0))
    {
        int rc;
        struct stat info;

        /*
         * Also, since the previous_release is used by the apiserver to
         * determine whether or not to display the rollback button, we
         * need to determine if the previous release from the versions
         * file is one that we were at. We could have started at the
         * current release (via an image programmed to SD card and so
         * did not actually upgrade from the previous release. If we
         * didn't actually upgrade then we certainly can't rollback.
         * The only way we have to determine this is to check for the
         * existence of a backup file. If present we can only assume
         * that it was the result of an upgrade, hopefully from the
         * previously release.
         */
        rc = stat(backup_file, &info);
        if (rc < 0)
            log_entry(LEVEL_WARNING, "[%s] backup file for %s does not exist...no rollback option", __func__, previous_release);
        else
            strcpy(config->previous_release, previous_release);
    }

    rc = pthread_create(&update_thread_id, NULL, update_monitor_thread, config);
    if (rc < 0)
        log_entry(LEVEL_ERROR, "[%s] failed to create thread: %s", __func__, strerror(errno));
        
}

void update_shutdown(
    station_config_t *config
)
{
    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    pthread_kill(update_thread_id, SIGTERM);
}
