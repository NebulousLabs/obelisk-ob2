/*
 * Copyright (C) 2016-2018 MapleLeaf Software, Inc
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * ipc.c
 *
 * Dan Jozwiak: djozwiak@mlsw.biz
 * Date Created: 2016-11-27
 *
 * Description: 
 *
 */

#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/mount.h>
#include <arpa/inet.h>
#define __USE_GNU
#include <pthread.h>

#include "common.h"
#include "logger.h"
#include "list.h"
#include "info.h"
#include "hb.h"
#include "ipc_messages.h"
#include "ports.h"
#include "ipc.h"
#include "config.h"
#include "job.h"
#include "fan.h"
#include "led.h"
#include "hbmonitor.h"
#include "update.h"
#include "utils.h"

static struct list client_list;

static bool update_available = false;
static bool update_inprogress = false;

static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

static ipc_info_t *find_client(
    ipc_id_t id
)
{
    struct list_entry *entry;

    TAILQ_FOREACH(entry, &client_list.head, next_entry)
    {
        ipc_info_t *client = entry->data;

        if (client && (client->id == id))
        {
            return client;
            break;
        }
    }
    return NULL;
}

static hb_node_t *find_hb(
    station_config_t *config,
    uint8_t id
)
{
    struct list_entry *entry;

    TAILQ_FOREACH(entry, &config->hb_list->head, next_entry)
    {
        hb_node_t *hb = entry->data;

        if (hb && (hb->id == id))
        {
            return hb;
            break;
        }
    }
    return NULL;
}

static void send_ipc_msg(
    ipc_info_t *client,
    msg_ipc_tlv_t *msg
)
{
    if (client && (client->fd > 0))
    {
        pthread_mutex_lock(&mutex);
        if (msg)
        {
            int bytes = send(client->fd, msg, sizeof(msg_ipc_tlv_t) + msg->len, MSG_NOSIGNAL);
            if (bytes < 0)
                log_entry(LEVEL_ERROR, "[%s] send failed: %s", __func__, strerror(errno));
            else if (bytes == 0)
                log_entry(LEVEL_ERROR, "[%s] sent 0 bytes: %s", __func__, strerror(errno));
            free(msg);
        }
        else
            log_entry(LEVEL_ERROR, "[%s] msg is NULL", __func__);
        pthread_mutex_unlock(&mutex);
    }
    else
        log_entry(LEVEL_ERROR, "[%s] client is NULL or invalid file descriptor", __func__);
}

static void update_cgm_stats(
    station_config_t *config,
    tlv_ipc_cgminer_stats_t *stats
)
{
    int i;
    pool_stats_t *lstats = NULL;

    /* First find the corresponding pool entry */
    for (i = 0; i < MAX_POOLS; ++i)
    {
        if (config->pool_stats[i].pool_index == stats->pool_index)
        {
            lstats = &config->pool_stats[i];
            break;
        }
    }

    if (lstats == NULL)
    {
        /* Find the next available slot to use to store stats */
        for (i = 0; i < MAX_POOLS; ++i)
        {
            if (config->pool_stats[i].pool_index == DEFAULT_POOL_INDEX)
            {
                lstats = &config->pool_stats[i];
                break;
            }
        }
    }

    if (lstats)
    {
        lstats->pool_index = stats->pool_index;
        lstats->accepted = stats->accepted;
        lstats->rejected = stats->rejected;
        lstats->status = stats->status;
        lstats->idle = stats->idle;
    }
}

static msg_ipc_tlv_t *build_hb_info(
    station_config_t *config
)
{
    void *msg = NULL;
    size_t msg_len = sizeof(msg_ipc_tlv_t) +
                     sizeof(tlv_ipc_hb_info_t) +
                     (config->hb_list->size * sizeof(tlv_hb_info_t));

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    msg = calloc(1, msg_len);
    if (msg)
    {
        msg_ipc_tlv_t *tlv = msg;
        tlv_ipc_hb_info_t *info = (tlv_ipc_hb_info_t *)tlv->value;
        tlv_hb_info_t *hbinfo = info->hbinfo;
        int count = 0;
        struct list_entry *entry;

        tlv->type = ATTR_TYPE_IPC_HB_INFO;
        tlv->len = msg_len - sizeof(msg_ipc_tlv_t);

        info->count = config->hb_list->size;
        TAILQ_FOREACH(entry, &config->hb_list->head, next_entry)
        {
            hb_node_t *hb = entry->data;
    
            if (hb)
            {
                hbinfo[count].type = hb->type;
                hbinfo[count].id = hb->id;
                strcpy(hbinfo[count].devnode, hb->devnode);
                strcpy(hbinfo[count].version, hb->version);
                if (hb->asic_diags_valid)
                    memcpy(hbinfo[count].asic_diags, hb->asic_diags, MAX_ASICS_PER_HASHING_BOARD);
                ++count;
            }
        }
    }

    return msg;
}

static msg_ipc_tlv_t *build_notification_measurement_info(
    hb_node_t *hb,
    uint8_t type
)
{
    void *msg = NULL;
    size_t msg_len = sizeof(msg_ipc_tlv_t) +
                     sizeof(tlv_ipc_notification_t) +
                     sizeof(tlv_ipc_notification_hb_measurement_t);

    // log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    msg = calloc(1, msg_len);
    if (msg)
    {
        msg_ipc_tlv_t *tlv = msg;
        tlv_ipc_notification_t *notification = (tlv_ipc_notification_t *)tlv->value;
        tlv_ipc_notification_hb_measurement_t *info = (tlv_ipc_notification_hb_measurement_t *)notification->value;

        tlv->type = ATTR_TYPE_IPC_CHANGE_NOTIFICATION;
        tlv->len = msg_len - sizeof(msg_ipc_tlv_t);
        notification->type = type;
        info->hb_id = hb->id;
        switch (type)
        {
            case NOTIFICATION_HB_TEMP1:
                info->value = hb->temp1;
            break;
            case NOTIFICATION_HB_TEMP2:
                info->value = hb->temp2;
            break;
            case NOTIFICATION_HB_VOLTAGE:
                info->value = hb->voltage;
            break;
            case NOTIFICATION_HB_CURRENT:
                info->value = hb->current;
            break;
            default:
                free(msg);
                return NULL;
            break;
        }
    }

    return msg;
}

static msg_ipc_tlv_t *build_notification_hb_stats(
    hb_node_t *hb
)
{
    void *msg = NULL;
    size_t msg_len = sizeof(msg_ipc_tlv_t) +
                     sizeof(tlv_ipc_notification_t) +
                     sizeof(tlv_ipc_notification_hb_stats_t);

    // log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    msg = calloc(1, msg_len);
    if (msg)
    {
        msg_ipc_tlv_t *tlv = msg;
        tlv_ipc_notification_t *notification = (tlv_ipc_notification_t *)tlv->value;
        tlv_ipc_notification_hb_stats_t *info = (tlv_ipc_notification_hb_stats_t *)notification->value;

        tlv->type = ATTR_TYPE_IPC_CHANGE_NOTIFICATION;
        tlv->len = msg_len - sizeof(msg_ipc_tlv_t);
        notification->type = NOTIFICATION_HB_STATS;
        info->hb_id = hb->id;
        info->relative_voltage = hb->relative_voltage;
        info->noise_counter = hb->noise_counter;
        info->watchdog_counter = hb->watchdog_counter;
#if 1 /* SIA multiplier isn't working so do this for all for now */
        info->hashes = (uint64_t)(hb->job_info.good_nonces * DCR_HASH_MULT);
#else
        switch (hb->type)
        {
            case HB_DCR:
                info->hashes = (uint64_t)(hb->job_info.good_nonces * DCR_HASH_MULT);
            break;
            case HB_SIA:
                info->hashes = (uint64_t)(hb->job_info.good_nonces * SIA_HASH_MULT);
            break;
            default:
                log_entry(LEVEL_ERROR, "[%s] unknown board type: %d", __func__, hb->type);
                free(msg);
                return NULL;
            break;
        }
#endif
    }

    return msg;
}

static msg_ipc_tlv_t *build_notification_cgm_stats(
    tlv_ipc_cgminer_stats_t *stats
)
{
    void *msg = NULL;
    size_t msg_len = sizeof(msg_ipc_tlv_t) +
                     sizeof(tlv_ipc_notification_t) +
                     sizeof(tlv_ipc_cgminer_stats_t);

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    msg = calloc(1, msg_len);
    if (msg)
    {
        msg_ipc_tlv_t *tlv = msg;
        tlv_ipc_notification_t *notification = (tlv_ipc_notification_t *)tlv->value;
        tlv_ipc_cgminer_stats_t *info = (tlv_ipc_cgminer_stats_t *)notification->value;

        tlv->type = ATTR_TYPE_IPC_CHANGE_NOTIFICATION;
        tlv->len = msg_len - sizeof(msg_ipc_tlv_t);
        notification->type = NOTIFICATION_CGM_STATS;
        memcpy(info, stats, sizeof(tlv_ipc_cgminer_stats_t));
    }

    return msg;
}

static msg_ipc_tlv_t *build_notification_swupdate(
    char *latest_release,
    char *previous_release
)
{
    void *msg = NULL;
    size_t msg_len = sizeof(msg_ipc_tlv_t) +
                     sizeof(tlv_ipc_notification_t) +
                     sizeof(tlv_ipc_notification_swupdate_t);

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    msg = calloc(1, msg_len);
    if (msg)
    {
        msg_ipc_tlv_t *tlv = msg;
        tlv_ipc_notification_t *notification = (tlv_ipc_notification_t *)tlv->value;
        tlv_ipc_notification_swupdate_t *info = (tlv_ipc_notification_swupdate_t *)notification->value;

        tlv->type = ATTR_TYPE_IPC_CHANGE_NOTIFICATION;
        tlv->len = msg_len - sizeof(msg_ipc_tlv_t);
        notification->type = NOTIFICATION_SWUPDATE;
        strcpy(info->latest_release, latest_release);
        strcpy(info->previous_release, previous_release);
        log_entry(LEVEL_INFO, "[%s] latest: %s, previous: %s",
                  __func__,
                  (latest_release != NULL) ? latest_release:"NONE",
                  (previous_release != NULL) ? previous_release:"NONE");
    }

    return msg;
}

static msg_ipc_tlv_t *build_notification_hb_performance_info(
    hb_node_t *hb,
    uint8_t type
)
{
    void *msg = NULL;
    size_t msg_len = sizeof(msg_ipc_tlv_t) +
                     sizeof(tlv_ipc_notification_t) +
                     sizeof(tlv_ipc_notification_hb_performance_t);

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    msg = calloc(1, msg_len);
    if (msg)
    {
        msg_ipc_tlv_t *tlv = msg;
        tlv_ipc_notification_t *notification = (tlv_ipc_notification_t *)tlv->value;
        tlv_ipc_notification_hb_performance_t *info = (tlv_ipc_notification_hb_performance_t *)notification->value;

        tlv->type = ATTR_TYPE_IPC_CHANGE_NOTIFICATION;
        tlv->len = msg_len - sizeof(msg_ipc_tlv_t);
        notification->type = type;
        info->hb_id = hb->id;
#ifdef SUPPORT_TUNING
        if (hb->tuned)
            info->tuning = false;
        else
            info->tuning = true;
#else
        info->tuning = false;
#endif /* SUPPORT_TUNING */
        info->performance = hb->current_performance;
        info->divider = hb->clock_divider;
        info->bias = hb->clock_bias;
        memcpy(info->bias_offsets, hb->bias_offsets, sizeof(hb->bias_offsets));
    }

    return msg;
}


static msg_ipc_tlv_t *build_notification_pool_difficulty(
    float difficulty
)
{
    void *msg = NULL;
    size_t msg_len = sizeof(msg_ipc_tlv_t) +
                     sizeof(tlv_ipc_notification_t) +
                     sizeof(tlv_ipc_notification_pool_difficulty_t);

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    msg = calloc(1, msg_len);
    if (msg)
    {
        msg_ipc_tlv_t *tlv = msg;
        tlv_ipc_notification_t *notification = (tlv_ipc_notification_t *)tlv->value;
        tlv_ipc_notification_pool_difficulty_t *info = (tlv_ipc_notification_pool_difficulty_t *)notification->value;

        tlv->type = ATTR_TYPE_IPC_CHANGE_NOTIFICATION;
        tlv->len = msg_len - sizeof(msg_ipc_tlv_t);
        notification->type = NOTIFICATION_POOL_DIFF;
        info->value = difficulty;
    }

    return msg;
}

static msg_ipc_tlv_t *build_notification_burnin_status(
    burnin_status_t bistatus,
    void *bidata
)
{
    void *msg = NULL;
    size_t msg_len;
    tlv_ipc_burnin_start_t *start_data = NULL;
    tlv_ipc_burnin_status_t *status_data = NULL;

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    switch (bistatus)
    {
        case BI_INPROGRESS:
            start_data = bidata;
        break;
        case BI_FAILED:
        case BI_PASSED:
            status_data = bidata;
        break;
        default:
        break;
    }

    if ((bistatus == BI_INACTIVE) || (bistatus == BI_INPROGRESS))
        msg_len = sizeof(msg_ipc_tlv_t) +
                         sizeof(tlv_ipc_notification_t) +
                         sizeof(tlv_ipc_notification_burnin_status_t);
    else
        msg_len = sizeof(msg_ipc_tlv_t) +
                         sizeof(tlv_ipc_notification_t) +
                         sizeof(tlv_ipc_notification_burnin_status_t) +
                         (sizeof(tlv_burnin_info_t) * status_data->total_hashing_boards);
        
    msg = calloc(1, msg_len);
    if (msg)
    {
        msg_ipc_tlv_t *tlv = msg;
        tlv_ipc_notification_t *notification = (tlv_ipc_notification_t *)tlv->value;
        tlv_ipc_notification_burnin_status_t *status = (tlv_ipc_notification_burnin_status_t *)notification->value;
        tlv_burnin_info_t *info = status->info;

        tlv->type = ATTR_TYPE_IPC_CHANGE_NOTIFICATION;
        tlv->len = msg_len - sizeof(msg_ipc_tlv_t);
        notification->type = NOTIFICATION_BI_STATUS;
        if (bidata == NULL)
            status->miner_status = bistatus;
        else if (bistatus == BI_INPROGRESS)
        {
            status->miner_status = bistatus;
            status->total_hashing_boards = start_data->total_hashing_boards;
        }
        else
        {
            status->miner_status = status_data->miner_status;
            status->total_hashing_boards = status_data->total_hashing_boards;
            memcpy(info, status_data->info, sizeof(tlv_burnin_info_t) * status_data->total_hashing_boards);
        }
    }

    return msg;
}

static msg_ipc_tlv_t *build_start_swupdate(
    char *url
)
{
    void *msg = NULL;
    size_t msg_len = sizeof(msg_ipc_tlv_t) +
                     sizeof(tlv_ipc_update_request_t);

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    msg = calloc(1, msg_len);
    if (msg)
    {
        msg_ipc_tlv_t *tlv = msg;
        tlv_ipc_update_request_t *info = (tlv_ipc_update_request_t *)tlv->value;

        tlv->type = ATTR_TYPE_IPC_UPDATE;
        tlv->len = sizeof(tlv_ipc_update_request_t);
        strcpy(info->url, url);
    }

    return msg;
}

static msg_ipc_tlv_t *build_dcr_nonce(
    hb_dcr_nonce_t *nonce_info
)
{
    void *msg = NULL;
    size_t msg_len = sizeof(msg_ipc_tlv_t) +
                     sizeof(tlv_ipc_cgminer_dcr_nonce_t);

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    msg = calloc(1, msg_len);
    if (msg)
    {
        msg_ipc_tlv_t *tlv = msg;
        tlv_ipc_cgminer_dcr_nonce_t *info = (tlv_ipc_cgminer_dcr_nonce_t *)tlv->value;

        tlv->type = ATTR_TYPE_IPC_DCR_NONCE;
        tlv->len = sizeof(tlv_ipc_cgminer_dcr_nonce_t);

        info->job_id = nonce_info->job;
        info->nonce = nonce_info->nonce,
        info->m5 = nonce_info->m5;
    }

    return msg;
}

static msg_ipc_tlv_t *build_sia_nonce(
    hb_sia_nonce_t *nonce_info
)
{
    void *msg = NULL;
    size_t msg_len = sizeof(msg_ipc_tlv_t) +
                     sizeof(tlv_ipc_cgminer_sia_nonce_t);

    // log_entry(LEVEL_DEBUG, "[%s] called: %016llx", __func__, nonce_info->nonce);

    msg = calloc(1, msg_len);
    if (msg)
    {
        msg_ipc_tlv_t *tlv = msg;
        tlv_ipc_cgminer_sia_nonce_t *info = (tlv_ipc_cgminer_sia_nonce_t *)tlv->value;

        tlv->type = ATTR_TYPE_IPC_SIA_NONCE;
        tlv->len = sizeof(tlv_ipc_cgminer_sia_nonce_t);

        info->job_id = nonce_info->job;
        info->nonce = nonce_info->nonce;
    }

    return msg;
}
 static msg_ipc_tlv_t *build_job_complete(
    uint8_t job_id
)
{
    void *msg = NULL;
    size_t msg_len = sizeof(msg_ipc_tlv_t) +
                     sizeof(tlv_ipc_cgminer_job_t);

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    msg = calloc(1, msg_len);
    if (msg)
    {
        msg_ipc_tlv_t *tlv = msg;
        tlv_ipc_cgminer_job_t *info = (tlv_ipc_cgminer_job_t *)tlv->value;

        tlv->type = ATTR_TYPE_IPC_JOB_COMPLETE;
        tlv->len = sizeof(tlv_ipc_cgminer_job_t);

        info->job_id = job_id;
    }

    return msg;
}

static msg_ipc_tlv_t *build_work_fail(
    uint8_t job_id
)
{
    void *msg = NULL;
    size_t msg_len = sizeof(msg_ipc_tlv_t) +
                     sizeof(tlv_ipc_cgminer_job_t);

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    msg = calloc(1, msg_len);
    if (msg)
    {
        msg_ipc_tlv_t *tlv = msg;
        tlv_ipc_cgminer_job_t *info = (tlv_ipc_cgminer_job_t *)tlv->value;

        tlv->type = ATTR_TYPE_IPC_WORK_FAIL;
        tlv->len = sizeof(tlv_ipc_cgminer_job_t);

        info->job_id = job_id;
    }

    return msg;
}

static msg_ipc_tlv_t *build_reset(void)
{
    void *msg = NULL;
    size_t msg_len = sizeof(msg_ipc_tlv_t);

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    msg = calloc(1, msg_len);
    if (msg)
    {
        msg_ipc_tlv_t *tlv = msg;

        tlv->type = ATTR_TYPE_IPC_RESTART_CGM;
        tlv->len = 0;
    }

    return msg;
}

static void build_dcr_request(
    tlv_ipc_cgminer_dcr_work_req_t *in,
    hb_dcr_work_req_t *out
)
{
    int i;
    int j;

    out->job_id = in->job_id;

    /* Copy pool difficulty */
    out->difficulty = in->difficulty;

    /* Copy in the V-registers... */
    for (i = 0, j = HB_DCR_V_REG_START; i < DCR_NUM_VREGS; ++i, ++j)
        out->registers[j] = in->vreg[i];
    
    /* ...then the M-registers... */
    for (i = 0, j = HB_DCR_M_REG_START; i < DCR_NUM_MREGS; ++i, ++j)
    {
        if (i == 3)
        {
            --j;
            continue;
        }
#if 0
        if (i == 5)
            out->registers[j] = 0;
        else
#endif
            out->registers[j] = in->mreg[i];

    }

    /* ...and finally set the remaining DCR registers */
    out->registers[HB_DCR_LIM_REG] = 0x38;
    out->registers[HB_DCR_MATCH_REG] = in->vreg[7];
    out->registers[HB_DCR_LBR_REG] = 0x00000000;
    out->registers[HB_DCR_UBR_REG] = 0xFFFFFFFF;
}

static void ipc_notify_cgm_dcr_stats(
    station_config_t *config,
    tlv_ipc_cgminer_stats_t *stats
)
{
    struct list_entry *entry;

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    update_cgm_stats(config, stats);

    TAILQ_FOREACH(entry, &client_list.head, next_entry)
    {
        ipc_info_t *client = entry->data;
        if (client && (client->notifications & NOTIFICATION_CGM_STATS))
        {
            msg_ipc_tlv_t *msg = build_notification_cgm_stats(stats);
            if (msg)
                send_ipc_msg(client, msg);
            else
                log_entry(LEVEL_ERROR, "[%s] failed to build CGM stats message", __func__);
        }
    }
}

static void build_sia_request(
    tlv_ipc_cgminer_sia_work_req_t *in,
    hb_sia_work_req_t *out
)
{
    out->job_id = in->job_id;

    /* Copy pool difficulty */
    out->difficulty = in->difficulty;

    /* Copy in the M-registers... */
    memcpy(out->registers, in->mreg, sizeof(uint64_t) * SIA_NUM_MREGS);
}

static int process_ipc_tlv(
    station_config_t *config,
    ipc_info_t *client,
    msg_ipc_tlv_t *tlv
)
{
    msg_ipc_tlv_t *rsp = NULL;

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    switch (tlv->type)
    {
        case ATTR_TYPE_IPC_IDENTITY:
        {
            log_entry(LEVEL_DEBUG, "[%s] received client identity", __func__);
            tlv_ipc_identity_t *identity = (tlv_ipc_identity_t *)tlv->value;
            client->id = identity->id;
        }
        break;

        case ATTR_TYPE_IPC_REGISTER_NOTIFICATION:
        {
            log_entry(LEVEL_DEBUG, "[%s] received client %s notification registration", __func__, id_to_str(client->id));
            tlv_ipc_notify_registration_t *info = (tlv_ipc_notify_registration_t *)tlv->value;
            client->notifications |= info->notifications;
            ipc_notify_current(config, client);
        }
        break;

        case ATTR_TYPE_IPC_UNREGISTER_NOTIFICATION:
        {
            log_entry(LEVEL_DEBUG, "[%s] received client %s notification unregistration", __func__, id_to_str(client->id));
            tlv_ipc_notify_registration_t *info = (tlv_ipc_notify_registration_t *)tlv->value;
            client->notifications &= ~(info->notifications);
        }
        break;

        case ATTR_TYPE_IPC_HB_INFO:
        {
            log_entry(LEVEL_DEBUG, "[%s] received client %s HB info request", __func__, id_to_str(client->id));
            rsp = build_hb_info(config);
        }
        break;

        case ATTR_TYPE_IPC_RUN_DCR_TEST_JOB:
        {
            log_entry(LEVEL_DEBUG, "[%s] received client %s run DCR test job request", __func__, id_to_str(client->id));
            job_dcr_test(config);
        }
        break;

        case ATTR_TYPE_IPC_ABORT_DCR_TEST_JOB:
        {
            log_entry(LEVEL_DEBUG, "[%s] received client %s abort DCR test job request", __func__, id_to_str(client->id));
            job_dcr_stop(config);
        }
        break;

        case ATTR_TYPE_IPC_DCR_POOL_DIFFICULTY:
        {
            tlv_ipc_cgminer_difficulty_t *info = (tlv_ipc_cgminer_difficulty_t *)tlv->value;

            log_entry(LEVEL_DEBUG, "[%s] received client %s DCR pool difficulty request", __func__, id_to_str(client->id));
            config->pool_difficulty = info->difficulty;
            job_dcr_difficulty_set(config, info->difficulty);
            ipc_notify_pool_difficulty(info->difficulty);
        }
        break;

        case ATTR_TYPE_IPC_DCR_WORK_REQ:
        {
            tlv_ipc_cgminer_dcr_work_req_t *ipc_request = (tlv_ipc_cgminer_dcr_work_req_t *)tlv->value;
            hb_dcr_work_req_t dcr_request;

            log_entry(LEVEL_DEBUG, "[%s] received client %s DCR job request", __func__, id_to_str(client->id));
            build_dcr_request(ipc_request, &dcr_request);
            if (ipc_request->clean)
                job_dcr_stop(config);
            job_dcr_program_request(config, &dcr_request);
        }
        break;

        case ATTR_TYPE_IPC_DCR_HASH_LIMIT:
        {
            tlv_ipc_cgminer_dcr_hash_limit_t *info = (tlv_ipc_cgminer_dcr_hash_limit_t *)tlv->value;

            // log_entry(LEVEL_DEBUG, "[%s] received client %s DCR hash limit request", __func__, id_to_str(client->id));
            job_dcr_hash_limit_set(config, info->limit);
        }
        break;

        case ATTR_TYPE_IPC_RUN_SIA_TEST_JOB:
        {
            // log_entry(LEVEL_DEBUG, "[%s] received client %s run SIA test job request", __func__, id_to_str(client->id));
            job_sia_test(config);
        }
        break;

        case ATTR_TYPE_IPC_ABORT_SIA_TEST_JOB:
        {
            // log_entry(LEVEL_DEBUG, "[%s] received client %s abort SIA test job request", __func__, id_to_str(client->id));
            job_sia_stop(config);
        }
        break;

        case ATTR_TYPE_IPC_SIA_POOL_DIFFICULTY:
        {
            tlv_ipc_cgminer_difficulty_t *info = (tlv_ipc_cgminer_difficulty_t *)tlv->value;

            // log_entry(LEVEL_DEBUG, "[%s] received client %s SIA pool difficulty request", __func__, id_to_str(client->id));
            job_sia_difficulty_set(config, info->difficulty);
            ipc_notify_pool_difficulty(info->difficulty);
        }
        break;

        case ATTR_TYPE_IPC_SIA_WORK_REQ:
        {
            tlv_ipc_cgminer_sia_work_req_t *ipc_request = (tlv_ipc_cgminer_sia_work_req_t *)tlv->value;
            hb_sia_work_req_t sia_request;

            // log_entry(LEVEL_DEBUG, "[%s] received client %s SIA job request", __func__, id_to_str(client->id));
            build_sia_request(ipc_request, &sia_request);
            if (ipc_request->clean)
                job_sia_stop(config);
            job_sia_program_request(config, &sia_request);
        }
        break;

        case ATTR_TYPE_IPC_POOL_STATS:
        {
            tlv_ipc_cgminer_stats_t *info = (tlv_ipc_cgminer_stats_t *)tlv->value;

            // log_entry(LEVEL_DEBUG, "[%s] received client %s DCR stats", __func__, id_to_str(client->id));
            ipc_notify_cgm_dcr_stats(config, info);
        }
        break;

        case ATTR_TYPE_IPC_FACTORY_RESET:
            log_entry(LEVEL_DEBUG, "[%s] received factory reset request from client %s", __func__, id_to_str(client->id));
            factory_reset();
            backup_config();
        break;

        case ATTR_TYPE_IPC_UPDATE:
        case ATTR_TYPE_IPC_ROLLBACK:
        {
            ipc_info_t *updatemgr = find_client(IPC_UPDATE_MANAGER);

            log_entry(LEVEL_INFO, "[%s] received update/rollback request from client %s", __func__, id_to_str(client->id));

            /*
             * Since the user authorized the update we can update to
             * the latest revision. We save this off to the system
             * info file so that, if there are multiple revisions
             * prior to the latest that we have to update to, the
             * update will automatically kick off following each
             * reboot until we've reached the latest at which point
             * we'll clear the updateto version.
             */
            if (tlv->type == ATTR_TYPE_IPC_UPDATE)
            {
                strcpy(config->system_info.updateto, config->latest_release);
                info_system_update(SYSTEM_INFO_FILE, &(config->system_info));
                backup_config();
            }

            /* Forward the message to the update manager if found */
            if (updatemgr)
            {
                /*
                 * We need to shutdown the HBs...otherwise they
                 * will keep talking to us and interfere with
                 * the reboot.
                 */
                hbmonitor_shutdown(config);
                sleep(2);
                if (tlv->type == ATTR_TYPE_IPC_UPDATE)
                {
                    update_inprogress = true;
                    ipc_start_swupdate(config->system_info.update_url);
                }
                else
                {
                    msg_ipc_tlv_t *msg = calloc(1, sizeof(msg_ipc_tlv_t) + tlv->len);
                    if (msg)
                    {
                        memcpy(msg, tlv, sizeof(msg_ipc_tlv_t) + tlv->len);
                        send_ipc_msg(updatemgr, msg);
                        free(msg);
                    }
                }
            }
            else
                log_entry(LEVEL_ERROR, "[%s] update manager not found", __func__);
        }
        break;

        case ATTR_TYPE_IPC_CHECK_FOR_UPDATE:
        {
            tlv_ipc_update_check_t *info = (tlv_ipc_update_check_t *)tlv->value;

            log_entry(LEVEL_INFO, "[%s] received check for updates request from client %s: %s", __func__, id_to_str(client->id), (info->autoupdate ? "true":"false"));

            update_check(config, info->autoupdate);
        }
        break;

        case ATTR_TYPE_IPC_FAN_SPEED:
        {
            tlv_ipc_fan_speed_t *info = (tlv_ipc_fan_speed_t *)tlv->value;

            log_entry(LEVEL_INFO, "[%s] received fan speed set request from client %s: %d", __func__, id_to_str(client->id), info->speed);

            if ((info->speed >= MIN_FAN_SPEED) && (info->speed <= MAX_FAN_SPEED))
            {
                config->system_info.fanspeed = info->speed;
                config->fan_setting_front = info->speed - FAN_SPEED_DELTA;
                config->fan_setting_rear = info->speed;
                info_system_update(SYSTEM_INFO_FILE, &(config->system_info));
                backup_config();
                fan_set_front(config, config->fan_setting_front);
                fan_set_rear(config, config->fan_setting_rear);
            }
            else
                log_entry(LEVEL_ERROR, "[%s] invalid fan speed: %d", __func__, info->speed);
        }
        break;

        case ATTR_TYPE_IPC_AUTO_FAN_SPEED:
        {
            tlv_ipc_fan_speed_t *info = (tlv_ipc_fan_speed_t *)tlv->value;

            log_entry(LEVEL_DEBUG, "[%s] received auto fan speed set request from client %s: %d", __func__, id_to_str(client->id), info->speed);

            if ((info->speed >= MIN_FAN_SPEED) && (info->speed <= MAX_FAN_SPEED))
            {
                config->fan_setting_front = info->speed - FAN_SPEED_DELTA;
                config->fan_setting_rear = info->speed;
                fan_set_front(config, config->fan_setting_front);
                fan_set_rear(config, config->fan_setting_rear);
            }
            else
                log_entry(LEVEL_ERROR, "[%s] invalid fan speed: %d", __func__, info->speed);
        }
        break;

        case ATTR_TYPE_IPC_REBOOT_PERIOD:
        {
            tlv_ipc_reboot_period_t *info = (tlv_ipc_reboot_period_t *)tlv->value;

            log_entry(LEVEL_INFO, "[%s] received reboot period request from client %s: %d", __func__, id_to_str(client->id), info->period);
            config->system_info.rebootperiod = info->period;
            config->time_until_reboot = info->period;
            config->reboot_start_time = time(NULL);
            info_system_update(SYSTEM_INFO_FILE, &(config->system_info));
            backup_config();
        }
        break;

        case ATTR_TYPE_IPC_RESTART_CGM:
        {
            tlv_ipc_burnin_start_t *info = (tlv_ipc_burnin_start_t *)tlv->value;

            log_entry(LEVEL_DEBUG, "[%s] received client %s restart CGMiner request", __func__, id_to_str(client->id));
            job_dcr_stop(config);
            job_sia_stop(config);
            ipc_send_reset(config, IPC_CGMINER_DCR);
            ipc_send_reset(config, IPC_CGMINER_SIA);
            if (client->id == IPC_BURNIN_MANAGER)
            {
                config->burnin_in_progress = true; /* Marks the start of burnin */
                ipc_notify_burnin_status(BI_INPROGRESS, info);
            }
        }
        break;

        case ATTR_TYPE_IPC_BURNIN_STATUS:
        {
            tlv_ipc_burnin_status_t *info = (tlv_ipc_burnin_status_t *)tlv->value;

            log_entry(LEVEL_INFO, "[%s] received client %s burnin status: %s", __func__, id_to_str(client->id), bistatus_to_str(info->miner_status));
            if (info->miner_status == BI_PASSED)
                led_set(config, LED_GREEN, RATE_FAST, false);
            else
                led_set(config, LED_RED, RATE_FAST, false);
            ipc_notify_burnin_status(info->miner_status, info);
        }
        break;

        case ATTR_TYPE_IPC_CLEAR_PERFORMANCE:
        {
            log_entry(LEVEL_DEBUG, "!!!!!!!!!![%s] received clear performance request from client %s", __func__, id_to_str(client->id));
            hbmonitor_shutdown(config);
            hbmonitor_clear_tuning();
            sleep(2);
            system_reboot(config);
        }
        break;

        case ATTR_TYPE_IPC_SAVE_PERFORMANCE:
        {
            log_entry(LEVEL_DEBUG, "!!!!!!!!!![%s] received save performance request from client %s", __func__, id_to_str(client->id));
            hbmonitor_save_performance();
        }
        break;

        case ATTR_TYPE_IPC_SET_PERFORMANCE:
        {
            tlv_ipc_set_performance_t *info = (tlv_ipc_set_performance_t *)tlv->value;
            hb_node_t *hb;

            log_entry(LEVEL_INFO, "+++++++++++++++++++++++++++[%s] received performance set request from client %s: HB[%d] -> %lu", __func__, id_to_str(client->id), info->hb_id, info->performance);
            hb = find_hb(config, info->hb_id);
            if (hb)
            {
            log_entry(LEVEL_INFO, "+++++++++++++++++++++++++++[%s] Found HB: id=%d", __func__, hb->id);
                uint32_t max_performance = ((hb->type == HB_DCR) ? HB_DCR_MAX_PERFORMANCE:HB_SIA_MAX_PERFORMANCE);
                if ((info->performance < HB_MIN_PERFORMANCE) || (info->performance > max_performance))
                {
                    log_entry(LEVEL_ERROR, "[%s] received invalid performance value", __func__);
                    goto out;
                }
                hbmonitor_set_performance(hb, info->performance);
            }
        }
        break;

        case ATTR_TYPE_IPC_AUTO_FAN_MODE:
        {
            tlv_ipc_auto_fan_mode_t *info = (tlv_ipc_auto_fan_mode_t *)tlv->value;

            log_entry(LEVEL_INFO, "[%s] received auto fan mode set request from client %s: %s", __func__, id_to_str(client->id), (info->enabled ? "enabled":"disabled"));
            config->system_info.autofan = info->enabled;
            info_system_update(SYSTEM_INFO_FILE, &(config->system_info));
            backup_config();
        }
        break;

        case ATTR_TYPE_IPC_AUTO_FAN_TEMP:
        {
            tlv_ipc_auto_fan_temp_t *info = (tlv_ipc_auto_fan_temp_t *)tlv->value;

            log_entry(LEVEL_INFO, "[%s] received auto fan temp set request from client %s: %.2f", __func__, id_to_str(client->id), info->temp);
            if ((info->temp >= MIN_AUTOFAN_TEMP) && (info->temp <= MAX_AUTOFAN_TEMP))
            {
                config->system_info.autofantemp = info->temp;
                info_system_update(SYSTEM_INFO_FILE, &(config->system_info));
                backup_config();
            }
        }
        break;

        case ATTR_TYPE_IPC_ALTERNATE_LEDS:
        {
            tlv_ipc_alternating_leds_t *info = (tlv_ipc_alternating_leds_t *)tlv->value;

            if (info->enabled)
                led_set(config, LED_BOTH, RATE_FAST, true);
            else
                led_set(config, LED_GREEN, RATE_NONE, false);
        }
        break;

        case ATTR_TYPE_IPC_SET_CLOCK:
        {
            tlv_ipc_set_clock_t *info = (tlv_ipc_set_clock_t *)tlv->value;
            hb_node_t *hb;

            log_entry(LEVEL_INFO, "[%s] received clock set request from client %s for HB%d: bias: %d, divider: %d", __func__, id_to_str(client->id), info->hb_id, info->clock_bias, info->clock_divider);
            hb = find_hb(config, info->hb_id);
            if (hb)
            {
                log_entry(LEVEL_INFO, "+++++++++++++++++++++++++++[%s] Found HB: id=%d", __func__, hb->id);
                if ((info->clock_bias < HB_MIN_BIAS) || (info->clock_bias > HB_MAX_BIAS))
                {
                    log_entry(LEVEL_ERROR, "[%s] received invalid bias value", __func__);
                    goto out;
                }
                else if ((info->clock_divider != 1) && (info->clock_divider != 2) && (info->clock_divider != 4) && (info->clock_divider != 8))
                {
                    log_entry(LEVEL_ERROR, "[%s] received invalid divider value", __func__);
                    goto out;
                }
                hbmonitor_set_clock(hb, info->clock_bias, info->clock_divider);
            }
        }
        break;

        // case ATTR_TYPE_IPC_SET_ASIC_CLOCK:
        // {
        //     tlv_ipc_set_asic_clock_t *info = (tlv_ipc_set_asic_clock_t *)tlv->value;
        //     hb_node_t *hb;

        //     log_entry(LEVEL_INFO, "[%s] received ASIC clock set request from client %s for HB%d: asic: %d, delta: %d", __func__, id_to_str(client->id), info->hb_id, info->asic, info->clock_delta);
        //     hb = find_hb(config, info->hb_id);
        //     if (hb)
        //     {
        //         if ((info->asic < 0) || (info->asic > (HB_ASIC_COUNT - 1)))
        //         {
        //             log_entry(LEVEL_ERROR, "[%s] received asic value", __func__);
        //             goto out;
        //         }
        //         else if ((info->clock_delta < HB_ASIC_CLOCK_DELTA_MIN) || (info->clock_delta > HB_ASIC_CLOCK_DELTA_MAX))
        //         {
        //             log_entry(LEVEL_ERROR, "[%s] received invalid ASIC clock delta value", __func__);
        //             goto out;
        //         }
        //         // hbmonitor_set_asic_clock(hb, info->asic, info->clock_delta);
        //     }
        // }
        // break;

        default:
            log_entry(LEVEL_ERROR, "[%s] received unknown TLV type:%d", __func__, tlv->type);
            return -1;
        break;
    }

    if (rsp)
    {
        if (client->fd > 0)
        {
            int bytes = send(client->fd, rsp, sizeof(msg_ipc_tlv_t) + rsp->len, MSG_NOSIGNAL);
            if (bytes < 0)
                log_entry(LEVEL_ERROR, "[%s] send failed: %s", __func__, strerror(errno));
            else if (bytes == 0)
                log_entry(LEVEL_ERROR, "[%s] sent 0 bytes: %s", __func__, strerror(errno));
        }
        else
            log_entry(LEVEL_WARNING, "[%s] not connected, message will not be sent", __func__);
        free(rsp);
    }
out:
    return sizeof(msg_ipc_tlv_t) + tlv->len;
}

int ipc_process_message(
    station_config_t *config,
    ipc_info_t *client
)
{
    uint8_t buf[MAX_RECV_LEN];
    int bytes_received;

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    bytes_received = recv(client->fd, buf, sizeof(buf), 0);
    switch (bytes_received)
    {
        case -1:
            /* Receive error...log it and return */
            log_entry(LEVEL_ERROR, "[%s] recv error: %s", __func__, strerror(errno));
            return -1;
        break;

        case 0:
            /* Far-end connection closed...cleanup client */
            log_entry(LEVEL_WARNING, "[%s] client %s closed connection",
                      __func__, (client->id == 0) ? "not registered":id_to_str(client->id));
            return 0;
        break;

        default:
        {
            uint8_t *tmp_tlv;

            /*
             * All messages coming from threads will be in TLV format. Because
             * all messaging is internal to the process all messaging will remain
             * in host byte-order.
             */
            msg_ipc_tlv_t *tlv = (msg_ipc_tlv_t *)buf;
            tmp_tlv = (uint8_t *)tlv;
            while (bytes_received > 0)
            {
                process_ipc_tlv(config, client, tlv);
                bytes_received -= (sizeof(msg_ipc_tlv_t) + tlv->len);
                if (bytes_received > 0)
                {
                    tmp_tlv += (sizeof(msg_ipc_tlv_t) + tlv->len); /* move to the next message */
                    tlv = (msg_ipc_tlv_t *)tmp_tlv;
                }
            }
        }
        break;
    }
    return 1;
}

void ipc_accept_connection(
    station_config_t *config
)
{
    struct sockaddr_in addr;
    socklen_t len = sizeof(addr);
    ipc_info_t client;

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    memset(&client, 0, sizeof(ipc_info_t));
    client.magic = IPC_MAGIC;
    client.fd = accept(config->ipc_fd, (struct sockaddr*)&addr, &len);
    if (client.fd < 0)
    {
        log_entry(LEVEL_ERROR, "[%s] accept failed: %s", __func__, strerror(errno));
        return;
    }
    if (!add_entry(&client_list, &client, sizeof(ipc_info_t)))
    {
        log_entry(LEVEL_ERROR, "[%s] add_entry failed", __func__);
        close(client.fd);
        return;
    }
    log_entry(LEVEL_DEBUG, "[%s] client connection accepted", __func__);
}

void ipc_notify_swupdate(
    char *latest_release,
    char *previous_release
)
{
    ipc_info_t *client = find_client(IPC_API_SERVER);

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    if (update_inprogress)
        return;

    if (client)
    {
        msg_ipc_tlv_t *msg = build_notification_swupdate(latest_release, previous_release);
        send_ipc_msg(client, msg);
    }
    else
    {
        log_entry(LEVEL_ERROR, "[%s] apiserver not found", __func__);
        update_available = true;
    }
}

void ipc_start_swupdate(
    char *url
)
{
    ipc_info_t *client = find_client(IPC_UPDATE_MANAGER);

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    if (client)
    {
        msg_ipc_tlv_t *msg = build_start_swupdate(url);
        send_ipc_msg(client, msg);
    }
    else
        log_entry(LEVEL_ERROR, "[%s] update manager not found", __func__);
}

void ipc_notify_hb_change(
    hb_node_t *hb,
    notification_t notification
)
{
    struct list_entry *entry;

    // log_entry(LEVEL_DEBUG, "[%s] called", __func__);
    TAILQ_FOREACH(entry, &client_list.head, next_entry)
    {
        ipc_info_t *client = entry->data;
        if (client && (client->notifications & notification))
        {
            switch (notification)
            {
                case NOTIFICATION_HB_TEMP1:
                case NOTIFICATION_HB_TEMP2:
                case NOTIFICATION_HB_VOLTAGE:
                case NOTIFICATION_HB_CURRENT:
                {
                    msg_ipc_tlv_t *msg = build_notification_measurement_info(hb, notification);
                    if (msg)
                        send_ipc_msg(client, msg);
                    else
                        log_entry(LEVEL_ERROR, "[%s] failed to build HB temp message", __func__);
                }
                break;

                case NOTIFICATION_HB_STATS:
                {
                    msg_ipc_tlv_t *msg = build_notification_hb_stats(hb);
                    if (msg)
                        send_ipc_msg(client, msg);
                    else
                        log_entry(LEVEL_ERROR, "[%s] failed to build HB temp message", __func__);
                }
                break;

                case NOTIFICATION_HB_PERF:
                {
                    msg_ipc_tlv_t *msg = build_notification_hb_performance_info(hb, notification);
                    if (msg)
                        send_ipc_msg(client, msg);
                    else
                        log_entry(LEVEL_ERROR, "[%s] failed to build HB performance message", __func__);
                }
                break;

                default:
                break;
            }
        }
    }
}

void ipc_notify_pool_difficulty(
    float difficulty
)
{
    struct list_entry *entry;

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);
    TAILQ_FOREACH(entry, &client_list.head, next_entry)
    {
        ipc_info_t *client = entry->data;
        if (client && (client->notifications & NOTIFICATION_POOL_DIFF))
        {
            msg_ipc_tlv_t *msg = build_notification_pool_difficulty(difficulty);
            if (msg)
                send_ipc_msg(client, msg);
            else
                log_entry(LEVEL_ERROR, "[%s] failed to build pool difficulty message", __func__);
        }
    }
}

void ipc_notify_burnin_status(
    burnin_status_t status,
    void *data
)
{
    struct list_entry *entry;

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);
    TAILQ_FOREACH(entry, &client_list.head, next_entry)
    {
        ipc_info_t *client = entry->data;
        if (client && (client->notifications & NOTIFICATION_BI_STATUS))
        {
            msg_ipc_tlv_t *msg = build_notification_burnin_status(status, data);
            if (msg)
                send_ipc_msg(client, msg);
            else
                log_entry(LEVEL_ERROR, "[%s] failed to build pool difficulty message", __func__);
        }
    }
}

void ipc_notify_current(
    station_config_t *config,
    ipc_info_t *client
)
{
    notification_t i;
    struct list_entry *entry;

    log_entry(LEVEL_INFO, "[%s] called", __func__);

    for (i = 1; i < NOTIFICATION_END; i <<= 1)
    {
        if (client->notifications & i)
        {
            /* Send current values for these notification */
            switch (i)
            {
                case NOTIFICATION_HB_TEMP1:
                    TAILQ_FOREACH(entry, &config->hb_list->head, next_entry)
                    {
                        hb_node_t *hb = entry->data;
                        if (hb)
                            ipc_notify_hb_change(hb, i);
                    }
                break;
                case NOTIFICATION_HB_TEMP2:
                    TAILQ_FOREACH(entry, &config->hb_list->head, next_entry)
                    {
                        hb_node_t *hb = entry->data;
                        if (hb)
                            ipc_notify_hb_change(hb, i);
                    }
                break;
                case NOTIFICATION_HB_VOLTAGE:
                    TAILQ_FOREACH(entry, &config->hb_list->head, next_entry)
                    {
                        hb_node_t *hb = entry->data;
                        if (hb)
                            ipc_notify_hb_change(hb, i);
                    }
                break;
                case NOTIFICATION_HB_CURRENT:
                    TAILQ_FOREACH(entry, &config->hb_list->head, next_entry)
                    {
                        hb_node_t *hb = entry->data;
                        if (hb)
                            ipc_notify_hb_change(hb, i);
                    }
                break;
                case NOTIFICATION_HB_PERF:
                    TAILQ_FOREACH(entry, &config->hb_list->head, next_entry)
                    {
                        hb_node_t *hb = entry->data;
                        if (hb)
                            ipc_notify_hb_change(hb, i);
                    }
                break;
                case NOTIFICATION_CGM_STATS:
                {
                    int i;

                    for (i = 0; i < MAX_POOLS; ++i)
                    {
                        if (config->pool_stats[i].pool_index != DEFAULT_POOL_INDEX)
                        {
                            tlv_ipc_cgminer_stats_t stats;

                            stats.pool_index = config->pool_stats[i].pool_index;
                            stats.accepted = config->pool_stats[i].accepted;
                            stats.rejected = config->pool_stats[i].rejected;
                            stats.status = config->pool_stats[i].status;
                            stats.idle = config->pool_stats[i].idle;
                            ipc_notify_cgm_dcr_stats(config, &stats);
                        }
                    }
                }
                break;
                case NOTIFICATION_SWUPDATE:
                    ipc_notify_swupdate(config->latest_release, config->previous_release);
                break;
                case NOTIFICATION_POOL_DIFF:
                    ipc_notify_pool_difficulty(config->pool_difficulty);
                break;
                case NOTIFICATION_BI_STATUS:
                    ipc_notify_burnin_status(BI_INACTIVE, NULL);
                break;
                /*
                 * Control manager doesn't keep data on these
                 * notifications.
                 */
                case NOTIFICATION_HB_STATS:
                default:
                break;
            }
        }
    }
}

void ipc_send_job_complete(
    station_config_t *config,
    ipc_id_t id,
    uint8_t job_id
)
{
    ipc_info_t *client = find_client(id);
    
    if (client)
    {
        msg_ipc_tlv_t *msg;

        log_entry(LEVEL_DEBUG, "[%s] sending DCR job complete", __func__);
        msg = build_job_complete(job_id);
        send_ipc_msg(client, msg);
    }
    else
    {
        log_entry(LEVEL_ERROR, "[%s] cgminer not found", __func__);
    }
}

void ipc_send_work_fail(
    station_config_t *config,
    ipc_id_t id,
    uint8_t job_id
)
{
    ipc_info_t *client = find_client(id);
    
    if (client)
    {
        msg_ipc_tlv_t *msg;

        log_entry(LEVEL_DEBUG, "[%s] sending DCR job failure", __func__);
        msg = build_work_fail(job_id);
        send_ipc_msg(client, msg);
    }
    else
        log_entry(LEVEL_ERROR, "[%s] cgminer not found", __func__);
}

void ipc_send_reset(
    station_config_t *config,
    ipc_id_t id
)
{
    ipc_info_t *client = find_client(id);
    
    if (client)
    {
        msg_ipc_tlv_t *msg;

        log_entry(LEVEL_DEBUG, "[%s] sending cgminer reset", __func__);
        msg = build_reset();
        send_ipc_msg(client, msg);
    }
    else
        log_entry(LEVEL_WARNING, "[%s] cgminer not found", __func__);
}

void ipc_send_dcr_nonce(
    station_config_t *config,
    hb_dcr_nonce_t *info
)
{
    ipc_info_t *client = find_client(IPC_CGMINER_DCR);
    
    if (client)
    {
        msg_ipc_tlv_t *msg;

        log_entry(LEVEL_DEBUG, "[%s] sending DCR nonce", __func__);
        msg = build_dcr_nonce(info);
        send_ipc_msg(client, msg);
    }
}

void ipc_send_sia_nonce(
    station_config_t *config,
    hb_sia_nonce_t *info
)
{
    ipc_info_t *client = find_client(IPC_CGMINER_SIA);
    
    if (client)
    {
        msg_ipc_tlv_t *msg;

        log_entry(LEVEL_DEBUG, "[%s] sending SIA nonce", __func__);
        msg = build_sia_nonce(info);
        send_ipc_msg(client, msg);
    }
}

void ipc_server_init(
    station_config_t *config
)
{
    int opt = 1;
    struct sockaddr_in addr;

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    config->ipc_fd = socket(PF_INET, SOCK_STREAM, 0);
    if (config->ipc_fd < 0)
    {
        log_entry(LEVEL_ERROR, "[%s] socket failed: %s", __func__, strerror(errno));
        config->ipc_fd = -1;
        return;
    }

    if (setsockopt(config->ipc_fd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt)) < 0)
    {
        log_entry(LEVEL_ERROR, "[%s] setsockopt failed: %s", __func__, strerror(errno));
        goto err;
    }

    memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(CONTROL_MGR_PORT);
    addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);

    if (bind(config->ipc_fd, (struct sockaddr*)&addr, sizeof(addr)) != 0)
    {
        log_entry(LEVEL_ERROR, "[%s] bind failed: %s", __func__, strerror(errno));
        goto err;
    }
    if (listen(config->ipc_fd, 10) != 0)
    {
        log_entry(LEVEL_ERROR, "[%s] listen failed: %s", __func__, strerror(errno));
        goto err;
    }
    return;

err:
    close(config->ipc_fd);
    config->ipc_fd = -1;
}

void ipc_server_shutdown(
    station_config_t *config
)
{
    struct list_entry *entry;

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    TAILQ_FOREACH(entry, &client_list.head, next_entry)
    {
        ipc_info_t *client = entry->data;

        if (client && (client->magic == IPC_MAGIC))
        {
            close(client->fd);
            del_entry(&client_list, entry);
        }
    }
    if (config->ipc_fd > 0)
        close(config->ipc_fd);
    config->ipc_fd = -1;
}

/***********************************************************
 * NAME: init_ipc
 * DESCRIPTION: initialize the IPC configuration
 *
 * IN:  none
 * OUT: client list
 ***********************************************************/
struct list *ipc_init(void)
{
    init_list(&client_list);

    return &client_list;
}
