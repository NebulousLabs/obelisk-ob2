/*
 * Copyright (C) 2018 MapleLeaf Software, Inc
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * swbutton.c
 *
 * Dan Jozwiak: djozwiak@mlsw.biz
 * Date Created: 2018-12-15
 *
 * Description: 
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "common.h"
#include "logger.h"
#include "config.h"
#include "led.h"
#include "utils.h"
#include "swbutton.h"
#include "multicast.h"

typedef struct
{
    struct timeval time;
    unsigned short type;
    unsigned short code;
    unsigned int value;
} input_event_t;

void swbutton_check(
    station_config_t *config
)
{
    if (config->button_state == BUTTON_PRESSED)
    {
        log_entry(LEVEL_DEBUG, "[%s] factory reset in progress", __func__);
        led_set(config, LED_BOTH, RATE_NONE, false);
        factory_reset();
    }
}

void swbutton_process_event(
    station_config_t *config
)
{
    uint8_t buf[1024];
    int bytes_received;
    time_t current = time(NULL);

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    bytes_received = read(config->button_fd, buf, sizeof(buf));
    switch (bytes_received)
    {
        case -1:
            /* Receive error...log it and return */
            log_entry(LEVEL_ERROR, "[%s] recv error: %s", __func__, strerror(errno));
            goto out;
        break;

        case 0:
            /* No data */
            goto out;
        break;

        default:
        {
            input_event_t *event = (input_event_t *)buf;
            while (bytes_received > 0)
            {
                log_entry(LEVEL_DEBUG, "[%s] event received: %d, %d", __func__, event->code, event->value);
                bytes_received -= sizeof(input_event_t);
                switch (event->value)
                {
                    case 0:
                        log_entry(LEVEL_DEBUG, "[%s] button pressed", __func__);
                        config->button_state = BUTTON_PRESSED;
                        config->button_press = current;
                    break;
                    case 1:
                        log_entry(LEVEL_DEBUG, "[%s] button released", __func__);
                        if (config->button_state == BUTTON_PRESSED)
                        {
                            config->button_state = BUTTON_RELEASED;
                            config->button_press = 0;
                            send_mdns_response(config);
                            led_set(config, LED_GREEN, RATE_NONE, false);
                        }
                    break;
                    default:
                    break;
                }
            }
        }
        break;
    }
out:
    return;
}

void swbutton_init(
    station_config_t *config
)
{
    int fd;

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);
    fd = open(INPUT_DEVICE, O_RDONLY);
    if (fd < 0)
    {
        log_entry(LEVEL_ERROR, "[%s] failed to open %s: %s", __func__, INPUT_DEVICE, strerror(errno));
        config->button_fd = -1;
    }
    config->button_fd = fd;
}

void swbutton_shutdown(
    station_config_t *config
)
{
    log_entry(LEVEL_DEBUG, "[%s] called", __func__);
    if (config->button_fd > 0)
    {
        close(config->button_fd);
        config->button_fd = -1;
    }
}
