/*
 * Copyright (C) 2018-2019 MapleLeaf Software, Inc
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * hbmonitor.c
 *
 * Dan Jozwiak: djozwiak@mlsw.biz
 * Date Created: 2018-10-28
 *
 * Description: f
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <libxml/parser.h>
#include <libxml/tree.h>
#define __USE_GNU
#include <pthread.h>

#include "common.h"
#include "logger.h"
#include "list.h"
#include "ipc.h"
#include "hb.h"
#include "hbmonitor.h"
#include "job.h"
#include "led.h"
#include "utils.h"

#define PERIODIC_TIME   30

#define STARTUP_PERIOD      (60 * 10) /* 10 minutes */
#define PERFORMANCE_PERIOD  (60 * 2)  /*  2 minutes */

#define MAX_BAD_JOB_RETRY_COUNT 5
#define MAX_CONTENT_SIZE (HB_ASIC_COUNT * 3 + 1)  // Allows for up to 3 characters per entry (e.g., "-5,")

/* XML tags in hb_info */
#define HB          "hb"            /* Marks an HB block */
#define HASHRATE    "hashrate"
#define PERFORMANCE "performance"
#define DIVIDER "divider"
#define BIAS "bias"
#define BIAS_OFFSETS "biasOffsets"

typedef struct
{
    uint8_t bad_job_id;
    uint8_t retries;
} bad_job_retries_t;

time_t first_job_start_time;

static station_config_t *station_config;
#ifdef SUPPORT_TUNING
static bool startup_period_completed;
#endif /* SUPPORT_TUNING */
static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

/* Unprocessed message data */
static uint8_t pending[MAX_HASHING_BOARDS][4096];
static int pending_len[MAX_HASHING_BOARDS];
static bad_job_retries_t bad_job_retries[MAX_HASHING_BOARDS];
static uint64_t stat_notification_counts[MAX_HASHING_BOARDS] = {0, 0, 0, 0, 0, 0};

typedef union
{
    uint32_t u32;
    float f32;
} conv_t;

static void msg_dump(
    uint8_t hbid,
    void *data,
    int len
)
{
    int i, j;
    int current = 0;
    uint8_t *buf = (uint8_t *)data;
    char log_info[256];

    memset(log_info, 0, sizeof(log_info));
    for (i = 0; current < len; i += 16)
    {
        char value[5];
        sprintf(log_info, "%04x: ", i);
        for (j = 0; (j < 16) && (current < len); ++j)
        {
            sprintf(value, "%02x ", buf[current++]);
            strcat(log_info, value);
        }
        // log_entry(LEVEL_INFO, "[%s] HB%d: %s", __func__, hbid, log_info);
        memset(log_info, 0, sizeof(log_info));
    }
}

// Assumes the buffer is big enough
static void format_bias_offsets(char *buffer, hb_params_t *params)
{
    int charIndex = 0;
    for (int i = 0; i < HB_ASIC_COUNT; i++)
    {
        charIndex += sprintf(buffer + charIndex, "%d", params->bias_offsets[i]);

        // Append comma except for final offset
        if (i != HB_ASIC_COUNT - 1)
        {
            buffer[charIndex] = ',';
            charIndex++;
            buffer[charIndex] = 0; // Ensure NULL termination
        }
    }
}

// Send messages to the control board to initialize the performance number, board clock divider/bias,
// and the per-ASIC bias offsets.
void set_hb_performance_and_clocks(station_config_t *config, uint8_t hb_id) {
    int i = 0;
    struct list_entry *entry;
    TAILQ_FOREACH(entry, &config->hb_list->head, next_entry)
    {
        hb_node_t *hb = entry->data;
        if (hb && hb->id == hb_id) {
            hb_params_t* hb_params = &config->hb_info.hbparams[i++];

            log_entry(LEVEL_INFO, "[%s] !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! Setting performance (%d) and clock (%d / %d) for HB%d", __func__,
                    hb_params->performance, hb_params->divider, hb_params->bias, hb->id);

            // Make the hb_node_t current values match with the hb_params
            hb->max_performance = hb->current_performance = hb_params->performance;
            hb->clock_divider = hb_params->divider;
            hb->clock_bias = hb_params->bias;

            // Set the actual params to the hashboard
            // TODO: An improveent here would be to add conditionals to decide whether the clock or performance
            // is moving up or down more, and which one should be set first.  A further improvement would be to
            // add a system that would walk the values up over time to avoid current spikes that can cause the board
            // or the PSU to shutdown.
            job_clock_set(hb, hb_params->divider, hb_params->bias);
            job_performance_set(hb);

            // Set bias offset for each ASIC on the HB
            // for (int a=0; a<HB_ASIC_COUNT; a++) {
            //     job_asic_clock_set(hb, a, hb_params->bias_offsets[a]);
            // }

            // if (i < HB_ASIC_COUNT) {
            //     sleep(2);
            // }
        }
    }
}

static void add_hb_block(
    xmlNodePtr parent,
    hb_params_t *params
)
{
    xmlNodePtr block;
    xmlNodePtr entry;
    char content[MAX_CONTENT_SIZE];

    block = xmlNewChild(parent, NULL, (xmlChar *)"hb", NULL);
    if (!block)
    {
        log_entry(LEVEL_ERROR, "[%s] failed to create HB block", __func__);
        return;
    }

    sprintf(content, "%d", params->hashrate);
    entry = xmlNewChild(block, NULL, (xmlChar *)HASHRATE, (xmlChar *)content);
    if (!entry)
    {
        log_entry(LEVEL_ERROR, "[%s] failed to create HB hashrate tag", __func__);
        return;
    }

    sprintf(content, "%d", params->performance);
    entry = xmlNewChild(block, NULL, (xmlChar *)PERFORMANCE, (xmlChar *)content);
    if (!entry)
    {
        log_entry(LEVEL_ERROR, "[%s] failed to create HB performance tag", __func__);
        return;
    }

    sprintf(content, "%d", params->divider);
    entry = xmlNewChild(block, NULL, (xmlChar *)DIVIDER, (xmlChar *)content);
    if (!entry)
    {
        log_entry(LEVEL_ERROR, "[%s] failed to create HB divider tag", __func__);
        return;
    }

    sprintf(content, "%d", params->bias);
    entry = xmlNewChild(block, NULL, (xmlChar *)BIAS, (xmlChar *)content);
    if (!entry)
    {
        log_entry(LEVEL_ERROR, "[%s] failed to create HB bias tag", __func__);
        return;
    }

    format_bias_offsets(content, params);
    entry = xmlNewChild(block, NULL, (xmlChar *)BIAS_OFFSETS, (xmlChar *)content);
    if (!entry)
    {
        log_entry(LEVEL_ERROR, "[%s] failed to create HB biasOffsets tag", __func__);
        return;
    }
}

/***********************************************************
 * NAME: save_hb_info
 * DESCRIPTION: this function opens the station info XML
 * file and reads its contents.
 *
 * IN:  station info pointer
 * OUT: none
 ***********************************************************/
static void save_hb_info(
    char *info_file,
    hb_info_t *info
)
{
    int i;
    xmlDocPtr doc = NULL;
    xmlNodePtr rootElement;

    log_entry(LEVEL_DEBUG, "[%s] called: hbcount=%d", __func__, info->hbcount);

    doc = xmlNewDoc((xmlChar *)"1.0");
    rootElement = xmlNewNode(NULL, (xmlChar *)"hbinfo");
    xmlDocSetRootElement(doc, rootElement);

    // Update or add the blocks to define the HB parameters
    for (i = 0; i < info->hbcount; ++i)
    {
        add_hb_block(rootElement, &info->hbparams[i]);
    }

    xmlSaveFormatFileEnc(info_file, doc, "utf-8", 1);
    xmlFreeDoc(doc);
}

#ifdef SUPPORT_TUNING
static void check_tuning(
    station_config_t *config
)
{
    bool tuning_complete = true;
    struct list_entry *entry;

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    if (config->hb_info.hbvalid)
        return;

    pthread_mutex_lock(&mutex);
    TAILQ_FOREACH(entry, &config->hb_list->head, next_entry)
    {
        hb_node_t *hb = entry->data;
        if (hb)
        {
            if (!hb->tuned)
                tuning_complete = false;
        }
    }

    if (tuning_complete)
    {
        config->hb_info.hbcount = config->hb_list->size;
        save_hb_info(HB_INFO_FILE, &config->hb_info);
        backup_config();
        config->hb_info.hbvalid = true;
    }
    pthread_mutex_unlock(&mutex);
}
#endif /* SUPPORT_TUNING */

/******************************************************************
 * Function: restart_board
 * Description: powerup the specified HB.
 * NOTE: in V3.15 of the firmware and earlier, once an HB has shut
 * itself down, it cannot be powered up again. This function is in
 * place as a recovery mechanism for future firmware revisions.
 * 
 * IN:  HB node
 * Out: none
 ******************************************************************/
static void restart_board(
    hb_node_t *hb
)
{
    int rc;

#if 1 /*
       * This recovery isn't working and will need changes in the HB
       * firmware so recovery is disabled for now.
       */
    static bool status_reported = false;

    if (station_config->burnin_in_progress && status_reported)
        return;
    log_entry(LEVEL_INFO, "[%s] HB%d unexpectedly shutdown", __func__, hb->id);
    led_set(station_config, LED_RED, RATE_FAST, false);
    status_reported = true;
    if (station_config->burnin_in_progress)
        return; /* The recovery below doesn't work in V3.15...retry later */
    else
        system_reboot(station_config);
#endif
    log_entry(LEVEL_INFO, "[%s] attempting to restart HB%d", __func__, hb->id);

    /* The board has shutdown. Reset basic parameters and try to restart */
    hb->powered = false;
    hb->tuned = false;
    hb->current_performance = HB_DEFAULT_PERFORMANCE;
    hb->peak_performance = 0;
    if (hb->type == HB_DCR)
        hb->max_performance = HB_DCR_MAX_PERFORMANCE;
    else if (hb->type == HB_SIA)
        hb->max_performance = HB_SIA_MAX_PERFORMANCE;
    hb->job_info.last_hashrate_5min = 0;
    hb->job_info.current_hashrate_5min = 0;
    hb->job_info.last_hash_stat_time_nanos = 0;

    hb_close_serial(hb);
    hb->fd = hb_open_serial(hb->devnode);
    if (hb->fd < 0)
    {
        log_entry(LEVEL_ERROR, "[%s] failed to reopen HB%d", __func__, hb->id);
        return;
    }

    rc = hbmonitor_send_message(hb, MSG_TYPE_HB_PWR_DN, NULL, 0, NULL, 0);
    if (rc < 0)
        log_entry(LEVEL_ERROR, "[%s] failed to power down HB%d", __func__, hb->id);
    else
        log_entry(LEVEL_INFO, "[%s] HB%d array power down complete", __func__, hb->id);

    sleep(2);

    rc = hbmonitor_send_message(hb, MSG_TYPE_HB_PWR_UP, NULL, 0, NULL, 0);
    if (rc < 0)
        log_entry(LEVEL_ERROR, "[%s] failed to power up HB%d", __func__, hb->id);
    else
    {
        log_entry(LEVEL_INFO, "[%s] HB%d array power up complete", __func__, hb->id);
        hb->powered = true;
    }
}

/******************************************************************
 * Function: request_diag_info
 * Description: 
 * 
 * IN:  HB node
 * Out: none
 ******************************************************************/
static void request_diag_info(
    hb_node_t *hb
)
{
    int rc;

    rc = hbmonitor_send_message(hb, MSG_TYPE_HB_GET_ADIAG, NULL, 0, NULL, 0);
    if (rc < 0)
        log_entry(LEVEL_ERROR, "[%s] failed to request ASIC diag info from HB%d", __func__, hb->id);
    else
        log_entry(LEVEL_INFO, "[%s] HB%d ASIC diag info request sent", __func__, hb->id);
}

static void update_hb_stats(
    hb_node_t *hb
)
{
    uint64_t hashes = 0;
    struct timespec now;
    uint64_t now_nanos;
    uint64_t elapsed;
    double elapsed_secs;
    double gigahashes_done;
    double ftotal, fprop;

#if 1 /* SIA multiplier isn't working so do this fall all for now */
    hashes = (uint64_t)(hb->job_info.good_nonces * DCR_HASH_MULT);
#else
    switch (hb->type)
    {
        case HB_DCR:
            hashes = (uint64_t)(hb->job_info.good_nonces * DCR_HASH_MULT);
        break;
        case HB_SIA:
            hashes = (uint64_t)(hb->job_info.good_nonces * SIA_HASH_MULT);
        break;
        default:
            log_entry(LEVEL_ERROR, "[%s] unknown board type: %d", __func__, hb->type);
            return;
        break;
    }
#endif
    clock_gettime(CLOCK_MONOTONIC, &now);
    now_nanos = now.tv_sec * (uint64_t)1000000000L + now.tv_nsec;
    elapsed = now_nanos - hb->job_info.last_hash_stat_time_nanos;
    elapsed_secs = ((double)elapsed) / 1000000000.0;
    gigahashes_done = ((double)hashes) / 1000000000.0;
    if (elapsed_secs > 0)
    {
        fprop = 1.0 - 1 / (exp(elapsed_secs / 300.0));
        ftotal = 1.0 + fprop;
        hb->job_info.current_hashrate_5min += (gigahashes_done / elapsed_secs * fprop);
        hb->job_info.current_hashrate_5min /= ftotal;
    }
    hb->job_info.last_hash_stat_time_nanos = now_nanos;
}

/***********************************************************
 * NAME: process_hb_signal
 * DESCRIPTION: 
 *
 * IN:  generated signal number
 * OUT: none
 ***********************************************************/
static void process_hb_signal(int sig)
{
    switch(sig)
    {
        case SIGTERM:
            log_entry(LEVEL_INFO, "[%s] thread exiting...", __func__);
        break;
    }
}

/******************************************************************
 * Function: process_hb_tlv
 * Description: this function processes TLVs coming from the HB
 * 
 * IN:  HB node
 *      TLV
 * Out: TLV length on success, -1 on failure
 ******************************************************************/
static int process_hb_tlv(
    hb_node_t *hb,
    hb_tlv_t *tlv
)
{
    conv_t rval;

    // log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    switch (tlv->type)
    {
        case MSG_TYPE_HB_DCR_JOB_DONE:
        {
#ifdef DEBUG
            printf("\n");
#endif /* DEBUG */
            if (hb->job_info.a1state == JOB_ACTIVE)
                hb->job_info.a1state = JOB_STOP;
            else if (hb->job_info.a2state == JOB_ACTIVE)
            {
                hb->job_info.a2state = JOB_STOP;
                hb->job_info.end = time(NULL);
                pthread_mutex_lock(&hb->mutex);
                ipc_send_job_complete(station_config, IPC_CGMINER_DCR, hb->job_info.id);
                pthread_mutex_unlock(&hb->mutex);
                // log_entry(LEVEL_INFO, "[%s] received DCR job complete from HB%d", __func__, hb->id);
                // log_entry(LEVEL_DEBUG, "[%s] job stats for HB%d:", __func__, hb->id);
                // log_entry(LEVEL_DEBUG, "[%s] \t           job ID: %d", __func__, hb->job_info.id);
                // log_entry(LEVEL_DEBUG, "[%s] \t     total nonces: %d", __func__, hb->job_info.total_nonces);
                // log_entry(LEVEL_DEBUG, "[%s] \t total CRC errors: %d", __func__, hb->job_info.total_crc_errors);
                // log_entry(LEVEL_DEBUG, "[%s] \ttotal time (secs): %d", __func__, hb->job_info.end - hb->job_info.start);
                // log_entry(LEVEL_DEBUG, "[%s] \t   min bytes rcvd: %d", __func__, min_bytes_received);
                // log_entry(LEVEL_DEBUG, "[%s] \t   max bytes rcvd: %d", __func__, max_bytes_received);
            }
        }
        break;

        case MSG_TYPE_HB_DCR_BAD_JOB:
        {
            hb_bad_job_t *info = (hb_bad_job_t *)tlv->value;

            // log_entry(LEVEL_WARNING, "[%s] received bad job from HB%d: %d, last: %d", __func__, hb->id, info->job_id, last_dcr_request.job_id);
            if (info->job_id == bad_job_retries[hb->id].bad_job_id)
                ++bad_job_retries[hb->id].retries;
            else
            {
                bad_job_retries[hb->id].bad_job_id = info->job_id;
                bad_job_retries[hb->id].retries = 1;
            }
            if (bad_job_retries[hb->id].retries < MAX_BAD_JOB_RETRY_COUNT)
                job_dcr_reprogram_request(station_config, hb, info->job_id);
        }
        break;

        case MSG_TYPE_HB_DCR_NONCE:
        {
            hb_dcr_nonce_t *info = (hb_dcr_nonce_t *)tlv->value;
#ifdef DEBUG
            static uint32_t count = 0;
#endif /* DEBUG */

            info->nonce = be32toh(info->nonce);
            info->m5 = be32toh(info->m5);
            // log_entry(LEVEL_DEBUG, "[%s] received DCR job nonce from HB%d", __func__, hb->id);
            // log_entry(LEVEL_DEBUG, "[%s] \tjob ID: %d", __func__, info->job);
            // log_entry(LEVEL_DEBUG, "[%s] \t nonce: %08x", __func__, info->nonce);
            // log_entry(LEVEL_DEBUG, "[%s] \t    m5: %08x", __func__, info->m5);
            hb->job_info.total_nonces++;
            pthread_mutex_lock(&hb->mutex);
            ipc_send_dcr_nonce(station_config, info);
            pthread_mutex_unlock(&hb->mutex);

#ifdef DEBUG
            /* For debug output only... */
            ++count;
            if (count > 100)
            {
                printf(".");
                fflush(stdout);
                count = 0;
            }
            /* ...end debug output */
#endif /* DEBUG */
        }
        break;

        case MSG_TYPE_HB_SIA_JOB_DONE:
        {
#ifdef DEBUG
            printf("\n");
#endif /* DEBUG */
            if (hb->job_info.a1state == JOB_ACTIVE)
                hb->job_info.a1state = JOB_STOP;
            else if (hb->job_info.a2state == JOB_ACTIVE)
            {
                hb->job_info.a2state = JOB_STOP;
                hb->job_info.end = time(NULL);
                pthread_mutex_lock(&hb->mutex);
                ipc_send_job_complete(station_config, IPC_CGMINER_SIA, hb->job_info.id);
                pthread_mutex_unlock(&hb->mutex);
                // log_entry(LEVEL_INFO, "[%s] received SIA job complete from HB%d", __func__, hb->id);
                // log_entry(LEVEL_DEBUG, "[%s] job stats for HB%d:", __func__, hb->id);
                // log_entry(LEVEL_DEBUG, "[%s] \t           job ID: %d", __func__, hb->job_info.id);
                // log_entry(LEVEL_DEBUG, "[%s] \t     total nonces: %d", __func__, hb->job_info.total_nonces);
                // log_entry(LEVEL_DEBUG, "[%s] \t total CRC errors: %d", __func__, hb->job_info.total_crc_errors);
                // log_entry(LEVEL_DEBUG, "[%s] \ttotal time (secs): %d", __func__, hb->job_info.end - hb->job_info.start);
                // log_entry(LEVEL_DEBUG, "[%s] \t   min bytes rcvd: %d", __func__, min_bytes_received);
                // log_entry(LEVEL_DEBUG, "[%s] \t   max bytes rcvd: %d", __func__, max_bytes_received);
            }
        }
        break;

        case MSG_TYPE_HB_SIA_BAD_JOB:
        {
            hb_bad_job_t *info = (hb_bad_job_t *)tlv->value;

            // log_entry(LEVEL_WARNING, "[%s] received bad job from HB%d: %d, last: %d", __func__, hb->id, info->job_id, last_dcr_request.job_id);
            if (info->job_id == bad_job_retries[hb->id].bad_job_id)
                ++bad_job_retries[hb->id].retries;
            else
            {
                bad_job_retries[hb->id].bad_job_id = info->job_id;
                bad_job_retries[hb->id].retries = 1;
            }
            if (bad_job_retries[hb->id].retries < MAX_BAD_JOB_RETRY_COUNT)
                job_sia_reprogram_request(station_config, hb, info->job_id);
        }
        break;

        case MSG_TYPE_HB_SIA_NONCE:
        {
            hb_sia_nonce_t *info = (hb_sia_nonce_t *)tlv->value;
#ifdef DEBUG
            static uint32_t count = 0;
#endif /* DEBUG */

            info->nonce = be64toh(info->nonce);
            // log_entry(LEVEL_DEBUG, "[%s] received SIA job nonce from HB%d", __func__, hb->id);
            // log_entry(LEVEL_DEBUG, "[%s] \tjob ID: %d", __func__, info->job);
            // log_entry(LEVEL_DEBUG, "[%s] \t nonce: %016llx", __func__, info->nonce);
            hb->job_info.total_nonces++;
            pthread_mutex_lock(&hb->mutex);
            ipc_send_sia_nonce(station_config, info);
            pthread_mutex_unlock(&hb->mutex);
#ifdef DEBUG
            /* For debug output only... */
            ++count;
            if (count > 100)
            {
                printf(".");
                fflush(stdout);
                count = 0;
            }
            /* ...end debug output */
#endif /* DEBUG */
        }
        break;

        case MSG_TYPE_HB_ABORT_JOB:
        {
            printf("\n");
            hb->job_info.a1state = hb->job_info.a2state = JOB_IDLE;
            hb->job_info.end = time(NULL);
            hb->clean_occurred = true;
            // log_entry(LEVEL_DEBUG, "[%s] received job aborted from HB%d", __func__, hb->id);
            // log_entry(LEVEL_DEBUG, "[%s] job stats for HB%d:", __func__, hb->id);
            // log_entry(LEVEL_DEBUG, "[%s] \t           job ID: %d", __func__, hb->job_info.id);
            // log_entry(LEVEL_DEBUG, "[%s] \t     total nonces: %d", __func__, hb->job_info.total_nonces);
            // log_entry(LEVEL_DEBUG, "[%s] \t total CRC errors: %d", __func__, hb->job_info.total_crc_errors);
            // log_entry(LEVEL_DEBUG, "[%s] \ttotal time (secs): %d", __func__, hb->job_info.end - hb->job_info.start);
            // log_entry(LEVEL_DEBUG, "[%s] \t   min bytes rcvd: %d", __func__, min_bytes_received);
            // log_entry(LEVEL_DEBUG, "[%s] \t   max bytes rcvd: %d", __func__, max_bytes_received);
        }
        break;

        case MSG_TYPE_HB_GET_VOLT:
        {
            hb_measurement_t *info = (hb_measurement_t *)tlv->value;
            rval.u32 = info->value;
            if (rval.f32 != hb->voltage)
            {
                hb->voltage = rval.f32;
                pthread_mutex_lock(&hb->mutex);
                ipc_notify_hb_change(hb, NOTIFICATION_HB_VOLTAGE);
                pthread_mutex_unlock(&hb->mutex);
            }
            // log_entry(LEVEL_DEBUG, "[%s] received HB%d voltage: %.2f, %d", __func__, hb->id, rval.f32, (int)rval.f32);

            /*
             * Check to see if the board shut itself down and, if so,
             * power it back up and start things from scratch.
             */
            if (hb->powered && ((int)rval.f32 == 0))
                restart_board(hb);
        }
        break;

        case MSG_TYPE_HB_GET_CURR:
        {
            hb_measurement_t *info = (hb_measurement_t *)tlv->value;
            rval.u32 = info->value;
            if (rval.f32 != hb->current)
            {
                hb->current = rval.f32;
                pthread_mutex_lock(&hb->mutex);
                ipc_notify_hb_change(hb, NOTIFICATION_HB_CURRENT);
                pthread_mutex_unlock(&hb->mutex);
            }
            // log_entry(LEVEL_DEBUG, "[%s] received HB%d current: %.2f", __func__, hb->id, rval.f32);
        }
        break;

        case MSG_TYPE_HB_GET_TEMP1:
        {
            hb_measurement_t *info = (hb_measurement_t *)tlv->value;
            rval.u32 = info->value;
            if (rval.f32 != hb->temp1)
            {
                hb->temp1 = rval.f32;
                pthread_mutex_lock(&hb->mutex);
                ipc_notify_hb_change(hb, NOTIFICATION_HB_TEMP1);
                pthread_mutex_unlock(&hb->mutex);
            }
            // log_entry(LEVEL_DEBUG, "[%s] received HB%d temp1: %.2f", __func__, hb->id, rval.f32);
        }
        break;

        case MSG_TYPE_HB_GET_TEMP2:
        {
            hb_temp_t *info = (hb_temp_t *)tlv->value;
            rval.u32 = info->temp;
            if (rval.f32 != hb->temp2)
            {
                hb->temp2 = rval.f32;
                pthread_mutex_lock(&hb->mutex);
                ipc_notify_hb_change(hb, NOTIFICATION_HB_TEMP2);
                pthread_mutex_unlock(&hb->mutex);
            }
            // log_entry(LEVEL_DEBUG, "[%s] received HB%d temp2: %.2f", __func__, hb->id, rval.f32);
        }
        break;

        case MSG_TYPE_HB_CHECK_JOB:
        {
            hb_check_job_t *info = (hb_check_job_t *)tlv->value;
            hb->job_info.total_nonces = be32toh(info->nonces);
            hb->job_info.good_nonces = be32toh(info->good_nonces);
            hb->job_info.pool_nonces = be32toh(info->pool_nonces);
            hb->relative_voltage = be32toh(info->voltage);
            hb->clock_bias = (int8_t)be32toh(info->bias);
            hb->clock_divider = divider_enum_to_value((uint8_t)be32toh(info->divider));
            log_entry(LEVEL_WARNING, "[%s] HB%d divider=%d bias=%d", __func__, hb->id, hb->clock_divider, hb->clock_bias);

#ifdef SUPPORT_NOISE
            if (info->noise)
            {
                // log_entry(LEVEL_WARNING, "[%s] HB%d reported noise", __func__, hb->id);
                ++hb->noise_counter;
            }
#endif

            update_hb_stats(hb);

            log_entry(LEVEL_INFO, "[%s] received HB%d job stats:", __func__, hb->id);
            log_entry(LEVEL_DEBUG, "[%s] \t            busy1: %s", __func__, (info->busy1) ? "true":"false");
            log_entry(LEVEL_DEBUG, "[%s] \t            busy2: %s", __func__, (info->busy2) ? "true":"false");
            log_entry(LEVEL_DEBUG, "[%s] \t       total jobs: %d", __func__, be32toh(info->jobs));
            log_entry(LEVEL_DEBUG, "[%s] \t     total nonces: %d", __func__, hb->job_info.total_nonces);
            log_entry(LEVEL_DEBUG, "[%s] \ttotal good nonces: %d", __func__, hb->job_info.good_nonces);
            log_entry(LEVEL_DEBUG, "[%s] \ttotal pool nonces: %d", __func__, hb->job_info.pool_nonces);
            log_entry(LEVEL_DEBUG, "[%s] \t relative voltage: %d", __func__, hb->relative_voltage);
            log_entry(LEVEL_INFO,  "[%s] \t       clock bias: %d", __func__, hb->clock_bias);
            log_entry(LEVEL_INFO,  "[%s] \t    clock divider: %d", __func__, hb->clock_divider);
            log_entry(LEVEL_INFO,  "[%s] \t     current perf: %d", __func__, hb->current_performance);
#ifdef SUPPORT_NOISE
            log_entry(LEVEL_DEBUG, "[%s] \t            noise: %d", __func__, info->noise);
#endif
            stat_notification_counts[hb->id]++;
            if (stat_notification_counts[hb->id] == 10) {
                // We use this to wait a while before setting the performance, otherwise the messages are ignored
                set_hb_performance_and_clocks(station_config, hb->id);
            }

            pthread_mutex_lock(&hb->mutex);
            ipc_notify_hb_change(hb, NOTIFICATION_HB_STATS);
            ipc_notify_hb_change(hb, NOTIFICATION_HB_PERF);
            pthread_mutex_unlock(&hb->mutex);
        }
        break;

        case MSG_TYPE_HB_GET_ADIAG:
        {
            // int i;
            hb_asic_diag_status_t *info = (hb_asic_diag_status_t *)tlv->value;

            pthread_mutex_lock(&mutex);
            // log_entry(LEVEL_INFO, "[%s] received HB%d ASIC diag stats:", __func__, hb->id);
            // for (i = 0; i < MAX_ASICS_PER_HASHING_BOARD; ++i)
            //     log_entry(LEVEL_INFO, "[%s] \tASIC%d: %d", __func__, i, info->asic[i]);
            pthread_mutex_unlock(&mutex);
            memcpy(hb->asic_diags, info->asic, MAX_ASICS_PER_HASHING_BOARD);
            hb->asic_diags_valid = true;
        }
        break;

        case MSG_TYPE_HB_WDOG_RESET:
            log_entry(LEVEL_INFO, "[%s] received HB%d watchdog reset", __func__, hb->id);
            job_stats_set(hb, 0);
            hb->powered = false;
            hb->tuned = false;
            hb->current_performance = HB_DEFAULT_PERFORMANCE;
            hb->peak_performance = 0;
            if (hb->type == HB_DCR)
                hb->max_performance = HB_DCR_MAX_PERFORMANCE;
            else if (hb->type == HB_SIA)
                hb->max_performance = HB_SIA_MAX_PERFORMANCE;
            hb->job_info.last_hashrate_5min = 0;
            hb->job_info.current_hashrate_5min = 0;
            hb->job_info.last_hash_stat_time_nanos = 0;
            ++hb->watchdog_counter;
            pthread_mutex_lock(&hb->mutex);
            ipc_notify_hb_change(hb, NOTIFICATION_HB_STATS);
            pthread_mutex_unlock(&hb->mutex);
        break;

        case MSG_TYPE_HB_GOOD_NONCES:
        case MSG_TYPE_HB_BAD_NONCES:
        break;

        default:
#ifdef DEBUG
            msg_dump(hb->id, tlv, sizeof(hb_tlv_t) + tlv->len + 1);
#endif /* DEBUG */
        break;
    }

    return sizeof(hb_tlv_t) + tlv->len;
}

/******************************************************************
 * Function: valid_hb_tlv
 * Description: this function validates TLVs coming from the HB
 * 
 * IN:  HB node
 *      TLV
 * Out: TLV length on success, -1 on failure
 ******************************************************************/
static bool valid_hb_tlv(
    hb_tlv_t *tlv
)
{
    /* Check all the known valid message types */
    switch (tlv->type)
    {
        case MSG_TYPE_HB_NAK:
        case MSG_TYPE_HB_NOP:
        case MSG_TYPE_HB_VERSION:
        case MSG_TYPE_HB_BOOT_MODE:
        case MSG_TYPE_HB_PWR_UP:
        case MSG_TYPE_HB_PWR_DN:
        case MSG_TYPE_HB_SET_VOLT:
        case MSG_TYPE_HB_GET_VOLT:
        case MSG_TYPE_HB_GET_CURR:
        case MSG_TYPE_HB_GET_TEMP1:
        case MSG_TYPE_HB_GET_TEMP2:
        case MSG_TYPE_HB_SET_CLKS:
        case MSG_TYPE_HB_START_JOB:
        case MSG_TYPE_HB_ABORT_JOB:
        case MSG_TYPE_HB_CHECK_JOB:
        case MSG_TYPE_HB_JREG_WRITE:
        case MSG_TYPE_HB_JMAN_WRITE:
        case MSG_TYPE_HB_GET_PROC_ID:
        case MSG_TYPE_HB_SET_POOLD:
        case MSG_TYPE_HB_SET_PERF:
        case MSG_TYPE_HB_GET_ADIAG:
        case MSG_TYPE_HB_SET_STATS:
        case MSG_TYPE_HB_WDOG_RESET:
        case MSG_TYPE_HB_SET_ASIC_CLK:
        case MSG_TYPE_HB_GOOD_NONCES:
        case MSG_TYPE_HB_BAD_NONCES:
        case MSG_TYPE_HB_DCR_NONCE:
        case MSG_TYPE_HB_DCR_JOB_DONE:
        case MSG_TYPE_HB_DCR_BAD_JOB:
        case MSG_TYPE_HB_SIA_NONCE:
        case MSG_TYPE_HB_SIA_JOB_DONE:
        case MSG_TYPE_HB_SIA_BAD_JOB:
            return true;
        break;
    }
    return false;
}

/******************************************************************
 * Function: process_hb_async_message
 * Description: this function processes data coming from the HB.
 * The data may consist of multiple TLVs.
 * 
 * IN:  HB node
 * Out: none
 ******************************************************************/
static void process_hb_async_message(
    hb_node_t *hb
)
{
    uint8_t buf[MAX_HB_MSG_LEN];
    int bytes_received;

    // log_entry(LEVEL_DEBUG, "[%s] called for HB%d", __func__, hb->id);

    bytes_received = read(hb->fd, buf, sizeof(buf));
    switch (bytes_received)
    {
        case -1:
            /* Receive error...log it and return */
            // log_entry(LEVEL_ERROR, "[%s] recv error: %s", __func__, strerror(errno));
            return;
        break;

        case 0:
            /* No data */
            //log_entry(LEVEL_WARNING, "[%s] 0 bytes from HB %s", __func__, hb->devnode);
            return;
        break;

        default:
        {
            uint8_t *tmp_tlv;
            hb_tlv_t *tlv;
            uint8_t *thread_pending = pending[hb->id];            

            /* Keep some stats on HB comms */
            if (bytes_received < min_bytes_received)
                min_bytes_received = bytes_received;
            if (bytes_received > max_bytes_received)
                max_bytes_received = bytes_received;

            memcpy(&thread_pending[pending_len[hb->id]], buf, bytes_received);
            pending_len[hb->id] += bytes_received;
            tlv = (hb_tlv_t *)thread_pending;
            tmp_tlv = (uint8_t *)tlv;
            while (pending_len[hb->id] > 0)
            {
                /* Check to make sure we've got a whole TLV to process */
                if ((pending_len[hb->id] >= 2) && (pending_len[hb->id] >= (sizeof(hb_tlv_t) + tlv->len + CSUM_SIZE)))
                {
                    uint8_t msg_csum;
                    uint8_t csum_check;
                    uint8_t total_len = sizeof(hb_tlv_t) + tlv->len + CSUM_SIZE;

                    if (valid_hb_tlv(tlv))
                    {
                        msg_csum = tmp_tlv[total_len - 1];
                        csum_check = hb_generate_checksum(tlv);
                        if (csum_check != msg_csum)
                        {
                            // log_entry(LEVEL_ERROR, "[%s] HB%d CSUM check failed: calculated 0x%x, received 0x%x, type: %d, len: %d",
                            //           __func__, hb->id, csum_check, msg_csum, tlv->type, tlv->len);
                            hb->job_info.total_crc_errors++;
                            msg_dump(hb->id, tlv, total_len);
                            total_len = 1;
                            goto next_msg;
                        }
                    }
                    else
                    {
                        msg_dump(hb->id, tlv, 3);
                        total_len = 1;
                        goto next_msg;
                    }

                    /* Integrity check passed...process the TLV... */
                    process_hb_tlv(hb, tlv);
next_msg:
                    pending_len[hb->id] -= total_len;
                    if (pending_len[hb->id] > 0)
                    {
                        tmp_tlv += total_len; /* move to the next message */
                        tlv = (hb_tlv_t *)tmp_tlv;
                    }
                }
                else
                    break; /* Not enough bytes for a TLV */
            }
            // log_entry(LEVEL_DEBUG, "[%s] bytes_received: %d, pending_len: %d",
            //          __func__, bytes_received, pending_len[hb->id]);
            /*
             * If we have bytes still pending move them up to the front
             * of the pending buffer.
             */
            if (pending_len[hb->id] > 0)
                memcpy(thread_pending, tmp_tlv, pending_len[hb->id]);
        }
        break;
    }
}

/***********************************************************
 * NAME: hbmonitor_thread
 * DESCRIPTION: 
 *
 * IN:  data passed during thread creation
 * OUT: none
 ***********************************************************/
static void *hbmonitor_thread(
    void *data
)
{
    hb_node_t *hb = data;
    sigset_t mask;
    sigset_t orig_mask;
    struct sigaction act;
#ifdef SUPPORT_TUNING
    time_t last_performance_adjustment = 0;
#endif /* SUPPORT_TUNING */
    sprintf(hb->name, "HB%d-th", hb->id);
    pthread_setname_np(pthread_self(), hb->name);
    log_entry(LEVEL_INFO, "[%s] %s thread started: id: %d, type: %s, version: %s",
              __func__, hb->name, hb->id, hbtype_to_str(hb->type), hb->version);

    memset (&act, 0, sizeof(act));
    act.sa_handler = process_hb_signal;
    /* This thread should shut down on SIGTERM. */
    if (sigaction(SIGTERM, &act, NULL))
        log_entry(LEVEL_ERROR, "[%s] sigaction failed: %s", __func__, strerror(errno));

    sigemptyset(&mask);
    sigaddset(&mask, SIGTERM);

    if (sigprocmask(SIG_BLOCK, &mask, &orig_mask) < 0)
        log_entry(LEVEL_ERROR, "[%s] sigprocmask failed: %s", __func__, strerror(errno));

    request_diag_info(hb); /* Performed once at init */

    while(true)
    {
        int status;
        int track_fd;
        fd_set socket_set;
        struct timeval timeout;
#ifdef SUPPORT_TUNING
        time_t current;
#endif /* SUPPORT_TUNING */
        track_fd = 0;
        FD_ZERO(&socket_set);
        memset(&timeout, 0, sizeof(timeout));

        if (hb->fd > 0)
        {
            FD_SET(hb->fd, &socket_set);
            if (hb->fd > track_fd)
                track_fd = hb->fd;
        }

        timeout.tv_sec = PERIODIC_TIME;

        /* wait for activity on the file descriptors */
        status = select(track_fd + 1, &socket_set, NULL, NULL, &timeout);
#ifdef SUPPORT_TUNING
        current = time(NULL);
#endif /* SUPPORT_TUNING */
        if (status < 0)
        {
            log_entry(LEVEL_DEBUG, "[%s] select error: %s", __func__, strerror(errno));
            continue;
        }
        else if (status == 0)
        {
            log_entry(LEVEL_DEBUG, "[%s] select timeout", __func__);
            goto process_timeout;
        }

        if (FD_ISSET(hb->fd, &socket_set))
            process_hb_async_message(hb);

        if (hb->job_info.a1state == JOB_STOP)
            hb->job_info.a1state = JOB_COMPLETE;

        if (hb->job_info.a2state == JOB_STOP)
            hb->job_info.a2state = JOB_COMPLETE;

process_timeout:
#ifdef SUPPORT_TUNING
        if (first_job_start_time > 0)
        {
            if (startup_period_completed)
            {
                if ((current - last_performance_adjustment) >= PERFORMANCE_PERIOD)
                {
                    job_performance_adjust(station_config, hb);
                    last_performance_adjustment = current;
                    check_tuning(station_config);
                }
            }
            else
            {
                if ((current - first_job_start_time) >= STARTUP_PERIOD)
                {
                    log_entry(LEVEL_INFO, "[%s] startup period completed", __func__);
                    startup_period_completed = true;
                    job_performance_adjust(station_config, hb);
                    last_performance_adjustment = current;
                }
            }
        }
#else
        ; /* Nothing to do for now */
#endif /* SUPPORT_TUNING */
    }
    return NULL;
}

/***********************************************************
 * NAME: launch_hb_threads
 * DESCRIPTION: 
 *
 * IN:  HB node list
 * OUT: none
 ***********************************************************/
static void launch_hb_threads(
    struct list *list
)
{
    struct list_entry *entry;

    TAILQ_FOREACH(entry, &list->head, next_entry)
    {
        hb_node_t *hb = entry->data;
        int rc;

        if (hb)
        {
            pthread_mutex_init(&hb->mutex, NULL);
            rc = pthread_create(&(hb->thread_id), NULL, hbmonitor_thread, hb);
            if (rc < 0)
                log_entry(LEVEL_ERROR, "[%s] thread create failed: %s", __func__, strerror(errno));
        }
    }
}

static void get_hb_entry(
    xmlNodePtr hb,
    hb_params_t *entry
)
{
    xmlNodePtr current;

    for (current = hb->children; current != NULL; current = current->next)
    {
        xmlChar *value;

        value = xmlNodeGetContent(current);
        if (strcmp((char *)current->name, HASHRATE) == 0) {
            entry->hashrate = atoi((char *)value);
            log_entry(LEVEL_ERROR, "[%s] hashrate=%d", __func__, entry->hashrate);
        }
        else if (strcmp((char *)current->name, PERFORMANCE) == 0) {
            entry->performance = atoi((char *)value);
            log_entry(LEVEL_ERROR, "[%s] performance=%d", __func__, entry->performance);
        }
        else if (strcmp((char *)current->name, DIVIDER) == 0) {
            entry->divider = atoi((char *)value);
            log_entry(LEVEL_ERROR, "[%s] divider=%d", __func__, entry->divider);
        }
        else if (strcmp((char *)current->name, BIAS) == 0) {
            entry->bias = atoi((char *)value);
            log_entry(LEVEL_ERROR, "[%s] bias=%d", __func__, entry->bias);
        }
        else if (strcmp((char *)current->name, BIAS_OFFSETS) == 0)
        {
            xmlChar *curr = value;
            for (int i = 0; i < HB_ASIC_COUNT; i++)
            {
                entry->bias_offsets[i] = atoi((char *)curr);
                log_entry(LEVEL_ERROR, "[%s] bias_ofset[%d]=%d", __func__, i, entry->bias_offsets[i]);

                while (*curr != ',' && *curr != 0)
                {
                    curr++;
                }
                curr++; // Skip over the comma (or the null in the last iteration, but it's OK since we don't use that final pointer)
            }
        }

        if (value)
            xmlFree(value);
    }
}

/***********************************************************
 * NAME: get_hb_info
 * DESCRIPTION: this function opens the station info XML
 * file and reads its contents.
 *
 * IN:  station info pointer
 * OUT: none
 ***********************************************************/
static void get_hb_info(
    char *info_file,
    hb_info_t *info
)
{
    int fd;
    xmlDocPtr doc = NULL;
    xmlNodePtr rootElement;
    xmlNodePtr current;

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    /* init XML library parser */
    xmlInitParser();

    /* weird but needed by client code for libxml */
    LIBXML_TEST_VERSION

    /* Allow formatting of the XML file */
    xmlKeepBlanksDefault(0);

    /* Open and lock the XML file */
    fd = open(info_file, O_RDWR);
    if (fd < 0)
    {
        log_entry(LEVEL_ERROR, "[%s] unable to open %s", __func__, info_file);
        return;
    }

    /* Load the specified XML file */
    doc = xmlReadFile(info_file, NULL, XML_PARSE_RECOVER); 
    if (!doc)
    {
        log_entry(LEVEL_ERROR, "[%s] xmlReadFile error", __func__);
        close(fd);
        return;
    }

    info->hbcount = 0;
    
    /* Get the root element */
    rootElement = xmlDocGetRootElement(doc);
    log_entry(LEVEL_ERROR, "[%s] get_hb_info() # children = %d", __func__, rootElement->children);

    for (current = rootElement->children; current != NULL; current = current->next)
    {
        if (strcmp((char *)current->name, HB) == 0)
        {
            log_entry(LEVEL_ERROR, "[%s] Found <hb> child", __func__);

            get_hb_entry(current, &(info->hbparams[info->hbcount]));
            info->hbcount++;
            info->hbvalid = true;
        }
    }
    xmlFreeDoc(doc);
    close(fd);
}

/***********************************************************
 * NAME: clear_hb_info
 * DESCRIPTION: this function opens the station info XML
 * file and reads its contents.
 *
 * IN:  station info pointer
 * OUT: none
 ***********************************************************/
static void clear_hb_info(
    char *info_file,
    hb_info_t *info
)
{
    int fd;
    xmlDocPtr doc = NULL;
    xmlNodePtr rootElement;
    xmlNodePtr current;

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    /* init XML library parser */
    xmlInitParser();

    /* weird but needed by client code for libxml */
    LIBXML_TEST_VERSION

    /* Allow formatting of the XML file */
    xmlKeepBlanksDefault(0);

    /* Open and lock the XML file */
    fd = open(info_file, O_RDWR);
    if (fd < 0)
    {
        log_entry(LEVEL_ERROR, "[%s] unable to open %s", __func__, info_file);
        return;
    }

    /* Load the specified XML file */
    doc = xmlReadFile(info_file, NULL, XML_PARSE_RECOVER); 
    if (!doc)
    {
        log_entry(LEVEL_ERROR, "[%s] xmlReadFile error", __func__);
        close(fd);
        return;
    }

    /* Get the root element */
    rootElement = xmlDocGetRootElement(doc);
    for (current = rootElement->children; current != NULL; current = current->next)
    {
        if (strcmp((char *)current->name, HB) == 0)
        {
            xmlNodePtr param;
        
            for (param = current->children; param != NULL; param = param->next)
            {
                if (strcmp((char *)param->name, HASHRATE) == 0)
                    xmlNodeSetContent(param, (xmlChar *)"0");
                else if (strcmp((char *)param->name, PERFORMANCE) == 0)
                    xmlNodeSetContent(param, (xmlChar *)"2250");
                else if (strcmp((char *)param->name, DIVIDER) == 0)
                    xmlNodeSetContent(param, (xmlChar *)"1");
                else if (strcmp((char *)param->name, BIAS) == 0)
                    xmlNodeSetContent(param, (xmlChar *)"0");
                else if (strcmp((char *)param->name, BIAS_OFFSETS) == 0)
                    xmlNodeSetContent(param, (xmlChar *)"0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0");
            }
        }
    }
    xmlSaveFormatFileEnc(info_file, doc, "utf-8", 1);
    xmlFreeDoc(doc);
    close(fd);
}

void hbmonitor_clear_tuning(void)
{
    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    pthread_mutex_lock(&mutex);
    clear_hb_info(HB_INFO_FILE, &station_config->hb_info);
    backup_config();
    pthread_mutex_unlock(&mutex);
}

void hbmonitor_save_performance(void)
{
    log_entry(LEVEL_ERROR, "[%s] called", __func__);

    pthread_mutex_lock(&mutex);
    save_hb_info(HB_INFO_FILE, &station_config->hb_info);
    backup_config();
    pthread_mutex_unlock(&mutex);
}

void hbmonitor_set_performance(
    hb_node_t *hb,
    uint16_t performance
)
{
    log_entry(LEVEL_ERROR, "++++++++++++++[%s] called hb->id=%d, perf=%d", __func__, hb->id, performance);

    pthread_mutex_lock(&mutex);
    station_config->hb_info.hbparams[hb->id].performance = performance;
    hb->max_performance = hb->current_performance = performance;

    // Make sure that hbcount is big enough
    if (hb->id >= station_config->hb_info.hbcount) {
        station_config->hb_info.hbcount = hb->id + 1;
    }

    pthread_mutex_unlock(&mutex);
    job_performance_set(hb);

    // TODO: Would be better to queue up the performance changes and have a thread that manages the waits
    //       so that these requests return immediately.
    // sleep(30);  // Give the boards a chance to settle in
}

void hbmonitor_set_clock(
    hb_node_t *hb,
    int8_t bias,
    uint8_t divider
)
{
    log_entry(LEVEL_ERROR, "++++++++++++++[%s] called: hb->id=%d, divider=%d, bias=%d", __func__, hb->id, divider, bias);

    pthread_mutex_lock(&mutex);
    station_config->hb_info.hbparams[hb->id].divider = divider;
    station_config->hb_info.hbparams[hb->id].bias = bias;
    hb->clock_divider = divider;
    hb->clock_bias = bias;

    // Make sure that hbcount is big enough
    if (hb->id >= station_config->hb_info.hbcount) {
        station_config->hb_info.hbcount = hb->id + 1;
    }

    job_clock_set(hb, divider, bias);
    pthread_mutex_unlock(&mutex);
}

// Set the clock bias of a single ASIC
// void hbmonitor_set_asic_clock(
//     hb_node_t *hb,
//     uint8_t asic,
//     int8_t delta
// )
// {
//     log_entry(LEVEL_ERROR, "[%s] called", __func__);

//     pthread_mutex_lock(&mutex);
//     station_config->hb_info.hbparams[hb->id].bias_offsets[asic] = delta;

//     // Make sure that hbcount is big enough
//     if (hb->id >= station_config->hb_info.hbcount) {
//         station_config->hb_info.hbcount = hb->id + 1;
//     }

//     // TEMP: job_asic_clock_set(hb, asic, delta);
//     pthread_mutex_unlock(&mutex);
// }

/******************************************************************
 * Function: hbmonitor_send_message
 * Description: sends a synchronous message to to the hashing board
 * first locking the mutex to insure that no other message
 * processing can occur.
 * 
 * IN:  HB node pointer
 * 
 * 
 * Out: Returns 0 on success, and -1 on failure or timeout
 * 
 ******************************************************************/
int hbmonitor_send_message(
    hb_node_t *hb,
    uint8_t type,
    void *req,
    int req_len,
    void *rsp,
    int rsp_len
)
{
    uint8_t csum;
    uint8_t buf[MAX_HB_MSG_LEN];
    uint8_t msg_len = sizeof(hb_tlv_t) + req_len + CSUM_SIZE;
    hb_tlv_t msg;
    int bytes;

    memset(&msg, 0, sizeof(msg));
    memset(buf, 0, MAX_HB_MSG_LEN);
    msg.type = type;
    if (req_len > 0)
    {
        uint8_t *pos = buf;
        msg.len = req_len;
        memcpy(pos, &msg, sizeof(msg));
        pos += sizeof(msg);
        memcpy(pos, req, req_len);
        csum = hb_generate_checksum(buf);
    }
    else
    {
        csum = hb_generate_checksum(&msg);
        memcpy (buf, &msg, sizeof(msg));
    }
    buf[msg_len - 1] = csum;
    pthread_mutex_lock(&hb->mutex);
    /*
     * We are waiting 30 msecs after the write followed by a second write
     * of the register to try to improve reliability and then another 30 msec
     * wait to insure that no other write can occur in less than 30 msecs. We
     * hold the mutex though only through the second write.
     */
    bytes = write(hb->fd, buf, msg_len);
    if (bytes < 0)
        log_entry(LEVEL_ERROR, "[%s] send failed: %s", __func__, strerror(errno));
    else if (bytes == 0)
        log_entry(LEVEL_ERROR, "[%s] sent 0 bytes: %s", __func__, strerror(errno));
    pthread_mutex_unlock(&hb->mutex);
    usleep(30000); /* Ensure a minimum of 30 msecs before the next send */
    return 0;
}

/***********************************************************
 * NAME: hbmonitor_init
 * DESCRIPTION: initialize the hashing board monitor
 *
 * IN:  flag indicating whether HB threads should be started
 * OUT: HB node list
 ***********************************************************/
void hbmonitor_init(
    station_config_t *config
)
{
    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    memset(bad_job_retries, 0, sizeof(bad_job_retries));
    station_config = config;
    config->hb_list = hb_init();
    get_hb_info(HB_INFO_FILE, &config->hb_info);

    job_init(config);
    launch_hb_threads(config->hb_list);
}

/***********************************************************
 * NAME: hbmonitor_shutdown
 * DESCRIPTION: shutdown the hashing board monitor
 *
 * IN:  HB node list
 * OUT: none
 ***********************************************************/
void hbmonitor_shutdown(
    station_config_t *config
)
{
    struct list_entry *entry;

    log_entry(LEVEL_INFO, "[%s] called", __func__);

    if (config->hb_list == NULL)
        return;

    job_shutdown(config);

    pthread_mutex_lock(&mutex);
    TAILQ_FOREACH(entry, &config->hb_list->head, next_entry)
    {
        hb_node_t *hb = entry->data;

        if (hb)
        {
            pthread_kill(hb->thread_id, SIGTERM);
            job_stats_set(hb, 0);
        }
    }
    hb_shutdown(config->hb_list);
    pthread_mutex_unlock(&mutex);

}
