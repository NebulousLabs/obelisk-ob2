/*
 * Copyright (C) 2018 MapleLeaf Software, Inc
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * hbmonitor.h
 *
 * Dan Jozwiak: djozwiak@mlsw.biz
 * Date Created: 2018-10-28
 *
 * Description: this file contains the contents and data structures used
 * for hashing board communications.
 */

#ifndef _HBMONITOR_H_
#define _HBMONITOR_H_

#include "common.h"
#include "config.h"

extern void hbmonitor_init(station_config_t *config);
extern void hbmonitor_shutdown(station_config_t *config);
extern int  hbmonitor_send_message(hb_node_t *hb, uint8_t type, void *req, int req_len, void *rsp, int rsp_len);
extern void hbmonitor_clear_tuning(void);
extern void hbmonitor_save_performance(void);
extern void hbmonitor_set_performance(hb_node_t *hb, uint16_t performance);
extern void hbmonitor_set_clock(hb_node_t *hb, int8_t bias, uint8_t divider);
extern void hbmonitor_set_asic_clock(hb_node_t *hb, uint8_t asic, int8_t delta);

#endif /* _HBMONITOR_H_ */
