/*
 * Copyright (C) 2018 MapleLeaf Software, Inc
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * utils.c
 *
 * Dan Jozwiak: djozwiak@mlsw.biz
 * Date Created: 2018-12-17
 *
 * Description: Miscellaneous utilities.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>       
#include <sys/reboot.h>
#include <sys/resource.h>

#include "common.h"
#include "logger.h"
#include "info.h"
#include "config.h"
#include "ipc.h"
#include "hbmonitor.h"
#include "utils.h"

#define FACTORY_RESET "/usr/local/ob2/bin/factory_reset"

#define NTP_TIMESTAMP_DELTA 2208988800ull

#define LI(packet)   (uint8_t)((packet.li_vn_mode & 0xC0) >> 6) // (li   & 11 000 000) >> 6
#define VN(packet)   (uint8_t)((packet.li_vn_mode & 0x38) >> 3) // (vn   & 00 111 000) >> 3
#define MODE(packet) (uint8_t)((packet.li_vn_mode & 0x07) >> 0) // (mode & 00 000 111) >> 0

typedef struct
{
    uint8_t li_vn_mode;      // Eight bits. li, vn, and mode.
                             // li.   Two bits.   Leap indicator.
                             // vn.   Three bits. Version number of the protocol.
                             // mode. Three bits. Client will pick mode 3 for client.

    uint8_t stratum;         // Eight bits. Stratum level of the local clock.
    uint8_t poll;            // Eight bits. Maximum interval between successive messages.
    uint8_t precision;       // Eight bits. Precision of the local clock.

    uint32_t rootDelay;      // 32 bits. Total round trip delay time.
    uint32_t rootDispersion; // 32 bits. Max error aloud from primary clock source.
    uint32_t refId;          // 32 bits. Reference clock identifier.

    uint32_t refTm_s;        // 32 bits. Reference time-stamp seconds.
    uint32_t refTm_f;        // 32 bits. Reference time-stamp fraction of a second.

    uint32_t origTm_s;       // 32 bits. Originate time-stamp seconds.
    uint32_t origTm_f;       // 32 bits. Originate time-stamp fraction of a second.

    uint32_t rxTm_s;         // 32 bits. Received time-stamp seconds.
    uint32_t rxTm_f;         // 32 bits. Received time-stamp fraction of a second.

    uint32_t txTm_s;         // 32 bits and the most important field the client cares about. Transmit time-stamp seconds.
    uint32_t txTm_f;         // 32 bits. Transmit time-stamp fraction of a second.
} ntp_packet_t;

static char *ntpserver = "216.239.35.0"; /* time.google.com */

static const char *new_xml_defaults_file = NEW_XML_DEFAULTS_FILE;

/***********************************************************
 * NAME: is_time_synced
 * DESCRIPTION: this function will attempt to connect to an
 * NTP server to determine if our system time is sync'd. If
 * no connection is possible then we assume that we're not
 * synced. Once we do have a connection we should find that
 * time has been sync'd.
 *
 * IN:  none
 * OUT: true if time is sync'd...false otherwise
 ***********************************************************/
bool is_time_synced(void)
{
    int fd;
    int rc;
    uint16_t portno = 123;
    ntp_packet_t packet;
    ntp_packet_t *pkt;
    struct sockaddr_in serv_addr;
    time_t txTm;
    time_t current;
    fd_set fds;
    struct timeval timeout;
    socklen_t len;
    char buf[2048];

    memset(&packet, 0, sizeof(ntp_packet_t));

    /*
     * Set the first byte's bits to 00,011,011 for li = 0, vn = 3, and
     * mode = 3. The rest will be left set to zero.
     */
    packet.li_vn_mode = 0x1b;

    fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (fd < 0)
        return false;

    memset(&serv_addr, 0, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = inet_addr(ntpserver);
    serv_addr.sin_port = htons(portno);

    rc = sendto(fd, &packet, sizeof(ntp_packet_t), 0, (struct sockaddr *)&serv_addr, sizeof(serv_addr));
    if (rc < 0)
    {
        log_entry(LEVEL_ERROR, "[%s] sendto failed: %s", __func__, strerror(errno));
        return false;
    }

    FD_ZERO(&fds);
    FD_SET(fd, &fds);

    timeout.tv_sec = 2;
    timeout.tv_usec = 0;

    rc = select(fd + 1, &fds, NULL, NULL, &timeout);
    if (rc <= 0)
    {
        if (rc == 0)
            log_entry(LEVEL_INFO, "[%s] select timeout", __func__);
        else
            log_entry(LEVEL_INFO, "[%s] select error: %s", __func__, strerror(errno));
        return false;
    }

    if ((fd > 0) && (FD_ISSET(fd, &fds)))
    {
        rc = recvfrom(fd, buf, sizeof(buf), 0, (struct sockaddr *)&serv_addr, &len);
        if (rc < 0)
        {
            // log_entry(LEVEL_ERROR, "[%s] recvfrom failed: %s", __func__, strerror(errno));
            return false;
        }
        close(fd);

        pkt = (ntp_packet_t *)buf;

        /*
         * These two fields contain the time-stamp seconds as the packet left
         * the NTP server. The number of seconds correspond to the seconds
         * passed since 1900.
         */
        pkt->txTm_s = ntohl(pkt->txTm_s);
        pkt->txTm_f = ntohl(pkt->txTm_f);
    
        /*
         * Extract the 32 bits that represent the time-stamp seconds (since
         * NTP epoch) from when the packet left the server. Subtract 70
         * years worth of seconds from the seconds since 1900. This leaves
         * the seconds since the UNIX epoch of 1970.
         */
        txTm = (time_t)(pkt->txTm_s - NTP_TIMESTAMP_DELTA);
        current = time(NULL);
        log_entry(LEVEL_INFO, "[%s] current: %d, ntp: %d", __func__, current, txTm);
        if ((current <= (txTm + 2)) &&
            (current >= (txTm - 2)))
        {
            log_entry(LEVEL_INFO, "[%s] time is synced", __func__);
            return true;
        }
        else
            log_entry(LEVEL_INFO, "[%s] time mismatch: expected: %d, got: %d", __func__, current, txTm);
    }

    return false;
}

/***********************************************************
 * NAME: factory_reset
 * DESCRIPTION: fork off a process that will run the
 * factory_reset script. That process will shutdown all
 * application processes. Then the scriptnwill perform a
 * factory reset of the system followed by a reboot.
 *
 * IN:  none
 * OUT: none
 ***********************************************************/
void factory_reset(void)
{
    char cmd[256];
    char *argv[4];
    pid_t pid;

    sprintf(cmd, "%s", FACTORY_RESET);
    argv[0] = "/bin/sh";
    argv[1] = cmd;
    argv[2] = NULL;

    pid = fork();
    switch (pid)
    {
        case -1:
            log_entry(LEVEL_ERROR, "[%s] fork of %s failed\n", __func__, argv[0]);
        break;

        case 0:
        {
            execvp(argv[0], argv);
        }
        break;

        default:
        break;
    }
}

void process_new_xml_defaults(void)
{
    int rc;
    struct stat info;
    FILE *fp;

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    /* verify directory doesn't exist before attempting to create it */
    rc = stat(new_xml_defaults_file, &info);
    if (rc < 0)
    {
        log_entry(LEVEL_DEBUG, "[%s] xml defaults file %s does not exist", __func__, new_xml_defaults_file);
        return;
    }

    fp = fopen(new_xml_defaults_file, "r");
    if (fp == NULL)
    {
        log_entry(LEVEL_ERROR, "[%s] failed to open xml defaults file: %s", __func__, strerror(errno));
        return;
    }
    while (!feof(fp))
    {
        char *line = NULL;
        char *filename;
        char *datatype;
        char *tag;
        char *value;
        size_t len;

        rc = getline(&line, &len, fp);
        if (rc < 0)
            goto out;

        /* Discard comment lines */
        if (strstr(line, "#"))
            continue;

        /*
         * New defaults has the following format:
         * file:type:tag:value
         */
        filename = strtok(line, ":");
        datatype = strtok(NULL, ":");
        tag = strtok(NULL, ":");
        value = strtok(NULL, ":");
        if ((filename == NULL) || (datatype == NULL) || (tag == NULL) || (value == NULL))
            continue;
        /* Remove the newline if present from the value before processing */
        if (value[strlen(value) - 1] == 0x0a)
            value[strlen(value) - 1] = 0;
        log_entry(LEVEL_DEBUG, "[%s] %s:%s:%s:%s\n", __func__, filename, datatype, tag, value);
        if (strcmp(datatype, "num") == 0)
            info_update_xml_default_number(filename, tag, atoi(value));
        else if (strcmp(datatype, "str") == 0)
        {
            
            
            info_update_xml_default_string(filename, tag, value);
        }
        else
            log_entry(LEVEL_ERROR, "[%s] unknown data type: %s", __func__, datatype);
    }

out:
    fclose(fp);
    unlink(new_xml_defaults_file);
    backup_config();
    sync();
}

void system_reboot(
    station_config_t *config
)
{
    log_entry(LEVEL_WARNING, "[%s] executing system reboot...", __func__);
    sync();
#if 1
    system("/sbin/reboot");
#else
    reboot(RB_AUTOBOOT);
#endif
}

void backup_config(void)
{
    log_entry(LEVEL_DEBUG, "[%s] backing up configuration...", __func__);
    system("/usr/local/ob2/bin/backup-config");
    sync();
}

/***********************************************************
 * NAME: get_peak_rss
 * DESCRIPTION: returns the peak (maximum so far) resident
 * set size (physical memory use) measured in bytes, or zero
 *
 * IN:  none
 * OUT: none
 ***********************************************************/
size_t get_peak_rss(void)
{
    struct rusage rusage;
    getrusage(RUSAGE_SELF, &rusage);
    return (size_t)(rusage.ru_maxrss * 1024L);
}

/***********************************************************
 * NAME: get_current_rss
 * DESCRIPTION: returns the current resident set size
 * (physical memory use) measured in bytes or zero.
 *
 * IN:  none
 * OUT: none
 ***********************************************************/
size_t get_current_rss(void)
{
    long rss = 0L;
    int rc;
    FILE* fp = NULL;

    fp = fopen("/proc/self/statm", "r" );
    if (fp == NULL )
    {
        log_entry(LEVEL_ERROR, "[%s] can't open /proc/self/statm", __func__);
        return (size_t)0L;
    }

    rc = fscanf(fp, "%*s%ld", &rss);
    if (rc != 1)
    {
        log_entry(LEVEL_ERROR, "[%s] unable to read /proc/self/statm: %s", __func__, strerror(errno));
        fclose(fp);
        return (size_t)0L;
    }
    fclose(fp);
    return (size_t)rss * (size_t)sysconf(_SC_PAGESIZE);
}

/******************************************************************
 * Function: divider_value_to_enum
 * Description: Convert the divider value we show to the user to
 *              the enum used by the hashboards .
 *
 * IN:  Divider value
 * Out: Divider enum
 ******************************************************************/
uint8_t divider_value_to_enum(uint8_t v) {
    switch (v) {
        case 1: return 3;
        case 2: return 2;
        case 4: return 1;
        case 8: return 0;

        default: return 2;  // Don't push to max on a conversion error
    }
}

/******************************************************************
 * Function: divider_enum_to_value
 * Description: Convert the divider enum used by the hashboards into
 *              the actual clock divider value.
 *
 * IN:  Divider enum
 * Out: Divider value
 ******************************************************************/
uint8_t divider_enum_to_value(uint8_t e) {
    switch (e) {
        case 0: return 8;
        case 1: return 4;
        case 2: return 2;
        case 3: return 1;

        default: return 2;  // Don't push to max on a conversion error
    }
}
