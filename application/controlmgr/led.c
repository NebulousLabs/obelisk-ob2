/*
 * Copyright (C) 2018 MapleLeaf Software, Inc
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * led.c
 *
 * Dan Jozwiak: djozwiak@mlsw.biz
 * Date Created: 2018-12-15
 *
 * Description: 
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#define __USE_GNU
#include <pthread.h>

#include "common.h"
#include "logger.h"
#include "led.h"

static FILE *led1; /* Green */
static FILE *led2; /* Red   */

static pthread_t led_thread_id;

static void *led_flash(
    void *data
)
{
    station_config_t *config = data;
    uint32_t sleep_interval;
    uint32_t state = 0;

    log_entry(LEVEL_DEBUG, "[%s] started", __func__);

    pthread_setname_np(pthread_self(), "led_flash");

    switch (config->led_state.rate)
    {
        case RATE_FAST:   sleep_interval = 250000; break;
        case RATE_MEDIUM: sleep_interval = 500000; break;
        case RATE_SLOW:   sleep_interval = 1000000; break;
        default:          sleep_interval = 1000000; break;
    }

    led1 = fopen(LED1, "w");
    if (led1 == NULL)
    {
        log_entry(LEVEL_ERROR, "[%s] unable to open %s: %s", __func__, LED1, strerror(errno));
        return NULL;
    }
    led2 = fopen(LED2, "w");
    if (led2 == NULL)
    {
        log_entry(LEVEL_ERROR, "[%s] unable to open %s: %s", __func__, LED2, strerror(errno));
        fclose(led1);
        return NULL;
    }

    /* Turn both LED's off to start with */
    fprintf(led1, "%u", 0);
    fprintf(led2, "%u", 0);
    
    while (config->led_state.flashing)
    {
        state = (state == 1) ? 0:1;
        if (config->led_state.alternating)
        {
            if (state)
            {
                fprintf(led1, "%u", 1);
                fprintf(led2, "%u", 0);
            }
            else
            {
                fprintf(led1, "%u", 0);
                fprintf(led2, "%u", 1);
            }
        }
        else
        {
            switch (config->led_state.current)
            {
                case LED_GREEN:
                    fprintf(led1, "%u", state);
                    fprintf(led2, "%u", 0);
                break;
                case LED_RED:
                    fprintf(led1, "%u", 0);
                    fprintf(led2, "%u", state);
                break;
                case LED_BOTH:
                    fprintf(led1, "%u", state);
                    fprintf(led2, "%u", state);
                break;
                default:
                break;
            }
        }
        fflush(led1);
        fflush(led2);
        if (state)
            log_entry(LEVEL_DEBUG, "[%s] %s: %d", __func__, led_to_str(config->led_state.current), state);
        else
            log_entry(LEVEL_DEBUG, "[%s] %s: %d", __func__, led_to_str(config->led_state.current), state);
        usleep(sleep_interval);
    }

    fclose(led1);
    led1 = NULL;
    fclose(led2);
    led2 = NULL;
    pthread_exit(NULL);
}

static void led_solid(
    station_config_t *config,
    led_t state
)
{
    log_entry(LEVEL_DEBUG, "[%s] called: %s", __func__, led_to_str(state));

    led1 = fopen(LED1, "w");
    if (led1 == NULL)
    {
        log_entry(LEVEL_ERROR, "[%s] unable to open %s: %s", __func__, LED1, strerror(errno));
        return;
    }
    led2 = fopen(LED2, "w");
    if (led2 == NULL)
    {
        log_entry(LEVEL_ERROR, "[%s] unable to open %s: %s", __func__, LED2, strerror(errno));
        fclose(led1);
        return;
    }

    switch (state)
    {
        case LED_GREEN:
            fprintf(led1, "%u", 1);
            fprintf(led2, "%u", 0);
            config->led_state.current = LED_GREEN;
        break;
        case LED_RED:
            fprintf(led1, "%u", 0);
            fprintf(led2, "%u", 1);
            config->led_state.current = LED_RED;
        break;
        case LED_BOTH:
            fprintf(led1, "%u", 1);
            fprintf(led2, "%u", 1);
            config->led_state.current = LED_BOTH;
        break;
        case LED_OFF:
            fprintf(led1, "%u", 0);
            fprintf(led2, "%u", 0);
            config->led_state.current = LED_OFF;
        break;
        default:
        break;
    }
    fflush(led1);
    fflush(led2);
    fclose(led1);
    led1 = NULL;
    fclose(led2);
    led2 = NULL;
    config->led_state.flashing = false;
    config->led_state.rate = RATE_NONE;
}

void led_set(
    station_config_t *config,
    led_t state,
    led_rate_t rate,
    bool alternating
)
{
    int rc;

    if (config->led_state.flashing)
    {
        /* Stop the thread before changing the LED state */
        config->led_state.flashing = false;
        pthread_join(led_thread_id, NULL);
    }

    if (rate == RATE_NONE)
        led_solid(config, state);
    else
    {
        config->led_state.current = state;
        config->led_state.rate = rate;
        config->led_state.flashing = true;
        config->led_state.alternating = alternating;
        rc = pthread_create(&led_thread_id, NULL, led_flash, config);
        if (rc < 0)
            log_entry(LEVEL_ERROR, "[%s] thread create failed: %s", __func__, strerror(errno));
    }
}

void led_init(
    station_config_t *config
)
{
    log_entry(LEVEL_DEBUG, "[%s] called", __func__);
    config->led_state.current = LED_OFF;
    config->led_state.rate = RATE_NONE;
    config->led_state.flashing = false;
}

void led_shutdown(
    station_config_t *config
)
{
    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    if (led_thread_id > 0)
    {
        /* Stop the thread before changing the LED state */
        config->led_state.flashing = false;
        pthread_kill(led_thread_id, SIGTERM);
    }
}
