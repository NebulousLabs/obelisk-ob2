/*
 * Copyright (C) 2018 MapleLeaf Software, Inc
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * fan.c
 *
 * Dan Jozwiak: djozwiak@mlsw.biz
 * Date Created: 2018-10-16
 *
 * Description: this file contains the software necessary to perform fan
 * control.
 *
 */

#include <stdio.h>
#include <errno.h>

#include "common.h"
#include "logger.h"
#include "list.h"
#include "fan.h"

static FILE *front_enable;
static FILE *front_duty;
static FILE *rear_enable;
static FILE *rear_duty;

static uint32_t front_period;
static uint32_t rear_period;

static char *front_pwm_enable = "/sys/class/pwm/pwmchip0/pwm0/enable";
static char *front_pwm_duty   = "/sys/class/pwm/pwmchip0/pwm0/duty_cycle";
static char *front_pwm_period = "/sys/class/pwm/pwmchip0/pwm0/period";
static char *rear_pwm_enable  = "/sys/class/pwm/pwmchip1/pwm0/enable";
static char *rear_pwm_duty    = "/sys/class/pwm/pwmchip1/pwm0/duty_cycle";
static char *rear_pwm_period  = "/sys/class/pwm/pwmchip1/pwm0/period";

/***********************************************************
 * NAME: disable_front_pwm
 * DESCRIPTION: disable the front PWM
 *
 * IN:  none
 * OUT: none
 ***********************************************************/
static void disable_front_pwm(void)
{
    uint32_t value = 0;
    if (front_enable)
    {
        int rc = fprintf(front_enable, "%u", value);
        if (rc < 0)
            log_entry(LEVEL_ERROR, "[%s] failed to disable front pwm: %s", __func__, strerror(errno));
        fflush(front_enable);
    }
}

/***********************************************************
 * NAME: disable_rear_pwm
 * DESCRIPTION: disable the rear PWM
 *
 * IN:  none
 * OUT: none
 ***********************************************************/
static void disable_rear_pwm(void)
{
    uint32_t value = 0;
    if (rear_enable)
    {
        int rc = fprintf(rear_enable, "%u", value);
        if (rc < 0)
            log_entry(LEVEL_ERROR, "[%s] failed to disable rear pwm: %s", __func__, strerror(errno));
        fflush(rear_enable);
    }
}

/***********************************************************
 * NAME: enable_front_pwm
 * DESCRIPTION: enable the front PWM
 *
 * IN:  none
 * OUT: none
 ***********************************************************/
static void enable_front_pwm(void)
{
    uint32_t value = 1;
    if (front_enable)
    {
        int rc = fprintf(front_enable, "%u", value);
        if (rc < 0)
            log_entry(LEVEL_ERROR, "[%s] failed to enable front pwm: %s", __func__, strerror(errno));
        fflush(front_enable);
    }
}

/***********************************************************
 * NAME: enable_rear_pwm
 * DESCRIPTION: enable the rear PWM
 *
 * IN:  none
 * OUT: none
 ***********************************************************/
static void enable_rear_pwm(void)
{
    uint32_t value = 1;
    if (rear_enable)
    {
        int rc = fprintf(rear_enable, "%u", value);
        if (rc < 0)
            log_entry(LEVEL_ERROR, "[%s] failed to enable rear pwm: %s", __func__, strerror(errno));
        fflush(rear_enable);
    }
}

/***********************************************************
 * NAME: set_front_duty
 * DESCRIPTION: set the front PWM duty cycle
 *
 * IN:  percentage
 * OUT: none
 ***********************************************************/
static void set_front_duty(
    uint8_t percentage
)
{
    uint32_t duty = front_period * (100 - percentage)/100;
    if (front_duty)
    {
        int rc = fprintf(front_duty, "%u", duty);
        if (rc < 0)
            log_entry(LEVEL_ERROR, "[%s] failed to enable rear pwm: %s", __func__, strerror(errno));
        fflush(front_duty);
    }
}

/***********************************************************
 * NAME: set_rear_duty
 * DESCRIPTION: set the rear PWM duty cycle
 *
 * IN:  percentage
 * OUT: none
 ***********************************************************/
static void set_rear_duty(
    uint8_t percentage
)
{
    int duty = rear_period * (100 - percentage)/100;
    if (rear_duty)
    {
        int rc = fprintf(rear_duty, "%u", duty);
        if (rc < 0)
            log_entry(LEVEL_ERROR, "[%s] failed to enable rear pwm: %s", __func__, strerror(errno));
        fflush(rear_duty);
    }
}

/***********************************************************
 * NAME: fan_set_front
 * DESCRIPTION: set the front fan speed
 *
 * IN:  station configuration
 * OUT: 0 success, -1 failure
 ***********************************************************/
void fan_set_front(
    station_config_t *config,
    uint8_t value
)
{
    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    disable_front_pwm();
    set_front_duty(config->fan_setting_front);
    enable_front_pwm();
}

/***********************************************************
 * NAME: fan_set_rear
 * DESCRIPTION: set the rear fan speed
 *
 * IN:  station configuration
 * OUT: 0 success, -1 failure
 ***********************************************************/
void fan_set_rear(
    station_config_t *config,
    uint8_t value
)
{
    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    disable_rear_pwm();
    set_rear_duty(config->fan_setting_rear);
    enable_rear_pwm();
}

/***********************************************************
 * NAME: fan_init
 * DESCRIPTION: initialize the fan controller
 *
 * IN:  station configuration
 * OUT: 0 success, -1 failure
 ***********************************************************/
void fan_init(
    station_config_t *config
)
{
    int rc;
    FILE *pwm_period;

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    front_enable = fopen(front_pwm_enable, "w");
    if (front_enable == NULL)
    {
        log_entry(LEVEL_WARNING, "[%s] unable to open %s: %s", __func__, front_pwm_enable, strerror(errno));
        goto err;
    }
    front_duty = fopen(front_pwm_duty, "w");
    if (front_duty == NULL)
    {
        log_entry(LEVEL_WARNING, "[%s] unable to open %s: %s", __func__, front_pwm_duty, strerror(errno));
        goto err;
    }

    rear_enable = fopen(rear_pwm_enable, "w");
    if (rear_enable == NULL)
    {
        log_entry(LEVEL_WARNING, "[%s] unable to open %s: %s", __func__, rear_pwm_enable, strerror(errno));
        goto err;
    }
    rear_duty = fopen(rear_pwm_duty, "w");
    if (rear_duty == NULL)
    {
        log_entry(LEVEL_WARNING, "[%s] unable to open %s: %s", __func__, rear_pwm_duty, strerror(errno));
        goto err;
    }

    pwm_period = fopen(front_pwm_period, "r");
    if (pwm_period == NULL)
    {
        log_entry(LEVEL_WARNING, "[%s] unable to open %s: %s", __func__, front_pwm_period, strerror(errno));
        goto err;
    }
    rc = fscanf(pwm_period, "%u", &front_period);
    if (rc == 0)
    {
        log_entry(LEVEL_WARNING, "[%s] unable to read front pwm period: %s", __func__, strerror(errno));
        fclose(pwm_period);
        goto err;
    }
    fclose(pwm_period);

    pwm_period = fopen(rear_pwm_period, "r");
    if (pwm_period == NULL)
    {
        log_entry(LEVEL_WARNING, "[%s] unable to open %s: %s", __func__, rear_pwm_period, strerror(errno));
        goto err;
    }
    rc = fscanf(pwm_period, "%u", &rear_period);
    if (rc == EOF)
    {
        log_entry(LEVEL_WARNING, "[%s] unable to read rear pwm period: %s", __func__, strerror(errno));
        fclose(pwm_period);
        goto err;
    }
    fclose(pwm_period);

    config->fan_status_front = FAN_STATUS_GOOD;
    config->fan_status_rear = FAN_STATUS_GOOD;
    config->fan_setting_front = STARTUP_FAN_SPEED - FAN_SPEED_DELTA;
    config->fan_setting_rear = STARTUP_FAN_SPEED;
    fan_set_front(config, config->fan_setting_front);
    fan_set_rear(config, config->fan_setting_rear);

    return;

err:
    if (front_enable)
        fclose(front_enable);
    if (front_duty)
        fclose(front_duty);
    if (rear_enable)
        fclose(rear_enable);
    if (rear_duty)
        fclose(rear_duty);
    config->fan_status_front = FAN_STATUS_FAILED;
    config->fan_status_rear = FAN_STATUS_FAILED;
}

/***********************************************************
 * NAME: fan_shutdown
 * DESCRIPTION: shutdown the fan controller
 *
 * IN:  station configuration
 * OUT: 0 success, -1 failure
 ***********************************************************/
void fan_shutdown(
    station_config_t *config
)
{
    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    disable_front_pwm();
    disable_rear_pwm();

    if (front_enable)
        fclose(front_enable);
    if (front_duty)
        fclose(front_duty);
    if (rear_enable)
        fclose(rear_enable);
    if (rear_duty)
        fclose(rear_duty);
}
