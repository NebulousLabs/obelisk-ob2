//===========================================================================
// Copyright 2018 Obelisk Inc.
//===========================================================================

#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>

#include "logger.h"
#include "config.h"
#include "multicast.h"

#define MDNS_PORT 5353
#define MDNS_GROUP "224.0.0.251"

void send_udp(
    const char* group,
    int port,
    const char *packet,
    int mlen
)
{
    int fd;
    ssize_t rc;
    struct sockaddr_in addr;

    fd = socket(AF_INET, SOCK_DGRAM, 0);
    if (fd < 0)
    {
        log_entry(LEVEL_ERROR, "[%s] failed to open UDP socket: %s", __func__, strerror(errno));
        return;
    }

    memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = inet_addr(group);
    addr.sin_port = htons(port);

    rc = sendto(fd, packet, mlen, 0, (struct sockaddr*)&addr, sizeof(addr));
    if (rc < 0)
        log_entry(LEVEL_ERROR, "[%s] sending mDNS response: %s", __func__, strerror(errno));
    close(fd);
}

void _send_mdns_response(
    const char *name,
    const char *ipv4_address,
    uint32_t ttl
)
{
    /* Construct domain and record fields */
    char domain[256];
    uint16_t type = htons(1); /* 'A' record */
    uint16_t class = htons(1); /* Internet */
    uint32_t rdata = inet_addr(ipv4_address);
    uint16_t rdlen = htons(sizeof(rdata));
    char buf[1024];
    int n = 0;
    
    memset(domain, 0, sizeof(domain));
    domain[0] = strlen(name);
    strcpy(domain + 1, name);
    domain[1 + strlen(name)] = strlen("local");
    strcat(domain, "local");

    ttl = htonl(ttl);

    /* Construct DNS message */
    memset(&buf, 0, sizeof(buf));

    buf[n + 0] = 0xAA; /* id */
    buf[n + 1] = 0xAA; /* id */
    buf[n + 2] = 0x84; /* flags: qr = 1, aa = 1 */
    buf[n + 7] = 0x01; /* answer count */
    n = 12;
    memcpy(&buf[n], domain, strlen(domain) + 1);
    n += strlen(domain) + 1;
    memcpy(&buf[n], (char*)&type, sizeof(type));
    n += sizeof(type);
    memcpy(&buf[n], (char*)&class, sizeof(class));
    n += sizeof(class);
    memcpy(&buf[n], (char*)&ttl, sizeof(ttl));
    n += sizeof(ttl);
    memcpy(&buf[n], (char*)&rdlen, sizeof(rdlen));
    n += sizeof(rdlen);
    memcpy(&buf[n], (char*)&rdata, sizeof(rdata));
    n += sizeof(rdata);

    send_udp(MDNS_GROUP, MDNS_PORT, buf, n);
}

void send_mdns_response(
    station_config_t *config
)
{
    char name[80];

    sprintf(name, "Obelisk %s", (config->miner_type == HB_DCR) ? "DCR1 Slim":"SC1 Slim");
    _send_mdns_response(name, config->station_info.ipaddr, 10);
    log_entry(LEVEL_INFO, "[%s] issued mDNS response: %s, %s", __func__, name, config->station_info.ipaddr);
}
