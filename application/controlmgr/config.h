/*
 * Copyright (C) 2012-2018 MapleLeaf Software, Inc
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * config.h
 *
 * Dan Jozwiak: djozwiak@mlsw.biz
 * Date Created: 2012-04-13
 *
 * Description: Header file for configuration processing.
 */

#ifndef _CONFIG_H_
#define _CONFIG_H_
#include <pthread.h>

#include "hb.h"
#include "list.h"
#include "ipc_messages.h"
#include "poolminer.h"

typedef enum
{
    RATE_NONE,
    RATE_FAST,
    RATE_MEDIUM,
    RATE_SLOW,

    RATE_END
} led_rate_t;

typedef struct
{
    led_t current;
    bool flashing;
    bool alternating;
    led_rate_t rate;
} led_state_t;

typedef struct
{
    uint16_t hashrate;
    uint16_t performance;
    uint8_t divider;
    int8_t bias;
    int8_t bias_offsets[HB_ASIC_COUNT];
} hb_params_t;

typedef struct
{
    bool hbvalid;
    uint8_t hbcount;
    hb_params_t hbparams[MAX_HASHING_BOARDS];
} hb_info_t;

typedef struct
{
    station_info_t station_info;
    system_info_t system_info;
    hb_info_t hb_info;
    int ipc_fd;
    int button_fd;
    button_state_t button_state;
    time_t button_press;
    led_state_t led_state;
    bool internet_connection;
    bool burnin_in_progress;
    fan_status_t fan_status_front;
    fan_status_t fan_status_rear;
    uint8_t fan_setting_front;
    uint8_t fan_setting_rear;
    struct list *hb_list;
    uint32_t dcr_hashes_per_board;
    uint32_t sia_hashes_per_board;
    pool_stats_t pool_stats[MAX_POOLS];
    double pool_difficulty;
    char latest_release[MAX_STATIONRELEASE_LEN];
    char previous_release[MAX_STATIONRELEASE_LEN];
    time_t reboot_start_time;
    time_t time_until_reboot;
    hb_board_type_t miner_type;
} station_config_t;

extern void config_init(station_config_t *config);

#endif /* _CONFIG_H_ */
