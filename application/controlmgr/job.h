/*
 * Copyright (C) 2018 MapleLeaf Software, Inc
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * job.h
 *
 * Dan Jozwiak: djozwiak@mlsw.biz
 * Date Created: 2018-10-31
 *
 * Description: this file contains the contents and data structures used
 * for hashing board communications.
 */

#ifndef _JOB_H_
#define _JOB_H_

#ifdef  __cplusplus
extern "C" {
#endif

#include "config.h"

extern uint32_t min_bytes_received;
extern uint32_t max_bytes_received;
extern hb_dcr_work_req_t last_dcr_request;
extern hb_sia_work_req_t last_sia_request;

extern void job_init(station_config_t *config);
extern void job_shutdown(station_config_t *config);
extern void job_stop(hb_node_t *hb);
extern void job_performance_set(hb_node_t *hb);
#ifdef SUPPORT_TUNING
extern void job_performance_adjust(station_config_t *config, hb_node_t *hb);
#endif /* SUPPORT_TUNING */
extern void job_clock_set(hb_node_t *hb, uint8_t divider, int8_t bias);
extern void job_asic_clock_set(hb_node_t *hb, uint8_t asic, int8_t delta);
extern void job_stats_set(hb_node_t *hb, uint8_t state);
extern void job_dcr_test(station_config_t *config);
extern void job_dcr_program_request(station_config_t *config, hb_dcr_work_req_t *request);
extern void job_dcr_reprogram_request(station_config_t *config, hb_node_t *hb, uint8_t job_id);
extern void job_dcr_difficulty_set(station_config_t *config, double difficulty);
extern void job_dcr_hash_limit_set(station_config_t *config, uint32_t limit);
extern void job_dcr_stop(station_config_t *config);
extern void job_sia_test(station_config_t *config);
extern void job_sia_program_request(station_config_t *config, hb_sia_work_req_t *request);
extern void job_sia_reprogram_request(station_config_t *config, hb_node_t *hb, uint8_t job_id);
extern void job_sia_difficulty_set(station_config_t *config, double difficulty);
extern void job_sia_stop(station_config_t *config);

#ifdef  __cplusplus
}
#endif

#endif /* _JOB_H_ */
