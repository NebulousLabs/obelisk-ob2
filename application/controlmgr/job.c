/*
 * Copyright (C) 2018-2019 MapleLeaf Software, Inc
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * job.c
 *
 * Dan Jozwiak: djozwiak@mlsw.biz
 * Date Created: 2018-10-31
 *
 * Description: 
 *
 */

#include <unistd.h>
#define __USE_GNU
#include <pthread.h>

#include "common.h"
#include "logger.h"
#include "hb.h"
#include "hbmonitor.h"
#include "ipc.h"
#include "fan.h"
#include "job.h"
#include "utils.h"

#if 0
static uint32_t dcr_test_registers[] = 
{
    0x7ba9a5fe, /* V-registers */
    0x5cf6a9c1,
    0xc91ec363,
    0x1981907c,
    0xfe6c5bdb,
    0x51adb96c,
    0x79c738e8,
    0x209faad8,
    0x243f6a88,
    0x85a308d3,
    0x13198a2e,
    0x03707344,
    0xA4093D82, //0xa4093822,
    0x299F3470, //0x299f31d0,
    0x082efa98,
    0xec4e6c89,
    0x6a880400, /* M-registers */
    0xa4210000,
    0x1726035c,
    0x600081a6,
    0x00000000,
    0x00000000,
    0x00000000,
    0x00000000,
    0x00000000,
    0x00000000,
    0x00000000,
    0x05000000,
    0x00000038, /* LIM */
    0x209faad8, /* Match register */
    0x00000000, /* LBR */
    0xffffffff, /* UBR */
};
#else
static uint32_t dcr_test_registers[] = 
{
    0x51F87D90, /* V-registers */
    0x660D07D4,
    0xA697C2B7,
    0xC52BB4EF,
    0xBB2C0B5E,
    0x41B8CB4C,
    0x9254A116,
    0xE2B2CC34,
    0x243F6A88,
    0x85A308D3,
    0x13198A2E,
    0x03707344,
    0xA4093D82,
    0x299F3470,
    0x082EFA98,
    0xEC4E6C89,
    0x6A5C0200, /* M-registers */
    0x39200000,
    0x74017B59,
    0x00000005,
    0x00000000,
    0x00000000,
    0x00000000,
    0x00000000,
    0x00000000,
    0x00000000,
    0x00000000,
    0x04000000,
    0x00000038, /* LIM */
    0xE2B2CC34, /* Match register */
    0x00000000, /* LBR */
    0xFFFFFFFF, /* UBR */
};
#endif
static int total_dcr_test_registers = sizeof(dcr_test_registers)/4;

#if 1
static uint64_t sia_test_registers[] = 
{
    0x0700000000000000, /* M-registers */
    0x186d6ab432932edc,
    0xaff6adc6c6229c62,
    0xb66bf114e15d538f,
    0x0000000000000000,
    0x000000005c1d39a7,
    0x524883353409e6fe,
    0xba8e84873068319b,
    0xe3070520f228abd1,
    0xc887bc942b76ad0c,
#if 0 /* In 3.14 these are hard-coded in the HB */
    0x6A09E667F2BDC928, /* V-registers */
    0xBB67AE8584CAA73B,
    0x3C6EF372FE94F82B,
    0xA54FF53A5F1D36F1,
    0x510E527FADE682D1,
    0x9B05688C2B3E6C1F,
    0x1F83D9ABFB41BD6B,
    0x5BE0CD19137E2179,
    0x6A09E667F3BCC908,
    0xBB67AE8584CAA73B,
    0x3C6EF372FE94F82B,
    0xA54FF53A5F1D36F1,
    0x510E527FADE68281,
    0x9B05688C2B3E6C1F,
    0xE07C265404BE4294,
    0x5BE0CD19137E2179,
    0x6A09E667F2BDC928, /* Match register */
    0x00000000FFFFFFFF, /* Match register */
    0x0000000000001009, /* Step register  */
#endif
};
#else
static uint64_t sia_test_registers[] = 
{
    0x1E63000000000000, /* M-registers */
    0xF78B797E5D753F6F,
    0xE0E21B09975ADACE,
    0x923C26B1B437E95D,
    0x0000000000000000,
    0x0000000056307FC8,
    0x1023C71D7FB4A5EB,
    0x459779BFC18FFED4,
    0x10EEA15DE88CAF87,
    0xCC2F2CEDD68DB7C6,
#if 0 /* In 3.14 these are hard-coded in the HB */
    0x6A09E667F2BDC928, /* V-registers */
    0xBB67AE8584CAA73B,
    0x3C6EF372FE94F82B,
    0xA54FF53A5F1D36F1,
    0x510E527FADE682D1,
    0x9B05688C2B3E6C1F,
    0x1F83D9ABFB41BD6B,
    0x5BE0CD19137E2179,
    0x6A09E667F3BCC908,
    0xBB67AE8584CAA73B,
    0x3C6EF372FE94F82B,
    0xA54FF53A5F1D36F1,
    0x510E527FADE68281,
    0x9B05688C2B3E6C1F,
    0xE07C265404BE4294,
    0x5BE0CD19137E2179,
    0x6A09E667F2BDC928, /* Match register */
    0x00000000FFFFFFFF, /* Match register */
    0x0000000000001009, /* Step register */
#endif
};
#endif
static int total_sia_test_registers = sizeof(sia_test_registers)/8;

extern time_t first_job_start_time;

uint32_t min_bytes_received = 1024;
uint32_t max_bytes_received = 0;

hb_dcr_work_req_t last_dcr_request;

hb_sia_work_req_t last_sia_request;

static pthread_mutex_t job_mutex = PTHREAD_MUTEX_INITIALIZER;

/***********************************************************
 * NAME: job_dcr_register_set
 * DESCRIPTION: 
 *
 * IN:  HB node pointer
 * OUT: none
 ***********************************************************/
static int job_dcr_register_set(
    hb_node_t *hb,
    uint8_t reg,
    uint32_t value
)
{
    int rc;
    hb_dcr_register_t req;

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    req.reg = reg;
    req.value = htobe32(value);
    rc = hbmonitor_send_message(hb, MSG_TYPE_HB_JREG_WRITE, &req, sizeof(req), NULL, 0);
    if (rc < 0)
    {
        log_entry(LEVEL_ERROR, "[%s] failed to write register for HB%d", __func__, hb->id);
        return -1;
    }
    return 0;
}

/***********************************************************
 * NAME: job_sia_register_set
 * DESCRIPTION: 
 *
 * IN:  HB node pointer
 * OUT: none
 ***********************************************************/
static int job_sia_register_set(
    hb_node_t *hb,
    uint8_t reg,
    uint64_t value
)
{
    int rc;
    hb_sia_register_t req;

    // log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    req.reg = reg;
    req.value = htobe64(value);
    rc = hbmonitor_send_message(hb, MSG_TYPE_HB_JREG_WRITE, &req, sizeof(req), NULL, 0);
    if (rc < 0)
    {
        log_entry(LEVEL_ERROR, "[%s] failed to write register for HB%d", __func__, hb->id);
        return -1;
    }
    return 0;
}

/***********************************************************
 * NAME: job_mgmt_set
 * DESCRIPTION: 
 *
 * IN:  HB node pointer
 * OUT: none
 ***********************************************************/
static int job_mgmt_set(
    hb_node_t *hb,
    uint32_t hash1_start,
    uint32_t hash1_end,
    uint32_t hash2_start,
    uint32_t hash2_end
)
{
    int rc;
    hb_command_t req;

    // log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    // log_entry(LEVEL_DEBUG, "[%s] Setting up HB%d for job %d", __func__, hb->id, hb->job_info.id);
    // log_entry(LEVEL_DEBUG, "[%s] \thash1 start: %08x", __func__, hash1_start);
    // log_entry(LEVEL_DEBUG, "[%s] \t  hash1 end: %08x", __func__, hash1_end);
    // log_entry(LEVEL_DEBUG, "[%s] \thash2 start: %08x", __func__, hash2_start);
    // log_entry(LEVEL_DEBUG, "[%s] \t  hash2 end: %08x", __func__, hash2_end);

    req.cmd = HB_JOB_CMD_JOB_ID;
    req.value = hb->job_info.id;
    rc = hbmonitor_send_message(hb, MSG_TYPE_HB_JMAN_WRITE, &req, sizeof(req), NULL, 0);
    if (rc < 0)
    {
        log_entry(LEVEL_ERROR, "[%s] failed to command job ID for HB%d", __func__, hb->id);
        goto err;
    }
    req.cmd = HB_JOB_CMD_JOB_HASH1_START;
    req.value = htobe32(hash1_start);
    rc = hbmonitor_send_message(hb, MSG_TYPE_HB_JMAN_WRITE, &req, sizeof(req), NULL, 0);
    if (rc < 0)
    {
        log_entry(LEVEL_ERROR, "[%s] failed to command hash1 start for HB%d", __func__, hb->id);
        goto err;
    }
    req.cmd = HB_JOB_CMD_JOB_HASH1_END;
    req.value = htobe32(hash1_end);
    rc = hbmonitor_send_message(hb, MSG_TYPE_HB_JMAN_WRITE, &req, sizeof(req), NULL, 0);
    if (rc < 0)
    {
        log_entry(LEVEL_ERROR, "[%s] failed to command hash1 end for HB%d", __func__, hb->id);
        goto err;
    }
    req.cmd = HB_JOB_CMD_JOB_HASH2_START;
    req.value = htobe32(hash2_start);
    rc = hbmonitor_send_message(hb, MSG_TYPE_HB_JMAN_WRITE, &req, sizeof(req), NULL, 0);
    if (rc < 0)
    {
        log_entry(LEVEL_ERROR, "[%s] failed to command hash2 start for HB%d", __func__, hb->id);
        goto err;
    }
    req.cmd = HB_JOB_CMD_JOB_HASH2_END;
    req.value = htobe32(hash2_end);
    rc = hbmonitor_send_message(hb, MSG_TYPE_HB_JMAN_WRITE, &req, sizeof(req), NULL, 0);
    if (rc < 0)
    {
        log_entry(LEVEL_ERROR, "[%s] failed to command hash2 end for HB%d", __func__, hb->id);
        goto err;
    }
    return 0;

err:
    return -1;
}

/***********************************************************
 * NAME: job_start
 * DESCRIPTION: 
 *
 * IN:  HB node pointer
 * OUT: none
 ***********************************************************/
static void job_start(
    hb_node_t *hb,
    uint32_t signature
)
{
    int rc;
    hb_signature_t req;

    log_entry(LEVEL_DEBUG, "[%s] job start HB%d: %08x", __func__, hb->id, signature);
    req.signature = htobe32(signature);
    rc = hbmonitor_send_message(hb, MSG_TYPE_HB_START_JOB, &req, sizeof(req), NULL, 0);
    if (rc < 0)
        log_entry(LEVEL_ERROR, "[%s] failed to start job for HB%d", __func__, hb->id);
    else
        hb->job_info.a1state = hb->job_info.a2state = JOB_ACTIVE;
}

/***********************************************************
 * NAME: job_stop
 * DESCRIPTION: 
 *
 * IN:  HB node pointer
 * OUT: none
 ***********************************************************/
void job_stop(
    hb_node_t *hb
)
{
    int rc;

    rc = hbmonitor_send_message(hb, MSG_TYPE_HB_ABORT_JOB, NULL, 0, NULL, 0);
    if (rc < 0)
        log_entry(LEVEL_ERROR, "[%s] failed to abort job for HB%d", __func__, hb->id);
    hb->job_info.a1state = hb->job_info.a2state = JOB_IDLE;
}

/***********************************************************
 * NAME: job_performance_set
 * DESCRIPTION: 
 *
 * IN:  HB node pointer
 * OUT: none
 ***********************************************************/
void job_performance_set(
    hb_node_t *hb
)
{
    hb_set_performance_t req;
    int rc;

    log_entry(LEVEL_ERROR, "[%s] setting HB%d performance: %d", __func__, hb->id, hb->current_performance);

    req.value = htobe32(hb->current_performance);
    rc = hbmonitor_send_message(hb, MSG_TYPE_HB_SET_PERF, &req, sizeof(req), NULL, 0);
    if (rc < 0)
        log_entry(LEVEL_ERROR, "[%s] failed to set performance for HB%d", __func__, hb->id);
    else
        ipc_notify_hb_change(hb, NOTIFICATION_HB_PERF);
}

#ifdef SUPPORT_TUNING
/***********************************************************
 * NAME: job_performance_adjust
 * DESCRIPTION: 
 *
 * IN:  HB node pointer
 * OUT: none
 ***********************************************************/
void job_performance_adjust(
    station_config_t *config,
    hb_node_t *hb
)
{
    hb_set_performance_t req;
    int rc;

    log_entry(LEVEL_DEBUG, "[%s] called for HB%d", __func__, hb->id);

    if (hb->tuned)
    {
        if (hb->current_performance == hb->max_performance)
            return;
        else
            hb->current_performance += HB_INC_RAMP;
    }
    else
    {
        if ((time(NULL) - first_job_start_time) >= 1800)
        {
            /*
             * Evaluate where we are now vs our best performance and,
             * if our best is better then we are now, use the
             * performance number that we captured at our best
             * hashrate as our new maximum.
             */
            if (hb->job_info.current_hashrate_5min < hb->job_info.perf_info.best_hashrate)
            {
                hb->max_performance = hb->job_info.perf_info.performance;
                hb->tuned = true;
                config->hb_info.hbparams[hb->id].hashrate = (uint16_t)hb->job_info.perf_info.best_hashrate;
                config->hb_info.hbparams[hb->id].performance = hb->job_info.perf_info.performance;
                log_entry(LEVEL_INFO, "[%s] HB%d tuned to %lu", __func__, hb->id, hb->max_performance);
                goto tuned;
            }
        }

        if (hb->job_info.last_hashrate_5min == 0)
            hb->current_performance = config->hb_info.hbparams[hb->id].performance;
        else if (hb->job_info.current_hashrate_5min >= hb->job_info.last_hashrate_5min)
        {
            if (hb->job_info.current_hashrate_5min > hb->job_info.perf_info.best_hashrate)
            {
                hb->job_info.perf_info.best_hashrate = hb->job_info.current_hashrate_5min;
                hb->job_info.perf_info.performance = hb->current_performance;
            }
            hb->current_performance += HB_INC_PERFORMANCE;
        }
        else
        {
            uint32_t delta = hb->peak_performance - hb->current_performance;

            /*
             * Don't make an adjustment if a pool clean request
             * occurred. The lower hash rate is probably a result of
             * the clean and so a downward adjustment would
             * only make it worse.
             */
            if (hb->clean_occurred)
            {
                hb->clean_occurred = false;
                return;
            }

            if ((delta > 0) && (delta >= (2 * HB_DEC_PERFORMANCE)))
                hb->current_performance += HB_DEC_PERFORMANCE;
            else
                hb->current_performance -= HB_DEC_PERFORMANCE;
        }
    }
    if (hb->current_performance > hb->max_performance)
        hb->current_performance = hb->max_performance;
    else if (hb->current_performance < HB_MIN_PERFORMANCE)
        hb->current_performance = HB_MIN_PERFORMANCE;

    if (hb->current_performance > hb->peak_performance)
        hb->peak_performance = hb->current_performance;
tuned:
    hb->job_info.last_hashrate_5min = hb->job_info.current_hashrate_5min;

    log_entry(LEVEL_INFO, "[%s]   current_hashrate_5min: %0.2f GH/s", __func__, hb->job_info.current_hashrate_5min);
    log_entry(LEVEL_INFO, "[%s] sending HB%d performance: %d", __func__, hb->id, hb->current_performance);

    req.value = htobe32(hb->current_performance);
    rc = hbmonitor_send_message(hb, MSG_TYPE_HB_SET_PERF, &req, sizeof(req), NULL, 0);
    if (rc < 0)
        log_entry(LEVEL_ERROR, "[%s] failed to set performance for HB%d", __func__, hb->id);
    else
        ipc_notify_hb_change(hb, NOTIFICATION_HB_PERF);
}
#endif /* SUPPORT_TUNING */

/***********************************************************
 * NAME: job_clock_set
 * DESCRIPTION: 
 *
 * IN:  HB node pointer, bias, divider
 * OUT: none
 ***********************************************************/
void job_clock_set(
    hb_node_t *hb,
    uint8_t divider,
    int8_t bias
)
{
    int rc;
    hb_set_clock_t req;

    log_entry(LEVEL_ERROR, "[%s] called: divider=%d bias=%d", __func__, divider, bias);

    req.bias = bias;
    req.divider = divider_value_to_enum(divider);  // Convert to what the boards understand
    rc = hbmonitor_send_message(hb, MSG_TYPE_HB_SET_CLKS, &req, sizeof(req), NULL, 0);
    if (rc < 0)
        log_entry(LEVEL_ERROR, "[%s] failed to set clock for HB%d", __func__, hb->id);
}

/***********************************************************
 * NAME: job_asic_clock_set
 * DESCRIPTION: 
 *
 * IN:  HB node pointer, bias, divider
 * OUT: none
 ***********************************************************/
void job_asic_clock_set(
    hb_node_t *hb,
    uint8_t asic,
    int8_t delta
)
{
    int rc;
    hb_set_asic_clock_t req;

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    req.asic = asic;
    req.clock_delta = delta;
    rc = hbmonitor_send_message(hb, MSG_TYPE_HB_SET_ASIC_CLK, &req, sizeof(req), NULL, 0);
    if (rc < 0)
        log_entry(LEVEL_ERROR, "[%s] failed to set ASIC clock for HB%d", __func__, hb->id);
}

/***********************************************************
 * NAME: job_stats_set
 * DESCRIPTION: 
 *
 * IN:  HB node pointer
 * OUT: none
 ***********************************************************/
void job_stats_set(
    hb_node_t *hb,
    uint8_t state
)
{
    hb_set_stats_state_t req;
    int rc;

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    req.state = state;
    log_entry(LEVEL_INFO, "[%s] setting stats message state for HB%d: %d", __func__, hb->id, state);
    rc = hbmonitor_send_message(hb, MSG_TYPE_HB_SET_STATS, &req, sizeof(req), NULL, 0);
    if (rc < 0)
        log_entry(LEVEL_ERROR, "[%s] failed to set stats state for HB%d", __func__, hb->id);
}

/***********************************************************
 * NAME: job_dcr_difficulty_set
 * DESCRIPTION: 
 *
 * IN:  HB node pointer
 * OUT: none
 ***********************************************************/
void job_dcr_difficulty_set(
    station_config_t *config,
    double difficulty
)
{
    int rc;
    hb_difficulty_t req;
    struct list_entry *entry;
    
    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    req.value = htobe64((uint64_t)difficulty);
    log_entry(LEVEL_DEBUG, "[%s] sending DCR difficulty: %.2f", __func__, difficulty);
    pthread_mutex_lock(&job_mutex);
    TAILQ_FOREACH(entry, &config->hb_list->head, next_entry)
    {
        hb_node_t *hb = entry->data;
        if (hb && (hb->type == HB_DCR))
        {
            rc = hbmonitor_send_message(hb, MSG_TYPE_HB_SET_POOLD, &req, sizeof(req), NULL, 0);
            if (rc < 0)
                log_entry(LEVEL_ERROR, "[%s] failed to set difficulty for HB%d", __func__, hb->id);
        }
    }
    pthread_mutex_unlock(&job_mutex);
}

/***********************************************************
 * NAME: job_dcr_hash_limit_set
 * DESCRIPTION: 
 *
 * IN:  HB node pointer
 * OUT: none
 ***********************************************************/
void job_dcr_hash_limit_set(
    station_config_t *config,
    uint32_t limit
)
{
    config->dcr_hashes_per_board = limit / config->hb_list->size;
    log_entry(LEVEL_INFO, "[%s] hash limit: %08x, per-board: %08x", __func__, limit, config->dcr_hashes_per_board);
}

/***********************************************************
 * NAME: job_dcr_stop
 * DESCRIPTION: 
 *
 * IN:  station config pointer
 * OUT: none
 ***********************************************************/
void job_dcr_stop(
    station_config_t *config
)
{
    int rc;
    struct list_entry *entry;

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    pthread_mutex_lock(&job_mutex);
    TAILQ_FOREACH(entry, &config->hb_list->head, next_entry)
    {
        hb_node_t *hb = entry->data;
        if (hb && (hb->type == HB_DCR))
        {
            rc = hbmonitor_send_message(hb, MSG_TYPE_HB_ABORT_JOB, NULL, 0, NULL, 0);
            if (rc < 0)
                log_entry(LEVEL_ERROR, "[%s] failed to stop job for HB%d", __func__, hb->id);
            hb->job_info.a1state = hb->job_info.a2state = JOB_IDLE;
            hb->job_info.end = time(NULL);
        }
    }
    pthread_mutex_unlock(&job_mutex);
}

void job_dcr_test(
    station_config_t *config
)
{
    int i;
    int rc;
    struct list_entry *entry;
    bool job_started = false;
    uint32_t hash1_start = 0;
    uint32_t hash1_end = 0;
    uint32_t hash2_start = 0;
    uint32_t hash2_end = 0;
    uint32_t hashes_per_bank = config->dcr_hashes_per_board/2 + 1;

    log_entry(LEVEL_DEBUG, "[%s] test job setup...", __func__);

    min_bytes_received = max_bytes_received = 0;

    TAILQ_FOREACH(entry, &config->hb_list->head, next_entry)
    {
        hb_node_t *hb = entry->data;
        if (hb && (hb->type == HB_DCR))
        {
            uint32_t signature = 0;
            static uint8_t job_id = 1;

            if (!hb->powered)
            {
                log_entry(LEVEL_WARNING, "[%s] cannot use HB%d...not powered", __func__, hb->id);
                continue;
            }

            hb->job_info.id = job_id++;
            hb->job_info.a1state = hb->job_info.a2state = JOB_PENDING;
            hb->job_info.total_nonces = 0;
            hb->job_info.total_crc_errors = 0;

            for (i = 0; i < total_dcr_test_registers; ++i)
            {
                log_entry(LEVEL_DEBUG, "[%s] setting DCR register %d: %08x", __func__, i, dcr_test_registers[i]);
                rc = job_dcr_register_set(hb, i, dcr_test_registers[i]);
                if (rc < 0)
                {
                    log_entry(LEVEL_ERROR, "[%s] failed to set DCR register %d", __func__, i);
                    goto next;
                }
                signature += dcr_test_registers[i];
            }

            if (hash1_start == 0)
                hash1_end = hashes_per_bank - 1;
            else
                hash1_end = hash1_start + hashes_per_bank - 1;
            hash2_start = hash1_end + 1;
            hash2_end = hash2_start + hashes_per_bank - 1;
            rc = job_mgmt_set(hb, hash1_start, hash1_end, hash2_start, hash2_end);
            if (rc < 0)
            {
                log_entry(LEVEL_ERROR, "[%s] failed to set management configuration", __func__);
                goto next;
            }
            signature += hb->job_info.id + hash1_start + hash1_end + hash2_start + hash2_end;

            hash1_start = hash2_end + 1;
            job_start(hb, signature);
            hb->job_info.start = time(NULL);
            job_started = true;
        }
next:
        continue;
    }

    if (job_started)
        log_entry(LEVEL_INFO, "[%s] DCR test job started", __func__);
}

void job_dcr_program_request(
    station_config_t *config,
    hb_dcr_work_req_t *request
)
{
    int i;
    int rc;
    struct list_entry *entry;
    bool job_started = false;
    uint32_t hash1_start = 0;
    uint32_t hash1_end = 0;
    uint32_t hash2_start = 0;
    uint32_t hash2_end = 0;
    uint32_t hashes_per_bank = config->dcr_hashes_per_board/2 + 1;

//#define FAKE_BAD_JOB
#ifdef FAKE_BAD_JOB
    static uint8_t job_count = 0;
    static uint8_t hb_fail = 0;
    bool failure_injected = false;
#endif /* FAKE_BAD_JOB */
    log_entry(LEVEL_DEBUG, "[%s] started - job ID: %d", __func__, request->job_id);

    pthread_mutex_lock(&job_mutex);
    memcpy(&last_dcr_request, request, sizeof(last_dcr_request));
#ifdef FAKE_BAD_JOB
    ++job_count;
#endif /* FAKE_BAD_JOB */
    TAILQ_FOREACH(entry, &config->hb_list->head, next_entry)
    {
        hb_node_t *hb = entry->data;
        if (hb && (hb->type == HB_DCR))
        {
            uint32_t signature = 0;

            if (!hb->powered)
            {
                log_entry(LEVEL_WARNING, "[%s] HB%d not powered...attempting to power up...", __func__, hb->id);
                if ((config->fan_status_front == FAN_STATUS_GOOD) &&
                    (config->fan_status_rear == FAN_STATUS_GOOD))
                {
                    /*
                     * Before we power on the arrays make sure that the
                     * fans are at a reasonable speed.
                     */
                    if (!hb->tuned)
                    {
                        config->fan_setting_front = TUNING_FAN_SPEED - FAN_SPEED_DELTA;
                        config->fan_setting_rear = TUNING_FAN_SPEED;
                    }
                    else
                    {
                        config->fan_setting_front = config->system_info.fanspeed - FAN_SPEED_DELTA;
                        config->fan_setting_rear = config->system_info.fanspeed;
                    }
                    fan_set_front(config, config->fan_setting_front);
                    fan_set_rear(config, config->fan_setting_rear);

                    rc = hbmonitor_send_message(hb, MSG_TYPE_HB_PWR_UP, NULL, 0, NULL, 0);
                    if (rc < 0)
                        log_entry(LEVEL_ERROR, "[%s] failed to power up HB%d", __func__, hb->id);
                    else
                    {
                        log_entry(LEVEL_INFO, "[%s] HB%d array power up complete", __func__, hb->id);
                        hb->powered = true;
                        sleep(10);
                        job_stats_set(hb, 1);
                        goto setup;
                    }
                    continue;
                }
                else
                {
                    log_entry(LEVEL_ERROR, "[%s] failed fan status...will not power up HB%d", __func__, hb->id);
                    continue;
                }
            }
setup:
            hb->job_info.id = request->job_id;
            hb->job_info.a1state = hb->job_info.a2state = JOB_PENDING;

#ifdef FAKE_BAD_JOB
            log_entry(LEVEL_INFO, "[%s] failure_injected: %s, job_count\%3: %d, %d:%d",
                      __func__, failure_injected ? "true":"false", job_count % 3, hb->id, hb_fail);
#endif /* FAKE_BAD_JOB */
            for (i = 0; i < HB_DCR_REGISTERS; ++i)
            {
                log_entry(LEVEL_DEBUG, "[%s] setting DCR register index %d: %08x", __func__, i, request->registers[i]);
#ifdef FAKE_BAD_JOB
                if (!failure_injected && ((job_count % 3) == 0) && (hb->id == hb_fail))
                {
                    rc = job_dcr_register_set(hb, i, 0x1123);
                    ++hb_fail;
                    if (hb_fail >= config->hb_list->size)
                        hb_fail = 0;
                    failure_injected = true;
                }
                else
#endif /* FAKE_BAD_JOB */
                rc = job_dcr_register_set(hb, i, request->registers[i]);
                if (rc < 0)
                {
                    log_entry(LEVEL_ERROR, "[%s] failed to set DCR register %d", __func__, i);
                    goto next;
                }
                signature += request->registers[i];
            }

            if (hash1_start == 0)
                hash1_end = hashes_per_bank - 1;
            else                
                hash1_end = hash1_start + hashes_per_bank - 1;

            hash2_start = hash1_end + 1;
            hash2_end = hash2_start + hashes_per_bank - 1;
            rc = job_mgmt_set(hb, hash1_start, hash1_end, hash2_start, hash2_end);
            if (rc < 0)
            {
                log_entry(LEVEL_ERROR, "[%s] failed to set management configuration", __func__);
                goto next;
            }
            signature += hb->job_info.id + hash1_start + hash1_end + hash2_start + hash2_end;
            hash1_start = hash2_end + 1;

            job_start(hb, signature);
            hb->job_info.start = time(NULL);
            job_started = true;
        }
next:
        continue;
    }

    if (job_started)
    {
        log_entry(LEVEL_DEBUG, "[%s] DCR job request started", __func__);
        if (first_job_start_time == 0)
        {
            first_job_start_time = time(NULL);
            log_entry(LEVEL_INFO, "[%s] startup period started", __func__);
        }
    }
    pthread_mutex_unlock(&job_mutex);
}

void job_dcr_reprogram_request(
    station_config_t *config,
    hb_node_t *hb,
    uint8_t job_id
)
{
    int i;
    int rc;
    uint32_t hashes_per_board = config->dcr_hashes_per_board + 1;
    uint32_t hashes_per_bank = config->dcr_hashes_per_board/2 + 1;
    uint32_t hash1_start = (hashes_per_board * (hb->id + 1)) - hashes_per_board;
    uint32_t hash1_end = hash1_start + hashes_per_bank - 1;
    uint32_t hash2_start = hash1_end + 1;
    uint32_t hash2_end = hash2_start + hashes_per_bank - 1;
    uint32_t signature = 0;

    log_entry(LEVEL_DEBUG, "[%s] started", __func__);

    if (hb->type != HB_DCR)
        return;

    pthread_mutex_lock(&job_mutex);
    if (job_id == last_dcr_request.job_id)
    {
        hb_dcr_work_req_t *request = &last_dcr_request;

        hb->job_info.id = request->job_id;
        hb->job_info.a1state = hb->job_info.a2state = JOB_PENDING;

        for (i = 0; i < HB_DCR_REGISTERS; ++i)
        {
            log_entry(LEVEL_DEBUG, "[%s] setting DCR register index %d: %08x", __func__, i, request->registers[i]);
            rc = job_dcr_register_set(hb, i, request->registers[i]);
            if (rc < 0)
            {
                log_entry(LEVEL_ERROR, "[%s] failed to set DCR register %d", __func__, i);
                goto out;
            }
            signature += request->registers[i];
        }

        rc = job_mgmt_set(hb, hash1_start, hash1_end, hash2_start, hash2_end);
        if (rc < 0)
            log_entry(LEVEL_ERROR, "[%s] failed to set management configuration", __func__);
        else
        {
            signature += hb->job_info.id + hash1_start + hash1_end + hash2_start + hash2_end;

            job_start(hb, signature);
            hb->job_info.start = time(NULL);
            log_entry(LEVEL_INFO, "[%s] DCR job request %d reprogrammed for HB%d", __func__, job_id, hb->id);
        }
    }
out:
    pthread_mutex_unlock(&job_mutex);
}

/***********************************************************
 * NAME: job_sia_difficulty_set
 * DESCRIPTION: 
 *
 * IN:  HB node pointer
 * OUT: none
 ***********************************************************/
void job_sia_difficulty_set(
    station_config_t *config,
    double difficulty
)
{
    int rc;
    hb_difficulty_t req;
    struct list_entry *entry;
    
    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    req.value = htobe64((uint64_t)difficulty);
    log_entry(LEVEL_DEBUG, "[%s] sending SIA difficulty: %.2f", __func__, difficulty);
    pthread_mutex_lock(&job_mutex);
    TAILQ_FOREACH(entry, &config->hb_list->head, next_entry)
    {
        hb_node_t *hb = entry->data;
        if (hb && (hb->type == HB_SIA))
        {
            rc = hbmonitor_send_message(hb, MSG_TYPE_HB_SET_POOLD, &req, sizeof(req), NULL, 0);
            if (rc < 0)
                log_entry(LEVEL_ERROR, "[%s] failed to set difficulty for HB%d", __func__, hb->id);
        }
    }
    pthread_mutex_unlock(&job_mutex);
}

/***********************************************************
 * NAME: job_sia_stop
 * DESCRIPTION: 
 *
 * IN:  station config pointer
 * OUT: none
 ***********************************************************/
void job_sia_stop(
    station_config_t *config
)
{
    int rc;
    struct list_entry *entry;

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    pthread_mutex_lock(&job_mutex);
    TAILQ_FOREACH(entry, &config->hb_list->head, next_entry)
    {
        hb_node_t *hb = entry->data;
        if (hb && (hb->type == HB_SIA))
        {
            rc = hbmonitor_send_message(hb, MSG_TYPE_HB_ABORT_JOB, NULL, 0, NULL, 0);
            if (rc < 0)
                log_entry(LEVEL_ERROR, "[%s] failed to stop job for HB%d", __func__, hb->id);
            hb->job_info.a1state = hb->job_info.a2state = JOB_IDLE;
            hb->job_info.end = time(NULL);
        }
    }
    pthread_mutex_unlock(&job_mutex);
}

void job_sia_test(
    station_config_t *config
)
{
    int i;
    int rc;
    struct list_entry *entry;
    bool job_started = false;
    uint32_t hash1_start = 0;
    uint32_t hash1_end = 0;
    uint32_t hash2_start = 0;
    uint32_t hash2_end = 0;
    uint32_t hashes_per_bank = config->sia_hashes_per_board/2 + 1;

    log_entry(LEVEL_INFO, "[%s] test job setup...", __func__);

    min_bytes_received = max_bytes_received = 0;

    TAILQ_FOREACH(entry, &config->hb_list->head, next_entry)
    {
        hb_node_t *hb = entry->data;
        if (hb && (hb->type == HB_SIA))
        {
            uint32_t signature = 0;
            static uint8_t job_id = 1;

            if (!hb->powered)
            {
                log_entry(LEVEL_WARNING, "[%s] cannot use HB%d...not powered", __func__, hb->id);
                continue;
            }

            hb->job_info.id = job_id++;
            hb->job_info.a1state = hb->job_info.a2state = JOB_PENDING;
            hb->job_info.total_nonces = 0;
            hb->job_info.total_crc_errors = 0;

            for (i = 0; i < total_sia_test_registers; ++i)
            {
                log_entry(LEVEL_DEBUG, "[%s] setting SIA register %d", __func__, i);
                rc = job_sia_register_set(hb, i, sia_test_registers[i]);
                if (rc < 0)
                {
                    log_entry(LEVEL_ERROR, "[%s] failed to set SIA register %d", __func__, i);
                    goto next;
                }
                if (i < 10)
                {
                    signature +=  (sia_test_registers[i] & 0x00000000FFFFFFFF);
                    signature += ((sia_test_registers[i] & 0xFFFFFFFF00000000) >> 32);
                }
            }

            if (hash1_start == 0)
                hash1_end = hashes_per_bank - 1;
            else
            {
                hash1_start += hash2_end + 1;
                hash1_end = hash1_start + hashes_per_bank - 1;
            }
            hash2_start = hash1_end + 1;
            hash2_end = hash2_start + hashes_per_bank - 1;
            rc = job_mgmt_set(hb, hash1_start, hash1_end, hash2_start, hash2_end);
            if (rc < 0)
            {
                log_entry(LEVEL_ERROR, "[%s] failed to set management configuration", __func__);
                goto next;
            }
            signature += hb->job_info.id + hash1_start + hash1_end + hash2_start + hash2_end;

            hash1_start = hash2_end + 1;
            job_start(hb, signature);
            hb->job_info.start = time(NULL);
            job_started = true;
        }
next:
        continue;
    }

    if (job_started)
        log_entry(LEVEL_INFO, "[%s] SIA test job started", __func__);
}

void job_sia_program_request(
    station_config_t *config,
    hb_sia_work_req_t *request
)
{
    int i;
    int rc;
    struct list_entry *entry;
    bool job_started = false;
    uint32_t hash1_start = 0;
    uint32_t hash1_end = 0;
    uint32_t hash2_start = 0;
    uint32_t hash2_end = 0;
    uint32_t hashes_per_bank = config->sia_hashes_per_board/2 + 1;

#ifdef FAKE_BAD_JOB
    static uint8_t job_count = 0;
    static uint8_t hb_fail = 0;
    bool failure_injected = false;
#endif /* FAKE_BAD_JOB */
    // log_entry(LEVEL_DEBUG, "[%s] started - job ID: %d", __func__, request->job_id);

    pthread_mutex_lock(&job_mutex);
    memcpy(&last_sia_request, request, sizeof(last_sia_request));
#ifdef FAKE_BAD_JOB
    ++job_count;
#endif /* FAKE_BAD_JOB */
    TAILQ_FOREACH(entry, &config->hb_list->head, next_entry)
    {
        hb_node_t *hb = entry->data;
        if (hb && (hb->type == HB_SIA))
        {
            uint32_t signature = 0;

            if (!hb->powered)
            {
                log_entry(LEVEL_WARNING, "[%s] HB%d not powered...attempting to power up...", __func__, hb->id);
                if ((config->fan_status_front == FAN_STATUS_GOOD) &&
                    (config->fan_status_rear == FAN_STATUS_GOOD))
                {
                    /*
                     * Before we power on the arrays make sure that the
                     * fans are at a reasonable speed.
                     */
                    if (!hb->tuned)
                    {
                        config->fan_setting_front = TUNING_FAN_SPEED - FAN_SPEED_DELTA;
                        config->fan_setting_rear = TUNING_FAN_SPEED;
                    }
                    else
                    {
                        config->fan_setting_front = config->system_info.fanspeed - FAN_SPEED_DELTA;
                        config->fan_setting_rear = config->system_info.fanspeed;
                    }
                    fan_set_front(config, config->fan_setting_front);
                    fan_set_rear(config, config->fan_setting_rear);

                    rc = hbmonitor_send_message(hb, MSG_TYPE_HB_PWR_UP, NULL, 0, NULL, 0);
                    if (rc < 0)
                        log_entry(LEVEL_ERROR, "[%s] failed to power up HB%d", __func__, hb->id);
                    else
                    {
                        log_entry(LEVEL_INFO, "[%s] HB%d array power up complete", __func__, hb->id);
                        hb->powered = true;
                        sleep(10);
                        job_stats_set(hb, 1);
                        goto setup;
                    }
                    continue;
                }
                else
                {
                    log_entry(LEVEL_ERROR, "[%s] failed fan status...will not power up HB%d", __func__, hb->id);
                    continue;
                }
            }
setup:
            hb->job_info.id = request->job_id;
            hb->job_info.a1state = hb->job_info.a2state = JOB_PENDING;

#ifdef FAKE_BAD_JOB
            log_entry(LEVEL_INFO, "[%s] failure_injected: %s, job_count\%3: %d, %d:%d",
                      __func__, failure_injected ? "true":"false", job_count % 3, hb->id, hb_fail);
#endif /* FAKE_BAD_JOB */
            for (i = 0; i < HB_SIA_REGISTERS; ++i)
            {
                // log_entry(LEVEL_DEBUG, "[%s] setting SIA register index %d", __func__, i);
#ifdef FAKE_BAD_JOB
                if (!failure_injected && ((job_count % 3) == 0) && (hb->id == hb_fail))
                {
                    rc = job_sia_register_set(hb, i, 0x1123);
                    ++hb_fail;
                    if (hb_fail >= config->hb_list->size)
                        hb_fail = 0;
                    failure_injected = true;
                }
                else
#endif /* FAKE_BAD_JOB */
                rc = job_sia_register_set(hb, i, request->registers[i]);
                if (rc < 0)
                {
                    log_entry(LEVEL_ERROR, "[%s] failed to set SIA register %d", __func__, i);
                    goto next;
                }
                if (i < 10)
                {
                    signature +=  (request->registers[i] & 0x00000000FFFFFFFF);
                    signature += ((request->registers[i] & 0xFFFFFFFF00000000) >> 32);
                }
            }

            if (hash1_start == 0)
                hash1_end = hashes_per_bank - 1;
            else
                hash1_end = hash1_start + hashes_per_bank - 1;

            hash2_start = hash1_end + 1;
            hash2_end = hash2_start + hashes_per_bank - 1;
            rc = job_mgmt_set(hb, hash1_start, hash1_end, hash2_start, hash2_end);
            if (rc < 0)
            {
                log_entry(LEVEL_ERROR, "[%s] failed to set management configuration", __func__);
                goto next;
            }
            signature += hb->job_info.id + hash1_start + hash1_end + hash2_start + hash2_end;

            hash1_start = hash2_end + 1;
            job_start(hb, signature);
            hb->job_info.start = time(NULL);
            job_started = true;
        }
next:
        continue;
    }

    if (job_started)
    {
        log_entry(LEVEL_DEBUG, "[%s] SIA job request started", __func__);
        if (first_job_start_time == 0)
        {
            first_job_start_time = time(NULL);
            log_entry(LEVEL_INFO, "[%s] startup period started", __func__);
        }
    }
    pthread_mutex_unlock(&job_mutex);
}

void job_sia_reprogram_request(
    station_config_t *config,
    hb_node_t *hb,
    uint8_t job_id
)
{
    int i;
    int rc;
    uint32_t hashes_per_board = config->sia_hashes_per_board + 1;
    uint32_t hashes_per_bank = config->sia_hashes_per_board/2 + 1;
    uint32_t hash1_start = (hashes_per_board * (hb->id + 1)) - hashes_per_board;
    uint32_t hash1_end = hash1_start + hashes_per_bank - 1;
    uint32_t hash2_start = hash1_end + 1;
    uint32_t hash2_end = hash2_start + hashes_per_bank - 1;
    uint32_t signature = 0;

    log_entry(LEVEL_DEBUG, "[%s] started", __func__);

    if (hb->type != HB_SIA)
        return;

    pthread_mutex_lock(&job_mutex);
    if (job_id == last_sia_request.job_id)
    {
        hb_sia_work_req_t *request = &last_sia_request;

        hb->job_info.id = request->job_id;
        hb->job_info.a1state = hb->job_info.a2state = JOB_PENDING;

        for (i = 0; i < HB_SIA_REGISTERS; ++i)
        {
            log_entry(LEVEL_DEBUG, "[%s] setting SIA register index %d: %08x", __func__, i, request->registers[i]);
            rc = job_sia_register_set(hb, i, request->registers[i]);
            if (rc < 0)
            {
                log_entry(LEVEL_ERROR, "[%s] failed to set SIA register %d", __func__, i);
                goto out;
            }
            signature += request->registers[i];
        }

        rc = job_mgmt_set(hb, hash1_start, hash1_end, hash2_start, hash2_end);
        if (rc < 0)
            log_entry(LEVEL_ERROR, "[%s] failed to set management configuration", __func__);
        else
        {
            signature += hb->job_info.id + hash1_start + hash1_end + hash2_start + hash2_end;

            job_start(hb, signature);
            hb->job_info.start = time(NULL);
            log_entry(LEVEL_INFO, "[%s] SIA job request %d reprogrammed for HB%d", __func__, job_id, hb->id);
        }
    }
out:
    pthread_mutex_unlock(&job_mutex);
}

/***********************************************************
 * NAME: job_init
 * DESCRIPTION: initialize the job configuration for the
 * specified hashing board
 *
 * IN:  HB node
 * OUT: none
 ***********************************************************/
void job_init(
    station_config_t *config
)
{
    struct list_entry *entry;

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    pthread_mutex_lock(&job_mutex);
    TAILQ_FOREACH(entry, &config->hb_list->head, next_entry)
    {
        hb_node_t *hb = entry->data;
        if (hb)
        {
            uint16_t max_performance = (hb->type == HB_DCR) ? HB_DCR_MAX_PERFORMANCE:HB_SIA_MAX_PERFORMANCE;

            config->miner_type = hb->type;
            if ((config->hb_info.hbcount == 0) ||
                (config->hb_info.hbparams[hb->id].performance == 0))
            {
                hb->current_performance = HB_DEFAULT_PERFORMANCE;
                hb->max_performance = max_performance;
                config->hb_info.hbvalid = false;
            }
            else
            {
                hb->current_performance = config->hb_info.hbparams[hb->id].performance;
                hb->max_performance = config->hb_info.hbparams[hb->id].performance;
                hb->tuned = true;
            }
        }
    }
    pthread_mutex_unlock(&job_mutex);

    if (config->hb_list->size > 0)
    {
        config->dcr_hashes_per_board = 0xFFFFF;
        config->sia_hashes_per_board = 0xFFFFF;
    }
    else
        log_entry(LEVEL_ERROR, "[%s] no hashing boards found", __func__);
}

/***********************************************************
 * NAME: job_shutdown
 * DESCRIPTION: shutdown the hashing board job configuration
 *
 * IN:  HB node
 * OUT: none
 ***********************************************************/
void job_shutdown(
    station_config_t *config
)
{
    int rc;
    struct list_entry *entry;

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    pthread_mutex_lock(&job_mutex);
    TAILQ_FOREACH(entry, &config->hb_list->head, next_entry)
    {
        hb_node_t *hb = entry->data;
        if (hb)
        {
            job_stop(hb);
            /* Power down the array */
            rc = hbmonitor_send_message(hb, MSG_TYPE_HB_PWR_DN, NULL, 0, NULL, 0);
            if (rc < 0)
                log_entry(LEVEL_ERROR, "[%s] failed to power down HB%d", __func__, hb->id);
            else
                log_entry(LEVEL_INFO, "[%s] HB%d array power down complete", __func__, hb->id);
        }
    }
    pthread_mutex_unlock(&job_mutex);
}
