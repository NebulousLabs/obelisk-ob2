/*
 * Copyright (C) 2016-2018 MapleLeaf Software, Inc
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * ipc.h
 *
 * Dan Jozwiak: djozwiak@mlsw.biz
 * Date Created: 2016-11-27
 *
 * Description: this file contains the contents and data structures used
 * for general IPC communication processing.
 */

#ifndef _IPC_H_
#define _IPC_H_

#include "config.h"
#include "ipc_messages.h"

#define IPC_MAGIC    0x434C4E54  /* CLNT */

typedef struct
{
    magic_t magic;
    int fd;
    ipc_id_t id;
    uint32_t notifications;
} ipc_info_t;

extern struct list *ipc_init(void);
extern void ipc_server_init(station_config_t *config);
extern void ipc_server_shutdown(station_config_t *config);
extern void ipc_accept_connection(station_config_t *config);
extern int  ipc_process_message(station_config_t *config, ipc_info_t *info);
extern void ipc_notify_swupdate(char *latest_release, char *previous_release);
extern void ipc_notify_hb_change(hb_node_t *hb, notification_t notification);
extern void ipc_notify_pool_difficulty(float difficulty);
extern void ipc_notify_burnin_status(burnin_status_t status, void *data);
extern void ipc_notify_current(station_config_t *config, ipc_info_t *client);
extern void ipc_send_job_complete(station_config_t *config, ipc_id_t id, uint8_t job_id);
extern void ipc_send_work_fail(station_config_t *config, ipc_id_t id, uint8_t job_id);
extern void ipc_send_reset(station_config_t *config, ipc_id_t id);
extern void ipc_send_dcr_nonce(station_config_t *config, hb_dcr_nonce_t *info);
extern void ipc_send_sia_nonce(station_config_t *config, hb_sia_nonce_t *info);
extern void ipc_start_swupdate(char *url);

#endif /* _IPC_H_ */
