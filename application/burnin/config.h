/*
 * Copyright (C) 2012-2018 MapleLeaf Software, Inc
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * config.h
 *
 * Dan Jozwiak: djozwiak@mlsw.biz
 * Date Created: 2012-04-13
 *
 * Description: Header file for configuration processing.
 */

#ifndef _CONFIG_H_
#define _CONFIG_H_

#ifdef  __cplusplus
extern "C" {
#endif

#include "common.h"
#include "ipc_messages.h"

#define BURNIN_PERIOD   (30 * 60)   /* 30 minutes */

typedef struct {
    hb_board_type_t type;
    uint8_t id;
    float intake_temp;
    float exhaust_temp;
    float voltage;
    float current;
    double hashrate_1min;
    double hashrate_5min;
    double hashrate_15min;
    uint64_t last_hash_stat_time_nanos;
} hb_info_t;

typedef struct {
    uint64_t accepted;
    uint64_t rejected;
    char status[MAX_POOL_STATUS_LEN];
} pool_info_t;

typedef struct
{
    station_info_t station_info;
    system_info_t system_info;
    char latest_firmware_version[MAX_FWVERSION_SIZE];
    int ipc_fd;
    int hb_count;
    hb_info_t hbinfo[MAX_HASHING_BOARDS];
    pool_info_t pool_info[MAX_POOLS];
    time_t start_time;
    time_t remaining;
    bool burnin_complete;
} station_config_t;

extern void config_init(station_config_t *config);

#ifdef  __cplusplus
}
#endif

#endif /* _CONFIG_H_ */
