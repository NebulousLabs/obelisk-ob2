/*
 * Copyright (C) 2014-2018 MapleLeaf Software, Inc
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * ipc.c
 *
 * Dan Jozwiak: djozwiak@mlsw.biz
 * Date Created: 2014-05-20
 *
 * Description: this file contains the code to process connections and
 * messages from the server. 
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/queue.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <errno.h>
#include <math.h>

#include "common.h"
#include "ipc_messages.h"
#include "logger.h"
#include "ports.h"
#include "config.h"
#include "ipc.h"

static void decay_time(
    double* f, double fadd,
    double fsecs,
    double interval
)
{
    double ftotal, fprop;

    if (fsecs <= 0)
        return;

    fprop = 1.0 - 1 / (exp(fsecs / interval));
    ftotal = 1.0 + fprop;
    *f += (fadd / fsecs * fprop);
    *f /= ftotal;
}

static msg_ipc_tlv_t *build_identity(void)
{
    void *msg = NULL;
    size_t msg_len = sizeof(msg_ipc_tlv_t) + sizeof(tlv_ipc_identity_t);

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    msg = calloc(1, msg_len);
    if (msg)
    {
        msg_ipc_tlv_t *tlv = msg;
        tlv_ipc_identity_t *identity = (tlv_ipc_identity_t *)tlv->value;

        tlv->type = ATTR_TYPE_IPC_IDENTITY;
        tlv->len = sizeof(tlv_ipc_identity_t);
        identity->id = IPC_BURNIN_MANAGER;
    }

    return msg;
}

static msg_ipc_tlv_t *build_registration(void)
{
    void *msg = NULL;
    size_t msg_len = sizeof(msg_ipc_tlv_t) + sizeof(tlv_ipc_notify_registration_t);

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    msg = calloc(1, msg_len);
    if (msg)
    {
        msg_ipc_tlv_t *tlv = msg;
        tlv_ipc_notify_registration_t *reg = (tlv_ipc_notify_registration_t *)tlv->value;

        tlv->type = ATTR_TYPE_IPC_REGISTER_NOTIFICATION;
        tlv->len = sizeof(tlv_ipc_notify_registration_t);
        reg->notifications = NOTIFICATION_HB_TEMP1 |
                             NOTIFICATION_HB_TEMP2 |
                             NOTIFICATION_HB_VOLTAGE |
                             NOTIFICATION_HB_CURRENT |
                             NOTIFICATION_HB_STATS |
                             NOTIFICATION_CGM_STATS;
    }

    return msg;
}

static msg_ipc_tlv_t *build_unregistration(void)
{
    void *msg = NULL;
    size_t msg_len = sizeof(msg_ipc_tlv_t) + sizeof(tlv_ipc_notify_registration_t);

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    msg = calloc(1, msg_len);
    if (msg)
    {
        msg_ipc_tlv_t *tlv = msg;
        tlv_ipc_notify_registration_t *reg = (tlv_ipc_notify_registration_t *)tlv->value;

        tlv->type = ATTR_TYPE_IPC_UNREGISTER_NOTIFICATION;
        tlv->len = sizeof(tlv_ipc_notify_registration_t);
        reg->notifications = NOTIFICATION_HB_TEMP1 |
                             NOTIFICATION_HB_TEMP2 |
                             NOTIFICATION_HB_VOLTAGE |
                             NOTIFICATION_HB_CURRENT |
                             NOTIFICATION_HB_STATS |
                             NOTIFICATION_CGM_STATS;
    }

    return msg;
}

static msg_ipc_tlv_t *build_simple_request(uint16_t type)
{
    void *msg = NULL;
    size_t msg_len = sizeof(msg_ipc_tlv_t);

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    msg = calloc(1, msg_len);
    if (msg)
    {
        msg_ipc_tlv_t *tlv = msg;

        tlv->type = type;
    }

    return msg;
}

static msg_ipc_tlv_t *build_burnin_start(
    station_config_t *config
)
{
    void *msg = NULL;
    size_t msg_len = sizeof(msg_ipc_tlv_t) +
                     sizeof(tlv_ipc_burnin_start_t);

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    msg = calloc(1, msg_len);
    if (msg)
    {
        msg_ipc_tlv_t *tlv = msg;
        tlv_ipc_burnin_start_t *info = (tlv_ipc_burnin_start_t *)tlv->value;

        tlv->type = ATTR_TYPE_IPC_RESTART_CGM;
        tlv->len = msg_len - sizeof(tlv_ipc_burnin_start_t);
        info->total_hashing_boards = config->hb_count;
    }

    return msg;
}

static msg_ipc_tlv_t *build_burnin_status(
    station_config_t *config,
    burnin_status_t bistatus
)
{
    void *msg = NULL;
    size_t msg_len = sizeof(msg_ipc_tlv_t) +
                     sizeof(tlv_ipc_burnin_status_t) +
                     (sizeof(tlv_burnin_info_t) * config->hb_count);

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    msg = calloc(1, msg_len);
    if (msg)
    {
        msg_ipc_tlv_t *tlv = msg;
        tlv_ipc_burnin_status_t *status = (tlv_ipc_burnin_status_t *)tlv->value;
        tlv_burnin_info_t *info = status->info;
        int i;

        tlv->type = ATTR_TYPE_IPC_BURNIN_STATUS;
        tlv->len = msg_len - sizeof(msg_ipc_tlv_t);
        status->miner_status = bistatus;
        status->total_hashing_boards = config->hb_count;
        for (i = 0; i < config->hb_count; ++i)
        {
            info[i].id = config->hbinfo[i].id;
            info[i].type = config->hbinfo[i].type;
            info[i].expected_hash_rate = (config->hbinfo[i].type == HB_DCR) ? MIN_DCR_HASHRATE:MIN_SIA_HASHRATE;
            info[i].actual_hash_rate = config->hbinfo[i].hashrate_5min;
            info[i].board_status = (info[i].actual_hash_rate >= info[i].expected_hash_rate) ? BI_PASSED:BI_FAILED;
        }
    }

    return msg;
}

static void send_ipc_msg(
    station_config_t *config,
    msg_ipc_tlv_t *msg
)
{
    if (config->ipc_fd > 0)
    {
        if (msg)
        {
            int bytes = send(config->ipc_fd, msg, sizeof(msg_ipc_tlv_t) + msg->len, MSG_NOSIGNAL);
            if (bytes < 0)
                log_entry(LEVEL_ERROR, "[%s] send failed: %s", __func__, strerror(errno));
            else if (bytes == 0)
                log_entry(LEVEL_ERROR, "[%s] sent 0 bytes: %s", __func__, strerror(errno));
            free(msg);
        }
        else
            log_entry(LEVEL_ERROR, "[%s] msg is NULL", __func__);
    }
    else
        log_entry(LEVEL_ERROR, "[%s] not connected...will not send message", __func__);
}

static void setup_burnin(
    station_config_t *config
)
{
    /*
     * First copy over the correct CGM configuration file. Note that for
     * now we don't have a requirement to support a mixed DCR/SIA
     * configuration so just use the first HB to determine what type
     * we are.
     */
    if (config->hb_count > 0)
    {
        int rc;
        char cmd[256];
        bool configured = false;

        switch (config->hbinfo[0].type)
        {
            case HB_DCR:
                sprintf(cmd, "cp -f /usr/local/ob2/data/burnin-dcr.conf /var/etc/ob2/cgminer.conf");
                rc = system(cmd);
                if (rc < 0)
                    log_entry(LEVEL_ERROR, "[%s] command failed: %s", __func__, cmd);
                else
                    configured = true;
            break;
            case HB_SIA:
                sprintf(cmd, "cp -f /usr/local/ob2/data/burnin-sia.conf /var/etc/ob2/cgminer.conf");
                rc = system(cmd);
                if (rc < 0)
                    log_entry(LEVEL_ERROR, "[%s] command failed: %s", __func__, cmd);
                else
                    configured = true;
            break;
            default:
            break;
        }
        if (configured)
        {
            msg_ipc_tlv_t *msg = build_burnin_start(config);
            send_ipc_msg(config, msg);
            config->start_time = time(NULL);
            log_entry(LEVEL_INFO, "[%s] burnin started", __func__);
        }
        else
            log_entry(LEVEL_ERROR, "[%s] invalid HB type found: %d", __func__);
    }
    else
        log_entry(LEVEL_ERROR, "[%s] no HBs found", __func__, config->hbinfo[0].type);
}

static int process_ipc_tlv(
    station_config_t *config,
    msg_ipc_tlv_t *tlv
)
{
    msg_ipc_tlv_t *msg = NULL;

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);
    switch (tlv->type)
    {
        case ATTR_TYPE_IPC_CHANGE_NOTIFICATION:
        {
            tlv_ipc_notification_t *change = (tlv_ipc_notification_t *)tlv->value;

            log_entry(LEVEL_DEBUG, "[%s] received notification change", __func__);
            switch (change->type)
            {
                case NOTIFICATION_HB_TEMP1:
                {
                    tlv_ipc_notification_hb_measurement_t *info = (tlv_ipc_notification_hb_measurement_t *)change->value;
                    log_entry(LEVEL_DEBUG, "[%s] received temp1 change for HB%d: %.2f", __func__, info->hb_id, info->value);
                    config->hbinfo[info->hb_id].intake_temp = info->value;
                }
                break;

                case NOTIFICATION_HB_TEMP2:
                {
                    tlv_ipc_notification_hb_measurement_t *info = (tlv_ipc_notification_hb_measurement_t *)change->value;
                    log_entry(LEVEL_DEBUG, "[%s] received temp2 change for HB%d: %.2f", __func__, info->hb_id, info->value);
                    config->hbinfo[info->hb_id].exhaust_temp = info->value;
                }
                break;

                case NOTIFICATION_HB_VOLTAGE:
                {
                    tlv_ipc_notification_hb_measurement_t *info = (tlv_ipc_notification_hb_measurement_t *)change->value;
                    log_entry(LEVEL_DEBUG, "[%s] received voltage change for HB%d: %.2f", __func__, info->hb_id, info->value);
                    config->hbinfo[info->hb_id].voltage = info->value;
                }
                break;

                case NOTIFICATION_HB_CURRENT:
                {
                    tlv_ipc_notification_hb_measurement_t *info = (tlv_ipc_notification_hb_measurement_t *)change->value;
                    log_entry(LEVEL_DEBUG, "[%s] received current change for HB%d: %.2f", __func__, info->hb_id, info->value);
                    config->hbinfo[info->hb_id].current = info->value;
                }
                break;

                case NOTIFICATION_HB_STATS:
                {
                    uint8_t hb_index;
                    struct timespec now;
                    uint64_t now_nanos;
                    uint64_t elapsed;
                    double elapsed_secs;
                    double gigahashes_done;
                    tlv_ipc_notification_hb_stats_t *info = (tlv_ipc_notification_hb_stats_t *)change->value;

                    hb_index = info->hb_id;
                    clock_gettime(CLOCK_MONOTONIC, &now);
                    now_nanos = now.tv_sec * (uint64_t)1000000000L + now.tv_nsec;
                    elapsed = now_nanos - config->hbinfo[hb_index].last_hash_stat_time_nanos;
                    elapsed_secs = ((double)elapsed) / 1000000000.0;
                    gigahashes_done = ((double)info->hashes) / 1000000000.0;
                    decay_time(&config->hbinfo[hb_index].hashrate_1min, gigahashes_done, elapsed_secs, 60.0);
                    decay_time(&config->hbinfo[hb_index].hashrate_5min, gigahashes_done, elapsed_secs, 300.0);
                    decay_time(&config->hbinfo[hb_index].hashrate_15min, gigahashes_done, elapsed_secs, 900.0);

                    log_entry(LEVEL_DEBUG, "[%s] received stats for HB%d: %llu", __func__, info->hb_id, info->hashes);
                    log_entry(LEVEL_DEBUG, "num_hashes=%llu", info->hashes);
                    log_entry(LEVEL_DEBUG, "elapsed_secs=%0.2f giga_hashes_done=%0.2f", elapsed_secs, gigahashes_done);
                    log_entry(LEVEL_DEBUG, "hashrate_1min =%0.2f GH/s", config->hbinfo[hb_index].hashrate_1min);
                    log_entry(LEVEL_DEBUG, "hashrate_5min =%0.2f GH/s", config->hbinfo[hb_index].hashrate_5min);
                    log_entry(LEVEL_DEBUG, "hashrate_15min=%0.2f GH/s", config->hbinfo[hb_index].hashrate_15min);
                
                    config->hbinfo[hb_index].last_hash_stat_time_nanos = now_nanos;
                }
                break;

                case NOTIFICATION_CGM_STATS:
                {
                    tlv_ipc_cgminer_stats_t *info = (tlv_ipc_cgminer_stats_t *)change->value;
                    log_entry(LEVEL_DEBUG, "$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
                    log_entry(LEVEL_DEBUG, "[%s] received CGM stats:", __func__);
                    log_entry(LEVEL_DEBUG, "[%s] \tpool index: %d", __func__, info->pool_index);
                    log_entry(LEVEL_DEBUG, "[%s] \taccepted: %lld", __func__, info->accepted);
                    log_entry(LEVEL_DEBUG, "[%s] \trejected: %lld", __func__, info->rejected);
                    char status[MAX_POOL_STATUS_LEN];
                    switch (info->status)
                    {
                        case PS_ENABLED:
                            if (info->idle) {
                                log_entry(LEVEL_DEBUG, "[%s] \tstatus: DEAD", __func__);
                                strncpy(status, "Dead", MAX_POOL_STATUS_LEN);
                            } else {
                                log_entry(LEVEL_DEBUG, "[%s] \tstatus: ALIVE", __func__);
                                strncpy(status, "Alive", MAX_POOL_STATUS_LEN);
                            }
                        break;
                        case PS_DISABLED:
                        case PS_REJECTING:
                        default:
                            log_entry(LEVEL_DEBUG, "[%s] \tstatus:", __func__, psstat_to_str(info->status));
                            strncpy(status, psstat_to_str(info->status), MAX_POOL_STATUS_LEN);
                        break;
                    }
                    //set_pool_info(info->pool_index, info->accepted, info->rejected, status);
                    log_entry(LEVEL_DEBUG, "$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
                }
                break;
            }
        }
        break;

        case ATTR_TYPE_IPC_HB_INFO:
        {
            tlv_ipc_hb_info_t *info = (tlv_ipc_hb_info_t *)tlv->value;
            int i;

            config->hb_count = info->count;
            for (i = 0; i < info->count; ++i)
            {
                config->hbinfo[info->hbinfo[i].id].type = info->hbinfo[i].type;
                config->hbinfo[info->hbinfo[i].id].id = info->hbinfo[i].id;
            }

            /* Start the burnin process... */
            setup_burnin(config);
        }
        break;

        default:
            log_entry(LEVEL_ERROR, "[%s] received unknown TLV type from server:%d", __func__, tlv->type);
            return -1;
        break;
    }

    if (msg)
        send_ipc_msg(config, msg);

    return sizeof(msg_ipc_tlv_t) + tlv->len;
}

void ipc_send_burnin_status(
    station_config_t *config,
    burnin_status_t status
)
{
    msg_ipc_tlv_t *msg = build_burnin_status(config, status);
    send_ipc_msg(config, msg);

    /* Unregister for notifications */
    msg = build_unregistration();
    send_ipc_msg(config, msg);

    /* Remove the burnin cgminer configuration file */
    unlink("/var/etc/ob2/cgminer.conf");

    /* 
     * If the burnin passed then remove the burnin startup link so that
     * we don't ever do the burnin again.
     */
    if (status == BI_PASSED)
    {
        log_entry(LEVEL_INFO, "[%s] burnin passed", __func__);
        system("/usr/local/ob2/bin/disable_burnin");
    }
    else
        log_entry(LEVEL_INFO, "[%s] burnin failed", __func__);

    log_entry(LEVEL_INFO, "[%s] burnin complete", __func__);
}

/***********************************************************
 * NAME: process_ipc_message
 * DESCRIPTION: process a message from IPC server
 *
 * IN:  server file descriptor
 * OUT: none
 ***********************************************************/
int ipc_process_message(
    station_config_t *config
)
{
    int bytes_received = 0;
    char buf[MAX_RECV_LEN];
   
    memset(buf, 0, MAX_RECV_LEN);
    bytes_received = recv(config->ipc_fd, buf, MAX_RECV_LEN, 0);
    switch (bytes_received)
    {
        case -1:
        {
            // log_entry(LEVEL_ERROR, "[%s] recv error: %s", __func__, strerror(errno));
            return -1;
        }
        break;
        
        case 0:
        {
            / log_entry(LEVEL_WARNING, "[%s] far end closed connection", __func__);
            return 0;
        }
        break;
        
        default:
        {
            uint8_t *tmp_tlv;

            /* One or more messages received...process */
            msg_ipc_tlv_t *tlv = (msg_ipc_tlv_t *)buf;
            tmp_tlv = (uint8_t *)tlv;
            while (bytes_received > 0)
            {
                process_ipc_tlv(config, tlv);
                bytes_received -= (sizeof(msg_ipc_tlv_t) + tlv->len);
                if (bytes_received > 0)
                {
                    tmp_tlv += (sizeof(msg_ipc_tlv_t) + tlv->len); /* move to the next message */
                    tlv = (msg_ipc_tlv_t *)tmp_tlv;
                }
            }
        }
    } /* End switch (bytes_received) */
    return 1;
}

/***********************************************************
 * NAME: ipc_server_connect
 * DESCRIPTION: connect to the server (station manager)
 *
 * IN:  none
 * OUT: server file descriptor or -1 on error
 ***********************************************************/
void ipc_server_connect(
    station_config_t *config
)
{
    int opt = 1;
    int fd;
    struct sockaddr_in server_socket;
    msg_ipc_tlv_t *msg;

    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    /* Get a tcp file descriptor */
    fd = socket(PF_INET, SOCK_STREAM, 0);
    if (fd < 0)
    {
        log_entry(LEVEL_ERROR, "[%s] failed to create a socket", __func__);
        config->ipc_fd = -1;
        return;
    }

    /* set this so that we can rebind to it after a crash or a manual kill */
    if (setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt)) < 0)
    {
        log_entry(LEVEL_ERROR, "[%s] failed to set socket option: %s", __func__, strerror(errno));
        goto err;
    }

    /* Setup the tcp port information */
    memset(&server_socket, 0, sizeof(server_socket));
    server_socket.sin_family = AF_INET;
    server_socket.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
    server_socket.sin_port = htons(CONTROL_MGR_PORT);

    if (connect(fd, (struct sockaddr *)&server_socket, sizeof(server_socket)) < 0)
    {
        log_entry(LEVEL_ERROR, "[%s] failed to connect: %s", __func__, strerror(errno));
        goto err;
    }

    log_entry(LEVEL_INFO, "[%s] ipc server connection opened", __func__);

    /* Send identity */
    msg = build_identity();
    if (msg)
    {
        int bytes = send(fd, msg, sizeof(msg_ipc_tlv_t) + msg->len, MSG_NOSIGNAL);
        if (bytes < 0)
            log_entry(LEVEL_ERROR, "[%s] send failed: %s", __func__, strerror(errno));
        else if (bytes == 0)
            log_entry(LEVEL_ERROR, "[%s] sent 0 bytes: %s", __func__, strerror(errno));
        free(msg);
    }

    /* Send request for HB configuration */
    msg = build_simple_request(ATTR_TYPE_IPC_HB_INFO);
    if (msg)
    {
        int bytes = send(fd, msg, sizeof(msg_ipc_tlv_t) + msg->len, MSG_NOSIGNAL);
        if (bytes < 0)
            log_entry(LEVEL_ERROR, "[%s] send failed: %s", __func__, strerror(errno));
        else if (bytes == 0)
            log_entry(LEVEL_ERROR, "[%s] sent 0 bytes: %s", __func__, strerror(errno));
        free(msg);
    }

    /* Send notification registration */
    msg = build_registration();
    if (msg)
    {
        int bytes = send(fd, msg, sizeof(msg_ipc_tlv_t) + msg->len, MSG_NOSIGNAL);
        if (bytes < 0)
            log_entry(LEVEL_ERROR, "[%s] send failed: %s", __func__, strerror(errno));
        else if (bytes == 0)
            log_entry(LEVEL_ERROR, "[%s] sent 0 bytes: %s", __func__, strerror(errno));
        free(msg);
    }

    config->ipc_fd = fd;
    return;
err:
    close(fd);
    config->ipc_fd = -1;
}

/***********************************************************
 * NAME: ipc_server_connect
 * DESCRIPTION: connect to the server (station manager)
 *
 * IN:  none
 * OUT: server file descriptor or -1 on error
 ***********************************************************/
void ipc_server_disconnect(
    station_config_t *config
)
{
    log_entry(LEVEL_DEBUG, "[%s] called", __func__);

    if (config->ipc_fd > 0)
    {
        close(config->ipc_fd);
        config->ipc_fd = -1;
        log_entry(LEVEL_INFO, "[%s] ipc server connection closed", __func__);
    }
}
