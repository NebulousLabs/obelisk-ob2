/*
 * Copyright (C) 2012-2018 MapleLeaf Software, Inc
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * config.c
 *
 * Dan Jozwiak: djozwiak@mlsw.biz
 * Date Created: 2012-04-13
 *
 * Description: 
 */

#include "common.h"
#include "logger.h"
#include "info.h"
#include "config.h"
#include <pthread.h>
#include <math.h>

static station_config_t* config;

void config_init(
    station_config_t *_config
)
{
    // Stash away a pointer for the functions below to use
    config = _config;

    info_station(STATION_INFO_FILE, &(config->station_info));
    info_system(SYSTEM_INFO_FILE, &(config->system_info));
    config->station_info.release[0] = 'v';

    // Intialize the hashboard info and pool info arrays
    memset(config->hbinfo, 0, MAX_HASHING_BOARDS * sizeof(hb_info_t));
    memset(config->pool_info, 0, MAX_POOLS * sizeof(pool_info_t));
}
