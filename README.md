# Obelisk Gen 2 Software and Firmware

## Overview

This README.md file contains the information required to build and install custom versions of the Obelisk software and firmware on Gen 2 Obelisk devices.

## Code Organization

The source code for Obelisk Gen 2 devices has two main parts:

- Hashboard Firmware
- Control board Software

## Hashboard Firmware

The hashboard firmware is written in C and runs on an STM32F101 microprocessor on each hashboard. Specifications can be found here: https://www.st.com/en/microcontrollers-microprocessors/stm32f101.html

The control board communicates with the hashboards over a serial port connection (UART) per hashboard using a custom messaging protocol.

Look for calls to `hbmonitor_send_message()` to see the message structs that are sent to the hashboards, while `process_hb_async_message()` handles the processing of async messages received from the hashboards.

## Control Board Software

The control board runs a custom Yocto-based Linux system on a TI Sitara AM3352 processor. The Sitara is the same processor found in the BeagleBone Black SBC (and in fact, that board was used for early board bring up before prototype boards were available). Specifications of the processor can be found here: http://www.ti.com/product/AM3352

The following programs are specific to Obelisk Gen 2:

- HB Programmer
- Update Manager
- Control Manager
- Burnin Manager
- API Server
- Web Client
- CGMiner

These processes are started at initialization time using associated scripts in `/etc/init.d`.

### HB Programmer

The HB (Hashboard) Programmer is the first Obelisk-specific task to start up. Its job is to detect the connected hashboards and query
them to see what version of firmware each is running. If the HB Programmer sees that it has a newer firmware version
available than the one in a given hashboard, then it begins the processor of upgrading the hashboard. It can take about a minute
to complete. The system checks each hashboard in series, so on SC1 Dual units, it could take an extra 2 minutes to boot, and on SC1 Immersion units, it could take an extra 4 minutes to boot.

Once the system has checked all hashboards and upgraded them if necessary, then the HB Programmer exits.

### Update Manager

The Update Manager is responsible for checking for incremental update packages, downloading them, and verifying them. The update server config is in the `update URL` setting in the `system_info` config file. By default, it refers to `http://obelisk.tech/swupdates`. The code appends `/versions.xml` to the URL and attempts to fetch the list of available versions. If there is a new version, it downloads the update package using the filename specified in the `versions.xml` file.

The update files are saved the right location for the HB Programmer to find them on the next reboot, or if the update command is issued directly.

### Burnin Manager

The Burnin Manager runs only during the manufacturing process. Its job is to connect to a default pool and start all hashboards hashing at full speed.  This burnin exercises the entire system and ensures that it meets the minimum required hashrate before being passed to the next phase of manufacturing.

The burnin process consists of setting a specific pool configuration into the miner temporarily, then mining for 10 minutes or so until the unit reaches the minimum required hashrate.

If the burnin is successful, the green LED on the front of the miner starts flashing. If there was any error, then the red LED on the front of the miner starts flashing instead. The log files contain additional information on the cause of any failure (typically low hashrate).

### Control Manager

The Control Manager is the central process in the system. It's responsible for:

- Taking hash jobs from CGMiner, splitting them up into sub-jobs, and sending them to the hashboards.
- Taking nonces received from the hashboards and sending them back to CGMiner so it can send them to the mining pool.
- Sending statistics and other updates from hashboards to the API Server, so they appear in the GUI.
- Receiving parameter change requests from the API Server and making necessary changes to configuration files and making
  changes to how the hashboards are operating (e.g., the API Server could send a request to increase the Performance Number
  to 2400, and the Control Manager would propagate that to the corresponding hashboard).

### API Server

The API Server is responsible for handling all API requests from the web-based miner GUI or any third-party applications that might use the API directly. Possible request types include:

- Login/logout
- Getting/setting miner and hashboard configuration
- Getting current statistics for the hashboards and pools
- Getting diagnostic information

The API Server runs on port 3001 and relies on Lighthttpd to forward API requests to it (from port 80 to port 3001),
although the server also accepts requests sent directly to port 3001.

Also, it keeps track of all statistics sent from the Control Manager and keeps a hashrate history for the past hour for each hashboard.

Note that there is a file that describes the API's REST endpoints in `/application/webclient/api.md`. This description is currently somewhat out-of-date as we wrote it originally for the Gen 1 Obelisks. It needs to be updated to reflect the Gen 2 REST API.

### Web Client

The Web Client is a web-based GUI served by Lighttpd. The Web Client is a React/Redux app written in TypeScript
using `create-react-app` as a base.  You need to install `node.js` before you can build the Web Client.  Follow the LTS Release
instructions here: https://tecadmin.net/install-latest-nodejs-npm-on-ubuntu/

In development, you can modify the `package.json` file to point to your Obelisk
miner's IP address, and then run `npm start`.  This command starts the app running locally, and all API requests are proxied over
to your Obelisk. GUI development is very efficient using this local server and proxy, since you don't need to build and copy the files over to
the Obelisk over and over.

The API Server processes all actions that the user initiates on the Web Client through REST API requests.

### CGMiner

CGMiner is open-source mining software (https://github.com/ckolivas/cgminer) for Bitcoin. It has evolved over its life from being a CPU miner to a GPU miner to an FPGA miner to an ASIC miner. Currently, it is an FPGA- & ASIC-focused miner. We decided that, with some modifications, we could adapt it for mining Siacoin and Decred.

The source code under `/application/cgminer` contains the heavily modified CGMiner code.

Note that unlike Gen 1 Obelisks, which relied more on `cgminer`'s built-in features, the Gen 2 Obelisk code takes over a lot
more functionality in the Control Manager and API Server. The Gen 2 code only uses the pool communications part of `cgminer`. As a result, the `cgminer` API, which is used by some third-party mine monitoring applications like Awesome Miner, can't gather much information for Gen 2 units. Information about hashrate, errors, and other stats is not tracked on the `cgminer` side anymore. Anyone wanting to integrate monitoring support for the Gen 2 Obelisks needs to communicate with the API Server directly.

## Logging

The various applications maintain their log files in the `/var/log/ob2/` folder. There is a separate log file in this folder for each Obelisk process. You can safely delete any of these files at any time, and the system recreates each one when it writes the next log message.

You can run `tail -F /var/log/ob2/apiserver`. for example, to monitor the API Server log messages.

## Configuration File Locations

The following are the main configuration files and locations used by the software.

- `/var/etc/ob2/station_info` - Contains version numbers of the software and firmware in XML format.
- `/var/etc/ob2/system_info` - Contains system configuration settings including fan speed, autofan mode, autofan target temperature, reboot period, update URL, etc.
- `/var/etc/ob2/hb_info` - Contains the performance number setting for each hashboard, along with the actual hashrate achieved by the corresponding hashboard the last time AutoTune ran.
- `/var/etc/ob2/cgminer.conf` - Contains standard `cgminer` configuration. In Gen 2 Obelisks, we don't rely on `cgminer` as much as we did in Gen 1. As a result, applications like AwesomeMiner, which can be used to perform basic monitor of Gen 1 devices don't work with Gen 2 devices (e.g., hashrates and other statistics are not reported to `cgminer` anymore. The role of `cgminer` is to receive jobs from the pool and submit nonces to the pool. The Control Manager and other processes handle all other tasks now.
- `/var/etc/ob2/auth.json` - Contains the salted and hashed password of the web user login. Note that this login info is completely separate from the Linux root account login.

## Read-Only Filesystem

The main filesystem is in read-only mode on boot. To make it writable to make any changes to files, including uploading new binaries for testing, you need to run the following command:

```
mount -o remount,rw /
```

The file system stays writable until you reboot. It returns to read-only again when the OS restarts.

## Default Logins

### Web Client

The default login for the Web Client is:

- Username: `admin`
- Password: `admin`

To change this, use the Change Password button on the System tab of the GUI or invoke the `changePassword` API endpoint.

### Linux/SSH Login

The default login for SSH is:

- Username: `root`
- Password: `obelisk`

To change this, run the `passwd` when logged in as `root`.

## Building the Hashboard Firmware

One of our subcontractors wrote and compiled the hashboard firmware using the IAR Embedded Workbench for ARM. Unfortunately, we found out too late that this tool is not an open-source compiler.  There is, however, a free 30-day trial available on their website: https://www.iar.com/iar-embedded-workbench/#!?architecture=Arm

We want the hashboard firmware to be compilable with open-source tools, but have not had the time to do so. Pull requests for this change would be gladly accepted.

To build the hashboard firmware, open the project in the IAR Embedded Workbench, and click Build.

## Building the Control Board Software

This section describes how to build the control board applications, including Control Manager, API Server, CGMiner, etc.

Note that we developed most of this software was performed under Ubuntu 18.04.3 LTS (Bionic Beaver), but it has also been built successfully under Ubuntu 19.04 (Disco Dingo). The tools and build scripts probably work with other Linux systems, too, but we have not tested this. Developing directly under Mac OS X or Windows OS is not supported, but using Ubuntu VMs of course works.

### Installing the Toolchain

To install the cross-compilation toolchain into `/opt/obelisk` for the Gen 2 Obelisks run the following commands:

```
cd setup
./setup
source /opt/obelisk/environment-setup-cortexa8hf-neon-poky-linux-gnueabi
```

You should now be ready to build the Gen 2 Obelisk executables.

### Building Executables

From the `application` folder, run `make` to build the release versions of all the applications or run `make debug` to build the debug versions. The main difference is that the `debug` version has the logging level set to `LEVEL_DEBUG` level (vs. `LEVEL_INFO` for the release version), so a lot more messages are logged.

You can also `cd` into a specified application's folder, such as `apiserver`, and run `make` or `make debug` for just that executable.

The release and debug executables are in the `arm/release` and `arm/debug` folders of each application, respectively.

## Testing New Executables

Once you have compiled new executables, you can `scp` the files to the system. The executables are located in `/usr/local/ob2/bin`. You should make backup copies of the original files before copying any modified files to the miner.  Example:

    scp arm/debug/apiserver root@10.0.0.4:/usr/local/ob2/bin

Also, remember that the file system is read-only by default, as mentioned above, so you may want to write a deploy script that first uses `ssh` to run the remount command from above.  Alternatively, you can open a second SSH window and just run the command there first.

For testing, you can use `ps` to find the PIDs of the executable you want to test and the corresponding `process_monitor` script. Then run `kill -9 <pid>` on each executable and `process_monitor` to kill them.  Alternatively, you can reboot the system. Sometimes if you kill an executable and then restart it, it doesn't reconnect with the Control Manager.

Note that you can quickly shut down all executables from the command line with the command `stop-all`.  A combination of `stop-all && reboot && exit` is often useful.  I recommend writing a script to mount the file system read/write, stop the executables, and then upload the new executables and reboot (see the previous sentence).  This sequence tends to be faster than trying restarting everything manually.
