#!/bin/sh
### BEGIN INIT INFO
# Provides:          mountall
# Required-Start:    mountvirtfs
# Required-Stop: 
# Default-Start:     S
# Default-Stop:
# Short-Description: Mount all filesystems.
# Description:
### END INIT INFO

check_writeable_filesystem()
{
    part3="/dev/mmcblk0p3"
    mountp="/var"
    recoverydir="/usr/local/ob2/data"
    recovery_needed=false
    umount $mountp
    /sbin/fsck.ext4 -f -p $part3 > /dev/null 2>&1
    ret=$?
    if [ $ret -eq 0 ]; then
        echo "writeable filesystem good"
    elif [ $ret -eq 1 ] || [ $ret -eq 2 ]; then
        echo "writeable filesystem had errors...recovering"
        recovery_needed=true
    else
        echo -n "writeable filesystem could not be automatically fixed...reformatting..."
        mkfs -F -q -t ext4 -L var $part3
        recovery_needed=true
        echo "formatting complete"
    fi
    if [ "$recovery_needed" == "true" ]; then
        echo -n "$part3 recovery in progress..."
        mount $part3 $mountp
        EXTRACT_UNSAFE_SYMLINKS=1 tar -C / -xf $recoverydir/var.tgz
        EXTRACT_UNSAFE_SYMLINKS=1 tar -C / -xf $recoverydir/ob2-config.tgz
        echo "complete"
    fi
}

. /etc/default/rcS

#
# Mount local filesystems in /etc/fstab. For some reason, people
# might want to mount "proc" several times, and mount -v complains
# about this. So we mount "proc" filesystems without -v.
#
test "$VERBOSE" != no && echo "Mounting local filesystems..."
check_writeable_filesystem
mount -at nonfs,nosmbfs,noncpfs 2>/dev/null

#
# We might have mounted something over /dev, see if /dev/initctl is there.
#
if test ! -p /dev/initctl
then
	rm -f /dev/initctl
	mknod -m 600 /dev/initctl p
fi
kill -USR1 1

#
# Execute swapon command again, in case we want to swap to
# a file on a now mounted filesystem.
#
[ -x /sbin/swapon ] && swapon -a

: exit 0

