#!/bin/sh -e

### BEGIN INIT INFO
# Provides:        hbprogrammer
# Required-Start:  $syslog
# Required-Stop:   $syslog
# Default-Start:   2 3 4 5
# Default-Stop:    0 1 6
# Short-Description: Start HB programmer
### END INIT INFO

check_configuration()
{
    stationinfo="/var/etc/ob2/station_info"
    systeminfo="/var/etc/ob2/system_info"
    hbinfo="/var/etc/ob2/hb_info"
    authfile="/var/etc/ob2/auth.json"
    cgmconf="/var/etc/ob2/cgminer.conf"
    if [[ ! -e $stationinfo || ! -e $systeminfo || ! -e $hbinfo || ! -e $authfile || ! -e $cgmconf ]]; then
        # If the serial number ever gets written to the EEPROM then we
        # can retrieve it here and set it...for now we'll just put unknown.
        /usr/local/ob2/bin/setup-station -s unknown
    fi
}

PROGRAM=/usr/local/ob2/bin/hbprogrammer
SIGNAL=QUIT

test -x $PROGRAM || exit 0

case "$1" in
start)
    # NOTE: this startup script does not background the hbprogrammer
    # by design. The intent of the programmer is to insure that the
    # hashing boards are programmed to the latest release before the
    # system software starts up. The programmer will run and verify
    # the boards, update them if required and then exit. While this
    # may delay system startup, it is important that the hashing boards
    # are running firmware that is compatible with the current software.
    echo -n "Starting hbprogrammer:"
    check_configuration
    sleep 30 # Give time for HB boot
    $PROGRAM -a > /dev/null 2>&1
    echo " Completed"
    ;;

*)
    echo "Usage: $0 {start}" >&2
    exit 1
    ;;
esac

exit 0

# vim:set ai et sts=2 sw=2 tw=0:
