#!/bin/bash
#
# Copyright (C) 2009-2018 MapleLeaf Software, Inc
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. The name of the author may not be used to endorse or promote products
#    derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
# OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
# NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
# THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# Generate Backup Script

ROLLBACK_SCRIPT=usr/local/ob2/bin/rollback # Rollback script

generate_rollback_script ()
{
    echo "#!/bin/bash" > $ROLLBACK_SCRIPT
    echo "#" >> $ROLLBACK_SCRIPT
    echo "# Copyright (C) 2009-2018 MapleLeaf Software, Inc" >> $ROLLBACK_SCRIPT
    echo "# All rights reserved." >> $ROLLBACK_SCRIPT
    echo "#" >> $ROLLBACK_SCRIPT
    echo "# Redistribution and use in source and binary forms, with or without" >> $ROLLBACK_SCRIPT
    echo "# modification, are permitted provided that the following conditions" >> $ROLLBACK_SCRIPT
    echo "# are met:" >> $ROLLBACK_SCRIPT
    echo "# 1. Redistributions of source code must retain the above copyright" >> $ROLLBACK_SCRIPT
    echo "#    notice, this list of conditions and the following disclaimer." >> $ROLLBACK_SCRIPT
    echo "# 2. Redistributions in binary form must reproduce the above copyright" >> $ROLLBACK_SCRIPT
    echo "#    notice, this list of conditions and the following disclaimer in the" >> $ROLLBACK_SCRIPT
    echo "#    documentation and/or other materials provided with the distribution." >> $ROLLBACK_SCRIPT
    echo "# 3. The name of the author may not be used to endorse or promote products" >> $ROLLBACK_SCRIPT
    echo "#    derived from this software without specific prior written permission." >> $ROLLBACK_SCRIPT
    echo "#" >> $ROLLBACK_SCRIPT
    echo "# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR" >> $ROLLBACK_SCRIPT
    echo "# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES" >> $ROLLBACK_SCRIPT
    echo "# OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED." >> $ROLLBACK_SCRIPT
    echo "# IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT," >> $ROLLBACK_SCRIPT
    echo "# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT" >> $ROLLBACK_SCRIPT
    echo "# NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE," >> $ROLLBACK_SCRIPT
    echo "# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY" >> $ROLLBACK_SCRIPT
    echo "# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT" >> $ROLLBACK_SCRIPT
    echo "# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF" >> $ROLLBACK_SCRIPT
    echo "# THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE." >> $ROLLBACK_SCRIPT
    echo "#" >> $ROLLBACK_SCRIPT
    echo "# Software Recovery Script"  >> $ROLLBACK_SCRIPT
    echo "# This will do the following actions:" >> $ROLLBACK_SCRIPT
    echo "# - Shutdown the OB2 applications" >> $ROLLBACK_SCRIPT
    echo "# - Restore the files and directories that were changed in the last update" >> $ROLLBACK_SCRIPT
    echo "# - Remove files and directories that were added in the last update" >> $ROLLBACK_SCRIPT
    echo "# - Request a reboot of the system" >> $ROLLBACK_SCRIPT
    echo "#" >> $ROLLBACK_SCRIPT
    echo "# Note: This file is automatically generated during the package building process" >> $ROLLBACK_SCRIPT
    echo "# This file was generated for Release: $RELVERS" >> $ROLLBACK_SCRIPT
    echo "" >> $ROLLBACK_SCRIPT
    
    echo "[ -f /etc/default/rcS ] && . /etc/default/rcS" >> $ROLLBACK_SCRIPT
    echo "" >> $ROLLBACK_SCRIPT

    echo "# Trap kill signals to ignore the exit of the SUM that spawned us" >> $ROLLBACK_SCRIPT
    echo "trap \"\" SIGHUP SIGINT SIGTERM" >> $ROLLBACK_SCRIPT
    echo "" >> $ROLLBACK_SCRIPT

    echo "ACTIVEOB2DIR=\"/var/etc/ob2\"" >> $ROLLBACK_SCRIPT
    echo "SYSINFOFILE=\$ACTIVEOB2DIR\"/system_info\"" >> $ROLLBACK_SCRIPT
    echo "OB2DIR=\"/usr/local/ob2\"" >> $ROLLBACK_SCRIPT
    echo "BACKUPDIR=\$OB2DIR\"/backup\"" >> $ROLLBACK_SCRIPT
    echo "BACKUPFILE=\$BACKUPDIR\"/ob2-backup.tgz\"" >> $ROLLBACK_SCRIPT
    echo "OB2_MODULES=\"cgminer-sia cgminer-dcr apiserver updatemgr controlmgr\"" >> $ROLLBACK_SCRIPT
    echo "ADDEDFILES=\"$ADDEDFILES\"" >> $ROLLBACK_SCRIPT
    echo "" >> $ROLLBACK_SCRIPT
    
    echo "if [ \"\$ROOTFS_READ_ONLY\" = \"yes\" ]; then" >> $ROLLBACK_SCRIPT
    echo "    mount -o remount,rw /" >> $ROLLBACK_SCRIPT
    echo "fi" >> $ROLLBACK_SCRIPT
    echo "" >> $ROLLBACK_SCRIPT

    echo "# Shutdown the application software" >> $ROLLBACK_SCRIPT
    echo "for service in \$OB2_MODULES ; do" >> $ROLLBACK_SCRIPT
    echo "    echo \"Shutting down \$service\"" >> $ROLLBACK_SCRIPT
    echo "    if [ -e /etc/init.d/\$service ]; then" >> $ROLLBACK_SCRIPT
    echo "        /etc/init.d/\$service stop" >> $ROLLBACK_SCRIPT
    echo "    fi" >> $ROLLBACK_SCRIPT
    echo "done" >> $ROLLBACK_SCRIPT
    echo "" >> $ROLLBACK_SCRIPT
  
    echo "if [ -z \"\$ADDEDFILES\" ]; then" >> $ROLLBACK_SCRIPT
    echo "    echo \"No new files from update...\"" >> $ROLLBACK_SCRIPT
    echo "else" >> $ROLLBACK_SCRIPT
    echo "    echo \"Deleting new files from update...\"" >> $ROLLBACK_SCRIPT
    echo "    # Delete any new files from last update" >> $ROLLBACK_SCRIPT
    echo "    cd /" >> $ROLLBACK_SCRIPT
    echo "    for file in \$ADDEDFILES ; do" >> $ROLLBACK_SCRIPT
    echo "        echo \"Deleting: \$file\"" >> $ROLLBACK_SCRIPT
    echo "        rm \$file" >> $ROLLBACK_SCRIPT
    echo "    done" >> $ROLLBACK_SCRIPT
    echo "fi" >> $ROLLBACK_SCRIPT
    echo "" >> $ROLLBACK_SCRIPT
    
    echo "# Unpack our backup package" >> $ROLLBACK_SCRIPT
    echo "if [ ! -e \$BACKUPDIR ] ; then" >> $ROLLBACK_SCRIPT
    echo "    echo \"\$BACKUPDIR directory not found!  Cannot perform rollback\"" >> $ROLLBACK_SCRIPT
    echo "    exit 1" >> $ROLLBACK_SCRIPT
    echo "fi" >> $ROLLBACK_SCRIPT
    echo "" >> $ROLLBACK_SCRIPT
    
    echo "# Restore the previous configuration" >> $ROLLBACK_SCRIPT
    echo "if [ -e \$BACKUPFILE ] ; then" >> $ROLLBACK_SCRIPT
    echo "    export EXTRACT_UNSAFE_SYMLINKS=1; tar -xz -C / -f \$BACKUPFILE" >> $ROLLBACK_SCRIPT
    echo "fi" >> $ROLLBACK_SCRIPT
    echo "" >> $ROLLBACK_SCRIPT

    echo "# Remove the updateto version to prevent a re-update" >> $ROLLBACK_SCRIPT
    echo "sed -i 's|<updateto>.*|<updateto/>|g' \$SYSINFOFILE"  >> $ROLLBACK_SCRIPT
    echo "" >> $ROLLBACK_SCRIPT
    
    echo "sync; sync" >> $ROLLBACK_SCRIPT
    echo "" >> $ROLLBACK_SCRIPT

    echo "if [ \"\$ROOTFS_READ_ONLY\" = \"yes\" ]; then" >> $ROLLBACK_SCRIPT
    echo "    mount -o remount,ro /" >> $ROLLBACK_SCRIPT
    echo "fi" >> $ROLLBACK_SCRIPT
    echo "" >> $ROLLBACK_SCRIPT

    echo "/sbin/reboot" >> $ROLLBACK_SCRIPT
}
