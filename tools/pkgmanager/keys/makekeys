#!/bin/bash

# makekeys - make secret key
# Builds the public, private, and clear text and encrypted secret keys.
# This should only be run once, then the private and encrypted
# secret keys must be present in /usr/scu/keys on every SCU..

BASEDIR=`pwd`
TOOLSDIR=$BASEDIR/tools
LOCALKEYSDIR=$TOOLSDIR/pkgmanager/keys

if [ ! -e $LOCALKEYSDIR ]; then
   echo "$LOCALKEYSDIR not found. Please run this from the top-level directory"
   exit 1
fi

SECRET_KEY=$LOCALKEYSDIR/secret.key
DECRYPT_KEY=$LOCALKEYSDIR/secret.decryp

PUBLIC_KEY=$LOCALKEYSDIR/upgrade.pub
PRIVATE_KEY=$LOCALKEYSDIR/upgrade.prv

if [ -f $SECRET_KEY ] ; then 
  echo "The Secret Key should be created only once and then saved."
  echo "If you want to run this you will need to remove $SECRET_KEY"
  exit 2
fi

if [ -f $DECRYPT_KEY ] ; then 
  echo "The Secret Key should be created only once and then saved."
  echo "If you want to run this you will need to remove $DECRYPT_KEY"
  exit 3
fi

if [ -f $PUBLIC_KEY ] ; then 
  echo "The Public Key should be created only once and then saved."
  echo "If you want to run this you will need to remove $PUBLIC_KEY"
  exit 4
fi

if [ -f $PRIVATE_KEY ] ; then 
  echo "The Private Key should be created only once and then saved."
  echo "If you want to run this you will need to remove $PRIVATE_KEY"
  exit 5
fi

GCMD="openssl genrsa -out $PRIVATE_KEY 2048"
eval $GCMD > /dev/null 2>&1
OPENSSLRET=$?

if [ X"0" != X"$OPENSSLRET" ] ; then
  echo "OpenSSL failed to generate private key"
  exit 6
fi

GCMD="openssl rsa -in $PRIVATE_KEY -out $PUBLIC_KEY -outform PEM -pubout"
eval $GCMD > /dev/null 2>&1
OPENSSLRET=$?

if [ X"0" != X"$OPENSSLRET" ] ; then
  echo "OpenSSL failed to generate public key"
  exit 7
fi

# Create the secret key. Note this should be done just one time
touch $DECRYPT_KEY && chmod 600 $DECRYPT_KEY

dd if=/dev/random of=$DECRYPT_KEY bs=100 count=1 > /dev/null 2>&1
# Encrypt the secret key.
GCMD="openssl rsautl -encrypt -inkey $PUBLIC_KEY -pubin -in $DECRYPT_KEY -out $SECRET_KEY"
eval $GCMD

if [ ! -f $SECRET_KEY ] ; then
  echo "Cannot find encrypted secret key $SECRET_KEY"
  exit 8
fi

if [ ! -f $DECRYPT_KEY ] ; then
  echo "Cannot find clear secret key $DECRYPT_KEY"
  exit 9
fi

echo "Upgrade key generation complete."
