/*
 * Copyright (C) 2009-2018 MapleLeaf Software, Inc
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * crc.c - tool to create a CRC for a file and optionally to append
 * the CRC to the file as an unsigned long.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdint.h>
#include <libgen.h>
#include <zlib.h>

#define false 0
#define true  1
typedef uint8_t bool;

char *inputPath = NULL;
char *outputPath = NULL;

bool storeCRC = 0;
bool stripCRC = 0;
bool calcCRC = 0;

/***********************************************************
 * NAME: readFile
 * DESCRIPTION: reads in an update tarball and loads it into an
 * allocated buffer.
 *
 * CALLER MUST FREE MEMORY!
 *
 * IN:  char * path,
 *      uint8_t *buffer
 * OUT: uint32_t length
 ***********************************************************/
static uint32_t readFile(
    char *path,
    uint8_t **buffer,
    FILE **fp
)
{
    uint32_t ret = 0;
    struct stat info;

    *fp = fopen(path, "r");
    if (*fp)
    {
        stat(path, &info);
        *buffer = (uint8_t*)calloc(1, info.st_size + sizeof(ulong));
        ret = fread(*buffer, 1, info.st_size, *fp);
        if (ret != info.st_size)
        {
            free(*buffer);
        }
        else
        {
            ret = info.st_size;
        }
    }

    return ret;
}

static uint32_t writeCRC(
    char *inputPath,
    char *outputPath
)
{
    uint32_t ret = 0;
    uint32_t size;
    uint8_t *buf = NULL;
    FILE *fp = NULL;

    size = readFile(inputPath, &buf, &fp);
    if (fp && size > 0)
    {
        uint32_t thisCRC;

        if (outputPath)
        {
            fclose(fp);
            fp = fopen(outputPath, "wb");
        }
        
        if (fp)
        {
            /* Compute CRC */
            thisCRC = crc32(0L, Z_NULL, 0);
            thisCRC = crc32(thisCRC, buf, size);

            ret = fwrite(buf, 1, size, fp);
            if (ret != size)
            {
                free(buf);
                fclose(fp);
                return 0;
            }

            ret = fwrite(&thisCRC, 1, sizeof(uint32_t), fp);
            if (ret != sizeof(uint32_t))
            {
                free(buf);
                fclose(fp);
                return 0;
            }
            else
            {
                ret = thisCRC;
            }
        }
    }

    if (fp)
        fclose(fp);

    return ret;
}

static void removeCRC(
    char *inputPath,
    char *outputPath
)
{
    uint32_t ret = 0;
    uint32_t size;
    uint8_t *buf = NULL;
    FILE *fp = NULL;

    size = readFile(inputPath, &buf, &fp);
    if (fp && size > 0)
    {
        uint32_t thisCRC;
        uint32_t fileCRC;
        uint32_t sizeCRC;

        /* Compute CRC */
        thisCRC = crc32(0L, Z_NULL, 0);
        thisCRC = crc32(thisCRC, buf, size - sizeof(uint32_t));

        fseek(fp, size - sizeof(uint32_t), SEEK_SET);
        sizeCRC = fread(&fileCRC, 1, sizeof(uint32_t), fp);
        if (sizeCRC <= 0)
        {
            fclose(fp);
            printf("[%s] failed to read %s\n", __func__, inputPath);
            return;
        }

        if (thisCRC != fileCRC)
        {
            fclose(fp);
            printf("[%s] input file %s does not contain calculated CRC 0x%08x\n",
                   __func__, inputPath, (uint32_t)thisCRC);
            return;
        }

        if (outputPath)
        {
            fclose(fp);
            fp = fopen(outputPath, "wb");
        }

        if (fp)
        {
            ret = fwrite(buf, 1, size - sizeof(uint32_t), fp);
            if (ret != (size - sizeof(uint32_t)))
            {
                free(buf);
                fclose(fp);
                return;
            }
        }
    }

    if (fp)
        fclose(fp);
}

static uint32_t getCRC(
    char *file
)
{
    uint32_t thisCRC;
    uint8_t *buf = NULL;
    uint32_t size;
    FILE *fp = NULL;

    size = readFile(file, &buf, &fp);
    if (size == 0)
    {
        printf("[%s] could not find file for CRC %s\n", __func__, file);
        if (fp)
            fclose(fp);
        return 0;
    }
    else
    {
        /* calc crc */
        thisCRC = crc32(0L, Z_NULL, 0);
        thisCRC = crc32(thisCRC, buf, size);
        free(buf);
        if (fp)
            fclose(fp);
    }
    return thisCRC;
}

static void usage(
    char *name
)
{
    printf("Usage: %s \n", basename(name));
    printf("\t-g <file>: calculate CRC for file\n"
           "\t-w       : calculate CRC and append to file (requires -i)\n"
           "\t-s       : strip CRC from file (requires -i)\n"
           "\t-i <file>: input file\n"
           "\t-o <file>: output file. If not specified input file will be overwritten\n"
          );
    exit(1);
}

static void processArgs(
    int argc,
    char**argv
)
{
    int c = 0;

    while ((c = getopt(argc, argv, "wsgi:o:")) != -1)
    {
        switch (c)
        {
            case 'g':
                calcCRC = true;
            break;

            case 'w':
                storeCRC = true;
            break;

            case 's':
                stripCRC = true;
            break;

            case 'i':
                inputPath = optarg;
            break;

            case 'o':
                outputPath = optarg;
            break;

            default:
                usage(argv[0]);
            break;
        }
    }
}

/***********************************************************
 * NAME:  main
 * DESCRIPTION:
 * IN:
 * OUT:
 ***********************************************************/
int main(int argc, char **argv)
{
    uint32_t calculatedCRC = 0;

    /* parse command line arguments */
    processArgs(argc, argv);

    if (!inputPath)
    {
        printf("no input file specified\n");
        usage(argv[0]);
    }

    if (calcCRC)
    {
        calculatedCRC = getCRC(inputPath);
        printf("0x%08x\n", (uint32_t)calculatedCRC);
    }
    else if (storeCRC)
    {
        writeCRC(inputPath, outputPath);
    }
    else if (stripCRC)
    {
        removeCRC(inputPath, outputPath);
    }
    return 0;
}
