/*
 * Copyright (C) 2009-2018 MapleLeaf Software, Inc
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/*
 * xml related functions
 */
#ifndef __XMLTOOLS_H__
#define __XMLTOOLS_H__

#include <stdint.h>
#include <libxml/parser.h>
#include <libxml/tree.h>

#ifndef __cplusplus
typedef uint8_t bool;
#endif // __cplusplus

#define true 1
#define false 0

extern char *getVersionID(xmlNodePtr pNode);
extern bool isVersionRecordPresent(char *filename, char *releaseVersionStr, int *maxID);
extern void initVersionsXmlFile(char *fname);
extern int updateVersions(xmlDocPtr templateDoc, xmlDocPtr versionsDoc,
                          char *versionsFilePath, char *timeStamp,
                          char *releaseVersion, char *previousVersion,
                          char *CRC, char *filePath, char *packageSize, int maxID);
extern int getMaxRecordID(xmlNodePtr parent);

/*
 * Most commonly used API functions
 */
extern char *getNodeContentByName(xmlNodePtr parent, char* name);
extern int addVersionRecord(xmlDocPtr doc, xmlNodePtr node, char *fupdate);
extern bool isVersionElement(xmlNodePtr p);

/*
 * These are currently only used internally, but available to 
 * public as well.
 */
extern xmlNodePtr getNodeByName(xmlNodePtr nodePtr, char * name);
extern void updateElementByName(xmlNodePtr nodePtr, char *elemName, char *newValue);

#endif /* __XMLTOOLS_H__ */
