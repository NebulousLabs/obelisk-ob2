/*
 * Copyright (C) 2009-2018 MapleLeaf Software, Inc
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/*
 * xmltools.c - XML parsing tools
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <libxml/parser.h>
#include <libxml/tree.h>

#ifdef __USE_XPATH_CODE_
#include <libxml/xpath.h>
#include <libxml/xmlmemory.h>
#endif /* __USE_XPATH_CODE_ */

#include "xmltools.h"

#ifdef __USE_XPATH_CODE_
xmlXPathObjectPtr getNodeSet (xmlDocPtr doc, xmlCharPtr xpath);
xpathtest();
#endif /* __USE_XPATH_CODE_ */

#define XMLT_NO_ERROR 0
#define XMLT_FAILURE (-1)

#define WRKBUFSZ 32

/***********************************************************
 * NAME: isVersionElement
 * DESCRIPTION: This function checks to see if the 
 *              p is of the XML_ELEMENT_NODE and the element 
 *              named "version"
 *
 * IN:  xmlNodePtr 
 * OUT: bool
 ***********************************************************/
bool isVersionElement(xmlNodePtr p)
{
    if ((p->type == XML_ELEMENT_NODE) &&
        (!xmlStrcmp(p->name, (const xmlChar*)"version")))
    {
        return true;
    }
    return false;
}

/***********************************************************
 * NAME: replaceXmlNodeInTree
 * DESCRIPTION: This function checks to see if the 
 *              old and new are pointing to the same content id
 *              if so, it updated the old entry with the new one.
 *
 * IN:  xmlNodePtr,  xmlNodePtr,  xmlNodePtr 
 * OUT: bool
 ***********************************************************/
bool replaceXmlNodeInTree(xmlNodePtr old, xmlNodePtr new, xmlNodePtr root)
{
    xmlNodePtr pFree;
    xmlChar *pMem  = xmlGetProp(new, (xmlChar*)"id");
    xmlChar *pMast = xmlGetProp(old, (xmlChar*)"id");
    bool ret = false;

    if (pMem && pMast)
    {
        /* Are they the same node?  */
        if (!xmlStrcmp(pMem, pMast))
        {
#ifdef DEBUG
            printf("%s-%d: same node, updating mem:%s, master:%s\n", __func__, __LINE__, pMem, pMast);
#endif /* DEBUG */
            /*
             * xmlReplaceNode returns a pointer to the old node
             * context that has already been unlinked from the tree,
             * it's ready to be freed now
             */
            pFree = xmlReplaceNode(old, new);
            if (pFree)
            {
                xmlFreeNode(pFree);
            }
            ret = true;
        }

        xmlFree(pMem);
        xmlFree(pMast);
    }

    return ret;
}

/***********************************************************
 * NAME: getNodeContentByName
 * DESCRIPTION: This function loops through a tree
 *              and looks for a given node matching the
 *              specified name.  It allocates a buffer
 *              for the return value of the nodes
 *              content.  
 *
 *              CALLER MUST FREE MEMORY
 *
 * IN:  xmlNodePtr, 
 *      char *
 *
 * OUT: char *
 ***********************************************************/
char *getNodeContentByName(xmlNodePtr parent, char* name)
{
    char *retVal = NULL;
    xmlNodePtr node;

    if (!parent || !name)
        return retVal;

    node = getNodeByName(parent, name);
    if (node)
    {
        retVal = (char*)xmlNodeGetContent(node);
    }

    return retVal;
}

/***********************************************************
 * NAME: getNodeByName
 * DESCRIPTION: This function loops through a tree
 *              and looks for a given node matching the
 *              specified name. It assumes that the parent
 *              passed in is the start of a tree.
 *
 * IN:  xmlNodePtr, 
 *      char *
 *
 * OUT: none
 ***********************************************************/
xmlNodePtr getNodeByName(xmlNodePtr parent, char * name)
{
    xmlNodePtr nodePtr;
    if (parent == NULL || name == NULL) 
        return NULL;

    for (nodePtr = parent->children; nodePtr != NULL; nodePtr = nodePtr->next)
    {
        if (nodePtr->name && (strcmp((char*)nodePtr->name,name) == 0))
        {
            return nodePtr;
        }
    }
    return NULL;
}

/***********************************************************
 * NAME: updateElementByName
 * DESCRIPTION: This function looks up the node specified
 *              and updates its value with newValue
 * IN:  xmlDocPtr, char *, char * 
 * OUT: none
 ***********************************************************/
void updateElementByName(xmlNodePtr nodePtr, char *elemName, char *newValue)
{
    xmlNodePtr nodeToUpdate;

    /* Find the element to update */
    nodeToUpdate = getNodeByName(nodePtr, elemName);

    xmlNodeSetContent(nodeToUpdate, (xmlChar*)newValue);

#ifdef DEBUG
    char *tmpcnt;
    tmpcnt = (char*)xmlNodeGetContent(nodeToUpdate);
    printf("%s-%d: node<%s> value:%s\n", __func__, __LINE__, nodeToUpdate->name, tmpcnt);
    xmlFree(tmpcnt);
#endif /* DEBUG */
}


/***********************************************************
 * NAME: updateVersions
 * DESCRIPTION: This function finds the content record
 *              associated with the specified ID and
 *              sends that record to the flash UI.
 *
 * IN:  xmlNodePtr: content record to adjust
 * OUT: none
 ***********************************************************/
int updateVersions(
    xmlDocPtr templateDoc,
    xmlDocPtr versionsDoc,
    char *versionsFilePath, 
    char *timeStamp,
    char *releaseVersion,
    char *previousVersion,
    char *CRC, 
    char *filePath,
    char *packageSize,
    int maxID
)
{
    int retval = XMLT_NO_ERROR;

    char tempID[WRKBUFSZ];

    xmlNodePtr templateRoot = NULL;
    xmlNodePtr templateVersionNode = NULL;

    xmlNodePtr versionRoot;
    xmlNodePtr upgradeFileNode;

    /* Allow formatting of the XML file */
    xmlKeepBlanksDefault(0);

    /*
    *   We have the template xml file already read in.
    *   We need to update the elements in the template record
    *   Then merge that record into the versionsDoc file.
    */

    templateRoot = xmlDocGetRootElement(templateDoc);
    if( templateRoot == NULL )
    {
        return XMLT_FAILURE;
    }

    // Here we need to update the versionList attribute
    // templateRoot should be versionList
    // Here we want to validate that and then
    // add/update an attribute for back-off
    versionRoot = xmlDocGetRootElement(versionsDoc);

    xmlAttrPtr upgradeBo  = xmlSetProp(versionRoot, (xmlChar*)"bo", (xmlChar *)"0");
    if(upgradeBo == NULL)
    {
        return XMLT_FAILURE;
    }


    templateVersionNode = getNodeByName(templateRoot, "version");
    if(templateVersionNode == NULL)
    {
        /* There is a problem with the versions.xml file */
        return XMLT_FAILURE;
    }

    // Get the attribute/property here
    sprintf(tempID, "%d", maxID);

    xmlAttrPtr upgradeID  = xmlSetProp(templateVersionNode, (xmlChar*)"id", (xmlChar *)tempID);
    if(upgradeID == NULL)
    {
        return XMLT_FAILURE;
    }

    updateElementByName( templateVersionNode, "date", timeStamp);
    updateElementByName( templateVersionNode, "upgradeFile", filePath);

    // Set the attribute here
    upgradeFileNode = getNodeByName(templateVersionNode, (char *)"upgradeFile");
    if(upgradeFileNode == NULL )
    {
        // We have a problem no upgradeFile tag found in record
        return XMLT_FAILURE;
    }

    xmlAttrPtr upgradeIC  = xmlSetProp(upgradeFileNode, (xmlChar*)"ic", (xmlChar *)CRC);
    if(upgradeIC == NULL)
    {
        return XMLT_FAILURE;
    }

    updateElementByName(templateVersionNode, "releaseVersion", releaseVersion);
    updateElementByName(templateVersionNode, "previousVersion", previousVersion);
    updateElementByName(templateVersionNode, "packageSize", packageSize);

    /*
     * Now we have the template version record updated and ready to merge into our 
     * local versions.xml file.
     * Do that merge here.
     */
    retval = addVersionRecord(versionsDoc, templateVersionNode, versionsFilePath);

    return retval;
}

/***********************************************************
 * NAME: getMaxRecordID
 * DESCRIPTION: This function loops through a tree
 *              and returns the highest id value found
 *
 * IN:  xmlNodePtr, 
 *      char *
 *
 * OUT: xmlNodePtr - a pointer to the node with the given content id
 ***********************************************************/
int getMaxRecordID(xmlNodePtr parent)
{
    xmlNodePtr nodePtr;
    int maxID = 0;

    if (parent == NULL)
    {
        return XMLT_FAILURE;
    }

    for (nodePtr = parent->children; nodePtr != NULL; nodePtr = nodePtr->next)
    {
        if ((nodePtr->type == XML_ELEMENT_NODE) && (nodePtr->name))
        {
            xmlChar *tmp =  xmlGetProp(nodePtr, (xmlChar*)"id");
            if (tmp)
            {
                if (atoi((char *)tmp) > maxID)
                {
                    maxID = atoi((char *)tmp);
                }
            }
            xmlFree(tmp);
        }
    }
    return maxID;
}

/***********************************************************
 * NAME: getVersionID
 * DESCRIPTION: This function returns the version ID from the
 *              specified record if one exists
 *
 * IN: xmlNodePtr pNode - A pointer to the content record
 *
 * OUT: char * - version ID or NULL if one does not exist
 * CALLER MUST FREE MEMORY 
 ***********************************************************/
char *getVersionID(xmlNodePtr pNode)
{
    char *vID = (char*)xmlGetProp(pNode, (xmlChar*)"id");

    return vID;
}

/***********************************************************
 * NAME: isVersionRecordPresent
 * DESCRIPTION:  This function searches the specified file
 *               for the specified package releaseVersion
 *
 * IN:  char *filename -- content file to open and parse
 *      char *releaseVersion
 * OUT: bool 
 *      integer pointed to by maxID is set to the ID attribute
 *      of the record if found.  Otherwise 0.
 ***********************************************************/
bool isVersionRecordPresent(char *filename, char *releaseVersionStr, int *maxID)
{
    xmlNodePtr root;
    xmlNodePtr pNode;
    xmlDocPtr cntDoc; 

    *maxID = 0;

    cntDoc = xmlReadFile(filename, NULL, XML_PARSE_RECOVER);
    if (cntDoc)
    {
        root = xmlDocGetRootElement(cntDoc);
        if (root && root->children)
        {
            for (pNode = root->children; pNode != NULL; pNode = pNode->next)
            {
                char *cID = getNodeContentByName(pNode, "releaseVersion") ;

                if (cID)
                {
                    if (strcmp(cID, releaseVersionStr) == 0)
                    {
                        xmlChar *upgradeID  = xmlGetProp(pNode, (xmlChar*)"id");
                        if (upgradeID != NULL)
                        {
                            int base = 16 ;
                            *maxID = strtoul((char *)upgradeID, NULL, base) ;
                            xmlFree(upgradeID);
                        }
                        free(cID);
                        xmlFreeDoc(cntDoc);
                        return true;
                    }
                    free(cID);
                }
            }
        }
        xmlFreeDoc(cntDoc);
    }

    return false;
}

/***********************************************************
 * NAME: initVersionsXmlFile
 * DESCRIPTION: creates an empty <versionList> XML file
 *              so that <version> records can be added to it
 *
 * IN:  none
 *
 * OUT: none
 ***********************************************************/
void initVersionsXmlFile(char *fname)
{
    xmlNodePtr root;
    xmlDocPtr doc;

    doc = xmlNewDoc((xmlChar *)"1.0");
    if (doc)
    {
        root = xmlNewDocNode(doc, NULL, (xmlChar *)"versionList", NULL);
        xmlDocSetRootElement(doc, root);
        xmlSaveFormatFile(fname, doc, 1);
        xmlFreeDoc(doc);
    }
}

/***********************************************************
 * NAME: addVersionRecord
 * DESCRIPTION: This function adds a new <version> record
 *              to the xml doc tree.
 *              If a duplicate exists, it removes it 
 *              and add the new <version> element to the tree.
 *
 * IN:  xmlDocPtr, xmlNodePtr 
 * OUT: Returns 0 for success, or error code
 ***********************************************************/
int addVersionRecord(xmlDocPtr doc, xmlNodePtr node, char *fupdate)
{
    xmlNodePtr versionRoot;
    xmlNodePtr current;
    xmlNodePtr firstVersionNode;
    xmlNodePtr workNode;

    int changed = 0;
    int found = 0;

    int retval = XMLT_NO_ERROR;

    if (!isVersionElement(node)) 
    {
        return XMLT_FAILURE;
    }

    /*
     * set the versionRoot pointer to 
     * point to the verions xml so that we
     * add the child to the correct XML tree.
     */

    versionRoot = xmlDocGetRootElement(doc);

    /* Search the master XML do determine if the node is already there */
    for (current = versionRoot->children; current != NULL; current = current->next)
    {
        /* make sure we're dealing ONLY with <version> tags */

        if (isVersionElement(current)) 
        {
            if (replaceXmlNodeInTree(current, node, versionRoot))
            {
                changed = 1;
                found = 1;
            }
        }
    } /* End for (current = versionRoot->children;... */

    /* If we didn't find entry then add the node */
    if (!found)
    {
        /*
        *   Here we need to find the first version tag entry
        *   Then add the prev sibling.
        */
        firstVersionNode = getNodeByName(versionRoot, "version") ;
        if (firstVersionNode)
        {
            // Create a text node, add as prev sibling
            xmlNodePtr textNode = xmlNewText((xmlChar *)"\n  ");
            workNode = xmlAddPrevSibling(firstVersionNode, textNode);
            if(!workNode)
            {
                printf("Failed to add text node to versions.xml file\n");
                return XMLT_FAILURE;
            }

            // Then add sibling node
            workNode = xmlAddPrevSibling(workNode, node);
            // Do not need a new line text node after the version node.
        }
        else
        {
            // No exsiting version node so we will add one
            // Need a newline and 2 spaces
            // Create a text node, add then add sibling.
            xmlNodePtr textNode = xmlNewText((xmlChar *)"\n  ");

            // Need to add the text node as a child of versionList
            workNode = xmlAddChild(versionRoot, textNode);
            if(!workNode)
            {
                printf("Failed to add text node to versions.xml file\n");
                return XMLT_FAILURE;
            }
            // Add the new version node as a sibling of the newly added text node
            workNode = xmlAddSibling(workNode, node);

            // Need just a text node with newline after the new version node
            textNode = xmlNewText((xmlChar *)"\n");
            workNode = xmlAddSibling(workNode, textNode);
        }

        if (!workNode)
        {
            printf("Failed to add node to versions.xml file\n");
        }
        else
        {
            changed = 1;
        }
    }

    /* If changes occured save the file */
    if (changed)
    {
        // fupdate is the file name to be saved to.
       retval = xmlSaveFormatFile(fupdate, doc, 1);
        if (retval <= 0)
        {
            printf("Failed to save versions.xml file\n");
            retval = XMLT_FAILURE;
        }
        else
        {
            retval = XMLT_NO_ERROR;
        }
    }
    return retval;
}

