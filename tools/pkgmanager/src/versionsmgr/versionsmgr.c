/*
 * Copyright (C) 2009-2018 MapleLeaf Software, Inc
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/*
 * versionsmgr.c
 */

/* Standard Linux headers */
#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/vfs.h>

#include "xmltools.h"

#define TEMPLATE_VERSIONS_FILE "/projects/obelisk/mining/tools/pkgmanager/templates/versionstmp.xml"

static char *filePath;
static char *packageSize;
static char *CRC;
static char *timeStamp;
static char *releaseVersion;
static char *previousVersion;
static char *versionsPath;

static void usage(char *name)
{
    printf("Usage:%s\n", name);
    printf(" -h: print this message\n");
    printf(" -f vfile: name and path of the versions.xml file.  If does not exist is created.\n");
    printf(" -p previousVersion: Previous version that update is based on.\n");
    printf(" -r releaseVersion: Version number of the new update.\n");
    printf(" -d timeStamp: Time stamp of the package file.\n");
    printf(" -c CRCvalue: CRC of the package file.\n");
    printf(" -s packageSize: Size of the package file.\n");
    printf(" -u pkgfileName: Name of the package file.\n");
    exit(1);
}

#define MIN_REQ_PARAMS 15

static int processArgs(int argc, char**argv)
{
    int c = 0;
    int retval;

    int maxID;

    xmlDocPtr templateOrigDoc;
    xmlDocPtr theVersionsOrigDoc;

    xmlDocPtr templateDoc;
    xmlDocPtr theVersionsDoc;
    xmlNodePtr rootVersions;


    struct stat info;
    bool isReleaseVerPresent;

    if (argc < MIN_REQ_PARAMS)
    {
        printf("ERROR: insufficient parameters for command\n");
        usage(argv[0]);
    }

    while ((c = getopt(argc, argv, "f:p:r:d:c:s:u:")) != -1)
    {
        switch (c)
        {
            case 'f': /* Path to the versions.xml file to process */
                if (argc < MIN_REQ_PARAMS)
                {
                    printf("ERROR: insufficient parameters for command\n");
                    usage(argv[0]);
                }

                versionsPath = (char *)malloc(strlen(optarg)+1);
                if (versionsPath)
                {
                    strcpy(versionsPath, optarg);
                }
                else
                {
                    printf("Insufficient memory to run\n");
                    return 1;
                }
            break;

            case 'p': /* Previous Version Number */
                if (argc < MIN_REQ_PARAMS)
                {
                    printf("ERROR: insufficient parameters for command\n");
                    usage(argv[0]);
                }

                previousVersion = (char *)malloc(strlen(optarg)+1);
                if (previousVersion)
                {
                    strcpy(previousVersion, optarg);
                }
                else
                {
                    printf("Insufficient memory to run\n");
                    return 1;
                }
            break;

            case 'r': /* Release Version Number */
                if (argc < MIN_REQ_PARAMS)
                {
                    printf("ERROR: insufficient parameters for command\n");
                    usage(argv[0]);
                }

                releaseVersion = (char *)malloc(strlen(optarg)+1);
                if (releaseVersion)
                {
                    strcpy(releaseVersion, optarg);
                }
                else
                {
                    printf("Insufficient memory to run\n");
                    return 1;
                }
            break;

            case 'd': /* Time Stamp (date) */
                if (argc < MIN_REQ_PARAMS)
                {
                    printf("ERROR: insufficient parameters for command\n");
                    usage(argv[0]);
                }

                timeStamp = (char *)malloc(strlen(optarg)+1);
                if (timeStamp)
                {
                    strcpy(timeStamp, optarg);
                }
                else
                {
                    printf("Insufficient memory to run\n");
                    return 1;
                }
            break;

            case 'c': /* CRC */
                if (argc < MIN_REQ_PARAMS)
                {
                    printf("ERROR: insufficient parameters for command\n");
                    usage(argv[0]);
                }

                CRC = (char *)malloc(strlen(optarg)+1);
                if (CRC)
                {
                    strcpy(CRC, optarg);
                }
                else
                {
                    printf("Insufficient memory to run\n");
                    return 1;
                }
            break;

            case 's': /* Package Size */
                if (argc < MIN_REQ_PARAMS)
                {
                    printf("ERROR: insufficient parameters for command\n");
                    usage(argv[0]);
                }

                packageSize = (char *)malloc(strlen(optarg)+1);
                if (packageSize)
                {
                    strcpy(packageSize, optarg);
                }
                else
                {
                    printf("Insufficient memory to run\n");
                    return 1;
                }
            break;

            case 'u': /* Path to package file */
                if (argc < MIN_REQ_PARAMS)
                {
                    printf("ERROR: insufficient parameters for command\n");
                    usage(argv[0]);
                }

                filePath = (char *)malloc(strlen(optarg)+1);
                if (filePath)
                {
                    strcpy(filePath, optarg);
                }
                else
                {
                    printf("Insufficient memory to run\n");
                    return 1;
                }
            break;

            case 'h':
                usage(argv[0]);
            break;

            default:
                usage(argv[0]);
            break;
        } /* End of switch */
    } /* End of while loop */

    /*
     * Then see if the versions.xml file exists
     * If not then create it
     */
    retval = stat(versionsPath, &info);
    if (retval) 
    {
        /* Assume no current verisons.xml file */
        initVersionsXmlFile(versionsPath);
    }

    /* Search for a record with the Release Version. */
    /* If found (do we update?) */
    isReleaseVerPresent = isVersionRecordPresent(versionsPath, releaseVersion, &maxID);

    if (isReleaseVerPresent)
    {
        /*
         * For now we will NOT process this update.
         *   Return an error code
         *   If we determine that we do want to process then we will NOT need to 
         *   get the highest ID number, etc.  We will just update the record with 
         *   the new data.
         */
        if (releaseVersion[0] != 'i')
        {
            printf("Release version: %s already in versions.xml file. Will not update\n", releaseVersion);
            retval = 2;
        }
        else
        {
            /*
             * The following code will handle the updating of the existing
             * record. Get the template XML file to use for the new record.
             */
            templateOrigDoc = xmlReadFile(TEMPLATE_VERSIONS_FILE, NULL, XML_PARSE_RECOVER);
            if (!templateOrigDoc)
            {
                printf("Cannot read the %s template file\n", TEMPLATE_VERSIONS_FILE);
                return 1;
            }
            
            /*
             * We need to operate on copies of the XML files involved
             * Attempting to free the doc pointers of the modified files
             * will result in a segfault.
             */
            templateDoc = xmlCopyDoc(templateOrigDoc, 1);
            
            if (!templateDoc)
            {
                xmlFreeDoc(templateOrigDoc);
                return 1;
            }
            xmlFreeDoc(templateOrigDoc);
            
            /* Get the current, may be empty, versions.xml file. */
            theVersionsOrigDoc = xmlReadFile(versionsPath, NULL, XML_PARSE_RECOVER);
            if (!theVersionsOrigDoc)
            {
                printf("Cannot read the %s file\n", versionsPath);
                xmlFreeDoc(templateOrigDoc);
                xmlFreeDoc(templateDoc);
                return 1;
            }
            
            /*
             * We need to operate on copies of the XML files involved
             * Attempting to free the doc pointers of the modified files
             * will result in a segfault.
             */
            theVersionsDoc = xmlCopyDoc(theVersionsOrigDoc, 1);
            if (!theVersionsDoc)
            {
                xmlFreeDoc(theVersionsOrigDoc);
                xmlFreeDoc(templateOrigDoc);
                xmlFreeDoc(templateDoc);
                return 1;
            }
            xmlFreeDoc(theVersionsOrigDoc);
            
            rootVersions = xmlDocGetRootElement(theVersionsDoc);
            if (!rootVersions)
            {
                printf("Cannot get root element in versions.xml\n");
                xmlFreeDoc(theVersionsDoc);
                xmlFreeDoc(templateDoc);
                return 1;
            }
            
            retval = updateVersions(templateDoc, theVersionsDoc, versionsPath,
                                    timeStamp, releaseVersion, previousVersion,
                                    CRC, filePath, packageSize, maxID);
        }
    }
    else
    {
        /* Get the template XML file to use for the new record. */
        templateOrigDoc = xmlReadFile(TEMPLATE_VERSIONS_FILE, NULL, XML_PARSE_RECOVER);
        if(!templateOrigDoc)
        {
            printf("Cannot read the %s template file\n", TEMPLATE_VERSIONS_FILE);
            return 1;
        }

        /*
         * We need to operate on copies of the XML files involved
         * Attempting to free the doc pointers of the modified files
         * will result in a segfault.
         */
        templateDoc = xmlCopyDoc(templateOrigDoc, 1);

        if(!templateDoc)
        {
            xmlFreeDoc(templateOrigDoc);
            return 1;
        }
        xmlFreeDoc(templateOrigDoc);

        /* Get the current, may be empty, versions.xml file. */
        theVersionsOrigDoc = xmlReadFile(versionsPath, NULL, XML_PARSE_RECOVER);
        if(!theVersionsOrigDoc)
        {
            printf("Cannot read the %s file\n", versionsPath);
            xmlFreeDoc(templateOrigDoc);
            xmlFreeDoc(templateDoc);
            return 1;
        }

        /*
         * We need to operate on copies of the XML files involved
         * Attempting to free the doc pointers of the modified files
         * will result in a segfault.
         */
        theVersionsDoc = xmlCopyDoc(theVersionsOrigDoc, 1);
        if(!theVersionsDoc)
        {
            xmlFreeDoc(theVersionsOrigDoc);
            xmlFreeDoc(templateOrigDoc);
            xmlFreeDoc(templateDoc);
            return 1;
        }
        xmlFreeDoc(theVersionsOrigDoc);

        rootVersions = xmlDocGetRootElement(theVersionsDoc);
        if(!rootVersions)
        {
            printf("Cannot get root element in versions.xml\n");
            xmlFreeDoc(theVersionsDoc);
            xmlFreeDoc(templateDoc);
            return 1;
        }

        /* Get the highest ID number in the file */
        maxID = getMaxRecordID(rootVersions);
        ++maxID; /* Use next ID value in the new record */

        retval = updateVersions(templateDoc, theVersionsDoc, versionsPath, timeStamp, 
                                releaseVersion, previousVersion,
                                CRC, filePath, packageSize, maxID);
    }

    /* Free memory allocated */
    free(timeStamp);
    free(releaseVersion);
    free(previousVersion);
    free(CRC);
    free(packageSize);
    free(versionsPath);

    /* Return completion status */
    return retval;
}


int main( int argc, char *argv[])
{
    int retval = 0;

    /* parse command line arguments and execute */
    retval = processArgs(argc, argv);
    return retval;
}
